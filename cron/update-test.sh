#!/bin/sh

LOG_DATE=`date +%Y-%m-%d`
AGENT_PATH="/var/www/c13690/test.trainingadvisorinc.com/htdocs/desktop/agent"
LOG_FILE="$AGENT_PATH/logs/$DATE_cron.log"
OUT_FILE="$AGENT_PATH/logs/$LOG_DATE.htm"
EXE_FILE="$AGENT_PATH/update.php"
#URL="http://www.hrtoolsonline.com/desktop/agent/update.php"

# -O is set to /dev/null because the output of the request will be written to
# OUT_FILE anyway by the web server or script itself.
#/usr/bin/wget -a $LOG_FILE -O /dev/null $URL
cd "$AGENT_PATH"
/usr/bin/php $EXE_FILE -o8 >> $LOG_FILE 2>&1
