{include file='presentation/presentation_header.tpl'}

<body class="faq">

<form name="ScorecardForm" method="POST" action="{$smarty.server.PHP_SELF}">
<p>
<select class="FormValue" name="dateOption" onChange="javascript:this.form.submit();">
{html_options options=$dateOptions selected=$dateOptionSelected}</select>
</p>

<span>Completed Assignments: {$stats.totalAnswered}</span><BR>
<span>Correctly Answered: {$stats.correct}</span><BR>
<span>Incorrectly Answered: {$stats.incorrect}</span><BR>

</form>
</body>
</html>
