<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
  <META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <Title>WTP Desktop Logout</Title>
  <link rel='stylesheet' href='../themes/{$uiTheme}/desktop.css' type='text/css'>
  <script language='javascript' type='text/javascript' src='../javascript/desktop.js'></script>
  <script language='javascript'>
    var imgBack = new Image();
    imgBack.src = "../themes/{$uiTheme}/presentation/goback_up.gif";
    var imgBackb = new Image();
    imgBackb.src = "../themes/{$uiTheme}/presentation/goback_down.gif";
    var imgExit = new Image();
    imgExit.src = "../themes/{$uiTheme}/presentation/exit_up.gif";
    var imgExitb = new Image();
    imgExitb.src = "../themes/{$uiTheme}/presentation/exit_down.gif";
  </script>
</HEAD>
<BODY bgcolor="#FFFFFF">

{include file='common/motif.tpl'}
<BR>
<table border='0' cellpadding='0' cellspacing='0' width='100%' height='100%'>
  <tr>
    <td align='center' valign='top'>
      <table border='0' cellpadding='0' cellspacing='0' width='100%'>
	<tr>
	  <td align='center' valign='top'>
	    <table border='0' cellpadding='10' cellspacing='0' width='474' height='57' background='../themes/{$uiTheme}/presentation/bg_logout.gif' style='background-repeat:no-repeat;'>
	      <tr>
		<td valign='top'>
		  <span class="OrgInfoText">{$statusMsg}</span>
		</td>
	      </tr>
	      <tr>
		<td align='center' valign='top'><br>
		  <p class='BigBlack'>Click <b>Exit</b> to leave the Training site.</p>
		  <p><a href='../login/{$loginUrl}' onMouseOver='swap(exit,imgExitb);' onMouseOut='swap(exit,imgExit);'>
		  <img name='exit' src='../themes/{$uiTheme}/presentation/exit_up.gif' border='0'></a></p>
		</td>
	      </tr>
	      <tr>
		<td align='center' valign='top'><br>
		  <p class='BigBlack'>Click <b>Go Back</b> to return to the Training site.</p>
		  <p><a href='javascript:history.go(-1);' onMouseOver='swap(back,imgBackb);' onMouseOut='swap(back,imgBack);'>
		  <img name='back' src='../themes/{$uiTheme}/presentation/goback_up.gif' border='0'></a></p>
		</td>
	      </tr>
	    </table>
	  </td>
	</tr>
      </table>
    </td>
  </tr>
</TABLE>

</BODY>
</HTML>
