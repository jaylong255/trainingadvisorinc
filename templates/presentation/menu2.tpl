<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
  <title></title>
  <META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <link rel='stylesheet' href='../themes/{$uiTheme}/desktop.css' type='text/css'>
  <SCRIPT LANGUAGE="Javascript" type="text/javascript" src="../javascript/desktop.js"></script>
</head>
<body class="theme" onLoad="javascript:document.getElementById('divAssignmentList').style.visibility = 'visible';">

{include file='presentation/assignments.tpl'}

</body>
</html>
