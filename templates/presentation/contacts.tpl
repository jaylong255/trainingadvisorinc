<div id="contactsContent" name="contactsContent" class="FeedbackContent">

  <img src="../themes/{$uiTheme}/presentation/contact_header.jpg" align="top">

  {if (isset($corpEmail) && $corpEmail) || (isset($myContactEmail) && $myContactEmail)}
    <div class="Feedback" style="text-align:center;">To send an email to a contact, click the contact's email address below:</div>
  {/if}
  
  {if !isset($corpFullName) && !isset($corpEmail)}
    <p class="HeaderFeedback">No Organization Contact Designated.</p>
  {else}
    <p> <div class="HeaderFeedback">Organization Contact:</div>
	<div class="Feedback" style="text-align:center;">{$corpFullName}<br>{$corpPhone}<br>
        {if isset($corpEmail) && $corpEmail}
	  <a href="javascript:DisplayMailForm('{$corpFullName}', '{$corpEmailEncoded}');">{$corpEmail}</a>
	{/if}
        </div>
    </p>
  {/if}

  {if isset($trackContacts)}
    <p class="Feedback" style="text-align:center;">Below are the contacts of each track in which you are currently participating:</p>
    {section name="trackContactsIdx" loop=$trackContacts}
    <p> <div class="HeaderFeedback"> Track: {$trackContacts[trackContactsIdx][0]}</div>
      <div class="Feedback" style="text-align:center;">{$trackContacts[trackContactsIdx][1]->firstName} {$trackContacts[trackContactsIdx][1]->lastName}<br>{$trackContacts[trackContactsIdx][1]->phone}<br>
      {if isset($trackContacts[trackContactsIdx][1]->email) && $trackContacts[trackContactsIdx][1]->email}
	<a href="javascript:DisplayMailForm('{$trackContacts[trackContactsIdx][1]->firstName} {$trackContacts[trackContactsIdx][1]->lastName}', '{$trackContacts[trackContactsIdx][1]->email}');">{$trackContacts[trackContactsIdx][1]->email}</a>
      {/if}
      </div>
    </p>
    {/section}
  {else}
    {if !isset($myContactFullName) && !isset($myContactEmail)}
    <p class="HeaderFeedback">No Class Contact(s) Designated.</p>
    {else}
    <p> <div class="HeaderFeedback">{if isset($a) && $a->categoryId == 118}Training Advisor Contact{else}Your Class Contact{/if}:</div>
      <div class="Feedback" style="text-align:center;">{$myContactFullName}<br>{$myContactPhone}<br>
      {if isset($myContactEmail) && $myContactEmail}
	<a href="javascript:DisplayMailForm('{$myContactFullName}', '{$myContactEmail}');">{$myContactEmail}</a>
      {/if}
      </div>
    </p>
    {/if}
  {/if}

  <p style="text-align:center;">
    <a href="javascript:{if $preview}window.close();{else}HideContacts(); if (!review) SubmitAnswer(); else window.location.href='portal.php';{/if}"
	 onmouseout="document.images.btnContactCloseImage.src='../themes/{$uiTheme}/presentation/btn_desktop.png'"
	 onmouseover="document.images.btnContactCloseImage.src='../themes/{$uiTheme}/presentation/btn_desktop-mo.png'">
      <img name="btnContactCloseImage" id="btnContactCloseImage" src="../themes/{$uiTheme}/presentation/btn_desktop.png" alt="Desktop" border="0">
    </a>
  </p>

</div>

<!-- Content for mail form displayed if user clicks to send a message to a contact -->
{include file='presentation/sendmail.tpl'}
