<div style="visibility:hidden" name="divAssignmentList" id="divAssignmentList">
{if empty($assignments) && empty($divAssignments) && empty($deptAssignments)}
  <p class="Black">There are no questions currently due at this time.</p>
{else}
  {if !empty($divAssignments) && !empty($deptAssignments)}
  Questions from your organization:<BR><hr width="100%">
  {/if}
  {section name="assignmentIdx" loop=$assignments}
    {if isset($printFeatureEnabled) && $printFeatureEnabled}
      {if $smarty.section.assignmentIdx.iteration == 1}
  <form method="POST" action="print_questions.php" name="printQuestionsForm" id="printQuestionsForm">
  <table width="100%" border="0">
    <tr>
      <td align="left">
        <a href="javascript:ToggleCheckBoxes('questionsToPrint', {$smarty.section.assignmentIdx.total}, true);">Select All</a> | 
        <a href="javascript:ToggleCheckBoxes('questionsToPrint', {$smarty.section.assignmentIdx.total}, false);">Unselect All</a><BR>
      </td>
      <td align="right">
        <a href="javascript:PrepareSelectedQuestions('print_questions.php', 'questionsToPrint',
						     {$smarty.section.assignmentIdx.start},
						     {$smarty.section.assignmentIdx.max});">
		Print Selected Question(s)</a>
	<!--input type="submit" name="submit" value="Print Selected Questions"-->
      </td>
    </tr>
  </table>
    {/if}

  <input type="checkbox" id="questionsToPrint{$smarty.section.assignmentIdx.iteration}"
  	 name="questionsToPrint[]" value="{$assignments[assignmentIdx].Assignment_ID}">
  {/if}
  <a class="bottomLinks" target="_parent" href="q_type.php?assignmentId={$assignments[assignmentIdx].Assignment_ID}{if isset($review) && $review}&review={$review}{/if}">{$assignments[assignmentIdx].Title}</a><BR>
  {/section}


  {if !empty($divAssignments)}
  Questions from your division:<BR><hr width="100%">
  {/if}
  {section name="assignmentIdx" loop=$divAssignments}
    {if isset($printFeatureEnabled) && $printFeatureEnabled}
      {if $smarty.section.assignmentIdx.iteration == 1}
  <form method="POST" action="print_questions.php" name="printQuestionsForm" id="printQuestionsForm">
  <table width="100%" border="0">
    <tr>
      <td align="left">
        <a href="javascript:ToggleCheckBoxes('questionsToPrint', {$smarty.section.assignmentIdx.total}, true);">Select All</a> | 
        <a href="javascript:ToggleCheckBoxes('questionsToPrint', {$smarty.section.assignmentIdx.total}, false);">Unselect All</a><BR>
      </td>
      <td align="right">
        <a href="javascript:PrepareSelectedQuestions('print_questions.php', 'questionsToPrint',
						     {$smarty.section.assignmentIdx.start},
						     {$smarty.section.assignmentIdx.max});">
		Print Selected Question(s)</a>
	<!--input type="submit" name="submit" value="Print Selected Questions"-->
      </td>
    </tr>
  </table>
    {/if}

  <input type="checkbox" id="questionsToPrint{$smarty.section.assignmentIdx.iteration}"
  	 name="questionsToPrint[]" value="{$divAssignments[assignmentIdx].Assignment_ID}">
  {/if}
  <a class="bottomLinks" target="_parent" href="q_type.php?assignmentId={$divAssignments[assignmentIdx].Assignment_ID}{if isset($review) && $review}&review={$review}{/if}">{$divAssignments[assignmentIdx].Title}</a><BR>
  {/section}



  {if !empty($deptAssignments)}
  Questions from your department:<BR><hr width="100%">
  {/if}
  {section name="assignmentIdx" loop=$deptAssignments}
    {if isset($printFeatureEnabled) && $printFeatureEnabled}
      {if $smarty.section.assignmentIdx.iteration == 1}
  <form method="POST" action="print_questions.php" name="printQuestionsForm" id="printQuestionsForm">
  <table width="100%" border="0">
    <tr>
      <td align="left">
        <a href="javascript:ToggleCheckBoxes('questionsToPrint', {$smarty.section.assignmentIdx.total}, true);">Select All</a> | 
        <a href="javascript:ToggleCheckBoxes('questionsToPrint', {$smarty.section.assignmentIdx.total}, false);">Unselect All</a><BR>
      </td>
      <td align="right">
        <a href="javascript:PrepareSelectedQuestions('print_questions.php', 'questionsToPrint',
						     {$smarty.section.assignmentIdx.start},
						     {$smarty.section.assignmentIdx.max});">
		Print Selected Question(s)</a>
	<!--input type="submit" name="submit" value="Print Selected Questions"-->
      </td>
    </tr>
  </table>
    {/if}

  <input type="checkbox" id="questionsToPrint{$smarty.section.assignmentIdx.iteration}"
  	 name="questionsToPrint[]" value="{$deptAssignments[assignmentIdx].Assignment_ID}">
  {/if}
  <a class="bottomLinks" target="_parent" href="q_type.php?assignmentId={$deptAssignments[assignmentIdx].Assignment_ID}">{$deptAssignments[assignmentIdx].Title}{if isset($review) && $review}&review={$review}{/if}</a><BR>
  {/section}


</form>
{/if}
</div>

<!--div style="visibility:hidden" name="divCompletedAssignmentList" id="divCompletedAssignmentList">
section name="completedAssignmentIdx" loop=completedAssignments
  <a class="bottomLinks" target="_parent" href="q_type.php?assignmentId=$completedAssignments[completedAssignmentIdx].Assignment_ID">$completedAssignments[completedAssignmentIdx].Title</a><BR>
sectionelse
  <p class="Black">There are no questions that you have completed.</p>
/section
</div-->
