<div id="msgFormContent" name="msgFormContent" class="FeedbackContent">

  <img src="../themes/{$uiTheme}/presentation/contact_header.jpg" align="top">

  <div class="Feedback">
    To: <span id="msgTxtToName" name="msgTxtToName">&nbsp;</span> &lt;<span id="msgTxtToAddr" name="msgTxtToAddr">&nbsp;</span>&gt;
  </div>
  
  <div class="Feedback">
  Subject:
  <input class="Feedback" type="text" id="msgFormSubject" name="msgFormSubject" alt="Message Subject" size="45" maxlength="128" value="">
  </div>

  <div class="Feedback">
  Message:<BR>
  <textarea id="msgFormBody" name="msgFormBody" alt="Message Body" rows="10" cols="54" value="">{if $preview}Because you are previewing this question, any text entered here will be discarded and no message will be delivered to the contact you selected.  This is here only so that you may see exactly what someone answering this question will see.  Someone taking this training will not see the text you are currently reading.{/if}</textarea>
  </div>

  <div class="Feedback">
    Thanks for completing this training question.  After clicking the 'Send Message' button below, your message will be sent and you will be taken back to the portal home page where you may answer any remaining questions you have waiting for you to complete.
  </div>

  <p style="text-align:center;">
    <a href="javascript:{if $preview}window.close();{else}HideMailForm(); SubmitAnswer();{/if}"
	 onmouseout="document.images.btnMsgCloseImage.src='../themes/{$uiTheme}/presentation/send_msg_up.jpg'"
	 onmouseover="document.images.btnMsgCloseImage.src='../themes/{$uiTheme}/presentation/send_msg_down.jpg'">
      <img name="btnContactCloseImage" id="btnMsgCloseImage" src="../themes/{$uiTheme}/presentation/send_msg_up.jpg" alt="Send Message" border="0">
    </a>
  </p>

</div>
