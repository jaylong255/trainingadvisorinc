<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
  <META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <Title>Question</Title>
  <link rel="stylesheet" href="../themes/{$uiTheme}/desktop.css" type="text/css">
</HEAD>
<BODY bgcolor="#FFFFFF"
      onLoad="MM_preloadImages('../themes/{$uiTheme}/presentation/qbar_left_long.png', '../themes/{$uiTheme}/presentation/btn_FAQ_back_dim.png', '../themes/{$uiTheme}/presentation/btn_FAQ_back_up.png',
			       '../themes/{$uiTheme}/presentation/qbar_left_long_empty_with_seal.png', '../themes/{$uiTheme}/presentation/qbar_left_long_empty.png');">


<div id="presentationWindow" name="presentationWindow" class="PresentationWindow">
  
  <!-- Fairly benign table at top of page displaying pretty stuff in template/header -->
  {include file='common/motif.tpl'}

  <!-- Shows the question answer header with back/next images -->
  <TABLE width="95%" cellpadding="0" cellspacing="0" align="center" border="0">
    <tr>
      <td id="qbarLeft" name="qbarLeft" width="223" height="105" valign="top"
  	style="background-image:url(../themes/{$uiTheme}/presentation/qbar_left_long.png); background-color:#FFFFFF;">
        &nbsp;
      </td>
      <td id="qbarMid" name="qbarMid" height="105" align="center" valign="center"
          style="background-image:url(../themes/{$uiTheme}/presentation/qbar_mid.png); background-repeat:repeat-x; background-color:#FFFFFF;">
        <div id="textHeader" class="HeaderText" name="textHeader">The Question</div>
      </td>
      <td id="qbarBack" name="qbarBack" align="right" valign="center" width="79" height="105"
	style="background-image:url(../themes/{$uiTheme}/presentation/qbar_mid.png); background-repeat:repeat-x; background-color:#FFFFFF;">
	<!-- BACK BUTTON -->
	<div id="backButtonDiv">
        <a id="btnBackLink" name="btnBackLink" href="portal.php" alt="Back Button"
           onmouseout="SetImage('btnBackImage', '../themes/{$uiTheme}/presentation/btn_FAQ_back_up.png', finalAnswer, strCorrect, requireCorrectAnswer);"
           onmouseover="SetImage('btnBackImage', '../themes/{$uiTheme}/presentation/btn_FAQ_back_down.png', finalAnswer, strCorrect, requireCorrectAnswer);">
        <img name="btnBackImage" id="btnBackImage" src="../themes/{$uiTheme}/presentation/btn_FAQ_back_up.png" alt="" border="0"></a>
        </div>
      </td>
      <td id="qbarNext" name="qbarNext" align="center" width="144" height="105"
          style="background-image:url(../themes/{$uiTheme}/presentation/qbar_mid.png); background-repeat:repeat-x; background-color:#FFFFFF;">
	&nbsp;
      </td>
      <td id="qbarRight" name="qbarRight" width="22" height="105" style="background-image:url(../themes/{$uiTheme}/presentation/qbar_right.png); background-color:#FFFFFF;">
        &nbsp;
      </td>
    </tr>
  </TABLE>

  <!-- QUESTION PAGES, EACH INCLUDE IS YET ANOTHER PAGE -->

  <!-- Actual question presentation -->
  <div id="page_0" name="page_0" class="presentationBodyStyleVisible">
    {if isset($contentErrorMessage)}
	<span class="Error">{$contentErrorMessage}</span>
    {else}
        <span class="QuestionText">Please complete all other sections in this class before completing this question.  Click on the Back button in the question bar above to return to your assignments.</span>
    {/if}
  </div>
</div>
</BODY>
</HTML>
