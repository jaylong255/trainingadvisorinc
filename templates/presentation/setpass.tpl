{include file='presentation/presentation_header.tpl'}

<BODY bgcolor="#FFFFFF" onLoad="return SetFocus(document.ChangePassword.New);">
<TABLE border='0' cellpadding='0' cellspacing='0' width='100%'>
  <tr>
    <td background='../themes/{$uiTheme}/presentation/header_middle.gif' style='background-repeat:repeat-x;'>
      <img src='../themes/{$uiTheme}/presentation/wtp_logo.gif' border='0' alt=''>
    </td>
    <td align='right' valign='top' width='350' background='../themes/{$uiTheme}/presentation/header_right.gif' style='background-repeat:no-repeat;'>
      <img id="Logo" src="{$orgLogo}">
    </td>
  </tr>
</TABLE>

<br>

<form name="ChangePassword" method="POST" action="{$smarty.server.PHP_SELF}">
  <table border='0' cellpadding='5' cellspacing='0' width='80%' align="left">
    <tr>
      <td valign='middle' align='right'>
        <span class="Links">New Password:</span>		
      </td>
      <td valign='middle'>
        <input type="password" name="New" size='20' maxlength='30'>
      </td>
    </tr>
    <tr>
      <td valign='middle' align='right'>
        <span class="Links">Confirm Password:</span>		
      </td>
      <td valign='middle'>
        <input type="password" name="Confirm" size='20' maxlength='30'><br>
      </td>
    </tr>
    <tr>
      <td valign='middle' align='center' colspan="2" >
        <br><a href="javascript:Formobj=MM_findObj('ChangePassword'); Formobj.submit();" 
		onmouseout="document.images.btnSubmitImage.src='../themes/{$uiTheme}/presentation/submit_up.gif' " 
		onmouseover="document.images.btnSubmitImage.src='../themes/{$uiTheme}/presentation/submit_down.gif' ">
		<img name="btnSubmitImage" id="btnSubmitImage" src="../themes/{$uiTheme}/presentation/submit_up.gif" alt="" border="0"  /></a>
	<a href="#" onClick="history.go(-1)" onmouseout="document.images.btnBackImage.src='../themes/{$uiTheme}/presentation/goback_up.gif' "
		onmouseover="document.images.btnBackImage.src='../themes/{$uiTheme}/presentation/goback_down.gif' ">
		<img name="btnBackImage" id="btnBackImage" src="../themes/{$uiTheme}/presentation/goback_up.gif" alt="" border="0" /></a>
     </td>
   </tr>
  </table>
</form>

{if isset($errMsg) && $errMsg}<div id='err' align='center' class='error'>{$errMsg}</div>{/if}

</BODY>
</HTML>
