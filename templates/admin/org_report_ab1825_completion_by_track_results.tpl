{include file='admin/admin_header.tpl'}
{include file='admin/org_header.tpl'}
{include file='admin/report_header.tpl'}
<!--SCRIPT language=JavaScript src="../javascript/ajax.js" type="text/javascript"></SCRIPT-->
{if !isset($reportsErrMsg)}

<table border="0" width="680" align="top" cellpadding="2" cellspacing="3">
  <tr>
    <td width="23">
      <a href="org_reports.php" onMouseOut="MM_swapImgRestore()"
	 onMouseOver="MM_swapImage('Back','','../themes/{$uiTheme}/admin/btn_back_down.gif',1)">
        <img name="Back" border="0" src="../themes/{$uiTheme}/admin/btn_back_up.gif" width="23" height="21" align="center"></a>
    </td>
    <td align="left">
      <a class="OrgInfoText"
	href="org_report_ab1825_completion_by_track.php?trackIdsSelected[]={$trackIdsSelected}{if $showOnlyIncomplete}&showOnlyIncomplete=1{/if}"
	alt="Return to Report Parameters">Return to Report Parameters</a>
    </td>
    <td align="right">
	&nbsp;
    </td>
  </tr>
  {if isset($reportsErrMsg) && $reportsErrMsg}
  <tr>
    <td colspan="3" class="error">
      {$reportsErrMsg}
    </td>
  </tr>
  {/if}
  <tr>
    <td colspan="3"><hr width="100%"></td>
  </tr>
</table>

<p><span class="OrgInfoText">To view the completion certificate for any individuals that have completed training click on 'Yes' under the Met Requirement column.</span></p>

{section name=trackListIdx loop=$trackList}
<div class="Stacking">
  <span class="OrgInfoText">Report for Track: {$trackList[trackListIdx][0][1]}</span>
</div>

<div class="Stacking">
<table border="0" width="100%" align="center" bgcolor="#FFFFFF">
  <tr bgcolor="#DDDDDD">
    <th class="OrgInfoText">User ID</th>
    <th class="OrgInfoText">Full Name</th>
    <th class="OrgInfoText">Assigned</th>
    <th class="OrgInfoText">Completed</th>
    <th class="OrgInfoText">Understood</th>
    <th class="OrgInfoText">Time Completed</th>
    <th class="OrgInfoText">Time Remaining</th>
    <th class="OrgInfoText">Met Requirement</th>
  </tr>
  {section name=trackListDataIdx loop=$trackList[trackListIdx] start=1}
  {math assign="compHrs" equation="(int)($trackList[trackListIdx][trackListDataIdx][5]/60/60)"}
  {math assign="compMts" equation="(int)(($trackList[trackListIdx][trackListDataIdx][5]/60)%60)"}
  {math assign="remHrs" equation="(int)((7200-$trackList[trackListIdx][trackListDataIdx][5])/60/60)"}
  {math assign="remMts" equation="(int)(((7200-$trackList[trackListIdx][trackListDataIdx][5])/60)%60)"}
  <tr bgcolor="#FFFFFF">
    <td>{$trackList[trackListIdx][trackListDataIdx][0]}</td>
    <td>{$trackList[trackListIdx][trackListDataIdx][1]}</td>
    <td align="center">{$trackList[trackListIdx][trackListDataIdx][2]}</td>
    <td align="center">{$trackList[trackListIdx][trackListDataIdx][3]}</td>
    <td align="center">{$trackList[trackListIdx][trackListDataIdx][4]}</td>
    <td align="center">{$compHrs} Hr(s), {$compMts} Mt(s)</td>
    <td align="center">{if ($remHrs < 0)}0{else}{$remHrs}{/if} Hr(s), {if ($remMts < 0)}0{else}{$remMts}{/if} Mt(s)</td>
    <td align="center">{if $trackList[trackListIdx][trackListDataIdx][5]>7200}<a target="_blank" href="/desktop/presentation/gen_ab1825_cert.php?luid={$trackList[trackListIdx][trackListDataIdx][0]}&trackId={$trackList[trackListIdx][0][0]}">Yes</a>{else}No{/if}</td>
  </tr>
  {/section}
</table>
</div>
<div class="Stacking">
  <hr width="100%">
</div>
{/section}
{/if}
<!--BR>
{include file='admin/report_footer.tpl'} -->

</body>
</html>
