{include file='admin/admin_header.tpl'}

<BODY class="ContentFrameBody" style="overflow:hidden;">

<form name="AdminForm" id="AdminForm" method="POST" action="org_data_edit.php">
<table border="0" class="PageText" width="{if $currentDataEditFrame == 7}680{else}350{/if}" align="left" valign="top">

  <tr>
    <td align="left">
      <div class="OrgInfoText">Select {$dataElement}:</div>
    </td>
    <td>
      {if isset($dataElementValues) && $dataElementValues}
      <select name="dataElementValues" onchange="javascript:OrgDataChangeItem('addOrgDataDiv');" class="FormValue">
        <option></option>
        {html_options options=$dataElementValues selected=$dataElementSelected}
      </select>
      {else}
	None defined
      {/if}
    </td>
  </tr>


  <tr>
    <td align="left">
      <div class="OrgInfoText">Name:</div>
    </td>
    <td>
      <input type="text" name="elementValue" value="{$elementValue}" size="20" maxlength="50"
	 class="FormValue" onKeyUp="OrgDataEditKeyPress(event, 'addOrgDataDiv');">
    </td>
  </tr>


  <tr>
    <td align="left">
      <div class="OrgInfoText">&nbsp;</div>
    </td>
    <td>
      <!-- The OrgDataEditSubmit method called below updates the value of this hidden field before submitting form -->
      <input type="hidden" name="operation" value="update">
      <a id="addOrgDataDiv" name="addOrgDataDiv" href="javascript:OrgDataEditSubmit('add');" class="OrgInfoText"{if isset($hideAddDiv) && $hideAddDiv} style="visibility:hidden;"{/if}>Add</a>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      {if $dataElementValues}<a href="javascript:OrgDataEditSubmit('update');" class="OrgInfoText">Update</a>{/if}
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      {if $dataElementValues}<a href="javascript:OrgDataEditSubmit('delete');" class="OrgInfoText">Delete</a>{/if}
    </td>
  </tr>
</table>
</form>
</BODY>
</HTML>
