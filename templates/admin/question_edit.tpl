{include file='admin/admin_header.tpl'}
{include file='admin/question_header.tpl'}

<!--div id="AreaMenu" style="position:absolute; left:28px; top:71px; z-index:30; visibility: hidden"-->

<table border="0" align="top" width="680">
  <tr>
    <td align="left" width="33%">
      <!--div id="Area" style="position:absolute; left:13px; top:52px; width:232px; height:22px; z-index:31"-->
      <a id="AreaLink" href="javascript:ShowPopupMenu('AreaMenu', 'AreaLink', true);" style="z-index:50;">
	<img src="../themes/{$uiTheme}/admin/pulldown_carrot.gif" width="9" height="8" border="0">
        <span id="AName" style="z-index:50;">Question Information</span></a>
      <!--/div-->
    <div id="AreaMenu" style="visibility:hidden; position:absolute; left:0px; top:0px; z-index:50;">
      <table class="table_popup" border="0">
        <tr><td id="Arrow1"> </td><td><a href="javascript:ChangeArea(1);">Question Information</a></td></tr>
        <tr><td id="Arrow2"> </td><td><a href="javascript:ChangeArea(2);">The Question</a></td></tr>
        <tr><td id="Arrow3"> </td><td><a href="javascript:ChangeArea(3);">Potential Answers</a></td></tr>
        <tr><td id="Arrow4"> </td><td><a href="javascript:ChangeArea(4);">The Purpose of the Question</a></td></tr>
        <tr><td id="Arrow5"> </td><td><a href="javascript:ChangeArea(5);">Possible Responses</a></td></tr>
        <tr><td id="Arrow6"> </td><td><a href="javascript:ChangeArea(6);">The Correct Response</a></td></tr>
        <tr><td id="Arrow7"> </td><td><a href="javascript:ChangeArea(7);">Supplemental Information</a></td></tr>
        <tr><td id="Arrow8"> </td><td><a href="javascript:ChangeArea(8);">Key Learning Points</a></td></tr>
        <tr><td id="Arrow9"> </td><td><a href="javascript:ChangeArea(9);">Additional Information</a></td></tr>
        <tr><td id="Arrow10"> </td><td><a href="javascript:ChangeArea(10);">Customize 'Thank You' Page</a></td></tr>
      </table>
    </div>
    </td>
    <td width="33%" align="center">
    {if $q->questionId}
      <a href="javascript:launchPopup('Popup','../presentation/q_type.php?questionId={$q->questionId}&review=0&preview=1&domainId={$q->domainId}&languageId={$q->languageId}','no',850, 1000);">Preview</a>
    {else}&nbsp;{/if}
    </td>
    <td width="34%" align="right">
      <a id="SectionBack" style="visibility:hidden" href="javascript:BackSection('{$uiTheme}');">
	 <img src="../themes/{$uiTheme}/admin/back_pointer.gif" width="12" height="11" border="0">
         Back</a>
      <a id="SectionNext" href="javascript:NextSection('{$uiTheme}');">
	 Next <img src="../themes/{$uiTheme}/admin/pointer.gif" width="12" height="11" border="0">
      </a>
    </td>
  </tr>
</table>  





<!--div id="Back" style="position:absolute; left:12px; top:4px; width:200px; height:27px; z-index:2" class="OrgInfoText">
  <table border="0" width="100%" align="center" cellpadding="2" cellspacing="3">
    <tr>
      <td>
	<a href="javascript:CheckSaveQuestionEdit('{$q->questionId}');" onMouseOut="MM_swapImgRestore()"
	   onMouseOver="MM_swapImage('Back','','../themes/{$uiTheme}/admin/btn_back_down.gif',1)">
	<img name="Back" border="0" src="../themes/{$uiTheme}/admin/btn_back_up.gif" width="23" height="21"></a>
      </td>
      <td><a class="OrgInfoText" href="javascript:CheckSaveQuestionEdit('{$q->questionId}');">Back to Question List</a></td>
    </tr>
  </table>
</div>



<div id="Break" style="position:absolute; left:11px; top:29px; width:100%; height:18px; z-index:3"><hr></div-->


<form id="AdminForm" name="AdminForm" method="post" action="{$smarty.server.PHP_SELF}">
  <div id="Section1" style="position:absolute; left:13px; top:80px; width:670px; height:286px; z-index:10; visibility: visible">
    <!--div id="Section1a" style="position:absolute; left:306px; top:16px; width:350px; height:221px; z-index:11"-->
  <table border="0">
    <tr>
      <td class="OrgInfoText">Question Number:</td>
      <td>
        <input class="FormDisabled" size="17" type="text" id="questionId" name="questionId" onchange="javascript:SetFlag();"
	       value="{if $q->questionId}{$q->questionId}{else}To Be Assigned{/if}" disabled>
	<input type="hidden" name="questionId" id="questionId" value="{if $q->questionId}{$q->questionId}{else}To Be Assigned{/if}">
	<input type="hidden" name="questionId" id="questionId" value="{if $q->questionId}{$q->questionId}{else}To Be Assigned{/if}">
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText">Title:</td>
      <td>
        <!--textarea class="FormValue" rows="8" cols="28" name="title" id="title" onchange="javascript:SetFlag();">{$q->title}</textarea-->
        <input class="FormValue" style="width:100%" type="text" name="title" id="title" value="{$q->title}" onchange="javascript:SetFlag();">
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText">Author:</td>
      <td>
        <input class="FormValue" style="width:100%" type="text" name="author" id="author" size="30" maxlength="30" value="{$q->author}"
	       onchange="javascript:SetFlag();">
      </td>
    </tr>
    <!--tr>
      <td class="OrgInfoText">Parent Question:</td>
      <td>
        <select class="FormDisabled" name="language" onchange="javascript:SetFlag();" disabled>
          <option value="0">&lt;No Parent Question&gt;</option>
          {html_options options=$parentQuestions selected=$q->parentQuestionId}
        </select>
      </td>
    </tr-->
    <tr>
      <td class="OrgInfoText">Version:</td>
      <td>
        <input class="FormDisabled" type="text" name="versionDate" id="versionDate" maxlength="45"
	       size="30" value="{$q->versionDate}" disabled>
	<input type="hidden" name="versionDate" value={$q->versionDate}>
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText"> Language: </td>
      <td>
        <select  class="FormValue" name="languageId" id="languageId" onchange="javascript:SetFlag();"
		{if $q->questionId > 0 && !$superUser}{/if}> <!-- previously disabled but then form val does not come through -->
            {html_options options=$q->GetLanguages() selected=$q->languageId}
        </select>
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText">Domain:&nbsp;</td>
      <td>
        <select  class="FormValue" name="domainId" onchange="javascript:SetFlag();"
		{if $q->questionId > 0 && !$superUser} disabled{/if}>
          <option></option>
          {html_options options=$q->GetDomains() selected=$q->domainId}
        </select>
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText">Frame Type:</td>
      <td>
        <select  class="FormValue" name="frameTypeId" id="frameTypeId" onchange="javascript:SetFlag();">
            {html_options options=$q->GetFrameTypes() selected=$q->frameTypeId}
        </select>
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText" height="29">Category:</td>
      <td height="29">
        <select  class="FormValue" name="categoryId" id="categoryId" onchange="javascript:SetFlag();">
          <option></option>
          {html_options options=$q->GetCategories() selected=$q->categoryId}
        </select>
      </td>
    </tr>
    <!--div id="Section1d" style="position:absolute; left:11px; top:193px; width:235px; height:49px; z-index:14"-->
    <tr>
      <td class="OrgInfoText">Question can be used for Non-Mangagement employees:</td>
      <td>
         <input type="checkbox" name="isManagement" id="isManagement" value="1" onchange="javascript:SetFlag();"
	{if $q->isManagement} checked{/if}>
      </td>
    </tr>
  </table>
 </div>
 

  <!--div id="Section1b" style="position:absolute; left:3px; top:0px; width:262px; height:30px; z-index:12">
    <table border="0">
      <tr>
        <td class="OrgInfoText">Question Number:</td>
        <td>
          <input class="FormDisabled" size="17" type="text" id="questionId" name="questionId" onchange="javascript:SetFlag();"
	       value="{if $q->questionId}{$q->questionId}{else}To Be Assigned{/if}" disabled>
        </td>
      </tr>
    </table>
  </div-->

    <!--div id="Section1c" style="position:absolute; left:3px; top:26px; width:200px; height:200px; z-index:13">
    <table border="1">
      <tr>
        <td colspan="2" class="OrgInfoText" height="17">Title:</td>
      </tr>
      <tr>
        <td>
        </td>
      </tr>
    </table>
   </div-->
  </div>
  <div id="Section2" style="position:absolute; left:13px; top:84px; width:670px; height:286px; z-index:15; visibility: hidden">
    <table border="0">
    <tr>
        <td  class="OrgInfoText" height="17">Question:</td>
    </tr>
    <tr>
       <td>
         <textarea class="FormValue" rows="10" cols="80" id="question" name="question"
		   onchange="javascript:SetFlag();">{$q->question}</textarea>
       </td>
    </tr>
    <tr>
      <td class="OrgInfoText" height="17">
	<input type="checkbox" name="requireCorrectAnswer"  onchange="javascript:SetFlag();" value="1"
		{if $q->requireCorrectAnswer} checked{/if}>
	    Require correct answer before allowing user to complete question
      </td>
    </tr>
    <tr>
	 <td class="OrgInfoText" height="17">Media File:</td>
    </tr>
    <tr>
       <td>
         <input class="FormValue" type="text" id="media" name="media" maxlength="100" size="40" value="{$q->mediaFilePath}">
       </td>
    </tr>
    <tr>
	  <td id="mediaText" class="OrgInfoText">&nbsp;</td>
    </tr>
    </table>
</div>
  <div id="Section3" style="position:absolute; left:13px; top:84px; width:742px; height:286px; z-index:16; visibility: hidden">
    <table border="0">
    <tr>
        <td class="OrgInfoText" height="17">Answer A:</td>
        <td>
          <textarea class="FormValue" rows="3" cols="75" id="answerA" name="answerA" onchange="javascript:SetFlag();">{$q->multipleChoice1}</textarea>
        </td>
    </tr>
    <tr>
        <td class="OrgInfoText" height="17">Answer B:</td>
        <td>
          <textarea class="FormValue" rows="3" cols="75" id="answerB" name="answerB" onchange="javascript:SetFlag();">{$q->multipleChoice2}</textarea>
        </td>
    </tr>
    <tr>
        <td class="OrgInfoText" height="17">Answer C:</td>
        <td>
          <textarea class="FormValue" rows="3" cols="75" id="answerC" name="answerC" onchange="javascript:SetFlag();">{$q->multipleChoice3}</textarea>
        </td>
    </tr>
    <tr>
        <td class="OrgInfoText" height="17">Answer D:</td>
        <td>
          <textarea class="FormValue" rows="3" cols="75" id="answerD" name="answerD" onchange="javascript:SetFlag();">{$q->multipleChoice4}</textarea>
        </td>
    </tr>
    <tr>
        <td class="OrgInfoText" height="17">Answer E:</td>
        <td>
          <textarea class="FormValue" rows="3" cols="75" id="answerE" name="answerE" onchange="javascript:SetFlag();">{$q->multipleChoice5}</textarea>
        </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td class="OrgInfoText" height="17">
        <input type="radio" name="displayChoices" value="right"{if isset($useQuestionSetting) && !$useQuestionSetting} disabled{elseif isset($displayChoices) && $displayChoices == 'right'} checked{/if}>Display these choices to the right of the question<BR>
	<input type="radio" name="displayChoices" value="bottom"{if isset($useQuestionSetting) && !$useQuestionSetting} disabled{elseif isset($displayChoices) && $displayChoices == 'bottom'} checked{/if}>Display these choices below the question<BR>
	<input type="radio" name="displayChoices" value="default"{if isset($useQuestionSetting) && !$useQuestionSetting} disabled{elseif isset($displayChoices) && ($displayChoices == 'default' || $displayChoices == '')} checked{/if}>Use the default selection from the Miscellaneious subtab of the Customize tab.<BR>
	{if isset($useQuestionSetting) && !$useQuestionSetting}<p>
	   To enable the options above on per question basis, to the customize tab and click on the Miscellaneous sub-tab.  Then select the option "Use setting from the question but if question setting is not defined, use the above selection as default option."</p>{/if}
      </td>
    </tr>
    </table>
  </div>


  <div id="Section4" style="position:absolute; left:13px; top:84px; width:670px; height:286px; z-index:17; visibility: hidden">
    <table border="0">
    <tr>
        <td  class="OrgInfoText">Purpose of the Question:</td>
    </tr>
    <tr>
        <td>
          <textarea class="FormValue" rows="10" cols="80" id="purpose" name="purpose" onchange="javascript:SetFlag();">{$q->purpose}</textarea>
        </td>
    </tr>
    </table>
  </div>


  <div id="Section5" style="position:absolute; left:13px; top:84px; width:670px; height:286px; z-index:18; visibility: hidden">
  <table border="0">
    <tr>
        <td class="OrgInfoText">Response to Answer A:</td>
        <td>
          <textarea class="FormValue" rows="3" cols="65" id="responseA" name="responseA" onchange="javascript:SetFlag();">{$q->feedbackChoice1}</textarea>
        </td>
    </tr>
    <tr>
        <td class="OrgInfoText">Response to Answer B:</td>
        <td>
          <textarea class="FormValue" rows="3" cols="65" id="responseB" name="responseB" onchange="javascript:SetFlag();">{$q->feedbackChoice2}</textarea>
        </td>
    </tr>
    <tr>
        <td class="OrgInfoText">Response to Answer C:</td>
        <td>
          <textarea class="FormValue" rows="3" cols="65" id="responseC" name="responseC" onchange="javascript:SetFlag();">{$q->feedbackChoice3}</textarea>
        </td>
    </tr>
    <tr>
        <td class="OrgInfoText">Response to Answer D:</td>
        <td>
          <textarea class="FormValue" rows="3" cols="65" id="responseD" name="responseD" onchange="javascript:SetFlag();">{$q->feedbackChoice4}</textarea>
        </td>
    </tr>
    <tr>
        <td class="OrgInfoText">Response to Answer E:</td>
        <td>
          <textarea class="FormValue" rows="3" cols="65" id="responseE" name="responseE" onchange="javascript:SetFlag();">{$q->feedbackChoice5}</textarea>
        </td>
    </tr>
    </table>
  </div>



  <div id="Section6" style="position:absolute; left:13px; top:84px; width:670px; height:286px; z-index:19; visibility: hidden">
    <table border="0" cellspacing="2">
    <tr>
        <td class="OrgInfoText">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td class="OrgInfoText">Correct Answer:</td>
	<td>&nbsp;&nbsp;&nbsp;&nbsp;
	  <select id="correctAnswer" name="correctAnswer" class="FormValue" onchange="javascript:SetFlag();">
	    <!--option></option-->
	    {if $q->correctAnswer > 0}
	    {html_options options=$correctOpts selected=$q->correctAnswer}
	    {else}
	    {html_options options=$correctOpts}
	    {/if}
	  </select>
	</td>
    </tr>
    </table>
  </div>


  <div id="Section7" style="position:absolute; left:13px; top:84px; width:670px; height:286px; z-index:20; visibility: hidden">
    <table border="0" cellspacing="2">
    <tr>
        <td class="OrgInfoText" height="17">Supplemental Information:</td>
    </tr>
    <tr>
        <td>
          <textarea class="FormValue" rows="10" cols="80" id="supportInfo" name="supportInfo" onchange="javascript:SetFlag();">{$q->supportInfo}</textarea>
        </td>
    </tr>
    </table>
  </div>


  <div id="Section8" style="position:absolute; left:13px; top:84px; width:670px; height:286px; z-index:21; visibility: hidden">
    <table border="0" cellspacing="2">
    <tr>
        <td class="OrgInfoText">Key Learning Points:</td>
    </tr>
    <tr>
        <td>
          <textarea class="FormValue" rows="10" cols="80" id="keyLearningPoints" name="keyLearningPoints"
		    onchange="javascript:SetFlag();">{$q->learningPoints}</textarea>
        </td>
    </tr>
    </table>
  </div>



  <div id="Section9" style="position:absolute; left:13px; top:84px; width:670px; height:286px; z-index:22; visibility: hidden">
    <table border="0" cellspacing="2">
    <tr>
        <td class="OrgInfoText" height="17">Additional Information</td>
    </tr>
    <tr>
        <td>
          <textarea class="FormValue" rows="10" cols="80" id="notes" name="notes" onchange="javascript:SetSupport();">{$q->notes}</textarea>
        </td>
    </TR>
    </TABLE>
  </DIV>



  <div id="Section10" style="position:absolute; left:13px; top:84px; width:670px; height:286px; z-index:22; visibility: hidden">
    <table border="0" cellspacing="2">
    <tr>
        <td class="OrgInfoText" height="17">Enter the 'Thank You' message that the user will see when completing the question:</td>
    </tr>
    <tr>
        <td>
          <textarea class="FormValue" rows="3" cols="80" id="capConfirmThanks" name="capConfirmThanks" onchange="javascript:SetFlag();">{$q->capConfirmThanks}</textarea><BR>
	  <INPUT type="checkbox" name="CapReplicate" value="1">
		<span class="OrgInfoText">Check here to use this caption for all questions.</span>
        </td>
    </TR>
    </TABLE>
  </DIV>


</FORM>
{if $errMsg}
  <BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR>
  <SPAN class="error">{$errMsg}</SPAN>
{/if}

</BODY>
</HTML>

