{include file='admin/admin_header.tpl'}

<BODY bgcolor="#FFFCED" class="noscroll">

<form name="AdminForm" id="AdminForm" method="POST" action="org_data_edit.php">
<table border="0" class="PageText" width="350" align="left" valign="top">
  <tr>
    <td colspan="3">
      {if $F_Item_Opts}
        <div class="OrgInfoText" align="left">Select {$TableName} To Be Edited:</div>
      {else}
        &nbsp;
      {/if}
    </td>
  </tr>
  <tr>
    <td colspan="3">
      {if $F_Item_Opts}
      <select name="F_Item" onchange="javascript:OrgDataChangeItem();" class="FormValue">
        <option></option>
        {html_options options=$F_Item_Opts selected=$F_Item}
      </select>
      {else}
        &nbsp;
      {/if}
    </td>
  </tr>
  <tr>
    <td align="left" colspan="3">
      {if $ErrMsg}
      <span class="error">{$ErrMsg}</span>
      {else}
      &nbsp;
      {/if}
    </td>
  </tr>
  <tr>
    <td colspan="3">
      <div align="left" class="OrgInfoText">{$TableName} Name:</div>
    </td>
  </tr>
  <tr>
    <td colspan="3">
      <input class="FormValue" type="text" name="F_Value" id="F_Value" size="50" maxlength="50">
    </td>
  </tr>
  <tr>
    <td align="left">
      <a href="javascript:OrgDataAdd();" class="OrgInfoText">Add</a>
    </td>
    <td align="center">
      {if $F_Item_Opts}<a href="javascript:OrgDataUpdate();" class="OrgInfoText">Update</a>{/if}
    </td>
    <td align="center">
      {if $F_Item_Opts}<a href="javascript:OrgDataDelete();" class="OrgInfoText">Delete</a>{/if}
    </td>
  </tr>
</table>
<input type="hidden" name="TableName" value="{$TableName}">
<input type="hidden" name="ColumnName" value="{$ColumnName}">
<input type="hidden" name="ColumnName2" value="{$ColumnName2}">
<input type="hidden" name="Insert" value="TRUE">
</form>
</BODY>
</HTML>
