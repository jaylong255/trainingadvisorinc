{if !isset($contentOnly) || !$contentOnly}
  {include file='admin/admin_header.tpl'}
  {include file='admin/question_header.tpl'}
{/if}


<!--div name="contentArea" id="contentArea"{if isset($contentOnly) && $contentOnly} style="width:auto; overflow:visible; display:inline; page-break-after:always;"{/if}-->
<table border="0" cellpadding="1" cellspacing="5">
{if !isset($contentOnly) || !$contentOnly}
  <tr>
    <td colspan="2">
      <input type="button" name="printButton" value="Print Question"
             onclick="javascript:if (window.print) window.print(); else alert('Sorry, your browser does not support this function.  Please upgrade your browser to a more recent version and try again.');">
    </td>                           
  </tr>
{/if}
  <tr>
    <td class="OrgInfoText">Question Number:</td>
    <td class="FormValue">{$q->questionId}</td>
  </tr>
  <tr>
    <td class="OrgInfoText">Parent Question Number:</td>
    <td class="FormValue">{if $q->parentQuestionId > 0}{$q->parentQuestionId}{else}No Parent{/if}</td>
  </tr>
  <tr>
    <td class="OrgInfoText">Language:</td>
    <td class="FormValue">{assign var='languages' value=$q->GetLanguages()}
			  {assign var='languageId' value=$q->languageId}
			  {$languages.$languageId}</td>
  </tr>
  <tr>
    <td class="OrgInfoText">Domain:</td>
    <td class="FormValue">{assign var='domains' value=$q->GetDomains()}
			  {assign var='domainId' value=$q->domainId}
			  {$domains.$domainId}</td>
  </tr>
  <tr>
    <td class="OrgInfoText">Version Date</td>
    <td class="FormValue">{$q->versionDate}</td>
  </tr>
  <tr>
    <td class="OrgInfoText">Category:</td>
    <td class="FormValue">{assign var='categories' value=$q->GetCategories()}
			  {assign var='categoryId' value=$q->categoryId}
			  {$categories.$categoryId}</td>
  </tr>
  <tr>
    <td class="OrgInfoText">Frametype:</td>
    <td class="FormValue">{assign var='frameTypes' value=$q->GetFrameTypes()}
			  {assign var='frameTypeId' value=$q->frameTypeId}
			  {$frameTypes.$frameTypeId}</td>
  </tr>
  <tr>
    <td class="OrgInfoText">Author:</td>
    <td class="FormValue">{$q->author}</td>
  </tr>
  <tr>
    <td class="OrgInfoText">Type of Question:</td>
    <td class="FormValue">{if $q->isManagement}Management{else}Standard{/if}</td>
  </tr>
  <tr><td class="OrgInfoText">Title:</td>
      <td class="FormValue">{$q->title}</td>
  </tr>
  {if !empty($q->mediaFilePath)}
  <tr><td class="OrgInfoText">Media File:</td>
      <td class="FormValue" id="media">{$q->mediaFilePath}</td>
  </tr>
  {/if}
  <tr><td class="OrgInfoText">Question:</td>
      <td class="FormValue" id="question">{$q->question}</td>
  </tr>
  <tr><td class="OrgInfoText" width="80">Answer A:</td>
      <td class="FormValue" id="answerA">{$q->multipleChoice1}</td>
  </tr>
  <tr><td class="OrgInfoText">Answer B:</td>
      <td class="FormValue" id="answerB">{$q->multipleChoice2}</td>
  </tr>
  <tr><td class="OrgInfoText">Answer C:</td>
      <td class="FormValue" id="answerC">{$q->multipleChoice3}</td>
  </tr>
  <tr><td class="OrgInfoText">Answer D:</td>
      <td class="FormValue" id="answerD">{$q->multipleChoice4}</td>
  </tr>
  <tr><td class="OrgInfoText">Answer E:</td>
      <td class="FormValue" id="answerE">{$q->multipleChoice5}</td>
  </tr>
  <tr>
    <td width="150" class="OrgInfoText">Correct Answer:</td>
    <td class="FormValue" id="correctAnswer">{$q->correctAnswer}</td>
  </tr>
  <tr>

    <td class="OrgInfoText">Purpose:</td>
    <td class="FormValue" id="purpose">{$q->purpose}</td>
  </tr>
  <tr>
    <td class="OrgInfoText" width="150">Answer A Feedback:</td>
    <td class="FormValue" id="responseA">{$q->feedbackChoice1}</td>
  </tr>
  <tr>
    <td class="OrgInfoText">Answer B Feedback:</td>
    <td class="FormValue" id="responseB">{$q->feedbackChoice2}</td>
  </tr>
  <tr>

    <td class="OrgInfoText">Answer C Feedback:</td>
    <td class="FormValue" id="responseC">{$q->feedbackChoice3}</td>
  </tr>
  <tr>
    <td class="OrgInfoText">Answer D Feedback:</td>
    <td class="FormValue" id="responseD">{$q->feedbackChoice4}</td>
  </tr>
  <tr>
    <td class="OrgInfoText">Answer E Feedback:</td>
    <td class="FormValue" id="responseE">{$q->feedbackChoice5}</td>
  </tr>
  <tr>
    <td class="OrgInfoText">Key Learning Points:</td>
    <td class="FormValue" id="keyLearningPoints">{$q->learningPoints}</td>
  </tr>
  {if !empty($q->supportInfo)}
  <tr>
    <td class="OrgInfoText">Supplemental Information:</td>
    <td class="FormValue" id="supportInfo">{$q->supportInfo}</td>
  </tr>
  {/if}
  {if !empty($q->Notes)}
  <tr>
    <td class="OrgInfoText">Additional Information:</td>
    <td class="FormValue" id="notes">{$q->notes}</td>
  </tr>
  {/if}
</table>
<!--/div-->
</body>
</html>
