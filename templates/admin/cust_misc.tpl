{include file='admin/admin_header.tpl'}

<BODY class="ContentFrameBody">

<form>
{if $superUser}
<span class="FormText">Options available only to super user</span><BR>
<hr style="height:4px">

<span class="RecurrenceFormText">Features:</span><BR>
<input type="checkbox" name="enableQuestionPrinting" value="1"{if $enableQuestionPrinting} checked{/if}> Check this to allow folks in this organization to print questions.<BR>
<input type="checkbox" name="showUserThatAnswered" value="1"{if $showUserThatAnswered} checked{/if}> Check this to show the user that answered each question in the Summary report.<BR>

<hr>

<span class="RecurrenceFormText">Content Licensing:</span><BR>
<input type="checkbox" name="licenseHr" value="1"{if isset($licenseHr) && $licenseHr} checked{/if}>This organization is licensed for HR content<BR>
<input type="checkbox" name="licenseAb1825" value="1"{if isset($licenseAb1825) && $licenseAb1825} checked{/if}>This organization is licensed for AB1825 content<BR>
<input type="checkbox" name="licenseEdu" value="1"{if isset($licenseEdu) && $licenseEdu} checked{/if}>This organization is licensed for School District content<BR>

{/if}

<hr>

<BR><BR>
<span class="FormText">Options available to organization admins</span><BR>
<hr style="height:4px">

<span class="RecurrenceFormText">Application Theme:</span><BR>
<select name="uiTheme" onChange="javascript:parent.location.href='main.php?changeUiTheme='+this.options[this.selectedIndex].value">
{html_options options=$uiThemes selected=$selectedTheme}
</select>

<hr>

<span class="RecurrenceFormText">Email Format:</span><BR>
Select the format for email messages delivered by the system to class participants:<BR>
<select name="emailFormat">
{html_options values=$emailFormats output=$emailFormats selected=$selectedEmailFormat}
</select>

<hr>

<span class="RecurrenceFormText">Content Layout Options:</span><BR>
<input type="radio" name="displayChoices" value="right"{if $displayChoices == 'right'} checked{/if}>Display choices to the right of the question (default)<BR>
<input type="radio" name="displayChoices" value="bottom"{if $displayChoices == 'bottom'} checked{/if}>Display choices below the question<BR>
<input type="checkbox" name="useQuestionSetting" value="1"{if isset($useQuestionSetting) && $useQuestionSetting} checked{/if}>Use setting from the question but if question setting is not defined, use the above selection as default option<BR>

<hr>

<span class="RecurrenceFormText">Data Reporting Options:</span><BR>
<input type="checkbox" name="trackAdminSummaryDefault" value="1"{if isset($trackAdminSummaryDefault) && $trackAdminSummaryDefault} checked{/if}> Limit summary data to class administrators be default.  Viewing all summary data will still be available.

<hr>

<input type="submit" name="submit" value="Set">

</form>


</BODY>
</HTML>
