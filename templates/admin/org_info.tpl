{include file='admin/admin_header.tpl'}
{include file='admin/org_header.tpl'}

<form name="OrgForm" method="post" action="{$smarty.server.PHP_SELF}">

  {if !$create}
    <input type="hidden" name="create" value="0">
  {else}
    <input type="hidden" name="create" value="1">
  {/if}
  <input type="hidden" name="saveMe" value="1">

  <table border="0" align="top">
  {if !$create}
    <tr>
      <td class="OrgInfoText"> Organization ID: &nbsp;&nbsp;</td>
      <td>{$orgId}&nbsp;</td>
    </tr>
  {/if}
    <tr>
      <td class="OrgInfoText"> Organization Name: &nbsp;&nbsp;</td>
      <td>
        <input class="FormValue" type="text" name="orgName" id="orgName" maxlength="45" size="70"
		value="{$orgName}" onchange="javascript:SetFlag();">
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText">Organization Directory:</td>
      <td>
        <input {if $create}class="FormValue"{else}class="FormDisabled" readonly{/if}
		 type="text" name="orgDirectory" id="orgDirectory" maxlength="255" size="70"
		 onchange="javascript:SetFlag();" value="{$orgDirectory}">
        </td>
    </tr>
    {if $licenseAb1825}
    <tr>
      <td class="OrgInfoText" valign="top">Policies:</td>
      <td>
	<textarea class="FormValue" name="orgPolicies" id="orgPolicies" rows="10" cols="50"
		onChange="javascript:SetFlag();">{$orgPolicies}</textarea>
      </td>
    </tr>
    {/if}
    <tr>
      <td colspan="2">&nbsp;</td>
    </tr>
    <tr>
      <td colspan="2" class="OrgInfoText">
	News URL and Motif selections have been moved to the customize tab.<BR>
        Click on that tab to configure the items previously available on this page.
	{if $create}
	<!--p>Note: If creating an organization, the customize tab will not be available<BR>
	until after the organization has been successfully added to the system.<BR>
	</p-->
	{/if}
      </td>
    </tr>
  </table>

</form>


<div id="Cover" class="WaitText" style="position:absolute; left:0px; top:0px;
	 width:716px; height:341px; z-index:25; background-color: #FFFBAF;
	 /* layer-background-color: #FFFBAF; */ border: 1px solid #000000; visibility: hidden">
  <table width="100%" width="716" height="341" valign="center">
    <tr><td align="center" valign="middle" class="WaitText">Please Wait...</td><tr>
    <tr><td align="center" valign="middle" class="WaitText">Creating a new Organization Database.</td><tr>
    <tr><td align="center" valign="middle" class="WaitText">This may take several minutes.</td><tr>
  </table>
</div>

</BODY>
</HTML>
