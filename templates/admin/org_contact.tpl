{include file='admin/admin_header.tpl'}
{include file='admin/org_header.tpl'}


<form class="orgForm" name="OrgForm" method="post" action="{$smarty.server.PHP_SELF}">

  <input type="hidden" name="saveMe" value="1">
  <table border="0">
    <tr>
      <td class="OrgInfoText" height="20">The Organization Contact is:&nbsp;&nbsp;</td>
      <td>
        <select class="FormValue" name="orgContactId" onChange="javascript:SetFlag();">
          <option></option>
	  {html_options options=$orgContacts selected=$orgContactId}
        </select></td>
    </tr>
    <tr><td colspan="2">&nbsp;</td></tr>
    <tr>
      <td class="OrgInfoText" width="500"> Specify the number of questions an employee can
        leave unanswered before a delinquency report is sent to the Organization
        HR Contact. Leave the field blank if no report is desired.</td>
      <td align="left" class="OrgInfoText" valign="middle">
        <input class="FormValue" type="text" name="delinquencies" size="4" maxlength="3"
		value="{if $delinquencies != 0}{$delinquencies}{/if}"
	        onChange="javascript:SetFlag();"
		title="Notify organization HR Contact when student has X or more questions assigned."></td>
    </tr>
  </table>
</form>

</body>
</html>
