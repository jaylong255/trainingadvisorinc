{include file='admin/admin_header.tpl'}


<BODY bgcolor="#FFFCED"
      onLoad="MM_preloadImages('../themes/{$uiTheme}/admin/check_mark.gif', '../themes/{$uiTheme}/admin/btn_go_up.gif','../themes/{$uiTheme}/admin/btn_go_down.gif','../themes/{$uiTheme}/admin/btn_reset_up.gif','../themes/{$uiTheme}/admin/btn_reset_down.gif','../themes/{$uiTheme}/admin/btn_back_up.gif','../themes/{$uiTheme}/admin/btn_back_down.gif')">


<!-- Display return link -->
{if $decodedReturnURL}
<a href="{$decodedReturnURL}" onMouseOut="MM_swapImgRestore()"
   onMouseOver="MM_swapImage('Back','','../themes/{$uiTheme}/admin/btn_back_down.gif',1)">
<img name="Back" border="0" align="absbottom" width="23" height="21" src="../themes/{$uiTheme}/admin/btn_back_up.gif">
&nbsp;&nbsp;<b>Return to Employee List</b></a><br>
<hr>
{/if}



<!-- DisplaySearchInfo -->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tr>
    <td height="50">
      <form method="get" id="search_form" onSubmit="return DoResetSearch('search_form', '{$searchNewPage}', false)">
      Search <select size="1" name="search_by" class="formvalue">
      <!--option %s>User Names</option>\n", ($get_search_by == "u.Full_Name") ? "selected" : ""-->
      <!--option %s>User IDs</option>\n", ($get_search_by == "u.User_ID") ? "selected" : ""-->
      <!--option %s>User Logins</option>\n", ($get_search_by == "u.Login_ID") ? "selected" : ""-->
      <!--option %s>Email Addresses</option>\n", ($get_search_by == "u.Email") ? "selected" : ""-->
      <!--option %s>Track Names</option>\n", ($get_search_by == "t.Name") ? "selected" : ""-->
      {html_options options=$search_by_Opts selected=$search_by}
      </select>
      &nbsp;for&nbsp;
      <input type="text" class="formvalue" name="search_input" size="25" value="{$get_search}">&nbsp;
      <a href="javascript:DoResetSearch('search_form', '{$searchNewPage}', false);" onMouseOut="MM_swapImgRestore()"
         onMouseOver="MM_swapImage('GoSearch','','../themes/{$uiTheme}/admin/btn_go_down.gif',1)">
        <img name="GoSearch" border="0" align="absbottom" width="25" height="22" src="../themes/{$uiTheme}/admin/btn_go_up.gif"></a>
      <a href="javascript:DoResetSearch('search_form', '{$searchNewPage}', false);" onMouseOut="MM_swapImgRestore()"
         onMouseOver="MM_swapImage('ResetSearch','','../themes/{$uiTheme}/admin/btn_reset_down.gif',1)">
        <img name="ResetSearch" border="0" align="absbottom" width="49" height="22" src="../themes/{$uiTheme}/admin/btn_reset_up.gif"></a>
      </form>
    </td>
    <td align="right" height="50">
      <form method="get" id="row_form" onSubmit="return ChangeMaxRows('row_input', '{$maxRowNewPage}', true)">
        Show &nbsp; <input type="text" class="formvalue" name="row_input" size="3" value="{$get_max_rows}">
	&nbsp;items per page&nbsp;
        <a href="javascript:ChangeMaxRows('row_input', '{$maxRowNewPage}', false);" onMouseOut="MM_swapImgRestore()"
	   onMouseOver="MM_swapImage('GoMaxRows','','../themes/{$uiTheme}/admin/btn_go_down.gif',1)">
	  <img name="GoMaxRows" border="0" align="absbottom" width="25" height="22" src="../themes/{$uiTheme}/admin/btn_go_up.gif"></a>
      </form>
    </td>
  </tr>
</table>

<hr>
    
<!-- Display text links -->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tr>
    <td>
      <a id="ColumnLink" href="javascript:ShowPopupMenu('ColumnMenu', 'ColumnLink', true);">Select columns</a>
      {if $usersLinkURL && $usersLinkText}
       | <a href="{$usersLinkURL}">{$usersLinkText}</a>
      {/if}
    </td>
  </tr>
</table>

<!-- Div for column display selection menu -->
<div id="ColumnMenu" style="visibility:hidden; position:absolute; left:0px; top:0px; z-index:10">
<table border="0" class="table_popup" cellpadding="2" cellspacing="0">
  {section name=colMenuLinkIndex loop=colImageList}
  <tr>
    <td>
      {$colImageList[colMenuLinkIndex]}
    </td>
    <td>
      <a class="popup" href="{$colLinkList[colMenuLinkIndex]}">{$colLabelList[colMenuLinkIndex]}</a>
    </td>
  </tr>
  {/section}
</table>
</div>


<!-- Display record and page information -->
<!-- Pagination information -->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tr>
    <td width="50%">&nbsp;</td>
    <td width="50%">&nbsp;</td>
  </tr>
  <tr>
    <td width="50%">
      {if $numRows}
        Viewing {$startRow} - {$lastRow} of {$numRows}
      {else}
        <font color="#FF0000">No matching records found</font>
      {/if}
    </td>
    <td width="50%" align="right">
      {if $numPages}
        Page {$numPage} of {$numPages}
      {else}
        &nbsp;
      {/if}
    </td>
  </tr>
  <tr>
    <td width="50%">&nbsp;</td>
    <td width="50%">&nbsp;</td>
  </tr>
</table>




<!-- Display the column heders -->
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="table_list">
  <tr>
    <td class="table_header" width="20">&nbsp;</td>
    {section name=colHdrIndex loop=$colHdrCaption}
    <td class="table_header" width="{$colHdrWidth[colHdrIndex]}">
      &nbsp;&nbsp;
      <a style="color:#FFFFFF"
         href="{$colHdrLink}{if $get_descend}&get_descend=true{/if}&get_sort={$colHdrSort[colHdrIndex]}">
      {$colHdrCaption[colHdrIndex]}
      {if $get_sort == $colHdrSort[colHdrIndex]}
	&nbsp;<img border="0" src="../themes/{$uiTheme}/admin/{if $get_descend}order_descend.gif{else}order_ascend.gif{/if}">
      {/if}
      </a>
    </td>
    {/section}
    <td class="table_header" width="100" align="center">
      Actions
    </td>
  </tr>


<!-- This section is for the actual records -->
  {section name=rowIndex loop=$rows}
  <tr>
    {cycle name="colMargin" values="tdDefault,tdDefaultAlt" assign="colMargin"}
    <td class="{$colMargin}" width="20">
      {if $get_assigned == $trackID[rowIndex]}
        <img border="0" src="../themes/{$uiTheme}/admin/check_mark.gif">
      {else}
        &nbsp;
      {/if}
    </td>
    {section name=colIndex loop=$rows[rowIndex]}
    <td class="{$colMargin}" width="{$colHdrWidth[colIndex]}">
      {if $rows[rowIndex][colIndex]}
        &nbsp;&nbsp;{$rows[rowIndex][colIndex]}
      {else}
        &nbsp;&nbsp;{$colDefault[colIndex]}
      {/if}
    </td>
    {/section}
    <td class="{cycle name="colAction" values="tdDefault,tdDefaultAlt"}" width="100" align="center">
      {if $editRefs[rowIndex] == '' && $delRefs[rowIndex] == ''}
        &nbsp;
      {else}
        {if $session_Is_Admin}
          <a href="{$editRefs[rowIndex]}">Edit</a>&nbsp;&nbsp;&nbsp;&nbsp;
          <a href="{$delRefs[rowIndex]}">Delete</a>
	{elseif $get_assigned == $trackID[rowIndex]}
	  <a href="{$editRefs[rowIndex]}">Remove</a>
	{else}
	  <a href="{$editRefs[rowIndex]}">Assign</a>
	{/if}
      {/if}
    </td>
  </tr>
  {/section}
</table>


<!-- Display bottom portion of page info -->


</BODY>
</HTML>
