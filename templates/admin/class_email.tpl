{include file='admin/admin_header.tpl'}
{include file='admin/track_header.tpl'}


<form name="AdminForm" class="orgForm" method="POST" action="{$smarty.server.PHP_SELF}">
  <input type="hidden" name="eQueueId" id="eQueueId" value="{if isset($queueEntry)}{$queueEntry->emailQueueId}{else}0{/if}"/>
  <table width="100%" border="0">
    {if isset($statusMsg)}
    <tr>
      <td class="error" colspan="2">{$statusMsg}</td>
    </tr>
    {/if}
    <tr>
      <td class="OrgInfoText" colspan="2">You may send messages to all the participants of this class by completing the subject and body of your message below.  The email will appear to come from you.</td>
    </tr>
    <tr>
      <td class="OrgInfoText" colspan="2">NOTE: If your organization has a very large number of recipients, it will take some time for everyone to receive the message.</td>
    </tr>
    <tr>
      <td class="OrgInfoText">From:&nbsp;&nbsp;</td>
      <td>
        <input class="{if $superUser}FormValue{else}FormDisabled{/if}" type="text" name="mailFrom" id="mailFrom" maxlength="80" size="110" value="{if isset($queueEntry)}{$queueEntry->fromAddress}{/if}"{if !$superUser} disabled{/if}/>
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText">Subject:&nbsp;&nbsp;</td>
      <td>
        <input class="FormValue" type="text" name="subject" id="subject" maxlength="80" size="110" value="{if isset($queueEntry)}{$queueEntry->subject}{/if}"/>
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText" valign="top">Message:&nbsp;&nbsp;</td>
      <td width="200">
        <textarea rows="10" cols="80" name="body" id="body" class="FormValue">{if isset($queueEntry) && $queueEntry->emailQueueId > 0}{$queueEntry->body}{/if}</textarea>
      </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td class="OrgInfoText">If you would like to schedule this message to be delivered at a later date, enter it below.<BR>
        Otherwise, leave blank and your message will be delivered as soon as possible.</td>
    <tr>
      <td class="OrgInfoText" width="90">Scheduled:</td>
      <td valign="center" width="150">
        <input class="FormValue" type="text" name="schedDate" id="schedDate" value ="{if isset($queueEntry) && $queueEntry->emailQueueId > 0 && $queueEntry->GetDatePart($queueEntry->scheduledDate) != '0000-00-00'}{$queueEntry->GetDatePart($queueEntry->scheduledDate)}{/if}"
	     onchange="javascript:SetFlag();" title="Scheduled Delivery Date"/>
        <a href="javascript:doNothing()"
	   onClick="setDateField(document.getElementById('schedDate'));top.newWin=window.open('calendar.html','cal','dependent=yes,width=210,height=230,screenX=200,screenY=300,titlebar=yes')">
	  <img id="cal" src="../themes/{$uiTheme}/admin/calendar.gif" border="0" width="16" height="16"></a>
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText">&nbsp;</td>
      <td>
        <input type="submit" name="submit" value="Submit"/>
      </td>
    </tr>
  </table>
{if isset($queueIds) && !empty($queueIds)}
<hr width="100%" align="left">
<div class="OrgInfoText">Previously sent messages:</div>
<table width="100%">
  <tr>
    <td class="table_header" width="26%">{$queueListLbls[0]}</td>
    <td class="table_header" width="30%">{$queueListLbls[1]}</td>
    <td class="table_header" width="7%">{$queueListLbls[2]}</td>
    <td class="table_header" width="7%">{$queueListLbls[3]}</td>
    <td class="table_header" width="10%" align="center">Actions</td>
  </tr>

  {section name=queueListIndex loop=$queueIds}
  <tr class="{cycle name="rowItem" values="trDefault,trDefaultAlt"}">
    {section name=queueFieldIndex loop=$queueRows[queueListIndex]}
    <td>{$queueRows[queueListIndex][queueFieldIndex]|escape:'html'}</td>
    {/section}
    <td>
    {if $queueRows[queueListIndex][3] == 'Not Yet'}<a href="{$smarty.server.PHP_SELF}?queueId={$queueIds[queueListIndex]}">Edit</a> | {/if}
    <a href="{$smarty.server.PHP_SELF}?queueId={$queueIds[queueListIndex]}&op=delete">Delete</a>
    </td>
  </tr>
  {/section}
</table>
{/if}
</form>
</body>
</html>
