<BODY class="ContentFrameBody"
      onLoad="MM_preloadImages('../themes/{$uiTheme}/admin/btn_update_up.gif','../themes/{$uiTheme}/admin/btn_update_down.gif',
				 '../themes/{$uiTheme}/admin/btn_add_up.gif','../themes/{$uiTheme}/admin/btn_add_down.gif',
				 '../themes/{$uiTheme}/admin/btn_back_down.gif');
	{if isset($user) && !$user->password}document.getElementById('password').value='';{/if}">

{if $currentSubTab > 0}
<table border="0" width="680" align="top" cellpadding="2" cellspacing="3">
  <tr>
    <td width="23">
      <A href="user_list.php" onClick="return CheckSave();"
	 onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Back','','../themes/{$uiTheme}/admin/btn_back_down.gif',1)">
        <img name="Back" border="0" src="../themes/{$uiTheme}/admin/btn_back_up.gif" width="23" height="21"></a>
    </td>
    <td align="left">
      <a class="OrgInfoText" href="user_list.php">Return to Employee List</a>
    </td>
    <td align="left">
    {if $user->userId != 0}
      <a href="user_edit.php?userIdAsTemplate={$user->userId}">Create User With Below As Template</a>
    {else}&nbsp;{/if}
    </td>
    <td align="right">
      {if $user->userId == 0}
	<a href="javascript:AddRecordUserEdit();" onMouseOut="MM_swapImgRestore()"
	   onMouseOver="MM_swapImage('AddBtn','','../themes/{$uiTheme}/admin/btn_add_down.gif',1)">
	<!--onClick="document.getElementById('Cover').style.visibility='visible';-->
	  <img name="Add" border="0" src="../themes/{$uiTheme}/admin/btn_add_up.gif" width="56" height="22"></a>
      {else}
	<a href="javascript:UpdateRecordUserEdit();" onMouseOut="MM_swapImgRestore()"
	   onMouseOver="MM_swapImage('Update','','../themes/{$uiTheme}/admin/btn_update_down.gif',1)">
	  <img name="Update" border="0" src="../themes/{$uiTheme}/admin/btn_update_up.gif" width="56" height="22"></a>
      {/if}
    </td>
  </tr>
  {if isset($errMsg) && $errMsg}
  <tr>
    <td colspan="4" class="error">
      {$errMsg}
    </td>
  </tr>
  {/if}
</table>

<hr width="680" align="left">
{/if}
