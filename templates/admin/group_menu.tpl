<div id="GroupMenu" style="visibility:hidden; position:absolute; left:0px; top:0px; z-index:-1">
<form method="GET" id="group_form" onSubmit="return DoChangeGroup(true)">
<table class="table_popup" border="0" cellpadding="2" cellspacing="0">
  <tr height="32" valign="middle">
    <td width="25" align="center" valign="middle">
      <table border="0" cellpadding="0" cellspacing="0" width="15">
        <tr height="12">
          <td valign="top">
            <a href="javascript:ScrollSequence(true, false, NaN, {$maxGroupId});">
              <img border="0" src="../themes/{$uiTheme}/admin/arrow_up.gif"></a>
          </td>
        </tr>
        <tr height="12">
          <td valign="bottom">
            <a href="javascript:ScrollSequence(false, false, NaN, {$maxGroupId});">
              <img border="0" src="../themes/{$uiTheme}/admin/arrow_down.gif"></a>
          </td>
        </tr>
      </table>
    </td>
    <td valign="middle" align="center">
      <input type="text" name="number" size="7" value="0" style="text-align:right">
      <input type="hidden" name="curgroup" size="7" value="0" style="text-align:right">
      &nbsp;&nbsp;<a href="javascript:DoChangeGroup(false);" onMouseOut="MM_swapImgRestore()"
		     onMouseOver="MM_swapImage('GoSearch','','../themes/{$uiTheme}/admin/btn_go_down.gif',1);">
		  <img name="GoSearch" border="0" align="absbottom" width="25" height="22" src="../themes/{$uiTheme}/admin/btn_go_up.gif"></a>
    </td>
    <td width="5"></td>
  </tr>
</form>
</div>
