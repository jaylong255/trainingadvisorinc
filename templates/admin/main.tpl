{include file='admin/admin_header.tpl'}

<BODY class="noscroll" bgcolor="#FFFFFF" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0" width="100%"
      onResize="javascript:GetSize();" onLoad="GetSize(); MM_preloadImages('../themes/{$uiTheme}/admin/btn_help_down.gif');
                SetupImgDesk('{$uiTheme}');">

{include file='common/motif.tpl'}
{assign var='tabsVPos' value=124}
{assign var='tabsHPos' value=26}
{assign var='subtabsVPos' value=148}

<!-- Organization Name -->
<div id="Title" style="position:absolute; left:25px; top:100px; width:600px; height:26px; text-align:left;" class="Title">
	{$orgName}</div>

<!-- To Desktop button -->
<div id="ToDesktop" style="position:absolute; left:565px; top:100px;">
      <a href="/desktop/login/change_prefs.php?currentTab=101&returnUrl=/desktop/presentation/portal.php"
	 onMouseOver="swap(toDesk,imgDeskb);"
	 onMouseOut="swap(toDesk,imgDesk);">
      <img name="toDesk" src="../themes/{$uiTheme}/admin/to_desktop_up.png" border="0"></a>
</div>

<!-- Top level tabs -->
<div id="Tab1Grey" style="position:absolute; left:{$tabsHPos}px; top:{$tabsVPos}px; z-index:2">
	{if $superUser && !isset($orgId)}
	  <a href="javascript:BranchTab('org_list.php');" target="FormWindow" onclick="javascript:SetTab(1, 0);">
	  <img src="../themes/{$uiTheme}/admin/builder_org_behind.png" border="0" id="OrgTab"></a>
	{elseif $role == ROLE_ORG_ADMIN}
	  <a href="javascript:BranchTab('org_info.php');" target="FormWindow" onclick="javascript:SetTab(1, 1);">
	  <img src="../themes/{$uiTheme}/admin/builder_org_behind.png" border="0" id="OrgTab"></a>
	{else}
	  <a href="javascript:BranchTab('org_reports.php');" target="FormWindow" onclick="javascript:SetTab(1, 5);">
	  <img src="../themes/{$uiTheme}/admin/builder_org_behind.png" border="0" id="OrgTab"></a>
        {/if}
	</div>
<div id="Tab2Grey" style="position:absolute; left:{$tabsHPos+100}px; top:{$tabsVPos}px; z-index:2">
	{if ($superUser || $role != ROLE_CLASS_CONTACT_SUPERVISOR)}
	  <a href="javascript:BranchTab('user_list.php');" target="FormWindow" onclick="javascript:SetTab(2, 0);">
	  <img src="../themes/{$uiTheme}/admin/builder_employee_behind.png" border="0" id="EmployeeTab"></a>
	{/if}
	</div>
<div id="Tab3Grey" style="position:absolute; left:{$tabsHPos+200}px; top:{$tabsVPos}px; width:107px; height:26px; z-index:2">
	  <a href="javascript:BranchTab('track_list.php');" target="FormWindow" onclick="javascript:SetTab(3, 0);">
	  <img src="../themes/{$uiTheme}/admin/builder_tracks_behind.png" border="0" id="TracksTab"></a>
	</div>
<div id="Tab4Grey" style="position:absolute; left:{$tabsHPos+301}px; top:{$tabsVPos}px; width:107px; height:26px; z-index:2">
	{if ($superUser || $role != ROLE_CLASS_CONTACT_SUPERVISOR)}
	  <a href="javascript:BranchTab('question_list.php');" target="FormWindow" onclick="javascript:SetTab(4, 0);">
	  <img src="../themes/{$uiTheme}/admin/builder_questions_behind.png" border="0" id="QuestionsTab"></a>
	{/if}
	</div>
<div id="Tab5Grey" style="position:absolute; left:{$tabsHPos+400}px; top:{$tabsVPos}px; width:107px; height:26px; z-index:2">
	{if ($superUser || $role != ROLE_CLASS_CONTACT_SUPERVISOR)}
	  <a href="javascript:BranchTab('file_man.php');" target="FormWindow" onclick="javascript:SetTab(5, 0);">
	  <img src="../themes/{$uiTheme}/admin/builder_customize_back.png" border="0" id="CustomizeTab"></a>
	{/if}
	</div>


<div id="Tab1" style="position:absolute; left:{$tabsHPos}px; top:{$tabsVPos}px; width:107px; height:26px; z-index:3; border:1px none #000000;
	{if $currentTab == 1 && ($superUser || $role != ROLE_END_USER)}visibility:visible{else}visibility:hidden{/if}">
	<img src="../themes/{$uiTheme}/admin/builder_org_forward.png" border="0" id="OrgTab"></div>
<div id="Tab2" style="position:absolute; left:{$tabsHPos+100}px; top:{$tabsVPos}px; width:107px; height:26px; z-index:3;
	{if $currentTab == 2}visibility:visible{else}visibility:hidden{/if}">
	<img src="../themes/{$uiTheme}/admin/builder_employee_forward.png" border="0" id="EmployeeTab"></div>
<div id="Tab3" style="position:absolute; left:{$tabsHPos+200}px; top:{$tabsVPos}px; width:107px; height:26px; z-index:3;
	{if $currentTab == 3}visibility:visible{else}visibility:hidden{/if}">
	<img src="../themes/{$uiTheme}/admin/builder_tracks_forward.png" border="0" id="TracksTab"></div>
<div id="Tab4" style="position:absolute; left:{$tabsHPos+300}px; top:{$tabsVPos}px; width:107px; height:26px; z-index:3;
	{if $currentTab == 4}visibility:visible{else}visibility:hidden{/if}">
	<img src="../themes/{$uiTheme}/admin/builder_questions_forward.png" border="0" id="QuestionsTab"></div>
<div id="Tab5" style="position:absolute; left:{$tabsHPos+400}px; top:{$tabsVPos}px; width:107px; height:26px; z-index:3;
	{if $currentTab == 5}visibility:visible{else}visibility:hidden{/if}">
	<img src="../themes/{$uiTheme}/admin/builder_customize_forward.png" border="0" id="CustomizeTab"></div>


<!-- These div tags make up the backgrounds of the primary and secondary tab folders -->
<div id="AdminOuterFolder" class="AdminOuterFolder" style="position:absolute; left:0px; top:{$subtabsVPos-1}px;"></div>

<div id="ManillaFolder" class="manillafolder" style="position:absolute; left:18px; top:{$tabsVPos+52}px;">&nbsp;</div>


<div id="FormTab" style="position:absolute; left:{$tabsHPos}px; top:{$subtabsVPos}px; width:211px; height:23px; z-index:11; border:1px none #000000;
	{if $currentSubTab == 0}visibility:visible;{else}visibility:hidden;{/if}">
	<!--img src="../themes/{$uiTheme}/admin/blank_up.png"--></div>


<!-- question subtabs -->
<div id="CurrentTab2" style="position:absolute; left:{$tabsHPos}px; top:{$subtabsVPos}px; z-index:12;
	{if $currentTab == 4 && $currentSubTab > 2 && $currentSubTab != 6}visibility:visible{else}visibility:hidden{/if}">
	<a href="javascript:BranchTab('question_list.php');" target="FormWindow" onclick="javascript:SetQuestionSubTab(1);">
	<img src="../themes/{$uiTheme}/admin/current_up.png" border="0"></a></div>

<div id="CurrentTab1" style="position:absolute; left:{$tabsHPos}px; top:{$subtabsVPos}px; z-index:13;
	{if $currentTab == 4 && ($currentSubTab == 1 || $currentSubTab == 2 || $currentSubTab == 6)} visibility:visible{else}visibility:hidden{/if}">
	<img src="../themes/{$uiTheme}/admin/current_down.png" border="0"></div>

<div id="ArchiveTab2" style="position:absolute; left:{$tabsHPos+66}px; top:{$subtabsVPos}px; z-index:14;
	{if $currentTab == 4 && $currentSubTab != 0 && $currentSubTab != 3 && $currentSubTab != 7}visibility:visible{else}visibility:hidden{/if}">
	<a href="javascript:BranchTab('archive_list.php');" target="FormWindow" onclick="javascript:SetQuestionSubTab(3);">
	<img src="../themes/{$uiTheme}/admin/archived_up.png" border="0"></a></div>

<div id="ArchiveTab1" style="position:absolute; left:{$tabsHPos+66}px; top:{$subtabsVPos}px; z-index:15;
	{if $currentTab == 4 && $currentSubTab == 3 || $currentSubTab == 7}visibility:visible{else}visibility:hidden{/if}">
	<img src="../themes/{$uiTheme}/admin/archived_down.png" border="0"></div>

<div id="SummaryTab2" style="position:absolute; left:{$tabsHPos+138}px; top:{$subtabsVPos}px; z-index:16;
	{if $currentTab == 4 && $currentSubTab != 0 && $currentSubTab != 4}visibility:visible{else}visibility:hidden{/if}">
	<a href="javascript:BranchTab('question_summary.php');" target="FormWindow" onclick="javascript:SetQuestionSubTab(4);">
	<img src="../themes/{$uiTheme}/admin/summary_up.png" border="0"></a></div>

<div id="SummaryTab1" style="position:absolute; left:{$tabsHPos+138}px; top:{$subtabsVPos}px; z-index:17;
	{if $currentTab == 4 && $currentSubTab == 4}visibility:visible{else}visibility:hidden{/if}">
	<img src="../themes/{$uiTheme}/admin/summary_down.png" border="0"></div>

<div id="AnalysisTab2" style="position:absolute; left:{$tabsHPos+217}px; top:{$subtabsVPos}px; z-index:18;
	{if $currentTab == 4 && $currentSubTab != 0 && $currentSubTab != 5}visibility:visible{else}visibility:hidden{/if}">
	<a href="javascript:BranchTab('question_analysis.php');" target="FormWindow" onclick="javascript:SetQuestionSubTab(5);">
	<img src="../themes/{$uiTheme}/admin/analysis_up.png" border="0"></a></div>

<div id="AnalysisTab1" style="position:absolute; left:{$tabsHPos+217}px; top:{$subtabsVPos}px; z-index:19;
	{if $currentTab == 4 && $currentSubTab == 5}visibility:visible{else}visibility:hidden{/if}">
	<img src="../themes/{$uiTheme}/admin/analysis_down.png" border="0"></div>

<div id="CategoriesUnselected" style="position:absolute; left:{$tabsHPos+286}px; top:{$subtabsVPos}px; z-index:18;
	{if $currentTab == 4 && $currentSubTab != 0 && $currentSubTab != 8}visibility:visible{else}visibility:hidden{/if}">
	<a href="javascript:BranchTab('org_data_edit.php?form=3');" target="FormWindow" onclick="javascript:SetQuestionSubTab(8);">
	<img src="../themes/{$uiTheme}/admin/categories_up.png" border="0"></a></div>

<div id="CategoriesSelected" style="position:absolute; left:{$tabsHPos+286}px; top:{$subtabsVPos}px; z-index:19;
	{if $currentTab == 4 && $currentSubTab == 8}visibility:visible{else}visibility:hidden{/if}">
	<img src="../themes/{$uiTheme}/admin/categories_down.png" border="0"></div>


<!-- track subtabs -->
<div id="Track1g" style="position:absolute; left:{$tabsHPos}px; top:{$subtabsVPos}px; z-index:20; border:1px none #000000;
	{if $currentTab == 3 && $currentSubTab > 1}visibility:visible{else}visibility:hidden{/if}">
	<a href="javascript:BranchTab('track_edit.php');" id="Track1Ref" target="FormWindow" onclick="javascript:SetTrackSubTab(1);">
	<img src="../themes/{$uiTheme}/admin/info_up.png" border="0"></a></div>
<div id="Track1" style="position:absolute; left:{$tabsHPos}px; top:{$subtabsVPos}px; z-index:21; border:1px none #000000;
	{if $currentTab == 3 && $currentSubTab == 1}visibility:visible{else}visibility:hidden{/if}">
	<img src="../themes/{$uiTheme}/admin/info_down.png" border="0"></div>
<div id="Track2g" style="position:absolute; left:{$tabsHPos+48}px; top:{$subtabsVPos}px; z-index:22; border:1px none #000000;
	{if $currentTab == 3 && $currentSubTab != 0 && $currentSubTab != 2}visibility:visible{else}visibility:hidden{/if}">
	<a href="javascript:BranchTab('participant_list.php');" id="Track2Ref" target="FormWindow" onclick="javascript:SetTrackSubTab(2);">
	<img src="../themes/{$uiTheme}/admin/participants_up.png" border="0"></a></div>
<div id="Track2" style="position:absolute; left:{$tabsHPos+48}px; top:{$subtabsVPos}px; z-index:23; border:1px none #000000;
	{if $currentTab == 3 && $currentSubTab == 2}visibility:visible{else}visibility:hidden{/if}">
	<img src="../themes/{$uiTheme}/admin/participants_down.png" border="0"></div>
<div id="Track3g" style="position:absolute; left:{$tabsHPos+136}px; top:{$subtabsVPos}px; z-index:24; border:1px none #000000;
	{if $currentTab == 3 && $currentSubTab != 0 && $currentSubTab != 3}visibility:visible{else}visibility:hidden{/if}">
	<a href="javascript:BranchTab('assignment_list.php');" id="Track3Ref" target="FormWindow" onclick="javascript:SetTrackSubTab(3);">
	<img src="../themes/{$uiTheme}/admin/assignments_up.png" border="0"></a></div>
<div id="Track3" style="position:absolute; left:{$tabsHPos+136}px; top:{$subtabsVPos}px; z-index:25; border:1px none #000000;
	{if $currentTab == 3 && $currentSubTab == 3}visibility:visible{else}visibility:hidden{/if}">
	<img src="../themes/{$uiTheme}/admin/assignments_down.png" border="0"></div>
<div id="Track4g" style="position:absolute; left:{$tabsHPos+236}px; top:{$subtabsVPos}px; z-index:26; border:1px none #000000;
	{if $currentTab == 3 && $currentSubTab != 0 && $currentSubTab != 4}visibility:visible{else}visibility:hidden{/if}">
	<a href="javascript:BranchTab('faq_list.php');" id="Track4Ref" target="FormWindow" onclick="javascript:SetTrackSubTab(4);">
	<img src="../themes/{$uiTheme}/admin/FAQs_up.png" border="0"></a></div>
<div id="Track4" style="position:absolute; left:{$tabsHPos+236}px; top:{$subtabsVPos}px; z-index:27; border:1px none #000000;
	{if $currentTab == 3 && $currentSubTab == 4}visibility:visible{else}visibility:hidden{/if}">
	<img src="../themes/{$uiTheme}/admin/FAQs_down.png" border="0"></div>
<div id="Track5g" style="position:absolute; left:{$tabsHPos+289}px; top:{$subtabsVPos}px; z-index:28; border:1px none #000000;
	{if $currentTab == 3 && $currentSubTab != 0 && $currentSubTab != 5}visibility:visible{else}visibility:hidden{/if}">
	<a href="javascript:BranchTab('progress_list.php');" id="Track5Ref" target="FormWindow" onclick="javascript:SetTrackSubTab(5);">
	<img src="../themes/{$uiTheme}/admin/progress_up.png" border="0"></a></div>
<div id="Track5" style="position:absolute; left:{$tabsHPos+289}px; top:{$subtabsVPos}px; z-index:29; border:1px none #000000;
	{if $currentTab == 3 && $currentSubTab == 5}visibility:visible{else}visibility:hidden{/if}">
	<img src="../themes/{$uiTheme}/admin/progress_down.png" border="0"></div>
<div id="Track6g" style="position:absolute; left:{$tabsHPos+362}px; top:{$subtabsVPos}px; z-index:28; border:1px none #000000;
	{if $currentTab == 3 && $currentSubTab != 0 && $currentSubTab != 6}visibility:visible{else}visibility:hidden{/if}">
	<a href="javascript:BranchTab('class_email.php');" id="Track6Ref" target="FormWindow" onclick="javascript:SetTrackSubTab(6);">
	<img src="../themes/{$uiTheme}/admin/builder_sendemail_up.png" border="0"></a></div>
<div id="Track6" style="position:absolute; left:{$tabsHPos+362}px; top:{$subtabsVPos}px; z-index:29; border:1px none #000000;
	{if $currentTab == 3 && $currentSubTab == 6}visibility:visible{else}visibility:hidden{/if}">
	<img src="../themes/{$uiTheme}/admin/builder_sendemail_down.png" border="0"></div>


<!-- organization subtabs -->
{if $role == ROLE_ORG_ADMIN}
<div id="Org1g" style="position:absolute; left:{$tabsHPos}px; top:{$subtabsVPos}px; z-index:30;
	{if $currentTab == 1 && $currentSubTab > 1}visibility:visible{else}visibility:hidden{/if}">
	<a href="org_info.php" id="Org1Ref" target="FormWindow" onclick="javascript:SetOrgSubTab(1);">
	<img src="../themes/{$uiTheme}/admin/info_up.png" border="0"></a></div>
<div id="Org1" style="position:absolute; left:{$tabsHPos}px; top:{$subtabsVPos}px; z-index:31;
	{if $currentTab == 1 && $currentSubTab == 1}visibility:visible{else}visibility:hidden{/if}">
	<img src="../themes/{$uiTheme}/admin/info_down.png" border="0"></div>
<div id="Org2g" style="position:absolute; left:{$tabsHPos+48}px; top:{$subtabsVPos}px; z-index:32;
	{if $currentTab == 1 && $currentSubTab != 0 && $currentSubTab != 2}visibility:visible{else}visibility:hidden{/if}">
	<a href="org_contact.php" id="Org2Ref" target="FormWindow" onclick="javascript:SetOrgSubTab(2);">
	<img src="../themes/{$uiTheme}/admin/contact_up.png" border="0"></a></div>
<div id="Org2" style="position:absolute; left:{$tabsHPos+48}px; top:{$subtabsVPos}px; z-index:33;
	{if $currentTab == 1 && $currentSubTab == 2}visibility:visible{else}visibility:hidden{/if}">
	<img src="../themes/{$uiTheme}/admin/contact_down.png" border="0"></div>
<div id="Org3g" style="position:absolute; left:{$tabsHPos+114}px; top:{$subtabsVPos}px; z-index:34;
	{if $currentTab == 1 && $currentSubTab != 0 && $currentSubTab != 3}visibility:visible{else}visibility:hidden{/if}">
	<a href="org_data.php" id="Org3Ref" target="FormWindow" onclick="javascript:SetOrgSubTab(3);">
	<img src="../themes/{$uiTheme}/admin/data_up.png" border="0"></a></div>
<div id="Org3" style="position:absolute; left:{$tabsHPos+114}px; top:{$subtabsVPos}px; z-index:35;
	{if $currentTab == 1 && $currentSubTab == 3}visibility:visible{else}visibility:hidden{/if}">
	<img src="../themes/{$uiTheme}/admin/data_down.png" border="0"></div>
<div id="Org4g" style="position:absolute; left:{$tabsHPos+160}px; top:{$subtabsVPos}px; z-index:36;
	{if $currentTab == 1 && $currentSubTab != 0 && $currentSubTab != 4}visibility:visible{else}visibility:hidden{/if}">
	<a href="org_utilities.php" id="Org3Ref" target="FormWindow" onclick="javascript:SetOrgSubTab(4);">
	<img src="../themes/{$uiTheme}/admin/utilities_up.png" border="0"></a></div>
<div id="Org4" style="position:absolute; left:{$tabsHPos+160}px; top:{$subtabsVPos}px; z-index:37;
	{if $currentTab == 1 && $currentSubTab == 4}visibility:visible{else}visibility:hidden{/if}">
	<img src="../themes/{$uiTheme}/admin/utilities_down.png" border="0"></div>
{/if}
<div id="Org5g" style="position:absolute; left:{$tabsHPos+226}px; top:{$subtabsVPos}px; z-index:38;
	{if $currentTab == 1 && $currentSubTab != 0 && $currentSubTab != 5}visibility:visible{else}visibility:hidden{/if}">
	<a href="org_reports.php" id="Org3Ref" target="FormWindow"{if $role == ROLE_ORG_ADMIN} onclick="javascript:SetOrgSubTab(5);"{/if}>
	<img src="../themes/{$uiTheme}/admin/reports_up.png" border="0"></a></div>
<div id="Org5" style="position:absolute; left:{$tabsHPos+226}px; top:{$subtabsVPos}px; z-index:39;
	{if $currentTab == 1 && $currentSubTab == 5}visibility:visible{else}visibility:hidden{/if}">
	<img src="../themes/{$uiTheme}/admin/reports_down.png" border="0"></div>


<!-- customize subtabs -->
<div id="FileMan2" style="position:absolute; left:{$tabsHPos}px; top:{$subtabsVPos}px; z-index:12;
	{if $currentTab == 5 && $currentSubTab > 1}visibility:visible{else}visibility:hidden{/if}">
	<a href="javascript:BranchTab('file_man.php');" target="FormWindow" onclick="javascript:SetCustomizeSubTab(1);">
	<img src="../themes/{$uiTheme}/admin/file_manager_up.png" border="0"></a></div>
<div id="FileMan1" style="position:absolute; left:{$tabsHPos}px; top:{$subtabsVPos}px; z-index:13;
	{if $currentTab == 5 && $currentSubTab == 1}visibility:visible{else}visibility:hidden{/if}">
	<img src="../themes/{$uiTheme}/admin/file_manager_down.png" border="0"></div>
{if $role == ROLE_ORG_ADMIN}
<div id="CustMsg2" style="position:absolute; left:{$tabsHPos+102}px; top:{$subtabsVPos}px; z-index:14;
	{if $currentTab == 5 && $currentSubTab != 0 && $currentSubTab != 2}visibility:visible{else}visibility:hidden{/if}">
	<a href="javascript:BranchTab('cust_misc.php');" target="FormWindow" onclick="javascript:SetCustomizeSubTab(2);">
	<img src="../themes/{$uiTheme}/admin/misc_up.png" border="0"></a></div>
<div id="CustMsg1" style="position:absolute; left:{$tabsHPos+102}px; top:{$subtabsVPos}px; z-index:15;
	{if $currentTab == 5 && $currentSubTab == 2}visibility:visible{else}visibility:hidden{/if}">
	<img src="../themes/{$uiTheme}/admin/misc_down.png" border="0"></div>
{/if}

<div id="HelpDiv" style="position:absolute; left:3px; top:{$tabsVPos+27}px; width:24px; height:25px; z-index:51">
	<a href="javascript:Help();" onMouseOut="MM_swapImgRestore()"
           onMouseOver="MM_swapImage('Help','','../themes/{$uiTheme}/admin/btn_help_down.gif',1)">
	<img name="Help" border="0" src="../themes/{$uiTheme}/admin/btn_help_up.gif" width="22" height="22"
	     alt="Online Help is not available yet."></a></div>

<div id="FormDiv" style="position:absolute; left:25px; top:{$tabsVPos+58}px; width:90%; height:90%; z-index:7; border:1px none #000000;">
  <IFRAME NAME="FormWindow" ID="FormWindow" SCROLLING="auto" WIDTH="100%" HEIGHT="100%"
	SRC="{$frameTarget}"  FRAMEBORDER="0"></IFRAME>
</div>

</body>
</html>
