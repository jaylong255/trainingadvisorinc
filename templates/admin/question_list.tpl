<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
  <head>
    <title>Question List</title>
    <META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link rel="stylesheet" href="../themes/{$uiTheme}/desktop.css" type="text/css">
    <script language=JavaScript src="../javascript/PopupMenu.js" type="text/javascript"></script>
    <script language=JavaScript src="../javascript/desktop.js" type="text/javascript"></script>
    <script language=JavaScript src="../javascript/admin.js" type="text/javascript"></script>
  </head>
  <body bgcolor="#FFFCED" onLoad="MM_preloadImages('../themes/{$uiTheme}/admin/check_mark.gif', '../themes/{$uiTheme}/admin/btn_go_up.gif','../themes/{$uiTheme}/admin/btn_go_down.gif','../themes/{$uiTheme}/admin/btn_reset_up.gif','../themes/{$uiTheme}/admin/btn_reset_down.gif','../themes/{$uiTheme}/admin/btn_back_up.gif','../themes/{$uiTheme}/admin/btn_back_down.gif','../themes/{$uiTheme}/admin/arrow_up.gif','../themes/{$uiTheme}/admin/arrow_down.gif')">
    <div id="SequenceMenu" style="visibility:hidden; position:absolute; left:0px; top:0px; z-index:10">
    <form method="get" id="sequence_form" onSubmit="return DoChangeSequence(true)">
  
    <table class="table_popup" border="0" cellpadding="2" cellspacing="0">
      <tr height="32" valign="middle">
        <td  width="25" align="center" valign="middle">
 	        <table border="0" cellpadding="0" cellspacing="0" width="15">
            <tr height="12">
              <td valign="top">
                <a href="javascript:ScrollSequence(true, true, 1, {$nMaxSequence})">
                  <img border="0" src="../themes/{$uiTheme}/admin/arrow_up.gif">
                </a>
              </td>
            </tr>
            <tr height="12">
              <td valign="bottom">
                <a href="javascript:ScrollSequence(false, true, 1, {$nMaxSequence})">
                  <img border="0" src="../themes/{$uiTheme}/admin/arrow_down.gif">
                </a>
              </td>
            </tr>
          </table>
        </td>
  
        <td valign="middle" align="center">
          <input type="text" name="number" size="7" value="0" style="text-align:right">
          &nbsp;&nbsp;<a href="javascript:DoChangeSequence(false);" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('GoSearch','','../themes/{$uiTheme}/admin/btn_go_down.gif',1)"><img name="GoSearch" border="0" align="absbottom" width="25" height="22" src="../themes/{$uiTheme}/admin/btn_go_up.gif"></a>
        </td>
  
        <td width="5"></td>
      </tr>
    </table>
  </form>
  </div>


<!-- Begin Group Menu div -->
  {if $getAssigned}
    <div id="GroupMenu" style="visibility:hidden; position:absolute; left:0px; top:0px; z-index:10">
    <form method="get" id="group_form" onSubmit="return DoChangeGroup(true)">

      <table class="table_popup" border="0" cellpadding="2" cellspacing="0">
        <tr height="32" valign="middle">
          <td  width="25" align="center" valign="middle">
            <table border="0" cellpadding="0" cellspacing="0" width="15">
              <tr height="12">
                <td valign="top">
            	    <a href="javascript:ScrollSequence(true, false, NaN, {$nMaxGroup})">
                  <img border="0" src="../themes/{$uiTheme}/admin/arrow_up.gif"></a>
                </td>
              </tr>
              <tr height="12">
                <td valign="bottom">
                  <a href="javascript:ScrollSequence(false, false, NaN, {$nMaxGroup})">
                  <img border="0" src="../themes/{$uiTheme}/admin/arrow_down.gif"></a>
                </td>
              </tr>
            </table>
          </td>
          <td valign="middle" align="center">
            <input type="text" name="number" size="7" value="0" style="text-align:right">
            <input type="hidden" name="curgroup" size="7" value="0" style="text-align:right">
              &nbsp;&nbsp;<a href="javascript:DoChangeGroup(false);" onMouseOut="MM_swapImgRestore()"
                         onMouseOver="MM_swapImage('GoSearch','','../themes/{$uiTheme}/admin/btn_go_down.gif',1)">
              <img name="GoSearch" border="0" align="absbottom" width="25" height="22" src="../themes/{$uiTheme}/admin/btn_go_up.gif"></a>
          </td>
          <td width="5">&nbsp;</td>
        </tr>
      </table>

    </form>
    </div>
  {/if}
<!-- End Group Menu Div -->


<!-- Return URL -->
  {if $returnURL}
    <a href="{$returnURL}" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Back','','../themes/{$uiTheme}/admin/btn_back_down.gif',1)">
    <img name="Back" border="0" align="absbottom" width="23" height="21" src="../themes/{$uiTheme}/admin/btn_back_up.gif">&nbsp;&nbsp;
    <b>Return to {$get_return_name}</b></a><br><hr>
  {/if}
<!-- end Return URL -->



<!-- display search info -->
  <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
      <td height="50">
      <form method="get" id="search_form" onSubmit="return DoResetSearch('search_form', '', false)">
      Search <select size="1" class="formvalue" name="search_by">
      {html_options options=$search_by_Opts selected=$search_by}
      </select>
      <!--select style="visibility:hidden; position:absolute; left:0px; top:0px; z-index:0" size="1" name="search_key">
      <option %s>t.Name</option>
      <option %s>t.Track_ID</option>
      <option %s>u.Full_Name</option>
      </select-->
      &nbsp;for&nbsp;<input type="text" class="formvalue" name="search_input" size="25" value="{$get_search}">&nbsp;
      <a href="javascript:DoResetSearch('search_form', '{$href}', false);" onMouseOut="MM_swapImgRestore()"
         onMouseOver="MM_swapImage('GoSearch','','../themes/{$uiTheme}/admin/btn_go_down.gif',1)">
         <img name="GoSearch" border="0" align="absbottom" width="25" height="22" src="../themes/{$uiTheme}/admin/btn_go_up.gif">
      </a>
      <a href="javascript:DoResetSearch('search_form', '{$href}', true);" onMouseOut="MM_swapImgRestore()"
         onMouseOver="MM_swapImage('ResetSearch','','../themes/{$uiTheme}/admin/btn_reset_down.gif',1)">
         <img name="ResetSearch" border="0" align="absbottom" width="49" height="22" src="../themes/{$uiTheme}/admin/btn_reset_up.gif">
      </a>
      </form>
      </td>
      <td align="right" height="50">
        <form method="get" id="row_form" onSubmit="return ChangeMaxRows('row_form', '{$href1}', true)">
          Show&nbsp;<input type="text" class="formvalue" name="row_input" size="3" value="{$get_max_rows}">
          &nbsp;items per page&nbsp;
          <a href="javascript:ChangeMaxRows('row_form', '{$href1}', false);" onMouseOut="MM_swapImgRestore()"
            onMouseOver="MM_swapImage('GoMaxRows','','../themes/{$uiTheme}/admin/btn_go_down.gif',1)">
            <img name="GoMaxRows" border="0" align="absbottom" width="25" height="22" src="../themes/{$uiTheme}/admin/btn_go_up.gif">
          </a>
        </form>
      </td>
    </tr>
  </table>

  <hr>


<!-- Display input for list -->
  <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
      <td height="30" valign="middle">
        Display
        <select size="1" class="formvalue" id="category_list" onchange="SelectCategory('{$newPage}')">
          <option value=0>All Categories</option>
          {html_options options=$arrayCategories selected=$get_category}
        </select>
      </td>
      <td align="right" height="30" valign="middle">
        {if $releaseURL}
          <input type="button" value="Release Questions" OnClick="BranchTo('{$releaseURL}')">
        {/if}
      </td>
    </tr>
  </table>

  <hr>


<!-- DisplayLinks() -->
  <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
      <td>
        <a id="ColumnLink" href="javascript:ShowPopupMenu('ColumnMenu', 'ColumnLink', true);">Select columns</a>
          {section name=linkRefsIndex loop=$linkRefs}
           | <a href="{$linkRefs[linkRefsIndex]}">{$linkCaps[linkRefsIndex]}</a>
          {/section}
      </td>
      <td width="50%" align="right">
        {if $startRow > 1}
          <a href="{$prev_ref}">Previous</a>
        {/if}
        &nbsp;&nbsp;&nbsp;
        {if $numPage < $numPages}
          <a href="{$next_ref}">Next</a>
        {/if}
      </td>
    </tr>
  </table>


<!-- Div for column display selection menu -->
  <div id="ColumnMenu" style="visibility:hidden; position:absolute; left:0px; top:0px; z-index:10">
    <table border="0" class="table_popup" cellpadding="2" cellspacing="0">
    {section name=colMenuLinkIndex loop=$colImageList}
      <tr>
        <td>
          {$colImageList[colMenuLinkIndex]}
        </td>
        <td>
          <a class="popup" href="{$colLinkList[colMenuLinkIndex]}">{$colLabelList[colMenuLinkIndex]}</a>
        </td>
      </tr>
    {/section}
    </table>
  </div>



<!-- Display record and page information -->
<!-- Pagination information -->
  <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
      <td width="50%">&nbsp;</td>
      <td width="50%">&nbsp;</td>
    </tr>
    <tr>
      <td width="50%">
        {if $numRows}
          Viewing {$startRow} - {$lastRow} of {$numRows}
        {else}
          <font color="#FF0000">No matching records found</font>
        {/if}
      </td>
      <td width="50%" align="right">
        {if $numPages}
          Page {$numPage} of {$numPages}
        {else}
          &nbsp;
        {/if}
      </td>
    </tr>
    <tr>
      <td width="50%">&nbsp;</td>
      <td width="50%">&nbsp;</td>
    </tr>
  </table>


<!-- Display the column heders -->
  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="table_list">
    <tr>
      <td class="table_header" width="20">&nbsp;</td>
      {section name=colHdrIndex loop=$colHdrCaption}
        <td class="table_header"{if $colHdrWidth[colHdrIndex] > 0} width="{$colHdrWidth[colHdrIndex]}"{/if}>
          &nbsp;
          <a style="color:#FFFFFF"
            href="{$colHdrLink[colHdrIndex]}{if $get_descend}{else}&get_descend=true{/if}&get_sort={$colHdrSort[colHdrIndex]}">
            {$colHdrCaption[colHdrIndex]}
            {if $get_sort == $colHdrSort[colHdrIndex]}
              <img border="0" src="../themes/{$uiTheme}/admin/{if $get_descend}order_descend.gif{else}order_ascend.gif{/if}">
            {/if}
          </a>
        </td>
      {/section}
      <td class="table_header" width="100" align="center">
        Actions
      </td>
    </tr>

    <!-- This section is for the actual records -->
    <!-- Handle differently colored backgrounds of rows for different groups -->
    {section name=rowIndex loop=$rows}
      {cycle name="rowStyle" values="trDefault,trDefaultAlt" assign="rowStyle"}
      {if $groupID[rowIndex] > 0}
        {if $groupID[rowIndex] % 2 == 0}
          {assign var="rowStyle" value="trGroupEven"}
        {else}
          {assign var="rowStyle" value="trGroupOdd"}
        {/if}
      {/if}
      <tr class="{$rowStyle}">
        <td width="20">
        {if $trackID[rowIndex] != ""}
          {if $getAssigned == $trackID[rowIndex]}
            <img border="0" src="../themes/{$uiTheme}/admin/check_mark.gif">
          {else}
            &nbsp;
          {/if}
        {else}
          &nbsp;
        {/if}
        </td>
        {section name=colIndex loop=$rows[rowIndex]}
          <td{if $colHdrWidth[colIndex] > 0} width="{$colHdrWidth[colIndex]}"{/if}>
            {if $rows[rowIndex][colIndex]}
              &nbsp;&nbsp;{$rows[rowIndex][colIndex]}
            {else}
              &nbsp;&nbsp;{$colDefault[colIndex]}
            {/if}
          </td>
        {/section}
        <!-- td class="{cycle name="colAction" values="tdDefault,tdDefaultAlt"}" width="100" align="center"-->
        <td width="100" align="center">
          {if ($editRefs[rowIndex] != '') && ($delRefs[rowIndex] != '')}
            <a href="{$editRefs[rowIndex]}">{$editCaps[rowIndex]}</a>&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="{$delRefs[rowIndex]}">{$delCaps[rowIndex]}</a>
          {else}
            &nbsp;
          {/if}
        </td>
      </tr>
    {/section}
  </table>


  <table border="0" cellpadding="10" cellspacing="0" width="100%" >
    <tr><td width="50%"></td><td width="50%" align="right"></td></tr>
    <tr>
      <td width="50%">&nbsp;</td>
      <td width="50%" align="right">
        {if $startRow > 1}
          <a href="{$prev_ref}">Previous</a>
        {/if}
        &nbsp;&nbsp;&nbsp;
        {if $numPage < $numPages}
          <a href="{$next_ref}">Next</a>
        {/if}
      </td>
    </tr>
  </table>


<!--td width=\"105\" align=\"center\"><a href=\"-->
