{include file='admin/admin_header.tpl'}

<BODY bgcolor="#FFFCED">
<div id="UpdateDiv" style="position:absolute; left:500px; top:160px; width:67px; height:25px; z-index:10"><a href="javascript:Find('');" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Update','','../themes/{$uiTheme}/admin/btn_search_down.gif',1)"><img name="Update" border="0" src="../themes/{$uiTheme}/admin/btn_search_up.gif" width="60" height="22"></a>
</div>
<div id="Title" style="position:absolute; left:11px; top:11px; width:100%; height:18px; z-index:2">
 <b>Analysis Results:</b>
<p>
</div>
<div id="Layer2" style="position:absolute; left:11px; top:150px; width:634px; height:128px; z-index:3">
  <b>Analysis Criteria:</b>
</div>
<form name="AdminForm" method="post">
  <div id="Layer2" style="position:absolute; left:60px; top:170px; width:634px; height:128px; z-index:4">
    <table border="0" width="100%">
      <tr>
        <td class="OrgInfoText" width="21%">Question Number:</td>
        <td width="79%">
          <select class="FormValue" name="F_Question_Number" id="F_Question_Number">
          <option></option>
          {html_options options=$F_Question_Number_Opts selected=$F_Question_Number}
          </select>
      </td>
	</tr>
	<tr>
        <td class="OrgInfoText" width="21%">Version Date:</td>
        <td class="OrgInfoText" width="79%">from
          <select class="FormValue" name="F_Version_Date1" id="F_Version_Date1">
            <option></option>
            {html_options options=$F_Version_Date1_Opts selected=$F_Version_Date1}
          </select>
          to
          <select class="FormValue" name="F_Version_Date2" id="F_Version_Date2">
            <option></option>
            {html_options options=$F_Version_Date2_Opts selected=$F_Version_Date2}
	  </select>
        </td>
    </tr>
	<tr>
        <td class="OrgInfoText" width="21%">Completion Date:</td>
        <td class="OrgInfoText" width="79%"> from
          <select class="FormValue" name="F_Completion_Date1" id="F_Completion_Date1">
            <option></option>
            {html_options options=$F_Completion_Date1_Opts selected=$F_Completion_Date1}
          </select>
          to
          <select class="FormValue" name="F_Completion_Date2" id="F_Completion_Date2">
            <option></option>
            {html_options options=$F_Completion_Date2_Opts selected=$F_Completion_Date2}
          </select>
        </td>
    </tr>
    <tr>
        <td class="OrgInfoText" height="20" width="21%">Domain: &nbsp;&nbsp;</td>
        <td height="20" width="79%">
          <select class="FormValue" name="F_Domain_ID" id="F_Domain_ID">
          <option></option>
          {html_options options=$F_Domain_ID_Opts selected=$F_Domain_ID}
          </select>
      </td>
	</tr>
	<tr>
        <td class="OrgInfoText" width="21%">Class:</td>
        <td width="79%">
          <select class="FormValue" name="F_Track_ID" id="F_Track_ID">
          <option></option>
          {html_options options=$F_Track_ID_Opts selected=$F_Track_ID}
          </select>
      </td>
    </tr>
	<tr>
        <td class="OrgInfoText" width="21%">Department:<? echo $Custom3_Label; ?></td>
        <td width="79%">
          <select class="FormValue" name="F_Department_ID" id="F_Department_ID">
          <option></option>
          {html_options options=$F_Department_ID_Opts selected=$F_Department_ID}
          </select>
      </td>
    </tr>
	<tr>
        <td class="OrgInfoText" width="21%">Result:</td>

        <td width="79%">
          <select class="FormValue" name="F_Result" id="F_Result">
	          <option></option>
	          {html_options options=$F_Result_Opts selected=$F_Result}
	          </select>
	      </td>
	    </tr>
		<tr>

        <td class="OrgInfoText" width="21%">Foil Selected:</td>

        <td width="79%">
          <select class="FormValue" name="F_Foil_Selected" id="F_Foil_Selected">
	          <option></option>
	          {html_options options=$F_Foil_Selected_Opts selected=$F_Foil_Selected}
	          </select>
      </td>
	<tr>
        <td class="OrgInfoText" width="21%">Exit Information:<? echo $Custom1_Label; ?></td>
        <td width="79%">
          <select class="FormValue" name="F_Exit_Info" id="F_Exit_Info">
          <option></option>
          {html_options options=$F_Exit_Info_Opts selected=$F_Exit_Info}
          </select>
      </td>
    </tr>
    <tr><td><input type="hidden" name="Insert" value="TRUE"></td></tr>
	<tr><td><input type="hidden" name="get_return_name" value="<? echo $get_return_name ?>"></td></tr>
    <tr><td><input type="hidden" name="get_return_URL" value="{$get_return_URL}"></td></tr>
  </table>
</div>
<div class="OrgInfoText" id="Results" style="position:absolute; left:60px; top:37px; width:548px; height:102px; z-index:6">
 <table border="0" cellpadding="0" cellspacing="0" width="100%" class="table_list">
 <tr>
 <td class="table_header" width="20">&nbsp;</td>
 <td class="table_header" >&nbsp;&nbsp;Rows</td>
 <td class="table_header" >&nbsp;&nbsp;Correct</td>
 <td class="table_header" >&nbsp;&nbsp;Incorrect</td>
 <td class="table_header" >&nbsp;&nbsp;Understood</td>
 <td class="table_header" >&nbsp;&nbsp;HR Contact</td>
 <td class="table_header" >&nbsp;&nbsp;A</td>
 <td class="table_header" >&nbsp;&nbsp;B</td>
 <td class="table_header" >&nbsp;&nbsp;C</td>
 <td class="table_header" >&nbsp;&nbsp;D</td>
 <td class="table_header" >&nbsp;&nbsp;E</td>
 </tr>
 <tr>
 <td width="20">&nbsp;</td>
 <td class='sm_font'>&nbsp;&nbsp;{$Rows_Returned}</td>
 <td class='sm_font'>&nbsp;&nbsp;{$Correct}</td>
 <td class='sm_font'>&nbsp;&nbsp;{$Incorrect}</td>
 <td class='sm_font'>&nbsp;&nbsp;{$Understood}</td>
 <td class='sm_font'>&nbsp;&nbsp;{$HR_Contact}</td>
 <td class='sm_font'>&nbsp;&nbsp;{$Answer_A}</td>
 <td class='sm_font'>&nbsp;&nbsp;{$Answer_B}</td>
 <td class='sm_font'>&nbsp;&nbsp;{$Answer_C}</td>
 <td class='sm_font'>&nbsp;&nbsp;{$Answer_D}</td>
 <td class='sm_font'>&nbsp;&nbsp;{$Answer_E}</td>
 </tr>
 </table>
 <p class='sm_font' align='center'>'X' indicates that the column is part of the search criteria.<br>
   '--' indicates that the column is excluded from the search.
</DIV>
</FORM>
</BODY>
</HTML>
