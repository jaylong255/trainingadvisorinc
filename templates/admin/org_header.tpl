<BODY class="ContentFrameBody"
      onLoad="{if isset($create) && $create}parent.AddOrg();{/if}
      MM_preloadImages('../themes/{$uiTheme}/admin/btn_update_up.gif','../themes/{$uiTheme}/admin/btn_update_down.gif','../themes/{$uiTheme}/admin/btn_back_down.gif');
      {if isset($currentDataEditFrame)}ChangePointer({$currentDataEditFrame});{/if}
      {if $currentSubTab == 0}document.search_form.searchFor.focus();{else}parent.SetTab(1, {$currentSubTab});{/if}">

{if $currentSubTab > 0}
<table border="0" width="680" align="top" cellpadding="2" cellspacing="3">
  <tr>
    {if $superUser}
      <td width="23">
	<a href="org_list.php" onMouseOut="MM_swapImgRestore()"
		 onMouseOver="MM_swapImage('Back','','../themes/{$uiTheme}/admin/btn_back_down.gif',1)"
		 onClick="javascript:parent.SetTab({$currentTab},0); return CheckSave();">
	    <img name="Back" border="0" src="../themes/{$uiTheme}/admin/btn_back_up.gif" width="23" height="21" align="center">
      </td>
      <td align="left">
	<a class="OrgInfoText" href="org_list.php" onClick="javascript:parent.SetTab({$currentTab}, 0);">
		Return to Organization List</a>
      </td>
    {else}
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    {/if}
    <td align="right">
      {if $currentSubTab < 3}
        {if $create}
	  <!--div id="AddDiv" style="position:absolute; left:589px; top:9px; width:67px;
		   height:25px; z-index:1;"-->
	  <a href="javascript:ValidateOrgForm({$currentSubTab});" onMouseOut="MM_swapImgRestore()"
	    onMouseOver="MM_swapImage('AddBtn','','../themes/{$uiTheme}/admin/btn_add_down.gif',1)"
	    onClick="document.getElementById('Cover').style.visibility='visible';">
	  <img name="AddBtn" border="0" src="../themes/{$uiTheme}/admin/btn_add_up.gif" width="56" height="22"></a>
	  <!--/div-->
        {else}
	  <!--div id="UpdateDiv" style="position:absolute; left:589px; top:9px;
		   width:67px; height:25px; z-index:1"-->
	  <a href="javascript:ValidateOrgForm({$currentSubTab});" onMouseOut="MM_swapImgRestore()"
	    onMouseOver="MM_swapImage('Update','','../themes/{$uiTheme}/admin/btn_update_down.gif',1)">
	  <img name="Update" border="0" src="../themes/{$uiTheme}/admin/btn_update_up.gif" width="56" height="22"></a>
	  <!--/div-->
        {/if}
      {/if}
    </td>
  </tr>
  {if isset($errMsg) && $errMsg}
  <tr>
    <td colspan="3" class="error">
      {$errMsg}
    </td>
  </tr>
  {/if}
  <tr>
    <td colspan="3"><hr width="100%"></td>
  </tr>
</table>
{/if}
