<BODY class="ContentFrameBody"
      onLoad="MM_preloadImages('../themes/{$uiTheme}/admin/btn_update_up.gif','../themes/{$uiTheme}/admin/btn_update_down.gif',
				 '../themes/{$uiTheme}/admin/btn_add_up.gif','../themes/{$uiTheme}/admin/btn_add_down.gif',
				 '../themes/{$uiTheme}/admin/btn_back_down.gif');
		      {if isset($q)}
			{if !isset($hideAction) || !$hideAction}ChangeArea(1);{/if}
		      {/if}
">
{if !isset($currentSubTab) || ($currentSubTab != 1 && $currentSubTab != 3)}

 {if !isset($contentOnly) || !$contentOnly}
<table border="0" width="680" align="top" cellpadding="2" cellspacing="3">
  <tr>
    <td width="23">
      <A href="{$retLink->href}" onClick="return CheckSave();"
	 onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Back','','../themes/{$uiTheme}/admin/btn_back_down.gif',1)">
        <img name="Back" border="0" src="../themes/{$uiTheme}/admin/btn_back_up.gif" width="23" height="21"></a>
    </td>
    <td align="left">
      <a class="OrgInfoText" href="{$retLink->href}{if $retLink->params}?{$retLink->params}{/if}">{$retLink->caption}</a>
    </td>
    <td align="right">
    {if !isset($hideAction) || !$hideAction}
      {if $q->questionId == 'To Be Assigned'}
	<a href="javascript:AddRecordQuestionEdit('');" onMouseOut="MM_swapImgRestore()"
	   onMouseOver="MM_swapImage('AddBtn','','../themes/{$uiTheme}/admin/btn_add_down.gif',1)">
	<img name="AddBtn" border="0" src="../themes/{$uiTheme}/admin/btn_add_up.gif" width="56" height="22"></a>
      {else}
	<a href="javascript:UpdateRecordQuestionEdit('');" onMouseOut="MM_swapImgRestore()"
	   onMouseOver="MM_swapImage('Update','','../themes/{$uiTheme}/admin/btn_update_down.gif',1)">
	<img name="Update" border="0" src="../themes/{$uiTheme}/admin/btn_update_up.gif" width="56" height="22"></a>
      {/if}
    {/if}
    </td>
  </tr>
  {if isset($errMsg) && $errMsg}
  <tr>
    <td colspan="3" class="error">
      {$errMsg}
    </td>
  </tr>
  {/if}
</table>
<hr width="680" align="left">
 {/if}
{/if}


