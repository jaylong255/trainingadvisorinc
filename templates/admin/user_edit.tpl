{include file='admin/admin_header.tpl'}
{include file='admin/user_header.tpl'}

<form name="AdminForm" method="POST" action="{$smarty.server.PHP_SELF}">
  {if isset($user->userId)}
  <input type="hidden" name="userId" value="{$user->userId}">
  {/if}
  <!--div id="Layer1" style="position:absolute; left:15px; top:57px; width:302px; height:31px; z-index:3"-->

<table border="0">
<tr>
<td>
  <table border="0">
    <tr>
      <td class="OrgInfoText">User ID: &nbsp;&nbsp;</td>
      <td><!--{$user->userId}-->
	<input type="text" name="userId" id="userId" onClick="alert('This field is automatically generated.');"valu
	       class="FormDisabled" maxlength="45" size="30"
	       value ="{if $user->userId == 0}To Be Assigned{else}{$user->userId}{/if}" disabled>
      </td>
    </tr>
    <tr>
        <td class="OrgInfoText" height="29">First Name: &nbsp;&nbsp;</td>
        <td height="29">
          <input class="FormValue" type="text" name="firstName" id="firstName" maxlength="45" size="30" value ="{$user->firstName}"
	   onchange="javascript:SetFlag();">
        </td>
    </tr>
    <tr>
      <td class="OrgInfoText">Last Name:</td>
      <td>
          <input class="FormValue" type="text" name="lastName" id="lastName" maxlength="45" size="30" value ="{$user->lastName}"
		 onchange="javascript:SetFlag();">
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText"><!--Last 6 digits<br>of SS#:-->Login:</td>
      <td>
	<!--table width="100%" cellpadding="0" cellspacing="0">
          <tr>
            <td>
              <input class="FormValue" type="text" name="ssn" maxlength="6" size="6" value ="user->ssn"
	             onchange="javascript:SetFlag();">
	    </td>
	    <td align="right" class="OrgInfoText">

            </td>
            <td align="right"-->
              <input class="FormValue" type="text" name="login" maxlength="45" size="30" value ="{$user->login}"
	             onchange="javascript:SetFlag();">
	      <!--input type="text" class="FormDisabled" name="login" id="login" maxlength="8" size="8" value="{$user->login}" disabled>
	    </td>
          </tr>
        </table-->
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText" height="29">Password: &nbsp;&nbsp;</td>
      <td height="29">
        <input class="FormValue" type="password" name="password" id="password" size="30" maxlength="45" value ="{$user->password}"
	       onchange="javascript:SetFlag();">
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText">E-Mail:</td>
      <td>
        <input class="FormValue" type="text" name="email" maxlength="80" size="30" value ="{$user->email}" onchange="javascript:SetFlag();">
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText">Phone #:</td>
      <td>
        <input class="FormValue" type="text" name="phone" size="30" maxlength="30" value ="{$user->phone}" onchange="javascript:SetFlag();">
      </td>
    </tr>
  </table>
</td>


<td>
  <table border="0">
    <tr>
      <td class="OrgInfoText">Status: &nbsp;&nbsp;</td>
      <td>
        <select  class="FormValue" name="employmentStatusId" onchange="javascript:SetFlag();">
          <option></option>
	  {html_options options=$employmentStatusValues selected=$user->employmentStatusId}
        </select>
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText">Language:</td>
      <td>
        <select class="FormValue" name="language" onchange="javascript:SetFlag();">
          <option></option>
	  {html_options options=$languages selected=$user->languageId}
        </select>
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText">Contact:</td>
      <td>
        <select name="contactId" id="contactId" size="1" onchange="javascript:SetFlag();"
		{if $user->useSupervisor}class="FormDisabled" disabled{else}class="FormValue"{/if}>
          <option value="0"></option>
	  {html_options options=$contacts selected=$user->contactId}
        </select>
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText">State:</td>
      <td>
        <select class="FormValue" name="domainId" onchange="javascript:SetFlag();">
          <option></option>
	  {html_options options=$domains selected=$user->domainId}
        </select>
      </td>
    </tr>
    <tr>
       <td class="OrgInfoText">Department</td>
       <td>
	 <select class="FormValue" name="departmentId" onchange="javascript:SetFlag();">
	   <option></option>
	   {html_options options=$departments selected=$user->departmentId}
	 </select>
       </td>
     </tr>
     <tr>
       <td class="OrgInfoText">Division</td>
       <td>
	 <select class="FormValue" name="divisionId" onchange="javascript:SetFlag();">
	   <option></option>
	   {html_options options=$divisions selected=$user->divisionId}
	 </select>
      </td>
     </tr>
    <tr>
       <td class="OrgInfoText">Role</td>
       <td>
         {html_options name="role" options=$roles selected=$user->role}
       </td>
    </tr>
    <tr>
      <td colspan="2">
         <input type="checkbox" name="useSupervisor" id="useSupervisor" value="1" onchange="javascript:SetFlag();"
	    onClick="javascript:ToggleContactId('useSupervisor', 'contactId');"
            {if $user->useSupervisor} checked{/if}>
	    <span class="OrgInfoText">Use the Class Supervisor as the Contact</span>
      </td>
    </tr>
  </table>
</td>
</tr>


{if isset($trackMemberIdList) && !empty($trackMemberIdList)}
<tr>
  <td colspan="2" class="OrgInfoText">This employee is currently a member of the following classes:</td>
</tr>
{section name=trackMemberListIdx loop=$trackMemberIdList}
{if $smarty.section.trackMemberListIdx.index%2 == 0}
<tr>
{/if}
  <td class="OrgInfoText">
    <input type="checkbox" name="keepTrackIds[]" value="{$trackMemberIdList[trackMemberListIdx]}" checked>
        {$trackMemberNameList[trackMemberListIdx]}
  </td>
  {if $smarty.section.trackMemberListIdx.last && $smarty.section.trackMemberListIdx.index%2 == 0}
  <td>&nbsp;</td>
  {/if}
{if $smarty.section.trackMemberListIdx.index%2 != 0}
</tr>
{/if}
{/section}
<tr>
  <td colspan="2" class="OrgInfoText" width="600">
  To remove a user from any of the classes above, uncheck the boxes next to each class and then click on the 'Update' button at the upper right.  To add a user to a class click on the Classes tab, click on the Edit link of the class to which you want to add this user, then click on the Participants subtab.  Click on the Show all users link and then browse to or search for the user(s) you would like to add.  Then click the Add to Class link in the last column of the page.
  </td>
</tr>
{/if}
</table>

</form>

</body>
</html>
