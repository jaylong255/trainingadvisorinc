{include file='admin/admin_header.tpl'}
{include file='admin/faq_header.tpl'}

<FORM name="AdminForm" method="POST" action="{$smarty.server.PHP_SELF}">
  <table width="680" border="0" align="top" cellpadding="2" cellspacing="3">
    <tr>
      <td class="OrgInfoText">Class ID:</td>
      <td><input class="FormDisabled" readonly type="text" name="trackId"
                 onClick="alert('This field is automatically generated and can not be changed.');"
		 value ="{$faq->trackId}">
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText">FAQ ID:</td>
      <td><input class="FormDisabled" readonly type="text" name="faqId"
                 onClick="alert('This field is automatically generated and can not be changed.');"
		 value ="{if $faq->faqId == 0}To Be Assigned{else}{$faq->faqId}{/if}">
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText" height="29">Question:</td>
      <td height="29">
        <textarea class="FormValue" rows="5" cols="75" name="question">{$faq->question}</textarea>
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText">Answer:</td>
      <td>
        <textarea class="FormValue" rows="5" cols="75" name="answer">{$faq->answer}</textarea>
      </td>
    </tr>
  </table>
</FORM>
</body>
</html>
