<!--BODY bgcolor="#FFFCED" onLoad="parent.ChangeOrgTabs(4); MM_preloadImages('../themes/{$uiTheme}/admin/btn_update_up.gif','../themes/{$uiTheme}/admin/btn_update_down.gif','../themes/{$uiTheme}/admin/btn_back_down.gif')"-->

{include file='admin/admin_header.tpl'}
{include file='admin/org_header.tpl'}


<div id="DivOrgForm" class="orgForm">


<!-- ======================================================================================================= -->
<!-- This is the form for performing a file upload specifically for the purpose of importing a list of users -->
<!-- ======================================================================================================= -->
<form name="ImportUsersForm" enctype="multipart/form-data" method="POST" action="{$smarty.server.PHP_SELF}">
  <table>
    <tr>
      <td><p class="OrgInfoText">Import Users:</p>
	    Click the browse button below to specify a file containing user data that will be imported into this system (CSV format).<BR>
	    You may also refer to the following user import guide (MS Word format) for instructions: <a href="/desktop/admin/user_import_guide.doc">User Import Guide</a><BR>
            For a sample of the file format, download the sample we have prepared here: <a href="/desktop/admin/user_import_sample.php">Sample User Import File</a>
      </td>
    </tr>
    <tr>
      <td>
	<!-- MAX_FILE_SIZE specification, both browser and php should honor it but it can not be depended upon -->
	<input type="hidden" name="MAX_FILE_SIZE" value="8096000">
	<input type="checkbox" name="sync" value="1">Synchronization mode: Check this to synchronize your users with the list being imported.
	<input type="file" name="userFile" size="80" maxlen="128">
	<input type="submit" name="userSubmit" value="Upload User File"><BR>
	    <!--a href="javascript:ImportUsers();" onMouseOut="MM_swapImgRestore()"
		  onMouseOver="MM_swapImage('GoSearch','','../themes/{$uiTheme}/admin/btn_go_down.gif',1)">
		<img name="GoSearch" border="0" align="absbottom" width="25"
		     height="22" src="../themes/{$uiTheme}/admin/btn_go_up.gif"></a-->
      </td>
    </tr>
    {if $userStatusMsg}
    <tr>
      <td class="trSelected">{$userStatusMsg}</td>
    </tr>
    {/if}
  </table>
</form>
<p><hr align="left" width="665"></p>


<!-- =========================================================================================================== -->
<!-- This is the form for performing a file upload specifically for the purpose of importing a list of questions -->
<!-- =========================================================================================================== -->
<form name="ImportQuestionsForm" enctype="multipart/form-data" method="POST" action="{$smarty.server.PHP_SELF}">
  <table>
    <tr>
      <td><p class="OrgInfoText">Import/Export Questions:</p>
	    Click the browse button below to specify a file containing questions that will be imported into this system (CSV format).<BR>
	    You may also refer to the following question import guide (MS Word format) for instructions: <a href="/desktop/admin/question_import_guide.doc">Question Import Guide</a><BR>
	    For a sample of the file format, download the sample we have prepared here: <a href="/desktop/admin/question_import_sample.php">Sample Question Import File</a>
	    <!-- MAX_FILE_SIZE specification, both browser and php should honor it but it can not be depended upon -->
	    <input type="hidden" name="MAX_FILE_SIZE" value="8192000">
      </td>
    </tr>
    {if $superUser}
    <tr>
      <td>
	<input type="checkbox" name="importToAllOrgs" value="1"> Check this box to import these questions to all organizations
      </td>
    </tr>
    {/if}
    <tr>
      <td>
	    <input type="file" name="questionFile" size="80" maxlen="128">
	    <input type="submit" name="questionSubmit" value="Upload Question File"><BR>
		<!--a href="javascript:ImportUsers();" onMouseOut="MM_swapImgRestore()"
		   onMouseOver="MM_swapImage('GoSearch','','../themes/{$uiTheme}/admin/btn_go_down.gif',1)">
			<img name="GoSearch" border="0" align="absbottom" width="25" height="22" src="../themes/{$uiTheme}/admin/btn_go_up.gif"></a-->
      </td>
    </tr>
    {if $questionStatusMsg}
    <tr>
      <td class="trSelected"><p><b>{$questionStatusMsg}</b></p></td>
    </tr>
    {/if}
    {if $superUser}
    <tr>
      <td>
        <hr width="285" align="left">
	<input type="submit" name="exportQuestions" value="Export Question Content (CSV Format)">
      </td>
    </tr>
    {/if}
  </table>
</form>
<p><hr align="left" width="665"></p>

<form name="DeleteUsersForm" method="POST" action="{$smarty.server.PHP_SELF}">
  <table>
    <tr>
      <td><p class="OrgInfoText">Delete Users:</p>
	Delete all users that have a "terminated" status on or before the following date:
      </td>
    </tr>
    <tr>
      <td class="PageText">
        <input class="FormValue" type="text" name="termStatusDate" id=termStatusDate" value ="{$termStatusDate}">
            <a href="javascript:doNothing()" onClick="setDateField(document.getElementById('termStatusDate'));top.newWin=window.open('calendar.html','cal','dependent=yes,width=210,height=230,screenX=200,screenY=300,titlebar=yes')">
            <img id="cal" src="../themes/{$uiTheme}/admin/calendar.gif" border=0 width="16" height="16"></a>
        <input type="submit" name="deleteSubmit" value="Delete Users">
        	<!--a href="javascript:DoSearch('F_StartDate', 'organization_edit4.php?FormAction=Delete');" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('GoSearch','','../themes/{$uiTheme}/admin/btn_go_down.gif',1)"><img name="GoSearch" border="0" align="absbottom" width="25" height="22" src="../themes/{$uiTheme}/admin/btn_go_up.gif"></a-->
      </td>
    </tr>
    {if $deleteStatusMsg}
    <tr>
      <td class="tdDefaultAlt">{$deleteStatusMsg}</td>
    </tr>
    {/if}
  </table>
</form>
<p><hr align="left" width="665"></p>


{if $superUser}
<form name="RunSystemAgentForm" method="POST" action="{$smarty.server.PHP_SELF}">
  <table>
    <tr>
      <td><p class="OrgInfoText">Manually Run System Agent:</p>
         <font color="red">Warning!</font> The system agent runs automatically on a daily basis via a cron job on the server.  Re-running the system agent will replace today's "System Agent Report" and will not update classes that it has already processed today.
      </td>
    </tr>
    <tr>
      <td>
	<input type="submit" name="agentSubmit" value="Run Agent">
	<!--a href="javascript:UpdateSystem();" onMouseOut="MM_swapImgRestore()"
	   onMouseOver="MM_swapImage('GoSearch','','../themes/{$uiTheme}/admin/btn_go_down.gif',1)">
	<img name="GoSearch" border="0" align="absbottom" width="25" height="22" src="../themes/{$uiTheme}/admin/btn_go_up.gif"></a-->
      </td>
    </tr>
    {if $agentStatusMsg}
    <tr>
      <td class="tdDefaultAlt">{$agentStatusMsg}</td>
    </tr>
    {/if}
  </table>
</form>
<p><hr align="left" width="665"></p>
{/if}


{if $superUser || $dbName == 'ORG2'}
<form name="UpdateQuestion" method="POST" action="{$smarty.server.PHP_SELF}">
  <table>
    <tr>
      <td><p class="OrgInfoText">Update "Today's Question":</p>
   	This will set the Delivery Date for the Questions assigned in the Demo Database to today's date. "Today's Question" on the Portal will then be populated.
      </td>
    </tr>
     <tr>
      <td>
	<!--a href="javascript:UpdateDemoQuestions();" onMouseOut="MM_swapImgRestore()"
	   onMouseOver="MM_swapImage('GoSearch','','../themes/{$uiTheme}/admin/btn_go_down.gif',1)">
	<img name="GoSearch" border="0" align="absbottom" width="25" height="22" src="../themes/{$uiTheme}/admin/btn_go_up.gif"></a-->
	<input type="submit" name="updateSubmit" value="Update Question">
      </td>
    </tr>
    {if $updateStatusMsg}
    <tr>
      <td class="tdDefaultAlt">{$updateStatusMsg}</td>
    </tr>
    {/if}
  </table>
</form>
<p><hr align="left" width="665"></p>
{/if}

<p>&nbsp;</p>
</body>
</html>
