{include file='admin/admin_header.tpl'}
{include file='admin/org_header.tpl'}
{include file='admin/report_header.tpl'}


<!--SCRIPT language=JavaScript src="../javascript/ajax.js" type="text/javascript"></SCRIPT-->
{if !isset($reportsErrMsg)}
<span class="OrgInfoText"><b>Report Parameters:</b></span>
<form name="AdminForm" method="post" action="{$smarty.server.PHP_SELF}">
<table border="0">
  <tr>
    <td class="OrgInfoText" height="20" colspan="2">
      Select Classe(s):
    </td>
  </tr>
  {section name=trackIdx loop=$trackIds}
  {if $smarty.section.trackIdx.index%2 == 0}
  <tr>
  {/if}
    <td class="OrgInfoText" height="20"{if $smarty.section.trackIdx.index%2 == 0 && $smarty.section.trackIdx.last} colspan="2"{/if}>
      <input type="checkbox" name="trackIdsSelected[]" value="{$trackIds[trackIdx]}" alt="{$trackNames[trackIdx]}"
	     id="trackCheck{$smarty.section.trackIdx.index}"
	     onChange="javascript:loadDistinctValues(document.AdminForm.trackIdsSelected[]);"
	     {if isset($trackIdsSelected) && in_array($trackIds[trackIdx], $trackIdsSelected)} checked{/if}>{$trackNames[trackIdx]}
    </td>
  {if $smarty.section.trackIdx.index%2 != 0}
  </tr>
  {/if}
  {/section}
  {if $smarty.section.trackIdx.total}
  <tr>
    <td class="OrgInfoText" height="20">
      <a href="javascript:reportAccuracyToggleTracks('trackCheck', {$smarty.section.trackIdx.total});"
	 alt="Select all">Toggle All Classes</a>
    </td>
  </tr>
  {/if}
  <tr><td>&nbsp;</td></tr>
  <tr>
    <td class="OrgInfoText" height="20">
      <input type="checkbox" id="showOnlyIncomplete" name="showOnlyIncomplete"{if isset($showOnlyIncomplete) && $showOnlyIncomplete} checked{/if}>
	Only show participats that have not yet met the AB1825 requirements
    </td>
  </tr>
  <tr>
    <td class="OrgInfoText" height="20" colspan="2"><BR>
      Select the output format for the report:<BR>
      <input type="radio" name="outputFormat" value="html"{if !isset($outputFormat) || $outputFormat == 'html'} checked{/if}>
      	     HTML (display as web page)<BR>
      <input type="radio" name="outputFormat" value="pdf"{if isset($outputFormat) && $outputFormat == 'pdf'} checked{/if}>
      	     PDF (used for hard copy, email attachments, and presentation)<BR>
      <input type="radio" name="outputFormat" value="csv"{if isset($outputFormat) && $outputFormat == 'csv'} checked{/if}>
      	     CSV (used for spreadsheets and charts)
    </td>
  </tr>
</table>
{/if}
<BR>
{include file='admin/report_footer.tpl'}
</form>
</body>
</html>
