<TABLE border="0" cellpadding="0" cellspacing="0" width="100%" height="97">
  <tr>
    <!--td background="{if isset($headerMiddle) && $headerMiddle}{$headerMiddle}{else}/desktop/themes/{$uiTheme}/presentation/header_middle.gif{/if}"
        style="background-repeat:repeat-x;"
        id="top_left_bg"-->
    <td {if isset($topLeftBack) && $topLeftBack}background="{$topLeftBack}"{/if}
        {if isset($topLeftBackRepeat) && $topLeftBackRepeat}style="background-repeat:repeat-x;"{else}style="background-repeat:no-repeat;"{/if}
        id="top_left_bg">
      <img src="{if isset($topLeftFront) && $topLeftFront}{$topLeftFront}{else}/desktop/themes/{$uiTheme}/presentation/transparent.gif{/if}"
	   border="0" alt="" id="top_left_img" height="97">
    </td>
    <td {if isset($topMiddleBack) && $topMiddleBack}background="{$topMiddleBack}"{/if}
	{if isset($topMiddleBackRepeat) && $topMiddleBackRepeat}style="background-repeat:repeat-x;"{else}style="background-repeat:no-repeat;"{/if}
        align="right" valign="top" id="top_middle_bg">
      <img src="{if isset($topMiddleFront) && $topMiddleFront}{$topMiddleFront}{else}/desktop/themes/{$uiTheme}/presentation/transparent.gif{/if}"
	border="0" alt="" id="top_middle_img" height="97">
    </td>
    <td {if isset($topRightBack) && $topRightBack}background="{$topRightBack}"{/if}
	{if isset($topRightBackRepeat) && $topRightBackRepeat}style="background-repeat:repeat-x;"{else}style="background-repeat:no-repeat;"{/if}
	align="right" valign="top" id="top_right_bg" width="340" height="97">
      <img src="{if isset($topRightFront) && $topRightFront}{$topRightFront}{else}/desktop/themes/{$uiTheme}/presentation/transparent.gif{/if}"
	border="0" alt="" id="top_right_img">
    </td>
  </tr>
</table>
