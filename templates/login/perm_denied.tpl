<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
  <TITLE>Training Advisor Portal Login</TITLE>
  <META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <LINK rel="stylesheet" href="../themes/{$uiTheme}/desktop.css" type="text/css">
  <SCRIPT language="javascript" type="text/javascript" src="../javascript/desktop.js"></script>
</HEAD>
<BODY class="theme" onLoad="MM_preloadImages('../themes/{$uiTheme}/login/btn_logon_dn.gif');
      {if isset($wrongTrack) && $wrongTrack}parent.SetTab(3,0);{/if}">
<center>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
{if isset($wrongTrack) && $wrongTrack}
  <span class="WaitText"><p>You are not the supervisor for this track.  Please <a href="../admin/track_list.php" class="Links">Return to the track list</a> and select a track for which you are the supervisor.</span>
{else}
  <span class="WaitText"><p>You have attempted to access a page that you do not have permission to access.  This access attempt has been recorded and security staff notified.</p>

<p>To continue using this application, please <a class="Links" target="_parent" href="../login/login.php">click here to log in again.</a></p><p>Thank You!</p></span>
{/if}
</center>
</body>
