﻿/*
  --- menu items --- 
  note that this structure has changed its format since previous version.
  additional third parameter is added for item scope settings.
  Now this structure is compatible with Tigra Menu GOLD.
  Format description can be found in product documentation.
*/
var MENU_ITEMS = [
	['&nbsp;', 'http://www.goalexandria.com/support/support_nav.html', null,
		
		['Contact Technical Support', 'http://www.goalexandria.com/support/contact_tech/contact_tech.html',
		],
		['Updates/Downloads', 'http://www.goalexandria.com/support/updates/updates_nav.html',
		],
		['Documentation', 'http://www.goalexandria.com/support/info_library/info_library.html',
		],
		['Training', 'http://www.goalexandria.com/support/training/trainingnav.html',
		],
		['Listserv Signup', 'http://www.goalexandria.com/support/community/community.html',
		],
		['Sales Team', 'http://www.goalexandria.com/sales_demo/sales_demo.php',
		],
		['Suggestions/Ideas', 'http://www.goalexandria.com/support/ideas/ideas.html',
		],
		
	],
	
	
	
];

