﻿/*
  --- menu items --- 
  note that this structure has changed its format since previous version.
  additional third parameter is added for item scope settings.
  Now this structure is compatible with Tigra Menu GOLD.
  Format description can be found in product documentation.
*/
var MENU_ITEMS = [
	['Library Solutions', 'http://www.goalexandria.com/solutions/index.html', null,
		['Alexandria', 'http://www.goalexandria.com/solutions/index.html', null,
		],
		['for K-12', 'http://www.goalexandria.com/solutions/k_12.html', null,
		],
		['for Universities/Corp./Public', 'http://www.goalexandria.com/solutions/uni_corp.html', null,
		],
		['for Churches', 'http://www.goalexandria.com/solutions/churches.html', null,
		],
		['Hardware Recommendations', 'http://www.goalexandria.com/solutions/hardware_req.html', null,
		],
		['Alexandria Options', 'http://www.goalexandria.com/solutions/options.html', null,
			['SmartMARC', 'http://www.smartmarc.com'],
			['Advanced Bookings', 'http://www.goalexandria.com/solutions/options/adv_bookings.html'],
			['Sneak Peek', 'http://www.goalexandria.com/solutions/options/sneakpeek.html'],
			['Lexile Measures', 'http://www.goalexandria.com/solutions/options/lexile.html'],
			['netTrekker', 'http://www.goalexandria.com/solutions/options/nettrekker.html'],
			['NetLink', 'http://www.goalexandria.com/solutions/options/netlink.html'],
			['SearchALL', 'http://www.goalexandria.com/solutions/options/searchall.html'],
			['Alexandria Explore', 'http://www.goalexandria.com/solutions/options/explore.html'],
			['Multi-Data Station Tech.', 'http://www.goalexandria.com/solutions/options/MDS.html'],
			['Web Management', 'http://www.goalexandria.com/solutions/options/web_management.html']	
		],
		['Accessories/Hardware', 'http://www.goalexandria.com/solutions/accessories/index.html', null,
		],
		['Other Products', 'http://www.goalexandria.com/solutions/osp.html', null,
			['Textbook Tracker', 'http://www.textbooktracker.com'],
			['eLunchroom', 'http://www.elunchroom.net'],
			['Big6 TurboTools', 'http://www.big6turbotools.com'],
			['SmartMARC', 'http://www.smartmarc.com']
		],
		
		
	],
	['Tour', 'http://www.goalexandria.com/tour/index.html', null,
	],
	['Contact', 'http://www.goalexandria.com/contact/index.html', null,
		['Contact Technical Support', 'http://www.goalexandria.com/contact/support.html',
		],
		['Sales Team', 'http://www.goalexandria.com/contact/sales.html',
		],
	],
	
	['Support', 'http://www.goalexandria.com/support/index.html', null,
		
		['Contact Technical Support', 'http://www.goalexandria.com/support/contact_support/index.html',
		],
		['Updates/Downloads', 'http://www.goalexandria.com/updates/index.html',
		],
		['Documents/Tech Notes', 'http://www.goalexandria.com/support/tech_notes/index.html',
		],
		['FAQ', 'http://www.goalexandria.com/support/FAQ/index.html',
		],
		['Training', 'http://www.goalexandria.com/support/training/index.html',
		],
		['Librarians Corner', 'http://www.goalexandria.com/support/lib_corner/index.html',
		],
		['Suggestions & Ideas', 'http://www.goalexandria.com/support/suggestions/index.html',
		],
	],
	
	['Store', 'http://www.goalexandria.com/store/index.html', null,
	
		['Online PDF Order Forms', 'http://www.goalexandria.com/store/pdf.html',
		],
	],
	
	['About Us', 'http://www.goalexandria.com/about/index.html', null,
		['Company Profile', 'http://www.goalexandria.com/about/profile/index.html',
		],
		['Contact Info', 'http://www.goalexandria.com/about/contact/index.html',
		],
		['Press Releases', 'http://www.goalexandria.com/about/press_releases/index.html',
		],
		['Tradeshows/Events', 'http://www.goalexandria.com/about/tradeshows/index.html',
		],
		['Customer Profiles/Quotes', 'http://www.goalexandria.com/about/profiles/index.html',
		],
		['Privacy Statement', 'http://www.goalexandria.com/about/privacy_statement/index.html',
		],
		['Partners/Associations', 'http://www.goalexandria.com/about/partners/index.html',
		],
	],
	
	
	
];

