/////////////////////////////////////////////////////////////////////////////
//	PopupMenu.js
//		Javascript functions for displaying/aligning and hiding a popup menu.
//		Call the ShowPopupMenu function to display and later auto-hide the menu.
//		Works with objects using tags A, DIV, TABLE, TR and TD.

opera = (navigator.userAgent.indexOf("Opera") != -1) ? true:false;
ns4 = (document.layers)?true:false;
ns6 = (navigator.userAgent.indexOf("Gecko") != -1) ? true:false;
ie = (navigator.userAgent.indexOf("MSIE") != -1) ? true:false;

var		g_bMonitorMouse = false;
var		g_rectPopupObject = Object();
var		g_objPopupMenu = 0;
var		g_rectPopupMenu = Object();

/////////////////////////////////////////////////////////////////////////////
//	PopupMouseMoved
//		Event handler used to monitor all mouse move events for the browser
//		window and automatically hide the popup menu when the mouse moves
//		outside of the menu and associated object.
function PopupMouseMoved(e)
{
	if (!g_bMonitorMouse)
		return;
	// Determine the mouse position within the page
	if (ns4 || ns6) {
		x = e.pageX;
		y = e.pageY;
	}
	else {
		x = event.clientX;
		y = event.clientY;
	}
	if(!opera && document.all) {
		x = x + document.body.scrollLeft;
		y = y + document.body.scrollTop;
	}

	// See if the mouse is outside the objects
	if ((x < g_rectPopupMenu.left || x > g_rectPopupMenu.right ||
		y < g_rectPopupMenu.top || y > g_rectPopupMenu.bottom) && 
		(x < g_rectPopupObject.left || x > g_rectPopupObject.right ||
		y < g_rectPopupObject.top || y > g_rectPopupObject.bottom)) {

		// Hide the menu now
		g_objPopupMenu.style.visibility = "hidden";
		g_objPopupMenu.style.zIndex = -1;
		g_bMonitorMouse = false;
	}
}

// Add our mouse move event handler
if(ns4)
	document.captureEvents(Event.MOUSEMOVE);
document.onmousemove = PopupMouseMoved;


/////////////////////////////////////////////////////////////////////////////
//	GetObjectRect
//		Attempt to determine and return the rectangle for the specified
//		object in coordinates relative to the document.
function GetObjectRect(obj)
{
	var		rect = Object();

	rect.left = obj.offsetLeft;
	rect.top = obj.offsetTop;

	objOffsetParent = obj.offsetParent;
	while (objOffsetParent) {
		if (objOffsetParent) {
			rect.left += objOffsetParent.offsetLeft;
			rect.top += objOffsetParent.offsetTop;
		}
		objOffsetParent = objOffsetParent.offsetParent;		
	}

	rect.right = rect.left + obj.offsetWidth;
	rect.bottom = rect.top + obj.offsetHeight;

	return rect;
}

/////////////////////////////////////////////////////////////////////////////
//	ShowPopupMenu
//		Display the menu object specified by strPopupMenu and position it
//		below the associated object specified by strPopupObj.
//		If bAutoHide is true then when the mouse leaves either the popup object
//		or the popup menu the menu object will be hidden automatically.
function ShowPopupMenu(strPopupMenu, strPopupObject, bAutoHide)
{
  g_bMonitorMouse = false;

  if (!strPopupMenu)
	return;

  // Get local references to objects
  objPopupMenu = document.getElementById(strPopupMenu);
  if (!objPopupMenu)
	return;

  objPopupObject = document.getElementById(strPopupObject);
  if (!objPopupObject)
	return;

  // Reposition and display the menu
  g_rectPopupObject = GetObjectRect(objPopupObject);

  objPopupMenu.style.left = g_rectPopupObject.left;
  objPopupMenu.style.top = g_rectPopupObject.bottom;
  objPopupMenu.style.visibility = "visible";
  objPopupMenu.style.zIndex = 100;

  // Start monitor the mouse for auto-hiding
  if (bAutoHide) {
	g_rectPopupMenu = GetObjectRect(objPopupMenu);
	g_objPopupMenu = objPopupMenu;
	g_bMonitorMouse = true;
  }
}


