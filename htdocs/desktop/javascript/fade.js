//change the opacity for different browsers
// ripped from brainerror.net/scripts/javascript/blendtrans/
function changeOpac(id, opacity) {
    var object = document.getElementById(id).style;
    object.opacity = (opacity / 100);
    object.MozOpacity = (opacity / 100);
    object.KhtmlOpacity = (opacity / 100);
    object.filter = "alpha(opacity=" + opacity + ")";
}


function opacity(id, opacStart, opacEnd, millisec) {
    //speed for each frame
    var speed = Math.round(millisec / 100);
    var timer = 0;

    // if delay is 0 then just set and forget
    if (millisec == 0) {
	changeOpac(id, opacEnd);
	return;
    }

    //determine the direction for the blending, if start and end are the same nothing happens
    if(opacStart > opacEnd) {
        for(i = opacStart; i >= opacEnd; i--) {
            setTimeout("changeOpac('" + id + "', " + i + ")",(timer * speed));
            timer++;
        }
    } else if(opacStart < opacEnd) {
        for(i = opacStart; i <= opacEnd; i++) {
            setTimeout("changeOpac('" + id + "', " + i + ")",(timer * speed));
            timer++;
        }
    }
}


function shiftOpacity(id, maxOpacity, millisec) {
    var elm = document.getElementById(id);

    //if an element is invisible, make it visible, else make it invisible
    if(getStyle(id, 'opacity') == 0 || getStyle(id, 'visibility') == 'hidden' || getStyle(id, 'display') == 'none') {
	changeOpac(id, 0);
	elm.style.display = 'block';
	elm.style.visibility = 'visible';
	if (maxOpacity)
		opacity(id, 0, maxOpacity, millisec);
	else
	        opacity(id, 0, 100, millisec);
    } else {
	if (maxOpacity)
		opacity(id, elm.style.opacity*100, 0, millisec);
	else
	        opacity(id, 100, 0, millisec);
	setTimeout("document.getElementById('" + id + "').style.visibility = 'hidden';", millisec);
	setTimeout("document.getElementById('" + id + "').style.display = 'none';", millisec);
	//elm.style.display = 'none';
	//elm.style.visibility = 'none';
    }
}


function blendImage(divid, imageid, imagefile, millisec) {
    var speed = Math.round(millisec / 100);
    var timer = 0;
    
    //set the current image as background
    document.getElementById(divid).style.backgroundImage = "url(" + document.getElementById(imageid).src + ")";
    
    //make image transparent
    changeOpac(0, imageid);
    
    //make new image
    document.getElementById(imageid).src = imagefile;

    //fade in image
    for(i = 0; i <= 100; i++) {
        setTimeout("changeOpac(" + i + ",'" + imageid + "')",(timer * speed));
        timer++;
    }
}


function currentOpac(id, opacEnd, millisec) {
    //standard opacity is 100
    var currentOpac = 100;
    
    //if the element has an opacity set, get it
    if(document.getElementById(id).style.opacity < 100) {
        currentOpac = document.getElementById(id).style.opacity * 100;
    }

    //call for the function that changes the opacity
    opacity(id, currentOpac, opacEnd, millisec)
} 
