__FIRST_NAME__ __LAST_NAME__, 

You are invited to review the latest training waiting for you at the following URL:
http://www.trainingadvisorinc.com/desktop

In your assignments list there are __NUMBER_UNANSWERED__ questions awaiting your review.

Participation in this training is vital to the success of our organization. Thank you for completing your reviews in a timely manner.

Sincerely,

__SUPERVISOR__
