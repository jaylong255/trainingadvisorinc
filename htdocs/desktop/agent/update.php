<?php
/////////////////////////////////////////////////////////////////////////////
// update.php
//	Page used for processing question scheduling and making user assignments.
//
// Get parameters:
//	[banner]		Display our banner indicating our page is processing
//	[disable_email]		Set to false in order to disable sending of email.
//	[force_update]		Forces updates on tracks already updated today.


// Included files and libs

require_once '../includes/common.php';
require_once '../includes/Config.php';
require_once '../includes/RecurrenceObject.php';
require_once '../includes/DateObject.php';
require_once '../includes/agent/agent.php';

// Global variables used for debugging and testing
$DEBUG      = 0;
$devEmail   = 'general@itpsg.com';
$devEmailArr = array(0 => 'general@itpsg.com', 1 => 'support@sabient.com', 3 => 'john@trainingadvisorinc.com', 4 => 'jared@trainingadvisorinc.com');
$limitOrgId = 0;
$limitTrackId = 0;
$disable_email = false;


// Variables used for database access
$session_Host     = DB_HOST;
$session_User     = "HR_Org_Admin";
$session_Password = "orG8Admin";
$session_DBName   = "HR_Tools_Authentication";


$str = "Starting";

$sopts = "o:t:eh";
// Apparently this version does not support long options
//$lopts = array("org::", "track::", "email");
// $options = getopt($sopts, $lopts);
$options = getopt($sopts);

if (array_key_exists('h', $options)) {
  echo "\nUsage:\n$argv[0] [-h] [-o<orgId> [-t<trackId>]] [-e]\n\n";
  echo "\t-o<orgId> Specifies that the agent will only run on specified org id\n";
  echo "\t-t<trackId> Specifies that the agent will only run on specified track id\n";
  echo "\t-e will prevent the sending of email notifications of any kind\n";
  echo "\t-h will output this usage information\n\n\n";
  exit(0);
}

if (array_key_exists('o', $options)) {
  $limitOrgId = $options['o'];
  $str .= ', org='.$limitOrgId;
}

if (array_key_exists('t', $options)) {
  $limitTrackId = $options['t'];
  $str .= ', track='.$limitTrackId;
}

if (array_key_exists('e', $options)) {
  $disable_email = true;
  if ($disable_email)
    $str .= ', sendmail=false';
}


// Need to be connected to the database for DateObject to work
$db = mysql_connect($session_Host, $session_User, $session_Password);
if (!$db) {
  output("Failed to connect to db(".mysql_errno()."): ".mysql_error());
}


$bLogFile = true;
$dirPath = APPLICATION_ROOT.'/agent/';
$dirOrgPath = APPLICATION_ROOT.'/organizations/';


// Specify if the outbound mail notifications should come from the Supervisors email address
// Note that setting the Reply-To address for any given track will override this 
$useSuperAddress = TRUE;
$defEmailName = "Training Advisor Agent";
$defEmailAddress = "agent@trainingadvisorinc.com";


// Output HTML header
output("<html>\n");
output("<head>\n");
output("<title>Update Agent</title>\n");
output("<style>\n");
output("<!--\n");
$outStr = "a, body, input, option, select, table, td, tr "
."{ font-family:tahoma,sans-serif; font-size:14px }\n";
output($outStr);
output("a { TEXT-DECORATION:none; color:#0800FC }\n");
output("-->\n");
output("</style>\n");

output("</head>\n");

// See if we should display our banner
// and redirect to our page to beging processing
if (isset($banner)) {
  
  echo "<script language=\"JavaScript\" type=\"text/javascript\">\n";
  echo "function PageLoaded() {\n";
  echo "	window.location.href = \"$PHP_SELF\";\n";
  echo "}\n";
  echo "</script>\n";
  
  echo "<body onLoad=\"PageLoaded()\">\n";
  echo "&nbsp;<br>\n";
  echo "<font face=\"Tahoma\" size=\"5\"><b>Training Advisor Inc System Agent</b></font><br>\n";
  
  echo "Please wait while database updates are being processed...<br>\n";
  echo "<p><img border=\"0\" src=\"graphics/progress.gif\"></p><br>\n";
  
  mysql_close($db);
} else {
  // Now that our processing message is displayed continue with updates
  output("<body>\n");
  output("&nbsp;<br>\n");
  output("<font face=\"Tahoma\" size=\"5\"><b>Training Advisor System Agent</b></font><br>\n");
  
  output('Command line options: '.print_r($options, TRUE)."<BR>\n");
  output("$str\n");
  //output("</body>\n");
  //exit(0);

  // Process the needed database updates
  output("Database updates processing, summary information to follow.<br>\n");
  output("<br>\n");
  
  $date = date("l, F j, Y - g:ia");
  output("$date<br>\n");
  
  // Attempt to open the authentication database
  if (!mysql_select_db("HR_Tools_Authentication",$db)) {
    output("An error occurred while attempting to connect to the authentication database(".mysql_errno()."): ".mysql_error()."<br>\n");
    //exit;
  }
  
  // Enumerate through list of organization databases
  $lSqlStr = "SELECT Database_Name FROM Organization";
  if ($limitOrgId != 0)
    $lSqlStr .= " WHERE Organization_ID=$limitOrgId";
  $result = mysql_query($lSqlStr, $db);
  mysql_close($db);
  
  while ($row = mysql_fetch_array($result)) {
    
    $database = $row['Database_Name'];
    if(strcmp('ORG2', $database)!=0)
      {
	UpdateCompany($database);
      }
  }
}

output("Run Completed<BR>\n");
output("</body>");
output("</html>");
?>

<?php

/////////////////////////////////////////////////////////////////////////////
//	output
//		Write the following string to the document and logfile.
function output($string)
{
  global		$banner;
  global		$strLogFile;
  global		$bLogFile;
  global 		$dirPath;

  // Output text to document
  echo $string;
  if ($banner || !$bLogFile)
    return;
  
  // Create log file path if needed
  if (!$strLogFile) {
    // See if the log directory exists, create if needed
    if (!is_dir("$dirPath/logs")) {
      
      if (!is_writable("./")) {
	echo "<br>NOTICE: Unable to create log directory, no log file will be saved. Path($dirPath/logs)<br>\n";
	$bLogFile = false;
	return;
      }
      if (!mkdir("$dirPath/logs", 0777)) {
	$bLogFile = false;
	return;
      }
    }
    // Ensure log directory is writable
    if (!is_writable("$dirPath/logs")) {
      echo "<br>NOTICE: File access to log folder not allowed on server, no log file will be saved. Path($dirPath/logs)<br>\n";
      $bLogFile = false;
      return;
    }
    
    // Create new log file name based on todays date
    $objToday = new DateObject();
    $date = $objToday->strDate;
    
    $strLogFile = "$dirPath/logs/$date.htm";
    // Create a new empty log file
    $fp = fopen($strLogFile, "w");
    if ($fp != FALSE)
      fclose($fp);
  }
  // Attempt to append to the log file
  $fp = fopen($strLogFile, "a");
  if ($fp != FALSE) {
    fwrite($fp, $string);
    fclose($fp);
  }
}

/////////////////////////////////////////////////////////////////////////////
//	UpdateCompany
//		Attempt to open and update the specified company database.
function UpdateCompany($database)
{
  global		$session_Host;
  global		$session_User;
  global		$session_Password;
  global		$Organization_Name;
  global		$Organization_Directory;
  global		$Organization_Delinquency_Notification;
  global		$Organization_HR_Contact_ID;
  global                $Organization_Email_Format;
  global		$arrayDelinquentEmails;
  global                $config;

  $orgId = (int)str_replace(ORG_DB_NAME_PREFIX, '', trim($database));

  // Get and display the company name
  //$sqlStr = "SELECT Name,Directory,Delinquency_Notification,HR_Contact_ID FROM Organization LIMIT 1";
  //$result = mysql_query($sqlStr, $db);
  $config = new Config($orgId);
  //if ($result && mysql_num_rows($result) != 0)
  if (!$config) {
    output("DB($database): Unable create new config object with org ID: $orgId, skipping this org...<BR>\n");
    return;
  }
  output(print_r($config->GetAllItems())."<BR>\n");
  //mysql_result($result, 0, "Name");
  $Organization_Name = $config->GetValue(CONFIG_ORG_NAME);
  //mysql_result($result, 0, "Directory");
  $Organization_Directory = $config->GetValue(CONFIG_ORG_DIRECTORY); 
  //mysql_result($result, 0, "Delinquency_Notification");
  $Organization_Delinquency_Notification = $config->GetValue(CONFIG_DELINQUENCY_NOTIFICATION);
  //mysql_result($result, 0, "HR_Contact_ID");
  $Organization_HR_Contact_ID = $config->GetValue(CONFIG_ORG_CONTACT_ID);
  $Organization_Email_Format = $config->GetValue(CONFIG_EMAIL_FORMAT);

  output("<br>Updated company database '$database' ('$Organization_Name')<br><br>\n");
    
  // Clear our delinquent email array
  $arrayDelinquentEmails = array();
    
  // Connect to the database server
  $db = mysql_connect($session_Host, $session_User, $session_Password);
  output("db = mysql_connect($session_Host, $session_User, $session_Password)\n");
  
  if (!$db) {
    output("Error connecting to database(".mysql_errno()."): ".mysql_error()."\n");
  }

  // Attempt to open the company's database
  if (!mysql_select_db($database, $db)) {
    output("An error occurred while attempting to connect to the organization database $database(".mysql_errno()."): ".mysql_error()."<br>\n");
    output("An error occurred while attempting to open database '$database'.<br>\n");
    mysql_close($db);
    return;
  }

  // Make sure we have a clean assignments table by removing any assignments to
  // users that no longer have a user account.  This will prevent rows with blank
  // names int the delinquency report that organizational admins would not be able
  // to do anything about leaving them frustrated and confused.
  $sqlStr = "DELETE FROM Assignment WHERE User_ID IN ".
    "(select User_ID FROM ".
    "(select DISTINCT(a.User_ID) FROM Assignment AS a LEFT JOIN User as u ON a.User_ID=u.User_ID ".
    "WHERE u.User_ID is null) x)";
  $result = mysql_query($sqlStr, $db);
  if ($result === FALSE) {
    output('Error running query $sqlStr: '.mysql_error());
    return;
  }

  // Update all the tracks listed for the company
  UpdateTracks($db);
    
  // Send out all waiting delinquent notification emails
  SendDelinquentEmails();
  //} else {
  //  output("Query: $sqlStr: failed(".mysql_errno()."): ".mysql_error()."\n");
  //}
  //  mysql_close($db);
}

/////////////////////////////////////////////////////////////////////////////
//	UpdateTracks
//		Attempt to open and update all tracks for the given database.
function UpdateTracks($db)
{
  global		$force_update;
  global                $limitTrackId;
  global                $DEBUG;

  // Enumerate through all tracks
  $joins = "LEFT JOIN User AS u ON t.Supervisor_ID=u.User_ID";
  $sqlStr = "SELECT t.Track_ID,t.Name,t.Is_Binding,t.Is_Looping,t.Recurrence, "
     ."t.Start_Date,t.Last_Question_Date,t.Last_Question_Number,u.First_Name as "
     ."Supervisor_First,u.Last_Name as Supervisor_Last FROM Track AS t $joins";
  if ($limitTrackId != 0)
    $sqlStr .= " WHERE t.Track_ID=$limitTrackId";
  $result = mysql_query($sqlStr, $db);
  
  if ($result) {
    while ($row = mysql_fetch_array($result)) {
      
      $Track_ID = $row["Track_ID"];
      $Name = $row["Name"];
      $Supervisor_First = $row["Supervisor_First"];
      $Supervisor_Last = $row["Supervisor_Last"];
      $Supervisor = "$Supervisor_First $Supervisor_Last";
      $Is_Binding = $row["Is_Binding"];
      $Is_Looping = $row["Is_Looping"];
      $Recurrence = $row["Recurrence"];
      $Start_Date = $row["Start_Date"];
      $Last_Question_Date = $row["Last_Question_Date"];
      $Last_Question_Number = $row["Last_Question_Number"];
      
      output("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n");
      output("<tr><td width=\"25\"></td><td>Track '$Name', ID=$Track_ID:</td></tr></table>\n");
      output("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n");
      output("<tr><td width=\"50\"></td><td>\n");
      
      // Get dates in similar formats
      $objToday = new DateObject();
      $today = $objToday->strDate;
      
      $last = "";
      if ($Last_Question_Date)
	$last = mysql_result(mysql_query("SELECT DATE_FORMAT('$Last_Question_Date', '%Y-%m-%d')"), 0);
      
      // Must have a start date before any assignments will be made
      $start = "";
      if ($Start_Date) {
	$start = mysql_result(mysql_query("SELECT DATE_FORMAT('$Start_Date', '%Y-%m-%d')"), 0);
	
	// See if we have reached the start date
	$objStart = new DateObject($start);
	if ($objStart->nBaseDays <= $objToday->nBaseDays) {
	  
	  // See if we have updated this track already today
	  if (isset($force_update) || !$last || $last != $today || $DEBUG) {
	    
	    // Need to assume last date was the start date, IsRecurrenceToday
	    // expects to always have a last date
	    if (!$last)
	      $last = $start;
	    // See if it's time to assign new questions
	    if ($DEBUG) {
	      output("Recurrence always met since DEBUG enabled.<br>\n");
	      AssignQuestion ($db, $Track_ID, $Name, $Supervisor, $Is_Looping, $Is_Binding, $Last_Question_Number);
	    } else {	      
	      if (IsRecurrenceToday($Recurrence, $start, $last))
		AssignQuestion ($db, $Track_ID, $Name, $Supervisor, $Is_Looping, $Is_Binding, $Last_Question_Number);
	      else
		output("Recurrence criteria NOT met, no question due at this time.<br>\n");
	    }
	  }
	  else
	    output("Already updated today '$last'.\n");
	}
	else
	  output("Start date '$start' not yet reached.\n");
      }
      else
	output("No start date assigned.\n");
      
      output("</td></tr></table><br>\n");
    }
  }
}

/////////////////////////////////////////////////////////////////////////////
//	IsRecurrenceToday
//		Compare the recurrence string, start date and last date to today's date
//		and return true if today falls on a recurrence date.
function IsRecurrenceToday($strTokenString, $strStartDate, $strLastDate)
{
  $objRecur = new RecurrenceObject($strTokenString);
  $objStart = new DateObject($strStartDate);
  $objLast = new DateObject($strLastDate);
  $objToday = new DateObject();
  //  $DEBUG = 1;
  global $DEBUG;

  if ($DEBUG) {
    echo "TokenString: $strTokenString<BR>\nStartDate: $strStartDate<BR>\nLastDate: $strLastDate<BR>\n";
  }

  if ($objRecur->IsDaily()) {
    if ($DEBUG) {
      echo "RECURRENCE: Daily<BR>\n";
    }
    // Check to see if the daily criteria has been met
    if ($objRecur->IsEveryWeekday()) {
      // See if today is a weekday
      if ($objToday->nWeekday > 1 && $objToday->nWeekday < 7)
	return true;
    }
    else {
      // If today is the start date go ahead
      if ($objStart->nBaseDays == $objToday->nBaseDays)
	return true;
      // See if enough days have elapsed
      if (($objToday->nBaseDays - $objLast->nBaseDays) >= $objRecur->GetDaysApart())
	return true;
    }
  }
  // Check to see if the weekly criteria has been met
  else if ($objRecur->IsWeekly()) {
    if ($DEBUG) {
      echo "RECURRENCE: Weekly, OBJTODAYNBASEWEEKS: ".$objToday->nBaseWeeks.", OBJTODAYWKDAY: ".$objToday->nWeekday.", OBJLASTNBASEWEEKS: ".$objLast->nBaseWeeks.", OBJLASTNBASEDAYS: ".$objLast->nBaseDays.", OBJRECURWEEKSAPART: ".$objRecur->GetWeeksApart().", OBJRECURGETWEEKDAYS: ";
      $weekDays = $objRecur->GetWeekdays();
      echo "<BR>\n";
    }
    $bContinue = false;
    // See if this is within the last week assigned
    if ($objToday->nBaseWeeks == $objLast->nBaseWeeks && $objToday->nBaseDays >= $objLast->nBaseDays)
      $bContinue = true;
    // See if enough weeks have passed
    else if (($objToday->nBaseWeeks - $objLast->nBaseWeeks) >= $objRecur->GetWeeksApart())
      $bContinue = true;
    // See if were on a specified day
    if ($bContinue) {
      $weekDays = $objRecur->GetWeekdays();
      if (1 << ($objToday->nWeekday - 1) & $weekDays)
	return true;
    }
  }
  // Check to see if the monthly criteria has been met
  else if ($objRecur->IsMonthly()) {
    
    if ($objRecur->IsMonthlyByDate()) {
      // See if today is the monthly date
      if ($objToday->nDay == $objRecur->GetMonthlyDate())
	return true;
    }
    else if ($objRecur->IsMonthlyByWeekday()) {
      // Determine the first, second, third, fourth and last weeks
      $weeks = array(0, 0, 0, 0, 0);
      
      $year = $objToday->nYear;
      $month = $objToday->nMonth;
      $objTemp = new DateObject("$year-$month-01");
      $weeks[0] = $objTemp->nBaseWeeks;
      
      $objTemp->SetDate("$year-$month-08");
      $weeks[1] = $objTemp->nBaseWeeks;
      
      $objTemp->SetDate("$year-$month-15");
      $weeks[2] = $objTemp->nBaseWeeks;
      
      $objTemp->SetDate("$year-$month-22");
      $weeks[3] = $objTemp->nBaseWeeks;
      
      for ($max=31; $max >= 28 && !checkdate($month, $max, $year); $max--);
      $objTemp->SetDate("$year-$month-$max");
      $weeks[4] = $objTemp->nBaseWeeks;
      
      // See if we are within the right week
      if ($objToday->nBaseWeeks == $weeks[$objRecur->GetWeekFlag()]) {
	$bContinue = false;
	// See if this is the same week as the last week assigned
	if ($objToday->nBaseWeeks == $objLast->nBaseWeeks && $objToday->nBaseDays >= $objLast->nBaseDays)
	  $bContinue = true;
	// See if enough months have passed
	else if (($objToday->nBaseMonths - $objLast->nBaseMonths) >= $objRecur->GetMonthsApart())
	  $bContinue = true;
	// See if were on a specified day
	if ($bContinue) {
	  $weekDays = $objRecur->GetWeekdays();
	  if (1 << ($objToday->nWeekday - 1) & $weekDays)
	    return true;
	}
      }
    }
  }
  return false;
}




function AssignQuestion($db, $Track_ID, $Track_Name, $Supervisor,
			$Is_Looping, $Is_Binding, $Last_Question_Number = 0) {

  // Imported global vars
  global           $DEBUG;

  // Local Variable Initialization
  $nAssignments    = 0;
  $Sequence        = 0;
  $Question_Number = -1;
  $Group           = 0;
  
  $outStr = "Recurrence criteria met, attempting to assign "
     ."next question to users of track.<br>\n";
  output($outStr);

  // If this is a binding question then we prep it now, otherwise
  // we wait until we are inside the loop.  A binding question is one
  // where the user is in sync with all users in the track.  A nonbinding
  // question is one in which the user gets the questions in the order
  // they appear even if they are out of sync with other users.  This
  // would for example be true when a user is assigned to the track
  // after others already in the track.  In the nonbinding case, the
  // new user would receive the question appearing first in the sequence
  // instead of receiving the same next question as another user.
  if ($Is_Binding) {

    // Get the Question_Number and Sequence with pass by reference parameters
    $ret = GetQuestionNumberAndSequence($db, $Track_ID, $Last_Question_Number,
					$Question_Number, $Sequence, $Group, $Is_Looping);

    // See if this is a non looping track.  If there are no more questions,
    // then there is no need to continue.
    if ($Question_Number < 0 && !$Is_Looping) {
      output("Skipping Track: $Track_ID because it is at end of loop and is a non looping track<BR>\n");
      return TRUE;
    }
	     
    if ($Question_Number <= 0 || $Sequence <= 0 || $Group < 0 || $ret == FALSE) {
      output("Unable to obtain Question_Number or Sequence for Binding Question, Question_Number=$Question_Number, Sequence=$Sequence, Group=$Group, ret=$ret<BR>\n");
      return FALSE;
      
    } else { // end if invalid data, but if valid data, grab questions to be assigned

      // If this question is part of a group, then we need the entire group of questions
      if ($Group > 0) {
	echo "Found Group: $Group for question: $Question_Number<BR>\n";
	$sqlStr = "SELECT * FROM Queue WHERE "
	  ."Track_ID=$Track_ID AND GroupID=$Group ORDER BY Sequence ASC";

	// Process result set
	$resultGrp = mysql_query($sqlStr, $db);
	if (!$resultGrp || mysql_num_rows($resultGrp) <= 0) {

	  $outMsg = "Error: SQL: $sqlStr<BR>\nUnable to obtain questions for Group: $Group: "
	    .mysql_error()."<BR>\n";
	  output($outMsg);
	  return FALSE;

	} // end if invalid result set or no rows
	
      } // end if question to be assigned is in a group

    } // end else valid data

    // Provide some degree of output for informational and diagnostic purposes
    // Also since this is a question/group for multiple users, dump a table
    output("Assigned question #$Question_Number to following users:<br>\n");
    output("</td></tr></table>\n");
    output("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n");
    output("<tr><td width=\"75\"></td><td>\n");

  } // end if this is a binding question ($Is_Binding)


  // Enumerate users for track
  $sqlStr = "SELECT p.User_ID, p.Last_Question_Number, u.Full_Name, e.Name "
    ."FROM Participant AS p LEFT JOIN User AS u USING (User_ID) "
    ."LEFT JOIN Employment_Status AS e ON u.Employment_Status_ID=e.Employment_Status_ID "
    ."WHERE p.Track_ID=$Track_ID ";

  // Execute and process query
  $result = mysql_query($sqlStr, $db);
  
  output("query executed<BR>\n");

  // If valid result set
  if ($result) {
    
    // Loop through the users, doing assignments
    while ($row = mysql_fetch_array($result)) {

      $Name       = $row["Full_Name"];             // Full First/Last Name
      $User_ID    = $row["User_ID"];               // Numeric unique UserID in DB
      $Status     = $row["Name"];                  // Employment status (e.g. On Leave)
      $nAssignments = 0;

      
      output("Looping results for user: $Name<BR>\n");

      // No matter what, we will need to know how many questions the user is behind
      // Because no assignments have been made yet, we can see how many they are behind
      $sqlStr = "SELECT COUNT(User_ID) as count FROM "
	."Assignment WHERE User_ID='$User_ID' "
	."AND DATE(Completion_Date)='0000-00-00' AND Track_ID=$Track_ID AND Assignment_Parent_ID=0";
      $count_result = mysql_query($sqlStr, $db);
      $nUnanswered = mysql_result($count_result, 0, "count");

      // Check for status that would cause the user not to receive an assignment
      if (trim($Status) == "On Leave" || trim($Status) == "Terminated") {
	$outMsg = "This user is either on leave or has been terminated and will "
	  ."not receive an assignment or a notifcation email.<BR>\n";
	output($outMsg);
	continue;
      }

      // Now check to see if the question is nonbinding.  If so, get info on a per user basis
      if (!$Is_Binding) {

	output("Assigning Question in Unbound Track to user $row[Full_Name], User_ID=$row[User_ID]<BR>\n");

	// If this is not a binding question then we also get the Last_Question_Number
	// because for nonbinding questions, this value will be different for each user
	$Last_Question_Number = $row["Last_Question_Number"];
	
	// Get the Question_Number and Sequence with pass by reference parameters
	$ret = GetQuestionNumberAndSequence($db, $Track_ID, $Last_Question_Number,
					    $Question_Number, $Sequence, $Group, $Is_Looping);

	// See if this is a non looping track.  If there are no more questions,
	// then there is no need to continue.
	if ($Question_Number < 0 && !$Is_Looping) {
	  output("Skipping Track: $Track_ID because it is at end of loop and is a non looping track<BR>\n");
	  continue;
	}
	
	if ($Question_Number <= 0 || $Sequence <= 0 || $Group < 0 || $ret == FALSE) {
	  $outMsg = "Unable to obtain Question_Number or Sequence for NonBinding "
	    ."Question in AssignQuestion()<BR>\n";
	  output($outMsg);
	  continue;      
	} // end if invalid data
	
      } // end if not a binding track
      
      // grab questions to be assigned
      // If this question is part of a group, then we need the entire group of questions
      if ($Group > 0) {
	
	$sqlStr = "SELECT * FROM Queue WHERE "
	  ."Track_ID=$Track_ID AND GroupID=$Group ORDER BY Sequence ASC";
	  
	// Process result set
	$resultGrp = mysql_query($sqlStr, $db);
	if (!$resultGrp || mysql_num_rows($resultGrp) <= 0) {
	  
	  $outMsg = "Error: Unable to obtain questions for Group: $Group: "
	    .mysql_error()."<BR>\n";
	  output($outMsg);
	  return FALSE;
	  
	} // end if invalid result set or no rows
	
      } // end if question to be assigned is in a group

      // Keep track of the different groups that we could see
      if ($Group > 0) {
	$grpSet = 0;
	while ($rowGrp = mysql_fetch_assoc($resultGrp)) {
	  $Question_Number = $rowGrp["Question_Number"];
	  $assignmentMade = AssignQuestionHelper($db, $Question_Number, $User_ID, $Track_ID, $nAssignments, $Supervisor);
	} // end while more rows in the group
      } else
	$assignmentMade = AssignQuestionHelper($db, $Question_Number, $User_ID, $Track_ID, $nAssignments, $Supervisor);

      // Update the users last question number field
      $sqlStr ="UPDATE Participant SET Last_Question_Number=$Question_Number "
      	."WHERE User_ID=$User_ID AND Track_ID=$Track_ID";

      // Execute query and check for erorrs
      $update = mysql_query($sqlStr, $db);
      if ($update == 0)
      	output("- WARNING: Unable to set Question in AssignQuestion(): $Question_Number "
	       ."for User: $User_ID and Track: $Track_ID, MERR:".mysql_error());
      //if ($assignmentMade)
      //output("Sending assignment email...<BR>\n");
      //else
      //output("assignmentMade is false...<BR>\n");
      // See if we need to send out an email notification
      // Assignment notification should only be made if assignment successfully completed
      if ($assignmentMade)
	SendAssignmentEmail($db, $User_ID, $Track_ID, $Supervisor, $nUnanswered, $nAssignments);
      
      // Check for delinquency
      CheckForDelinquency($db, $User_ID, $Name, $Track_ID, $Track_Name, $Supervisor, $nUnanswered, $nAssignments);

    } // end while more users in the result set

    // Update the track to indicate that deliveries have been made
    if ($Is_Binding)
      $sqlStr = "UPDATE Track SET Last_Question_Date=CURDATE(), "
	."Last_Question_Number='$Question_Number' WHERE Track_ID='$Track_ID'";
    else
      $sqlStr = "UPDATE Track SET Last_Question_Date=CURDATE(), "
	."Last_Question_Number=NULL WHERE Track_ID='$Track_ID'";
      
    // Execute and process Track update
    $update = mysql_query($sqlStr, $db);
    if ($update == 0) {

      $outMsg = "WARNING: Unable to update Track with current time stamp: "
	.mysql_error()." <br>\n";
      output($outMsg);

    } // end if there was a problem updating the track


  } else { // end if valid result set for user list
    
    $outMsg = "Unable to obtain list of users in AssignQuestion(), Track_ID: "
      ."$Track_ID, Last_Question_Number: $Last_Question_Number, Group: $Group<BR>\n";
    output($outMsg);
    return FALSE;

  } // end else result set invalid
  
  return TRUE;

} // end function AssignQuestion()






/////////////////////////////////////////////////////////////////////////////
//	CreateTemplateString
//		Create a new string using the described template file and supplied
//		replacement variables.
//		The parameters $root, $sub and $template indicate sub-directory and
//		template to use. The template can either be raw text or HTML.
//		The array $keys indicates placeholders and $values specifies the
//		values which are to be replaced in the template prior to sending.
function CreateTemplateString($root, $sub, $template, $keys = NULL, $values = NULL)
{
  global 		$dirPath;
  global 		$dirOrgPath;
  
  // Attempt to find a language/company specific template
  $path = "$dirOrgPath/$root/email/$sub/$template";
  if (!file_exists($path)) {
    // Attempt to find a company specific template
    $path = "$dirOrgPath/$root/email/$template";
    if (!file_exists($path)) {
      // Attempt to find our default template
      $path = "$dirPath/email/$template";
      if (!file_exists($path))
	{
	  output("ERROR: Unable to open the email template file '$path'.<br>\n");
	  return NULL;
	}
    }
  }

  // Attempt to open and read in the template
  $fp = fopen($path, "r");
  if ($fp != FALSE) {
    $message = fread($fp, filesize($path));
    fclose($fp);

    // Update all placeholders in the template if values provided
    if ($values)
      $message = str_replace($keys, $values, $message);
    
    return $message;
  }
  output("ERROR: Unable to open the email template file '$path'.<br>\n");
  return NULL;
}



/////////////////////////////////////////////////////////////////////////////
//	SendAssignmentEmail
//		Send a notification email to the specified user indicating that
//		they have been assigned a new question.
function SendAssignmentEmail($db, $User_ID, $Track_ID, $Supervisor, $nUnanswered, $nAssignments) {


  // Global variables imported into local namespace
  global                $dirOrgPath;
  global		$Organization_Name;
  global		$Organization_Directory;
  global                $Organization_Email_Format;
  global                $useSuperAddress;
  global                $config;
  global                $DEBUG;

  $superEmail = '';
  $superName = '';
  $LoginId = '';
  $lConfigLogoImage = '';
  $lConfigTheme = '';

  if (!isset($User_ID) || !is_numeric($User_ID)) {
    $outMsg = "SendAssignmentEmail: User_ID=$User_ID, user id is not set or not numeric<BR>\n";
    output($outMsg);
    return;
  }
  
  // Get the user's information
  $sqlStr = "SELECT u.First_Name, u.Last_Name, u.Full_Name, u.Email, u.Login_ID, l.Name "
    ."AS Language FROM User AS u LEFT JOIN Language AS l "
    ."ON u.Language_ID=l.Language_ID WHERE User_ID='$User_ID' LIMIT 1";
  $result = mysql_query($sqlStr, $db);
  if ($result && mysql_num_rows($result) == 1) {
    
    $FirstName = mysql_result($result, 0, "First_Name");
    $LastName  = mysql_result($result, 0, "Last_Name");
    $Name      = mysql_result($result, 0, "Full_Name");
    $Email     = mysql_result($result, 0, "Email");
    $LoginId   = mysql_result($result, 0, "Login_ID");
    $Language  = mysql_result($result, 0, "Language");
    //    $Track     = mysql_result($result, 0, "Track");
    
    
    // See if there is a replyto with the assigned track
    $ReplyTo = "";

    $sqlStr = "SELECT Track.Email_Reply_To, Track.Name, User.First_Name, User.Last_Name, User.Email "
      ."FROM Track, User WHERE  Track.Supervisor_ID = User.User_ID AND "
      ."Track.Track_ID='$Track_ID' LIMIT 1";
    $result = mysql_query($sqlStr, $db);
    if ($result && mysql_num_rows($result) == 1) {
      
      $superEmail = mysql_result($result, 0, "User.Email");
      $Track = mysql_result($result, 0, "Track.Name");
      $superName = mysql_result($result, 0, "User.First_Name")." "
	.mysql_result($result, 0, "User.Last_Name");
      $ReplyTo = "\"$superName\" <".mysql_result($result, 0, "Track.Email_Reply_To").">";

      if ($ReplyTo == "" && $useSuperAddress) {
        if ($superEmail != "")
          $ReplyTo = "\"$superName\" <$superEmail>";
        else
          $ReplyTo = "\"$defEmailName\" <$defEmailAddress>";
      }
    } else {
      $ReplyTo = "\"$defEmailName\" <$defEmailAddress>";
      $Track = '';
    }

    // Create our keys for the email template
    $keys = array();
    $values = array();
    $keys[0] = '__USER_NAME__';
    $values[0] = $Name;
    $keys[1] = '__FIRST_NAME__';
    $values[1] = $FirstName;
    $keys[2] = '__LAST_NAME__';
    $values[2] = $LastName;
    $keys[3] = '__TRACK_NAME__';
    $values[3] = $Track;
    $keys[4] = '__SUPERVISOR__';
    $values[4] = $Supervisor;
    $keys[5] = '__NUMBER_UNANSWERED__';
    $values[5] = $nUnanswered + $nAssignments;
    $keys[6] = '__HEADER_LOGO_IMAGE_URL__';
    $keys[7] = '__HEADER_LEFT_BACK_IMAGE_URL__';
    $keys[8] = '__HEADER_MIDDLE_BACK_IMAGE_URL__';
    $keys[9] = '__HEADER_MIDDLE_FRONT_IMAGE_URL__';
    $keys[10] = '__HEADER_RIGHT_BACK_IMAGE_URL__';
    $keys[11] = '__HEADER_RIGHT_FRONT_IMAGE_URL__';
    $keys[12] = '__BASE_URL__';
    $keys[13] = '__LOGIN_PATH__';
    $keys[14] = '__QUERY_PARAMS__';
    $lConfigLogoImage = $config->GetValue(CONFIG_TOP_LEFT_FRONT);
    output("Checking to see if top left front image is set...<BR>\n");
    if (!empty($lConfigLogoImage) &&
	file_exists("$dirOrgPath/$Organization_Directory".$lConfigLogoImage)) {
      output("Either top left image is set AND image specified exists, using that.<BR>\n");
      $motif_path = FQ_URL_ROOT.'/organizations/'.$Organization_Directory;
      output("motif_path set to $motif_path<BR>\n");
      $values[6] = $motif_path.$lConfigLogoImage;
      if ($config->GetValue(CONFIG_TOP_LEFT_BACK))
	$values[7] = $motif_path.$config->GetValue(CONFIG_TOP_LEFT_BACK);
      else
	$values[7] = '';
      if ($config->GetValue(CONFIG_TOP_MIDDLE_BACK))
	$values[8] = $motif_path.$config->GetValue(CONFIG_TOP_MIDDLE_BACK);
      else
	$values[8] = '';
      if ($config->GetValue(CONFIG_TOP_MIDDLE_FRONT))
	$values[9] = $motif_path.$config->GetValue(CONFIG_TOP_MIDDLE_FRONT);
      else
	$values[9] = '';
      if ($config->GetValue(CONFIG_TOP_RIGHT_BACK))
	$values[10] = $motif_path.$config->GetValue(CONFIG_TOP_RIGHT_BACK);
      else
	$values[10] = '';
      if ($config->GetValue(CONFIG_TOP_RIGHT_FRONT))
	$values[11] = $motif_path.$config->GetValue(CONFIG_TOP_RIGHT_FRONT);
      else
	$values[11] = '';
    } else {
      $lConfigTheme = $config->GetValue(CONFIG_UI_THEME);
      output("Setting theme to: $lConfigTheme<BR>\n");
      if (!empty($lConfigTheme)) {
	$motif_path = FQ_URL_ROOT."/themes/$lConfigTheme/presentation";
      } else {
	$motif_path = FQ_URL_ROOT.'/themes/Default/presentation';
      }
      output("motif_path set to $motif_path<BR>\n");
      $values[6] = $motif_path.'/wtp_logo.gif';
      $values[7] = $motif_path.'/header_middle.gif';
      $values[8] = $motif_path.'/header_middle.gif';
      $values[9] = $motif_path.'/transparent.gif';
      $values[10] = $motif_path.'/header_middle.gif';
      $values[11] = $motif_path.'/hrtools_logo.gif';
    }
    if (strtolower(ENV) == 'test')
      $values[12] = 'http://test.trainingadvisorinc.com';
    else
      $values[12] = 'http://www.trainingadvisorinc.com';

    // Account for Patrick Henry branded login
    if ((int)($config->orgId) == 404 || (int)($config->orgId) == 405)
      $values[13] = '/desktop/login/phi_login.php';
    else
      $values[13] = '/desktop/login/login.php';
    $values[14] = '?User_ID='.$LoginId.'&Org_ID='.$config->orgId.'&fromEmail=1';

    if ($nUnanswered < 6)
      $suffix = '_1-5';
    else if ($nUnanswered < 11)
      $suffix = '_6-10';
    else
      $suffix = '_11-';
    
    if ($Organization_Email_Format == 'html')
      $extension = '.html';
    elseif ($Organization_Email_Format == 'text')
      $extension = '.txt';
    else {
      $extension = '.html';
      output('*** ERROR: Unable to determine email format, defaulting to html');
    }
    $subject = CreateTemplateString($Organization_Directory, $Language, "Assignment_Subject$suffix.txt");
    $subject = str_replace("\n", "", $subject);
    $subject = str_replace("\r", "", $subject);
    
    $message = CreateTemplateString($Organization_Directory, $Language, "Assignment_Message$suffix$extension", $keys, $values);

    if ($Email && SendEmail($Email, $ReplyTo, $subject, $message))
      output("- email sent to $Email");
    else {
      output("- WARNING: Unable to send email notification to $Email");
      return FALSE;
    }
    
  } else {
    
    $outMsg = "Unable to select user information for sending email: ".mysql_error()."<BR>\n";
    output($outMsg);
    return FALSE;

  }

  return TRUE;

} // end function SendAssignmentEmai()




/////////////////////////////////////////////////////////////////////////////
//	SendDelinquentEmails
//		Send any emails waiting in our delinquent email array.
function SendDelinquentEmails()
{
  global                $Language;
  global		$arrayDelinquentEmails;
  global		$Organization_Directory;
  global                $defEmailName, $defEmailAddress;

  // Enumerate through all waiting notification emails
  foreach($arrayDelinquentEmails as $key => $array) {
    
    $name = $array["name"];
    $subject = $array["subject"];
    $message = $array["message"];
    
    $organization = $array["organization"];
    $language = $array["language"];
    $temp = CreateTemplateString($Organization_Directory, $Language, "Delinquent_Footer.txt");
    
    $message = $message.$temp;

    output("Sending Delinquency report to: $name &lt;$key&gt;<BR>\n");
    if (!SendEmail($key, "", $subject, $message, "From: \"$defEmailName\" <$defEmailAddress>\r\n"))
      output("WARNING: Unable to send email notification to $key");
  }
}

/////////////////////////////////////////////////////////////////////////////
//	AddToDelinquencyEmail
//		Append a new notification onto the delinquincy notice for either
//		the Track supervisor or company HR contact.
function AddToDelinquencyEmail($db, $bTrackSupervisor, $Contact_ID, $User_ID,
			       $User_Name, $Track_Name, $Supervisor, $nBehind)
{
  global		$Organization_Name;
  global		$Organization_Directory;
  global		$arrayDelinquentEmails;
  
  if (!isset($Contact_ID))
    return;

  // Get the contact persons info
  $sqlStr = "SELECT u.Full_Name,u.First_Name,u.Last_Name,u.Email,l.Name AS Language "
    ."FROM User AS u LEFT JOIN Language AS l ON u.Language_ID=l.Language_ID "
    ."WHERE User_ID='$Contact_ID' LIMIT 1";
  $result = mysql_query($sqlStr, $db);

  if ($result && mysql_num_rows($result) == 1) {
    
    $Contact_Name = mysql_result($result, 0, "Full_Name");
    $Contact_First_Name = mysql_result($result, 0, "First_Name");
    $Contact_Last_Name = mysql_result($result, 0, "Last_Name");
    $Email = mysql_result($result, 0, "Email");
    $Language = mysql_result($result, 0, "Language");
    
    
    if ($Email) {
      // Create our keys for the email template
      $keys = array();
      $values = array();
      $keys[0] = "__CONTACT_FIRST_NAME__";
      $values[0] = $Contact_First_Name;
      $keys[1] = "__CONTACT_LAST_NAME__";
      $values[1] = $Contact_Last_Name;
      $keys[2] = "__CONTACT_FULL_NAME__";
      $values[2] = $Contact_Name;
      $keys[3] = "__USER_NAME__";
      $values[3] = $User_Name;
      $keys[4] = "__TRACK_NAME__";
      $values[4] = $Track_Name;
      $keys[5] = "__DELINQUENT__";
      $values[5] = $nBehind;
      $keys[6] = "__SUPERVISOR__";
      $values[6] = $Supervisor;

      // Retrieve or add the contact to our email list
      if (!isset($arrayDelinquentEmails[$Email])) {
	$array = array();
	$array["name"] = $Contact_Name;
	
	$array["organization"] = $Organization_Name;
	$array["language"] = $Language;
	
	$temp = CreateTemplateString($Organization_Directory, $Language, "Delinquent_Subject.txt");
	$temp = str_replace("\n", "", $temp);
	$temp = str_replace("\r", "", $temp);
	$array["subject"] = $temp;
	
	$temp = CreateTemplateString($Organization_Directory, $Language, "Delinquent_Header.txt", $keys, $values);
	$array["message"] = $temp;
      } else
	$array = $arrayDelinquentEmails[$Email];
      
      // Append the new notification to the email message
      $temp = CreateTemplateString($Organization_Directory, $Language, "Delinquent_Row.txt", $keys, $values);
      $message = $array["message"];
      $array["message"] = $message.$temp;
      
      $arrayDelinquentEmails[$Email] = $array;
      
      output("Sending delinquincy email notification to '$Contact_Name' ($Email)<br>\n");
    }
    else
      output("WARNING: Unable to send delinquincy email notification to '$Contact_Name'<br>\n");
  }
}

/////////////////////////////////////////////////////////////////////////////
//	CheckForDelinquency
//		Check the assignments for the specified user and see if they
//		are delinquent compared to the track and organization limits.
//		If needed send an email notification to their track supervisor or
//		HR contact.
function CheckForDelinquency($db, $User_ID, $User_Name, $Track_ID, $Track_Name,
			     $Supervisor, $nUnanswered, $nAssignments)
{
  global		$Organization_Delinquency_Notification;
  global		$Organization_HR_Contact_ID;
  global                $DEBUG;
  
  //  if ($DEBUG)
  //    echo "Unanswered count: $nUnanswered<BR>\n";
  $bNotice = false;
  
  // Compare to the track delinquency limit
  $sqlStr = "SELECT Delinquency_Notification FROM Track WHERE Track_ID='$Track_ID' LIMIT 1";
  $result = mysql_query($sqlStr, $db);
  if ($result) {
    $Delinquency_Notification = mysql_result($result, 0, "Delinquency_Notification");
    if ($Delinquency_Notification > 0 && $nUnanswered > $Delinquency_Notification) {
      output("NOTICE: track limit of: $Delinquency_Notification reached<BR>\n");
      output("NOTICE: user is delinquent $nUnanswered questions<br>\n");
      $bNotice = true;
      
      // Determine who should receive the notification email
      $result = mysql_query("SELECT Use_Supervisor,HR_Contact_ID FROM User WHERE User_ID='$User_ID' LIMIT 1", $db);
      if ($result) {
	$Use_Supervisor = mysql_result($result, 0, "Use_Supervisor");
	$HR_Contact_ID = mysql_result($result, 0, "HR_Contact_ID");
	
	if ($Use_Supervisor) {
	  $result = mysql_query("SELECT Supervisor_ID FROM Track WHERE Track_ID='$Track_ID' LIMIT 1", $db);
	  $HR_Contact_ID = mysql_result($result, 0, "Supervisor_ID");
	}
	
	if ($HR_Contact_ID)
	  AddToDelinquencyEmail($db, true, $HR_Contact_ID, $User_ID, $User_Name, $Track_Name, $Supervisor, $nUnanswered);
      }
    }
  }

  // Compare to the organization delinquency limit
  if ($Organization_Delinquency_Notification > 0 && $nUnanswered >= $Organization_Delinquency_Notification) {
    if (!$bNotice) {
      output("NOTICE: Org limit of: $Organization_Delinquency_Notification reached<BR>\n");
      output("NOTICE: user is delinquent $nUnanswered questions<br>\n");
    }
    
    if ($Organization_HR_Contact_ID)
      AddToDelinquencyEmail($db, false, $Organization_HR_Contact_ID, $User_ID, $User_Name, $Track_Name, $Supervisor, $nUnanswered);
  }
}

?>
