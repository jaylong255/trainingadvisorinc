#!/usr/bin/php -q
<?php

require_once('../../../hrt_config.php');
require_once('../includes/constants.php');
require_once('../includes/lib.php');
#require_once('../includes/Organization.php');

$cmd     = '';
$rc      = 0;
$db      = NULL;
$matches = array();



$db = new DatabaseObject('mysql', 'vector', 'j2d4brok');
$db->Execute("DELETE FROM db WHERE Db LIKE 'ORG0%'");
$db->Execute("DELETE FROM db WHERE Db LIKE 'ORG1%'");
$db->Execute("DELETE FROM db WHERE Db LIKE 'ORG2%'");
$db->Execute("DELETE FROM db WHERE Db LIKE 'ORG3%'");
$db->Execute("DELETE FROM db WHERE Db LIKE 'ORG4%'");
$db->Execute("DELETE FROM db WHERE Db LIKE 'ORG5%'");
$db->Execute("DELETE FROM db WHERE Db LIKE 'ORG6%'");
$db->Execute("DELETE FROM db WHERE Db LIKE 'ORG7%'");
$db->Execute("DELETE FROM db WHERE Db LIKE 'ORG8%'");
$db->Execute("DELETE FROM db WHERE Db LIKE 'ORG9%'");
$db->Execute("UPDATE db SET Delete_priv='Y' WHERE User='HR_Auth_User'");
$db->Execute("FLUSH PRIVILEGES");
$db->SetDb(AUTH_DB_NAME);
$db->Select("Database_Name", ORG_LIST_TABLE);
$dbNames = $db->Query(MANY_VALUES, VALUES_FORMAT);

foreach ($dbNames as $dbName) {

  if (!$db->SetDb($dbName)) {
    echo "Failed to set DB to $dbName\n";
    exit(1);
  }

  $questionSqlColumns = unserialize(QUESTION_SQL_COLUMNS);
  $questionAnwers = array();

  if ($dbName == 'ORG115') {
    $sqlStr = "DELETE FROM Question WHERE Question_Number=399";
    if (!$db->Execute($sqlStr)) {
      echo "Failed to delete Question 399 from $dbName\n";
      exit(-1);
    }      
  }

  // Get correct answers to all questions in the DB
  $db->Select(array($questionSqlColumns[QUESTION_COLUMN_ID], $questionSqlColumns[QUESTION_COLUMN_CONTENT]),
	      QUESTION_TABLE);
  $rows = $db->Query(MANY_ROWS, VALUES_FORMAT);
  foreach ($rows as $row) {
    if ($row[1]) {
      if (preg_match("/strCorrect=(\d)/", $row[1], $matches)) {
	$questionAnswers[(int)$row[0]] = $matches[1];
	// Fix those that answered correctly but are are listed has having gotten it wrong in the DB
	$sqlStr = "UPDATE Question_Data SET Result=1 WHERE "
	  ."Completion_Date > '2007-01-01' AND Question_Number = $row[0] AND Foil_Selected=$matches[1]";
	//echo "\tSQL:$sqlStr\n";
	if (!$db->Execute($sqlStr)) {
	  echo "Failed to repaire Question_Data in db $dbName for question number $row[0]\n";
	  exit(2);
	}
      } else {
	echo "Failed to match correct answer in db $dbName for question number $row[0]\n";
	exit(3);
      }
    }
  }

  // Check for existance of Track_ID column
  $sqlStr = "SELECT Track_ID FROM User";
  if ($db->Execute($sqlStr))
    $userTrackIdExists = TRUE;
  else
    $userTrackIdExists = FALSE;

  // Check for existance of Last_Question_Number column
  $sqlStr = "SELECT Last_Question_Number FROM User";
  if ($db->Execute($sqlStr))
    $userLastQuestionNumberExists = TRUE;
  else
    $userLastQuestionNumberExists = FALSE;

  // If they both exist, migrate data to participants
  if ($userTrackIdExists && $userLastQuestionNumberExists) {
    // Move all track participants into the Participants table and drop the column from the Users table
    $db->Select(array('User_ID', 'Track_ID', 'Last_Question_Number'), USER_TABLE);
    $db->Where('Track_ID IS NOT NULL AND Track_ID != 0');
    $rows = $db->Query(MANY_ROWS, BOTH_FORMAT);
    //print_r($values);
    foreach($rows as $row) {
      $sqlStr = 'INSERT INTO '.PARTICIPANT_TABLE.' (Participant_ID, User_ID, Track_ID, Last_Question_Number, Placed_Date) '
	."VALUES ('', $row[User_ID], $row[Track_ID], $row[Last_Question_Number], NOW())";
      if (!$db->Execute($sqlStr)) {
	echo "Failed to migrate particpant ID $row[User_ID] to Participants table on Track $row[Track_ID] in $dbName\n";
	exit(4);
      }
    }
  }

  /*  // This is bad since it will prevent us from doing another migration if need be
  // If the track exists, drop it since we do not need it anymore
  if ($userTrackIdExists) {
    $sqlStr = "ALTER TABLE ".USER_TABLE." DROP COLUMN Track_ID";
    if (!$db->Execute($sqlStr)) {
      echo "Failed to drop column Track_ID from User table in $dbName\n";
      exit(5);
    }
  }

  // If the last question number exists then drop it since we do not need it anymore.
  if ($userLastQuestionNumberExists) {
    $sqlStr = "ALTER TABLE ".USER_TABLE." DROP COLUMN Last_Question_Number";
    if (!$db->Execute($sqlStr)) {
      echo "Failed to drop column Last_Question_Number from User table in $dbName\n";
      exit(6);
    }
  }
  */

  // Move all assignments to new table structure
  $db->Select('*', USER_ASSIGNMENT_TABLE);
  $db->Where("Track_ID IS NOT NULL");
  $rows = $db->Query(MANY_ROWS, BOTH_FORMAT);
  //print_r($values);
  foreach($rows as $row) {
    if (!$row['Completion_Date'])
      $row['Completion_Date'] = '0000-00-00 00:00:00';
    $sqlStr = 'INSERT INTO '.ASSIGNMENT_TABLE." (Assignment_ID, User_ID, Track_ID, Question_Number, Version_Date, Domain_ID, Language_ID, Delivery_Date, Due_Date, Completion_Date, First_Answer, Final_Answer, Answered_Correctly, Answered_Late, Exit_Info, Department_ID) VALUES ('', $row[User_ID], $row[Track_ID], $row[Question_Number], '$row[Version_Date]', $row[Domain_ID], $row[Language_ID], '$row[Delivery_Date]', '0000-00-00 00:00:00', '$row[Completion_Date]', 0, 0, 0, 0, $row[Exit_Info], 0)";
    if (!$db->Execute($sqlStr)) {
      echo "Failed to migrate assignment for user ID $row[User_ID] to Assignment table on Track $row[Track_ID] in $dbName\n";
      exit(7);
    }
  }

}

$db->Close();

?>