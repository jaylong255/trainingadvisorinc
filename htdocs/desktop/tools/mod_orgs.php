<?php

require_once('../includes/common.php');

$sqlStr       = "ALTER Table Queue ADD GroupID int(6) unsigned zerofill not null default 000000";
$errMsg       = '';

$dbHost       = DB_HOST;
$dbUser       = 'root';
$dbPass       = '';
$dbName       = 'HR_Tools_Authentication';

$dbLlink      = '\0';
$dbRow        = array();

$results      = '';

$DEBUG        = 0;

if (isset($_REQUEST['submit']) && $_REQUEST['submit'] == 'Update Databases') {

  $dbUser       = $_REQUEST['dbUser'];
  $dbPass       = $_REQUEST['dbPass'];

  $dbLink = mysql_connect($dbHost, $dbUser, $dbPass);
  if (!$dbLink) {

    $errMsg .= "Unable to connect to database: ".mysql_error()."<BR>\n";

  } else {    
  
    if (!mysql_select_db($dbName)) {

      $errMsg .= "Unable to select database: $dbName: ".mysql_error()."<BR>\n";

    } else {
    
      $sqlStr = "SELECT Database_Name FROM Organization";
      $dbRes = mysql_query($sqlStr);

      if (!$dbRes) {

	$errMsg .= "Unable to execute(org=$dbName): $sqlStr: ".mysql_error()."<BR>\n";

      } else {
	while ($dbRow = mysql_fetch_row($dbRes)) {
	  
	  if (!mysql_select_db($dbRow[0])) {
	    $errMsg .= "Unable to select DB: $dbRow[0] specified in HR_Tools_Authentication.Organization<BR>\n";
	    continue;
	  }

	  $sqlStr = $_REQUEST['sql'];
	  if ($DEBUG)
	    echo "DB: $dbRow[0], SQLSTR: $sqlStr\n";
	  $res = mysql_query($sqlStr);
	  if (!$res)
	    $errMsg .= "Unable to execute query(org=$dbRow[0]): $sqlStr: ".mysql_error()."<BR>\n";
	  else {
	    if (!strncasecmp($_REQUEST['sql'], 'select', 6)) {
	      $results .= "DB: $dbRow[0]: <BR>\n";
		while ($row = mysql_fetch_array($res)) {
		  $results .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".print_r($row, TRUE)."<BR>\n";
		}
	    } else {
	      $results .= "DB: $dbRow[0]: ".mysql_errno()."<BR>\n";
	    }
	  }
	  
	}
      }
    }

    mysql_close($dbLink);
    
  }
}
  
$smarty->assign('dbUser', $dbUser);
$smarty->assign('dbPass', $dbPass);
$smarty->assign('sql', $sqlStr);
$smarty->assign('errMsg', $errMsg);
$smarty->assign('results',  $results);
$smarty->assign('SELF', $_SERVER['PHP_SELF']);

$smarty->display('tools/mod_orgs.tpl');


?>
