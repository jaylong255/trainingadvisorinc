#!/usr/bin/php -q
<?php

chdir('../organizations');
$dirs = explode("\n", trim(`ls -1`));
//print_r($dirs, FALSE);

foreach ($dirs as $dir) {
  $dir = str_replace(' ', '\ ', $dir);
  echo "DIR::::::::::::::$dir\n";
  if (!chdir("$dir/email")) {
    echo("*** FAIL: chdir to $dir/email\n");
    continue;
  }
  $custEmailFileNames = explode("\n", trim(`ls -1`));
  //print_r($custEmailFileNames, FALSE);
  //echo "\tEMAILFL_COUNT=".count($custEmailFileNames)."\n";
  if (count($custEmailFileNames)) {
    foreach ($custEmailFileNames as $custEmailFileName) {
      // Do our thing to the files
      if (empty($custEmailFileName) || strpos($custEmailFileName, "Subject") !== FALSE)
	continue;
      $custEmailFileText = file_get_contents($custEmailFileName);
      $htmlEmailFileName = str_replace('.txt', '.html', $custEmailFileName);
      $htmlEmailFile = fopen($htmlEmailFileName, "w");
      fwrite($htmlEmailFile, "--__MULTIPART_BOUNDARY__
Content-Type: text/plain; charset=us-ascii
Content-Transfer-Encoding: 7bit

$custEmailFileText

--__MULTIPART_BOUNDARY__
Content-Type: text/html; charset=us-ascii
Content-Transfer-Encoding: 7bit

<html>
<body>
<TABLE border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" height=\"97\">
  <tr>
    <td background=\"__HEADER_LEFT_BACK_IMAGE_URL__\" style=\"background-repeat:repeat-x;\" id=\"top_left_bg\">
      <img src=\"__HEADER_LOGO_IMAGE_URL__\"
           border=\"0\" alt=\"\" id=\"top_left_img\" height=\"97\">
    </td>
    <td background=\"__HEADER_MIDDLE_BACK_IMAGE_URL__\" style=\"background-repeat:repeat-x;\" align=\"right\" valign=\"top\" id=\"top_middle_bg\">
      <img src=\"__HEADER_MIDDLE_FRONT_IMAGE_URL__\"
           border=\"0\" alt=\"\" id=\"top_middle_img\" height=\"97\">
    </td>
    <td background=\"__HEADER_RIGHT_BACK_IMAGE_URL__\" style=\"background-repeat:repeat-x;\" align=\"right\" valign=\"top\" id=\"top_right_bg\" width=\"340\" height=\"97\">
      <img src=\"__HEADER_RIGHT_FRONT_IMAGE_URL__\"
           border=\"0\" alt=\"\" id=\"top_right_img\">
    </td>
  </tr>
</table>
<!--img src=\"__HEADER_LOGO_IMAGE_URL__\"-->
<p style=\"font-family:tahoma,sans-serif; font-size:12px; color:#307aef; text-decoration:none;\">
".str_replace("\n", "<BR>\n", str_replace("\r", "", preg_replace("/http:\/\/(.*)/i", "<a href=\"http://$1\">http://$1</a>", $custEmailFileText)))."
</p>
</body>
</html>

--__MULTIPART_BOUNDARY__--
");
      fclose($htmlEmailFile);
    }
  }
  chdir("../..");
}

?>