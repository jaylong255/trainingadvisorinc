__FIRST_NAME__ __LAST_NAME__, 

You are invited to review the latest employment law question waiting for you at http://www.trainingadvisorinc.com/desktop .
 
In your assignments list there are __NUMBER_UNANSWERED__ questions awaiting your review.

Participation in the "Training Advisor" program is an easy and important way to stay fresh on employment and labor law. Thank you for completing your reviews in a timely manner.

Sincerely,

__SUPERVISOR__

P.S. You are at least six (6) questions behind. We recognize that there are many priorities in each day, however we encourage you to stay current on your questions. Thank you for your assistance in participating and staying current on these training issues.

