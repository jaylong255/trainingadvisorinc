__FIRST_NAME__ __LAST_NAME__,

It's time to answer the latest training question. 

Please go to http://www.trainingadvisorinc.com/desktop to login. 

Remember, we can customize these training questions.  So if you'd like to add questions you think would help us here in the Factory, let me know and I'll do what I can to get the questions added.

Sincerely,

Debbie Green,
Training Coordinator






 


