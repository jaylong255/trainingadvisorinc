<html>

<head>

<title>Winning Through Prevention</title>

<style>

<!--

a, body, input, option, select, table, td, tr { font-family:tahoma,sans-serif; font-size:14px }

-->

</style>

</head>

<body>



<p>__FIRST_NAME__ __LAST_NAME__,</p>



<p>You are invited to review the latest training waiting for you at

<a href="__BASE_URL____LOGIN_PATH____QUERY_PARAMS__">__BASE_URL____LOGIN_PATH____QUERY_PARAMS__</a> .
In your assignments list there are <b>__NUMBER_UNANSWERED__</b> questions awaiting your review.</p>



<p>You are at least eleven (11) questions behind. We are very concerned that you
are this far behind. If you will be unable to catch up in the next few days
please contact me and explain the situation. The company relies on this training
to assure that every employee is up to date on critical employment- law issues. Participation in the "Winning Through Prevention" program is
mandatory as well as vital to the success of our organization. Thank you for your assistance in participating and staying current on these training issues.

<br>

<br>

Sincerely,<br>

<br>

__SUPERVISOR__</p>



</body>

</html>

