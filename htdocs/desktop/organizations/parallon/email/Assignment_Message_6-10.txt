<html>

<head>

<title>Winning Through Prevention</title>

<style>

<!--a, body, input, option, select, table, td, tr { font-family:tahoma,sans-serif; font-size:14px }-->

</style>

</head>

<body>



<p>__FIRST_NAME__ __LAST_NAME__,&nbsp;</p>



<p>You are invited to review the latest training waiting for you at

<a href="__BASE_URL____LOGIN_PATH____QUERY_PARAMS__">__BASE_URL____LOGIN_PATH____QUERY_PARAMS__</a> .
In your assignments list there are <b>__NUMBER_UNANSWERED__</b>  questions awaiting your review.<br>

<br>

You are at least six (6) questions behind. We recognize that there are many
priorities in each day, however it is imperative that every employee stay current on their questions. Please find the time in the next several days to catch up on your overdue assignments. Participation in the "Winning Through Prevention" program is vital to the success of our organization. Thank you for your assistance in participating and staying current on these training issues.<br>

<br>

Sincerely,<br>

<br>

__SUPERVISOR__</p>



</body>

</html>

