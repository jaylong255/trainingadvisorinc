<?php

require_once('DatabaseObject.php');


define('CONFIG_ORG_CONTACT_ID', 'Org_Contact_Id');
define('CONFIG_ORG_NAME', 'Org_Name');
define('CONFIG_ORG_DIRECTORY', 'Org_Directory');
define('CONFIG_DELINQUENCY_NOTIFICATION', 'Delinquency_Notification');
define('CONFIG_ORG_NEWS_URL', 'Org_News_Url');
define('CONFIG_USE_NEWS_URL', 'Use_News_Url');
define('CONFIG_USE_DEFAULT_MOTIF', 'Use_Default_Motif');
define('CONFIG_TOP_LEFT_BACK', 'Top_Left_Back');
define('CONFIG_TOP_LEFT_BACK_REPEAT', 'Top_Left_Back_Repeat');
define('CONFIG_TOP_LEFT_FRONT', 'Top_Left_Front');
//define('CONFIG_TOP_LEFT_FRONT_REPEAT', 'Top_Left_Front_Repeat');
define('CONFIG_TOP_MIDDLE_BACK', 'Top_Middle_Back');
define('CONFIG_TOP_MIDDLE_BACK_REPEAT', 'Top_Middle_Back_Repeat');
define('CONFIG_TOP_MIDDLE_FRONT', 'Top_Middle_Front');
//define('CONFIG_TOP_MIDDLE_FRONT_REPEAT', 'Top_middle_Front_Repeat');
define('CONFIG_TOP_RIGHT_BACK', 'Top_Right_Back');
define('CONFIG_TOP_RIGHT_BACK_REPEAT', 'Top_Right_Back_Repeat');
define('CONFIG_TOP_RIGHT_FRONT', 'Top_Right_Front');
//define('CONFIG_TOP_RIGHT_FRONT_REPEAT', 'Top_Right_Front_Repeat');
define('CONFIG_DISPLAY_CHOICES', 'Display_Choices');
define('CONFIG_DISPLAY_CHOICES_USE_QUESTION_SETTING', 'Display_Choices_Use_Question_Setting');
define('CONFIG_LICENSED_PRINT_QUESTIONS', 'Licensed_Print_Questions');
define('CONFIG_SHOW_USER_THAT_ANSWERED', 'Show_User_That_Answered');
define('CONFIG_TRACK_ADMIN_SUMMARY_DEFAULT', 'Track_Admin_Summary_Default');

// Licensing feature
define('CONFIG_LICENSED_HR_CONTENT', 'Licensed_Hr_Content');
define('CONFIG_LICENSED_AB1825_CONTENT', 'Licensed_Ab1825_Content');
define('CONFIG_LICENSED_EDU_CONTENT', 'Licensed_Edu_Content');

// Theme feature
define('CONFIG_UI_THEME', 'UI_Theme');

// Email sending format
define('CONFIG_EMAIL_FORMAT', 'Email_Format');


class Config extends DatabaseObject {

  var $configSqlColumns = array();

  var $orgId = 0;
  var $configItems = array();

  var $cacheError = '';
  var $rc = 0;
  var $myqslOutput = '';

  function Config($newOrgId = 0) {

    if (DEBUG & DEBUG_CLASS_FUNCTIONS)
      error_log("Config::Config(newOrgId=$newOrgId): Contructor called");

    if (!is_numeric($newOrgId) || $newOrgId <= 0)
      return RC_INVALID_ARG;

    $this->orgId = (int)$newOrgId;
    $this->configSqlColumns = unserialize(CONFIG_SQL_COLUMNS);
    $this->DatabaseObject(ORG_DB_NAME_PREFIX.$this->orgId);

    $this->SetConfigOrgId($this->orgId);
  }


  function SetConfigOrgId($configOrgId) {

    $this->SetDb(ORG_DB_NAME_PREFIX.(int)$configOrgId);
    // Get the configuration from the system
    $this->Select(CONFIG_TABLE.'.*', CONFIG_TABLE);
    $results = $this->Query(MANY_ROWS, KEYS_FORMAT);
    if ($results === FALSE) {
      error_log("Config::Config(newOrgId=$newOrgId): Unable to query database for configuration");
      return RC_QUERY_FAILED;
    }
    
    foreach ($results as $result) {
      $val = $result[$this->configSqlColumns[CONFIG_COLUMN_VALUE]];
      if ($result['Type'] == 'array' || $result['Type'] == 'object')
	$val = unserialize($val);
      
      $dflt = $result[$this->configSqlColumns[CONFIG_COLUMN_TYPE]];
      if ($result['Type'] == 'array' || $result['Type'] == 'object')
	$dflt = unserialize($dflt);
      
      $this->configItems[$result[$this->configSqlColumns[CONFIG_COLUMN_NAME]]] =
	array('id' => $result[$this->configSqlColumns[CONFIG_COLUMN_ID]],
	      'value' => $val, 'default' => $dflt,
	      'type' => $result[$this->configSqlColumns[CONFIG_COLUMN_TYPE]],
	      'defaultType' => $result[$this->configSqlColumns[CONFIG_COLUMN_DEFAULT_TYPE]],
	      'created' => $result[$this->configSqlColumns[CONFIG_COLUMN_DATE_CREATED]],
	      'modified' => $result[$this->configSqlColumns[CONFIG_COLUMN_DATE_MODIFIED]],
	      'obsolete' => $result[$this->configSqlColumns[CONFIG_COLUMN_OBSOLETE]]);
    }
  }


  function GetItem($name) {
    if (!isset($this->configItems[$name]))
      return FALSE;

    return $this->configItems[$name];
  }


  function GetAllItems() {
    if (!isset($this->configItems))
      return FALSE;

    return $this->configItems;
  }


  function GetValue($name) {
    if (!isset($this->configItems[$name]))
      return FALSE;

    return $this->configItems[$name]['value'];
  }


  function GetDefault() {
    if (!isset($this->configItems[$name]))
      return FALSE;

    return $this->configItems[$name]['default'];
  }


  function SetValue($name, $value, $create=0) {

    if (!$create && !isset($this->configItems[$name])) {
      error_log("Config::SetValue(name=$name, value=$value): Unknown parameter name: $name");
      return FALSE;
    }

    if (gettype($value) == 'array' || gettype($value) == 'object') {
      $this->configItems[$name]['value'] = serialize($value);
    } else {
      $this->configItems[$name]['value'] = $value;
    }
    $this->configItems[$name]['type'] = gettype($value);

    // We can use replace here since we have an existance check on the name
    // of the config item we are working with at the beginning of this function
    if ($create) {
      if (!$this->Replace(array($this->configSqlColumns[CONFIG_COLUMN_NAME] => $name,
				$this->configSqlColumns[CONFIG_COLUMN_VALUE] => esql($this->configItems[$name]['value']),
				$this->configSqlColumns[CONFIG_COLUMN_TYPE] => $this->configItems[$name]['type'],
				$this->configSqlColumns[CONFIG_COLUMN_DATE_CREATED] => 'NOW()',
				$this->configSqlColumns[CONFIG_COLUMN_DATE_MODIFIED] => 'NOW()'),
			  CONFIG_TABLE)) {
	error_log("Config::SetValue(name=$name, value=$value): Failed to create configuration param, query failed");
	return FALSE;
      }
    } else {
      $this->Where($this->configSqlColumns[CONFIG_COLUMN_NAME]."='$name'");
      if (!$this->Update(array($this->configSqlColumns[CONFIG_COLUMN_VALUE] => esql($this->configItems[$name]['value']),
			       $this->configSqlColumns[CONFIG_COLUMN_TYPE] => $this->configItems[$name]['type']),
			 CONFIG_TABLE)) {
	error_log("Config::SetValue(name=$name, value=$value): Failed to update configuration param, query failed");
	return FALSE;
      }	
    }  

    return TRUE;    
  }


  function SetDefault($name, $defaultValue) {
    if (!isset($this->configItems[$name])) {
      error_log("Config::SetDefault(name=$name, defaultValue=$defaultValue): Unknown parameter name: $name");
      return FALSE;
    }
    
    if (gettype($defaultValue) == 'array') {
      $this->configItems[$name]['default'] = serialize($defaultValue);
      $this->configItems[$name]['defaultType'] = 'array';
    } elseif (gettype($defaultValue) == 'object') {
      $this->configItems[$name]['default'] = serialize($defaultValue);
      $this->configItems[$name]['defaultType'] = 'object';
    } else {
      $this->configItems[$name]['default'] = $defaultValue;
    }

    $this->Where($this->configSqlColumns[CONFIG_COLUMN_NAME]."='$name'");
    if (!$this->Update(CONFIG_TABLE,
		       array($this->configSqlColumns[CONFIG_COLUMN_VALUE] => "'".$this->configItems[$name]['default']."'",
			     $this->configSqlColumns[CONFIG_COLUMN_TYPE] => "'".$this->configItems[$name]['defaultType']."'"))) {
      error_log("Config::SetValue(name=$name, value=$value): Failed to update configuration, query failed");
      return FALSE;
    }

    return TRUE;    
  }

}
