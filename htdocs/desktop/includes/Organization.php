<?php

require_once('DatabaseObject.php');
require_once('User.php');
require_once('TplClasses.php');


class Organization extends DatabaseObject {

  var $orgListSqlColumns = array();
  var $orgListLabelColumns = array();
  //  var $userSqlColumns = array();
  var $departmentSqlColumns = array();
  var $divisionSqlColumns = array();
  var $custom1SqlColumns = array();
  var $custom2SqlColumns = array();
  var $custom3SqlColumns = array();
  var $customLabelSqlColumns = array();
  var $employmentStatusSqlColumns = array();
  var $categorySqlColumns = array();
  var $orgId = 0;
  //  var $dbName = '';
  var $maxRows = 0;
  //  var $orgLogo = '';
  //  var $orgName = '';
  //  var $contactId = 0;
  //  var $orgDirectory = '';
  //  var $newsUrl = '';
  //  var $delinquencyNotification = 0;
  //  var $useNews = 0;
  //  var $useMotif = 0;
  var $cacheError = '';
  var $rc = 0;
  var $myqslOutput = '';

  function Organization($newOrgId = 0) {

    $this->orgListSqlColumns = unserialize(ORG_LIST_SQL_COLUMNS);
    $this->orgListLabelColumns = unserialize(ORG_LIST_LABELS);
    $this->authUserSqlColumns = unserialize(AUTH_USER_SQL_COLUMNS);
    $this->departmentSqlColumns = unserialize(DEPARTMENT_SQL_COLUMNS);
    $this->divisionSqlColumns = unserialize(DIVISION_SQL_COLUMNS);
    $this->custom1SqlColumns = unserialize(CUSTOM1_SQL_COLUMNS);
    $this->custom2SqlColumns = unserialize(CUSTOM2_SQL_COLUMNS);
    $this->custom3SqlColumns = unserialize(CUSTOM3_SQL_COLUMNS);
    $this->customLabelSqlColumns = unserialize(CUSTOM_LABEL_SQL_COLUMNS);
    $this->employmentStatusSqlColumns = unserialize(EMPLOYMENT_STATUS_SQL_COLUMNS);
    $this->categorySqlColumns = unserialize(CATEGORY_SQL_COLUMNS);
    
    if ($newOrgId) {
      $this->SetOrgId($newOrgId);
    }
  }


  function SetOrgId($newOrgId, $newOrgName = '') {
    
    $values = array();
    
    if (!is_numeric($newOrgId))
      return RC_INVALID_ARG;

    if ($this->orgId != $newOrgId && $newOrgId > 0) {
      $this->orgId = $newOrgId;

      // Get DB name from HR_Tools_Organization db or table
      $this->Open(AUTH_DB_NAME, AUTH_DB_USER, AUTH_DB_PASS);
      $this->Select(array($this->orgListSqlColumns[ORG_LIST_COLUMN_DB_NAME],
			  $this->orgListSqlColumns[ORG_LIST_COLUMN_NAME]),
		    ORG_LIST_TABLE);
      $this->Where($this->orgListSqlColumns[ORG_LIST_COLUMN_ID]." = $this->orgId");
      list($this->dbName, $this->orgName) = $this->Query(SINGLE_ROW, VALUES_FORMAT);
      $this->Close();
      if (!empty($newOrgName) && $this->orgName != $newOrgName) {
	$this->Open(AUTH_DB_NAME, SA_DB_USER, SA_DB_PASS);
	$this->Where($this->orgListSqlColumns[ORG_LIST_COLUMN_ID]." = $this->orgId");
	$this->Update($this->orgListSqlColumns[ORG_LIST_COLUMN_NAME]."=$newOrgName", ORG_LIST_TABLE);
	$this->Close();
      }
      // Make sure db is opened
      if ($this->Open($this->dbName) === FALSE) {
	error_log("Failed to open database: $this->dbName");
	return;
      }
      /*
      $this->Select('*', ORG_TABLE);
      $values = $this->Query(SINGLE_ROW, KEYS_FORMAT);

      $this->contactId = $values[$this->orgSqlColumns[ORG_COLUMN_CONTACT_ID]];
      $this->orgName = $values[$this->orgSqlColumns[ORG_COLUMN_NAME]];
      $this->orgDirectory = $values[$this->orgSqlColumns[ORG_COLUMN_DIRECTORY]];
      $this->orgLogo = $values[$this->orgSqlColumns[ORG_COLUMN_LOGO]];
      $this->newsUrl = $values[$this->orgSqlColumns[ORG_COLUMN_NEWS_URL]];
      $this->delinquencyNotification = $values[$this->orgSqlColumns[ORG_COLUMN_DELINQUENCY_NOTIFICATION]];
      $this->useNews = $values[$this->orgSqlColumns[ORG_COLUMN_USE_NEWS]];
      $this->useMotif = $values[$this->orgSqlColumns[ORG_COLUMN_USE_MOTIF]];
      */
    }
  }


  function GetLanguages($orgId) {
    
    $orgId = (int)$orgId;

    if (!$orgId) {
      error_log("Organization::GetLanguages(orgId=$orgId): Invalid organization ID");
      return;
    }

    $languageSqlColumns = unserialize(LANGUAGE_SQL_COLUMNS);
    $db = new DatabaseObject(ORG_DB_NAME_PREFIX.(int)$orgId);
    $db->Select(array($languageSqlColumns[LANGUAGE_COLUMN_ID],
		      $languageSqlColumns[LANGUAGE_COLUMN_NAME]),
		LANGUAGE_TABLE);
    if (($rows = $db->Query(NAME_VALUES)) === FALSE) {
      error_log("Organization::GetLanguages(orgId=$orgId): Unable to select languages from db: "
		.ORG_DB_NAME_PREFIX.$orgId);
      return FALSE;
    }

    return $rows;
  }


  function GetDivisions($orgId) {
    
    $orgId = (int)$orgId;

    if (!$orgId) {
      error_log("Organization::GetDivisions(orgId=$orgId): Invalid organization ID");
      return;
    }

    $divisionSqlColumns = unserialize(DIVISION_SQL_COLUMNS);
    $db = new DatabaseObject(ORG_DB_NAME_PREFIX.(int)$orgId);
    $db->Select(array($divisionSqlColumns[DIVISION_COLUMN_ID],
		      $divisionSqlColumns[DIVISION_COLUMN_NAME]),
		DIVISION_TABLE);
    $db->SortBy(DIVISION_COLUMN_NAME);
    if (($rows = $db->Query(NAME_VALUES)) === FALSE) {
      error_log("Organization::GetDivisions(orgId=$orgId): Unable to select divisions from db: "
		.ORG_DB_NAME_PREFIX.$orgId);
      return FALSE;
    }

    return $rows;
  }


  function GetDepartments($orgId) {

    $orgId = (int)$orgId;

    if (!$orgId) {
      error_log("Organization::GetDepartments(orgId=$orgId): Invalid organization ID");
      return;
    }

    $departmentSqlColumns = unserialize(DEPARTMENT_SQL_COLUMNS);
    $db = new DatabaseObject(ORG_DB_NAME_PREFIX.(int)$orgId);
    $db->Select(array($departmentSqlColumns[DEPARTMENT_COLUMN_ID],
		      $departmentSqlColumns[DEPARTMENT_COLUMN_NAME]),
		DEPARTMENT_TABLE);
    $db->SortBy(DEPARTMENT_COLUMN_NAME);
    if (($rows = $db->Query(NAME_VALUES)) === FALSE) {
      error_log("Organization::GetDepartments(orgId=$orgId): Unable to select departments from db: "
		.ORG_DB_NAME_PREFIX.$orgId);
      return FALSE;
    }

    return $rows;
  }


  function GetDomains($orgId) {

    $orgId = (int)$orgId;

    if (!$orgId) {
      error_log("Organization::GetDomains(orgId=$orgId): Invalid organization ID");
      return;
    }

    $domainSqlColumns = unserialize(DOMAIN_SQL_COLUMNS);
    $db = new DatabaseObject(ORG_DB_NAME_PREFIX.(int)$orgId);
    $db->Select(array($domainSqlColumns[DOMAIN_COLUMN_ID],
		      $domainSqlColumns[DOMAIN_COLUMN_NAME]),
		DOMAIN_TABLE);
    if (($rows = $db->Query(NAME_VALUES)) === FALSE) {
      error_log("Organization::GetDomains(orgId=$orgId): Unable to select domains from db: "
		.ORG_DB_NAME_PREFIX.$orgId);
      return FALSE;
    }

    return $rows;
  }


  function GetDomainAbbreviations($orgId) {

    $orgId = (int)$orgId;

    if (!$orgId) {
      error_log("Organization::GetDomainAbbreviations(orgId=$orgId): Invalid organization ID");
      return;
    }

    $domainSqlColumns = unserialize(DOMAIN_SQL_COLUMNS);
    $db = new DatabaseObject(ORG_DB_NAME_PREFIX.(int)$orgId);
    $db->Select(array($domainSqlColumns[DOMAIN_COLUMN_ID],
		      $domainSqlColumns[DOMAIN_COLUMN_ABBREVIATION]),
		DOMAIN_TABLE);
    if (($rows = $db->Query(NAME_VALUES)) === FALSE) {
      error_log("Organization::GetDomainAbbreviations(orgId=$orgId): Unable to select domains from db: "
		.ORG_DB_NAME_PREFIX.$orgId);
      return FALSE;
    }

    return $rows;
  }


  function GetCustomLabels($orgId) {
    
    $orgId = (int)$orgId;

    if (!$orgId) {
      error_log("Organization::GetCustomLabels(orgId=$orgId): Invalid organization ID");
      return FALSE;
    }

    $customLabelSqlColumns = unserialize(CUSTOM_LABEL_SQL_COLUMNS);
    
    $db = new DatabaseObject(ORG_DB_NAME_PREFIX.(int)$orgId);
    $db->Select(array($customLabelSqlColumns[CUSTOM_LABEL_COLUMN_CUSTOM1],
		      $customLabelSqlColumns[CUSTOM_LABEL_COLUMN_CUSTOM2],
		      $customLabelSqlColumns[CUSTOM_LABEL_COLUMN_CUSTOM3]),
		CUSTOM_LABEL_TABLE);
    if (($row = $db->Query(SINGLE_ROW)) === FALSE) {
      error_log("Organization::GetCustomLabels(orgId=$orgId): Unable to select custom labels from db: "
		.ORG_DB_NAME_PREFIX.$orgId);
      return FALSE;
    }

    return $row;
  }


  function GetEmploymentStatusValues($orgId) {
    
    $orgId = (int)$orgId;

    if (!$orgId) {
      error_log("Organization::GetEmploymentStatusValues(orgId=$orgId): Invalid organization ID");
      return;
    }

    $employmentStatusSqlColumns = unserialize(EMPLOYMENT_STATUS_SQL_COLUMNS);
    $db = new DatabaseObject(ORG_DB_NAME_PREFIX.(int)$orgId);
    $db->Select(array($employmentStatusSqlColumns[EMPLOYMENT_STATUS_COLUMN_ID],
		      $employmentStatusSqlColumns[EMPLOYMENT_STATUS_COLUMN_NAME]),
		EMPLOYMENT_STATUS_TABLE);
    if (($rows = $db->Query(NAME_VALUES)) === FALSE) {
      error_log("Organization::GetEmploymentStatusValues(orgId=$orgId): Unable to select employment status values from db: "
		.ORG_DB_NAME_PREFIX.$orgId);
      return FALSE;
    }

    return $rows;
  }


  function GetId() {
    return $this->orgId;
  }


  static public function GetContacts($orgId, $userId = NULL, $db = NULL) {

    $orgId = (int)$orgId;
    if ($userId != NULL) {
      $userId = (int)$userId;
      $usr = new User($orgId, $userId);
      // If this is only a general contact then they are not authorized
      // to specify other contacts
      if ($usr->role == ROLE_CLASS_CONTACT_SUPERVISOR)
	return array();
    }
    
    if (!$orgId) {
      error_log("Organization::GetContacts(orgId=$orgId): Invalid organization ID");
      return FALSE;
    }
    
    $userSqlColumns = unserialize(USER_SQL_COLUMNS);
    
    if ($db == NULL)
      $db = new DatabaseObject(ORG_DB_NAME_PREFIX.$orgId);

    $db->Select(array($userSqlColumns[USER_COLUMN_ID],
		      $userSqlColumns[USER_COLUMN_FULL_NAME]),
		USER_TABLE);

    // Backward compatibility
    if ($userId == NULL)
      $db->Where($userSqlColumns[USER_COLUMN_ROLE]."!=".ROLE_END_USER);
    else {
      switch($usr->role) {
      case ROLE_ORG_ADMIN:
      case ROLE_TRACK_ADMIN:
	$db->Where($userSqlColumns[USER_COLUMN_ROLE]."!=".ROLE_END_USER);
	$db->Where($userSqlColumns[USER_COLUMN_ROLE]."!=".ROLE_ORG_ADMIN);
	break;
      case ROLE_DIV_ADMIN:
	$db->Where($userSqlColumns[USER_COLUMN_DIVISION].'='.$usr->divisionId);
	break;
      case ROLE_DEPT_ADMIN:
	$db->Where($userSqlColumns[USER_COLUMN_DEPARTMENT].'='.$usr->departmentId);
	break;
      default:
	break;
      }
    }
    
    return $db->Query(NAME_VALUES, KEYS_FORMAT);
  }
  
  
  function GetData($form) {

    $values = array();

    switch($form) {
    case ORG_DATA_EDIT_DEPARTMENTS:
      $this->Select(array($this->departmentSqlColumns[DEPARTMENT_COLUMN_ID],
			  $this->departmentSqlColumns[DEPARTMENT_COLUMN_NAME]),
		    DEPARTMENT_TABLE);
      $this->SortBy(DEPARTMENT_COLUMN_NAME);
      $values = $this->Query(NAME_VALUES);
      break;
    case ORG_DATA_EDIT_DIVISIONS:
      $this->Select(array($this->divisionSqlColumns[DIVISION_COLUMN_ID],
			  $this->divisionSqlColumns[DIVISION_COLUMN_NAME]),
		    DIVISION_TABLE);
      $this->SortBy(DIVISION_COLUMN_NAME);
      $values = $this->Query(NAME_VALUES);
      break;
      /*
    case ORG_DATA_EDIT_CUSTOM1:
      $this->Select(array($this->custom1SqlColumns[CUSTOM1_COLUMN_ID],
			  $this->custom1SqlColumns[CUSTOM1_COLUMN_NAME]),
		    CUSTOM1_TABLE);
      $values = $this->Query(NAME_VALUES);
      break;
    case ORG_DATA_EDIT_CUSTOM2:
      $this->Select(array($this->custom2SqlColumns[CUSTOM2_COLUMN_ID],
			  $this->custom2SqlColumns[CUSTOM2_COLUMN_NAME]),
		    CUSTOM2_TABLE);
      $values = $this->Query(NAME_VALUES);
      break;
    case ORG_DATA_EDIT_CUSTOM3:
      $this->Select(array($this->custom3SqlColumns[CUSTOM3_COLUMN_ID],
			  $this->custom3SqlColumns[CUSTOM3_COLUMN_NAME]),
		    CUSTOM3_TABLE);
      $values = $this->Query(NAME_VALUES);
      break;
    case ORG_DATA_EDIT_CUSTOM_LABELS:
      $this->Select(array($this->customLabelSqlColumns[CUSTOM_LABEL_COLUMN_CUSTOM1],
			  $this->customLabelSqlColumns[CUSTOM_LABEL_COLUMN_CUSTOM2],
			  $this->customLabelSqlColumns[CUSTOM_LABEL_COLUMN_CUSTOM3]),
		    CUSTOM_LABEL_TABLE);
      $values = $this->Query(SINGLE_ROW, VALUES_FORMAT);
      break;
      */
    case ORG_DATA_EDIT_EMPLOYMENT_STATUS:
      $this->Select(array($this->employmentStatusSqlColumns[EMPLOYMENT_STATUS_COLUMN_ID],
			  $this->employmentStatusSqlColumns[EMPLOYMENT_STATUS_COLUMN_NAME]),
		    EMPLOYMENT_STATUS_TABLE);
      $values = $this->Query(NAME_VALUES);
      break;
    case ORG_DATA_EDIT_QUESTION_CATEGORIES:
      $this->Select(array($this->categorySqlColumns[CATEGORY_COLUMN_ID],
			  $this->categorySqlColumns[CATEGORY_COLUMN_NAME]),
		    CATEGORY_TABLE);
      $_SESSION['currentSubTab'] = SUBTAB_QUESTION_CATEGORIES;
      $this->SortBy(CATEGORY_COLUMN_NAME);
      $values = $this->Query(NAME_VALUES);
      break;
    default:
      error_log("Organization::GetData(form=$form): Unknown form value");
      break;
    }

    return $values;
  }


  function SetData($form, $key, $value) {
    
    if (DEBUG & DEBUG_CALL_TRACE)
      error_log("Organization->SetData(form = $form, key = $key, value = $value)");

    // Validation
    if ($key && (($form != 5 && !is_numeric($key)) || ($form == 5 && !is_array($key)))) {
      error_log("In Organization->SetData a key=$key not numeric");
      return RC_INVALID_DATA_FORMAT;
    }

    $value = $value;
    $table = '';
    $indexCol = '';

    switch($form) {
    case ORG_DATA_EDIT_DEPARTMENTS:
      $table = DEPARTMENT_TABLE;
      $indexCol = $this->departmentSqlColumns[DEPARTMENT_COLUMN_ID];
      $insertUpdateMap = array($this->departmentSqlColumns[DEPARTMENT_COLUMN_NAME] => $value);
      break;
    case ORG_DATA_EDIT_DIVISIONS:
      $table = DIVISION_TABLE;
      $indexCol = $this->divisionSqlColumns[DIVISION_COLUMN_ID];
      $insertUpdateMap = array($this->divisionSqlColumns[DIVISION_COLUMN_NAME] => $value);
      break;
      /*
    case ORG_DATA_EDIT_CUSTOM1:
      $table = CUSTOM1_TABLE;
      $indexCol = $this->custom1SqlColumns[CUSTOM1_COLUMN_ID];
      $insertUpdateMap = array($this->custom1SqlColumns[CUSTOM1_COLUMN_NAME] => $value);
      break;
    case ORG_DATA_EDIT_CUSTOM2:
      $table = CUSTOM2_TABLE;
      $indexCol = $this->custom2SqlColumns[CUSTOM2_COLUMN_ID];
      $insertUpdateMap = array($this->custom2SqlColumns[CUSTOM2_COLUMN_NAME] => $value);
      break;
    case ORG_DATA_EDIT_CUSTOM3:
      $table = CUSTOM3_TABLE;
      $indexCol = $this->custom3SqlColumns[CUSTOM3_COLUMN_ID];
      $insertUpdateMap = array($this->custom3SqlColumns[CUSTOM3_COLUMN_NAME] => $value);
      break;
    case ORG_DATA_EDIT_CUSTOM_LABELS: // here the value is an array of 3 elements
      $table = CUSTOM_LABEL_TABLE;
      $indexCol = '';
      $insertUpdateMap = array($this->customLabelSqlColumns[CUSTOM_LABEL_COLUMN_CUSTOM1] => $key[0],
			       $this->customLabelSqlColumns[CUSTOM_LABEL_COLUMN_CUSTOM2] => $key[1],
			       $this->customLabelSqlColumns[CUSTOM_LABEL_COLUMN_CUSTOM3] => $key[2]);
      break;
      */
    case ORG_DATA_EDIT_EMPLOYMENT_STATUS:
      $table = EMPLOYMENT_STATUS_TABLE;
      $indexCol = $this->employmentStatusSqlColumns[EMPLOYMENT_STATUS_COLUMN_ID];
      $insertUpdateMap = array($this->employmentStatusSqlColumns[EMPLOYMENT_STATUS_COLUMN_NAME] => $value);
      break;
    case ORG_DATA_EDIT_QUESTION_CATEGORIES:
      $table = CATEGORY_TABLE;
      $indexCol = $this->categorySqlColumns[CATEGORY_COLUMN_ID];
      $insertUpdateMap = array($this->categorySqlColumns[CATEGORY_COLUMN_NAME] => $value);
      break;
    default:
      error_log("Organization::SetData(form=$form,key=$key,value=$value): Invalid form type specified");
      return;
    }

    if ($key) { // if key specified then we are updating or deleting a value
      if ($indexCol)
	$this->Where("$indexCol = $key");
      if ($value) { // an update
	$this->Update($insertUpdateMap, $table);
      } else { // a delete
	// The default user does not have permissions to delete records so we need to switch
	// and then change back if there is anything else to be done after this function executes
	$this->Open($_SESSION['dbName'], ORG_ADMIN_USER, ORG_ADMIN_PASS, DB_HOST);
	$this->Where("$indexCol = $key"); // Must set this up again since Open() wipes it out
	if (!$this->Delete($table)) {
	  error_log($this->GetErrorMessage());
	}
	$this->Open($_SESSION['dbName']);
      }
    } else {
      // An add operation
      return $this->Insert($insertUpdateMap, $table);
    }

    return RC_OK;
  }


  function GetColumnLabels() {
    return $this->orgListLabelColumns;
  }


  // Get an organization list based on part or all of a username
  // This is the list of organizations in which the username exists
  function GetUserOrganizationList($userPart) {
    
    if (empty($userPart))
      return array();

  }


  // Purely an administrative function so hard setting the db is fine for now
  function GetOrganizationList($columnIds, $searchColumnId, $searchFor, $sortColumnId,
			       $sortOrder, &$pageNumber, $rowsPerPage) {

    if (empty($columnIds)) {
      if (DEBUG & DEBUG_CLASS_FUNCTIONS)
	error_log("Organization.GetOrganizationList(): No columns selected for org list so returning with empty results set");
      return array();
    }

    if ($pageNumber <= 0) {
      if (DEBUG & DEBUG_CLASS_FUNCTIONS)
	error_log("Organization.GetOrganizationList(): Invalid Page Number($pageNumber) passed, adjusting to 1");
      $pageNumber = 1;
    }

    if ($rowsPerPage <= 0) {
      if (DEBUG & DEBUG_CLASS_FUNCTIONS)
	error_log("Organization.GetOrganizationList(): Invalid Rows Per Page($rowsPerPage) passed, adjusting to 1");
      $rowsPerPage = 1;
    }

    $orgListColumns = array();
    $orgIds = array();
    $searchExact = FALSE;

    if (!($this->Open(AUTH_DB_NAME, AUTH_DB_USER, AUTH_DB_PASS))) {
      return FALSE;
    }


    // Now only select the columns currently selected in the interface for display
    for ($i = 0; $i < count($this->orgListSqlColumns); $i++) {
      if (1 << $i & $columnIds) {
        array_push($orgListColumns, $this->orgListSqlColumns[1 << $i]);
      }
    }

    // Always select the organization id
    // This just gets popped off from the results before they are sent
    // to the template because this value is used internally if not displayed
    array_push($orgListColumns, $this->orgListSqlColumns[ORG_LIST_COLUMN_ID]);

    $this->Select($orgListColumns, ORG_LIST_TABLE);
    if ($searchFor) {
      if ($searchColumnId == ORG_LIST_COLUMN_ID)
	$searchExact = TRUE;
      $this->SearchBy($this->orgListSqlColumns[$searchColumnId], $searchFor, $searchExact);
    }
    $this->SortBy($this->orgListSqlColumns[$sortColumnId], $sortOrder);
    $pageCount = $this->GetPageCount($rowsPerPage);
    if ($pageNumber > $pageCount && $pageCount > 0) {
      error_log("Organization.GetOrganizationList(): Page Number ($pageNumber) is greater than actual pages ($pageCount)");
      $pageNumber = $pageCount;
      $this->SetPage($pageNumber, $rowsPerPage);
    }
    $this->SetPage($pageNumber, $rowsPerPage);

    $results = $this->Query(MANY_ROWS, VALUES_FORMAT);

    // Add actions for this listing
    for($i = 0; $i < count($results); $i++) {
      array_push($orgIds, array_pop($results[$i]));
      // If this is the first iteration, make this field a link and set it up to set session org id
      $results[$i][0] = new TplCaption($results[$i][0]);
      $results[$i][0]->href = "javascript:parent.location.href='main.php?changeOrgId=$orgIds[$i]'";
      //$results[$i][0]->params = 'returnUrl=../admin/org_list.php&orgId='.$orgIds[$i];
      $ref1 = new TplCaption('Edit');
      $ref1->href="javascript:parent.location.href='main.php?changeOrgId=$orgIds[$i]&tab=".TAB_ORG."&subtab=".SUBTAB_ORG_INFO."'";;
      //$ref1->href='../login/change_prefs.php';
      //$ref1->params='returnUrl=../admin/org_info.php';
      //$ref1->onClick='parent.SetTab(1, 1);';
      $ref2 = new TplCaption('Delete');
      $ref2->href='org_list.php';
      $ref2->onClick="return AskDeleteRecord('$orgIds[$i], $i');";
      array_push($results[$i], array($ref1, $ref2));
    }

    return array($orgIds, $results);
  }


  // This is used in the agent to get the initial list of organization to be processed
  function GetOrganizationIds() {

    if (!($this->Open(AUTH_DB_NAME, AUTH_DB_USER, AUTH_DB_PASS))) {
      return FALSE;
    }

    $this->Select($this->orgListSqlColumns[ORG_LIST_COLUMN_ID], ORG_LIST_TABLE);
    return($this->Query(MANY_VALUES, VALUES_FORMAT));
  }


  function CreateOrg($newName, $newDirectory) {

    // Function local vars
    $oldUmask = 0;
    $cmd = 'mysql';
    $this->rc = 0;
    $mysqlOutpt = '';
    $newDirectory = escapeshellcmd($newDirectory);
    $questionDataRow = array();
    $questionDataRows = array();
    $questionSqlColumns = unserialize(QUESTION_SQL_COLUMNS);
    $masterDirectory = '';

    // Make sure directory is specified
    if (empty($newDirectory))
      return RC_ORG_DIRECTORY_EMPTY;

    // Sanity checks, this is a major operation
    if (!file_exists(ORG_SQL_FILE))
      return RC_ORG_SQL_FILE_NOT_FOUND;
      
    if (strpos(trim($newDirectory), ' '))
      return RC_ORG_DIR_WHITESPACE;

    // Make sure directory does not already exists
    if (file_exists(APPLICATION_ROOT."/organizations/".trim($newDirectory)))
      return RC_ORG_DIRECTORY_EXISTS;

    // Make sure the directory where we create is writable
    if (!is_writable("../organizations"))
      return RC_CREATE_DIRECTORY_FAILED;

    // Create the directory
    $oldUmask = umask(0);
    if (!mkdir(APPLICATION_ROOT."/organizations/$newDirectory", 0775)) {
      umask($oldUmask);
      return RC_CREATE_DIRECTORY_FAILED;
    }
    
    if (!mkdir(APPLICATION_ROOT."/organizations/$newDirectory/email", 0775)) {
      umask($oldUmask);
      return RC_CREATE_DIRECTORY_FAILED;
    }
    
    if (!mkdir(APPLICATION_ROOT."/organizations/$newDirectory/motif", 0775)) {
      umask($oldUmask);
      return RC_CREATE_DIRECTORY_FAILED;
    }
    
    if (!mkdir(APPLICATION_ROOT."/organizations/$newDirectory/news", 0775)) {
      umask($oldUmask);
      return RC_CREATE_DIRECTORY_FAILED;
    }
    
    if (!mkdir(APPLICATION_ROOT."/organizations/$newDirectory/reports", 0775)) {
      umask($oldUmask);
      return RC_CREATE_DIRECTORY_FAILED;
    }

    // Copy email templates from authoritative source
    if (system("cp -pR ".APPLICATION_ROOT.'/organizations/'.DEFAULT_ORG_EMAIL_DIR.'/email/* '.
	       APPLICATION_ROOT."/organizations/$newDirectory/email")) {
      umask($oldUmask);
      system("rm -rf ".APPLICATION_ROOT."/organizations/$newDirectory");
      return RC_COPY_EMAIL_TEMPLATES_FAILED;
    }

    // Restore old umask value
    umask($oldUmask);

    // Close any existing connections and reopen to the master database
    $this->Open(AUTH_DB_NAME, SA_DB_USER, SA_DB_PASS);

    // Add new organization to master org table to get new org ID
    $this->orgId =
      $this->Insert(array($this->orgListSqlColumns[ORG_LIST_COLUMN_NAME] => esql($newName)),
		    ORG_LIST_TABLE);

    // See if insert statement failed
    if (!$this->orgId) {
      $this->cacheError = $this->GetErrorMessage();
      system("rm -rf ".APPLICATION_ROOT."/organizations/$newDirectory");
      return RC_CREATE_ID_FAILED;
    }

    // Assign our new directory name
    $this->dbName = ORG_DB_NAME_PREFIX.$this->orgId;

    // Now we have to go back and update the same table with the new database name
    // This is a lame architecture and at some point needs to be gutted!
    // TODO: redesign the whole HR_Tools_Authentication and HR_Tools_Organization
    // master database concepts
    $this->Where($this->orgListSqlColumns[ORG_LIST_COLUMN_ID]."=".$this->orgId);
    if (!$this->Update(array($this->orgListSqlColumns[ORG_LIST_COLUMN_DB_NAME] => $this->dbName),
		       ORG_LIST_TABLE)) {
      $this->cacheError = $this->GetErrorMessage();
      $this->Where($this->orgListSqlColumns[ORG_LIST_COLUMN_ID]."=".$this->orgId);
      $this->Delete(ORG_LIST_TABLE);
      system("rm -rf ".APPLICATION_ROOT."/organizations/$newDirectory");
      return RC_UPDATE_DB_NAME_FAILED;
    }

    // Create the new organization database
    if (!$this->CreateDb(ORG_DB_NAME_PREFIX.$this->orgId)) {
      if ($this->GetErrorNumber == 1007)
	error_log('*** ERROR: Database ('.ORG_DB_NAME_PREFIX.$this->orgId.') already exists, not deleting for safety...');
      else {
	$this->cacheError = $this->GetErrorMessage();
	$this->Where($this->orgListSqlColumns[ORG_LIST_COLUMN_ID]."=".$this->orgId);
	$this->Delete(ORG_LIST_TABLE);
	system("rm -rf ".APPLICATION_ROOT."/organizations/$newDirectory");
      }
      return RC_CREATE_DB_FAILED;
    }
    
    // Check for valid mysql executable
    if (MYSQL_CLIENT_PATH && file_exists(MYSQL_CLIENT_PATH) &&
	is_executable(MYSQL_CLIENT_PATH)) {
      $cmd = MYSQL_CLIENT_PATH;
    } else {
      // Otherwise, try to find it in th path
      $cmd = 'mysql';
      $cmd = `which $cmd 2> /dev/null`;
      if (!$cmd) {
	$this->Where($this->orgListSqlColumns[ORG_LIST_COLUMN_ID]."=".$this->orgId);
	$this->Delete(ORG_LIST_TABLE);
	system("rm -rf ".APPLICATION_ROOT."/organizations/$newDirectory");
	$this->DropDb($this->dbName);
	return RC_MYSQL_CLI_NOT_FOUND;
      }
    }

    // Execute the SQL sript
    // At first glance, the & at the end of this command may seem entirely unnecessary.
    // The problem is this.  If it is left off,  the system call executes the command
    // in a shell and keeps it in a shell until finished.  The return is designed to
    // be examined using some of the MACROS available in other languages see man
    // system for more info.  Basically the upper 2 bits or so of the return code are
    // used to signify things other than the programs actual return code.  So for a
    // return of 0 in a program you will actually see something like 128.  Using the &
    // drops the shell when the command is backgrounded and upon return you get the
    // actual unmodified return code.  Coincidentally, because the shell process is
    // dropped this is also a tad bit more efficient.
    if (preg_match("/^(\d{1,3}\.){3}\d{1,3}$/", DB_HOST)) {
      $cmd .= escapeshellcmd($cmd." -h ".DB_HOST." -u ".SA_DB_USER." -p".SA_DB_PASS." ".ORG_DB_NAME_PREFIX.$this->orgId)
	." < ".ORG_SQL_FILE." 2>&1 &";
    } else {
      $server = str_replace(':', '', DB_HOST);
      $cmd .= escapeshellcmd($cmd." -S $server -u ".SA_DB_USER." -p".SA_DB_PASS." ".ORG_DB_NAME_PREFIX.$this->orgId)
	." < ".ORG_SQL_FILE." 2>&1 &";
    }
    $this->mysqlOutput = system($cmd, $this->rc);

    // Make sure all went well
    if ($this->rc || $this->mysqlOutput) {
      $this->mysqlOutput = "Command($cmd) produced: ".$this->mysqlOutput;
      $this->Where($this->orgListSqlColumns[ORG_LIST_COLUMN_ID]."=".$this->orgId);
      $this->Delete(ORG_LIST_TABLE);
      system("rm -rf ".APPLICATION_ROOT."/organizations/$newDirectory");
      $this->DropDb(ORG_DB_NAME_PREFIX.$this->orgId);
      return RC_MYSQL_SCRIPT_FAILED;
    }

    // Close any existing connections and reopen to the master database
    if (!$this->Open(AUTHORITATIVE_QUESTION_DB, SA_DB_USER, SA_DB_PASS)) {
      $this->cacheError = $this->GetErrorMessage();
      $this->SetDb(AUTH_DB_NAME);
      $this->Where($this->orgListSqlColumns[ORG_LIST_COLUMN_ID]."=".$this->orgId);
      $this->Delete(ORG_LIST_TABLE);
      system("rm -rf ".APPLICATION_ROOT."/organizations/$newDirectory");
      $this->DropDb($this->ORG_DB_NAME_PREFIX.$this->orgId);

      return RC_SELECT_DB_FAILED;
    }
    $this->Select('*', QUESTION_TABLE);
    $this->Where($questionSqlColumns[QUESTION_COLUMN_ID].'>=400 AND '.
		 $questionSqlColumns[QUESTION_COLUMN_ID].'< 100000');
    $questionDataRows = $this->Query(MANY_ROWS, KEYS_FORMAT);

    // Make this new org the org for this object
    if (!$this->SetDb(ORG_DB_NAME_PREFIX.$this->orgId)) {
      $this->cacheError = $this->GetErrorMessage();
      $this->SetDb(AUTH_DB_NAME);
      $this->Where($this->orgListSqlColumns[ORG_LIST_COLUMN_ID]."=".$this->orgId);
      $this->Delete(ORG_LIST_TABLE);
      system("rm -rf ".APPLICATION_ROOT."/organizations/$newDirectory");
      $this->DropDb(ORG_DB_NAME_PREFIX.$this->orgId);

      return RC_SELECT_DB_FAILED;
    }

    $config = new Config(AUTHORITATIVE_ORG_ID);
    $masterDirectory = $config->GetValue(CONFIG_ORG_DIRECTORY);
    foreach ($questionDataRows as $questionDataRow) {
      if ($this->Insert($questionDataRow, QUESTION_TABLE) === FALSE) {
	$this->cacheError = $this->GetErrorMessage();
	$this->SetDb(AUTH_DB_NAME);
	$this->Where($this->orgListSqlColumns[ORG_LIST_COLUMN_ID]."=".$this->orgId);
	$this->Delete(ORG_LIST_TABLE);
	system("rm -rf ".APPLICATION_ROOT."/organizations/$newDirectory");
	$this->DropDb($this->dbName);
	
	return RC_ADD_QUESTION_FAILED;
      }
      $linkToFile = $questionDataRow[$questionSqlColumns[QUESTION_COLUMN_MEDIA_FILE_PATH]];
      if (!empty($linkToFile)) {
	$linkToDir = "../../$masterDirectory/motif";
	$linkFromDir = APPLICATION_ROOT."/organizations/$newDirectory/motif";
	$rc = LinkToFile($linkFromDir, $linkToDir, $linkToFile);
	if ($rc != RC_OK) {
	  error_log("Organization.CreateDb(newName=$newName, newDirectory=$newDirectory): ".
		    "LinkToFile failed with rc=$rc, please see previous log messages for reason");
	  return $rc;
	}
      }
    }
    
    $config->SetConfigOrgId($this->orgId);
    $config->SetValue(CONFIG_ORG_NAME, esql($newName), 1);
    $config->SetValue(CONFIG_ORG_DIRECTORY, esql($newDirectory), 1);
    $config->SetValue(CONFIG_ORG_CONTACT_ID, 0, 1);
    $config->SetValue(CONFIG_DELINQUENCY_NOTIFICATION, 0, 1);
    $config->SetValue(CONFIG_ORG_NEWS_URL, '', 1);
    $config->SetValue(CONFIG_USE_NEWS_URL, 0, 1);
    $config->SetValue(CONFIG_USE_DEFAULT_MOTIF, 1, 1);
    $config->SetValue(CONFIG_TOP_LEFT_BACK, '', 1);
    $config->SetValue(CONFIG_TOP_LEFT_BACK_REPEAT, 0, 1);
    $config->SetValue(CONFIG_TOP_LEFT_FRONT, '', 1);
    $config->SetValue(CONFIG_TOP_MIDDLE_BACK, '', 1);
    $config->SetValue(CONFIG_TOP_MIDDLE_BACK_REPEAT, 1, 1);
    $config->SetValue(CONFIG_TOP_MIDDLE_FRONT, '', 1);
    $config->SetValue(CONFIG_TOP_RIGHT_BACK, '', 1);
    $config->SetValue(CONFIG_TOP_RIGHT_BACK_REPEAT, 0, 1);
    $config->SetValue(CONFIG_TOP_RIGHT_FRONT, '', 1);
    $config->SetValue(CONFIG_DISPLAY_CHOICES, 'right', 1);
    $config->SetValue(CONFIG_DISPLAY_CHOICES_USE_QUESTION_SETTING, 0, 1);
    $config->SetValue(CONFIG_LICENSED_PRINT_QUESTIONS, 0, 1);
    $config->SetValue(CONFIG_SHOW_USER_THAT_ANSWERED, 0, 1);
    $config->SetValue(CONFIG_TRACK_ADMIN_SUMMARY_DEFAULT, 0, 1);
    $config->SetValue(CONFIG_LICENSED_HR_CONTENT, 0, 1);
    $config->SetValue(CONFIG_LICENSED_AB1825_CONTENT, 0, 1);
    $config->SetValue(CONFIG_LICENSED_EDU_CONTENT, 0, 1);
    $config->SetValue(CONFIG_UI_THEME, 'Default', 1);
    $config->SetValue(CONFIG_EMAIL_FORMAT, 'html', 1);

    // Make this new org the org for this object
    $this->SetOrgId($this->orgId);

    return RC_OK;
  }


  function DeleteOrg($orgDirectory) {
    
    $rc = 0;
    $orgId = $this->orgId;
    $dbName = $this->dbName;
    $this->cacheError = '';

    // Connect as sa account for drop privs
    $this->Open(AUTH_DB_NAME, SA_DB_USER, SA_DB_PASS);

    // Delete the users for this organization
    $this->Where($this->authUserSqlColumns[AUTH_USER_COLUMN_ORGANIZATION_ID]."=".$this->orgId);
    if (!$this->Delete(AUTH_USER_TABLE))
      $this->cacheError .= "Failed to delete this organizations users from master user table<BR>\n";

    // Delete this organization from the organization table
    $this->Where($this->orgListSqlColumns[ORG_LIST_COLUMN_ID]."=".$this->orgId);
    if (!$this->Delete(ORG_LIST_TABLE))
      $this->cacheError .= "Delete Organization from master organization table failed<BR>\n";

    // Drop the organizations database
    if (!$this->DropDb($dbName))
      $this->cacheError .= "Drop DB Failed<BR>\n";

    if (strlen($orgDirectory) > 0)
      system("rm -rf ".escapeshellarg(APPLICATION_ROOT."/organizations/$orgDirectory"), $rc);
    else
      error_log("Organization.DeleteOrg(): Empty organization directory, not deleting any org dirs");

    if ($rc)
      $this->cacheError .= "Unable to remove organization directory: $orgDirectory<BR>\n";
    
    if ($this->cacheError)
      return RC_DELETE_ORG_FAILED;

    return RC_OK;
  }


  function UpdateInfo() {
    
    // Force the update since there is no where clause and its basically a single row
    //if (!$this->Update(array($this->orgSqlColumns[ORG_COLUMN_NAME] => $this->orgName),
    //		       ORG_LIST_TABLE, TRUE))
    //     return RC_UPDATE_INFO_FAILED;

    if ($this->orgId) {
      $config = new Config($this->orgId);
      $config->SetValue('orgName', $this->orgName);
      
      $this->Open(AUTH_DB_NAME, AUTH_DB_USER, AUTH_DB_PASS);
      $this->Where($this->orgListSqlColumns[ORG_LIST_COLUMN_ID]." = ".$this->orgId);
      if (!$this->Update(array($this->orgListSqlColumns[ORG_LIST_COLUMN_NAME] => $this->orgName),
			 ORG_LIST_TABLE)) {
	$this->cacheError = $this->GetErrorMessage();
	return RC_UPDATE_INFO_FAILED;
      }
    }

    return RC_OK;
  }

}
