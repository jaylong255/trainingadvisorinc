<?php

require_once('DatabaseObject.php');
require_once('Config.php');
require_once('RecurrenceObject.php');
require_once('Organization.php');
require_once('agent/agent.php');

class Track extends DatabaseObject {

  var $trackListSqlColumns = array();
  var $trackListLabelColumns = array();
  var $trackSqlColumns = array();
  var $queueSqlColumns = array();
  var $trackId = '';
  var $dbName = '';
  var $orgId = 0;
  var $maxRows = 0;
  var $trackName = '';
  var $supervisorId = 0;
  var $supervisors = array();
  var $isManagement = FALSE;
  var $isBinding = FALSE;
  var $isLooping = FALSE;
  var $isAb1825 = FALSE;
  var $emailReplyTo = '';
  var $recurrence = '';
  var $strRecurrence = '';
  var $startDate = '';
  var $delinquencyNotification = 0;
  var $lastQuestionNumber = 0;
  var $lastQuestionDate = '';
  //  var $trackNewsUrl = '';
  //  var $useMotif = 0;
  //  var $cacheError = '';
  var $rc = 0;
  //  var $myqslOutput = '';
  //  var $assignmentTrackId = 0;

  function Track($orgId, $newTrackId = 0) {

    $this->trackListSqlColumns = unserialize(TRACK_LIST_SQL_COLUMNS);
    $this->trackListLabelColumns = unserialize(TRACK_LIST_LABELS);
    $this->trackSqlColumns = unserialize(TRACK_SQL_COLUMNS);
    $this->queueSqlColumns = unserialize(QUEUE_SQL_COLUMNS);
    $this->orgId = (int)$orgId;
    $this->DatabaseObject(ORG_DB_NAME_PREFIX.$this->orgId);
    
    if (!$this->initialized)
      return FALSE;

    if (is_numeric($newTrackId) && $newTrackId)
      $this->SetTrackId($newTrackId);

    // Get this list either way
    $this->supervisors = Organization::GetContacts($this->orgId);

  }


  function SetTrackId($newTrackId) {
    
    $values = array();
    
    if ($this->trackId != $newTrackId && $newTrackId > 0) {
      $this->trackId = $newTrackId;
      // Get DB name from HR_Tools_Track db or table
      $this->DatabaseObject(ORG_DB_NAME_PREFIX.$this->orgId);
      $this->Select('*', TRACK_TABLE);
      $this->Where($this->trackSqlColumns[TRACK_COLUMN_ID].'='.$newTrackId);
      $values = $this->Query(SINGLE_ROW, KEYS_FORMAT);
      $this->trackName = $values[$this->trackSqlColumns[TRACK_COLUMN_NAME]];
      $this->supervisorId = $values[$this->trackSqlColumns[TRACK_COLUMN_SUPERVISOR_ID]];
      $this->isManagement = $values[$this->trackSqlColumns[TRACK_COLUMN_IS_MANAGEMENT]];
      $this->isBinding = $values[$this->trackSqlColumns[TRACK_COLUMN_IS_BINDING]];
      $this->isLooping = $values[$this->trackSqlColumns[TRACK_COLUMN_IS_LOOPING]];
      $this->isAb1825 = $values[$this->trackSqlColumns[TRACK_COLUMN_IS_AB1825]];
      $this->emailReplyTo = $values[$this->trackSqlColumns[TRACK_COLUMN_EMAIL_REPLY_TO]];
      $this->recurrence = $values[$this->trackSqlColumns[TRACK_COLUMN_RECURRENCE]];
      $this->startDate = explode(' ', $values[$this->trackSqlColumns[TRACK_COLUMN_START_DATE]]);
      $this->startDate = $this->startDate[0];
      $this->delinquencyNotification = $values[$this->trackSqlColumns[TRACK_COLUMN_DELINQUENCY_NOTIFICATION]];
      $this->lastQuestionNumber = $values[$this->trackSqlColumns[TRACK_COLUMN_LAST_QUESTION_NUMBER]];
      $this->lastQuestionDate = $values[$this->trackSqlColumns[TRACK_COLUMN_LAST_QUESTION_DATE]];
      //$this->trackNewsUrl = $values[$this->trackSqlColumns[TRACK_COLUMN_TRACK_NEWS_URL]];
      $obj = new RecurrenceObject();
      $obj->ParseTokenString($this->recurrence);
      $this->strRecurrence = $obj->CreateVerboseTokenString();
    }
  }


  // Used in user_edit.php
  function GetTracks($orgId) {
    
    $orgId = (int)$orgId;

    if (!$orgId) {
      error_log("Track.GetTracks(orgId=$orgId): Invalid organization ID");
      return FALSE;
    }

    $trackSqlColumns = unserialize(TRACK_SQL_COLUMNS);
    $db = new DatabaseObject(ORG_DB_NAME_PREFIX.$orgId);
    $db->Select(array($trackSqlColumns[TRACK_COLUMN_ID],
		      $trackSqlColumns[TRACK_COLUMN_NAME]),
		TRACK_TABLE);
    if (($rows = $db->Query(NAME_VALUES)) === FALSE) {
      error_log("Track::GetTracks(orgId=$orgId): Unable to select tracks from db: ".ORG_DB_NAME_PREFIX.$orgId);
      return FALSE;
    }

    return $rows;
  }


  // Used in user_edit.php
  function GetMostRecentTrack($orgId) {
    
    $orgId = (int)$orgId;

    if (!$orgId) {
      error_log("Track.GetTracks(orgId=$orgId): Invalid organization ID");
      return FALSE;
    }

    $trackSqlColumns = unserialize(TRACK_SQL_COLUMNS);
    $db = new DatabaseObject(ORG_DB_NAME_PREFIX.$orgId);
    $db->Select($trackSqlColumns[TRACK_COLUMN_NAME], TRACK_TABLE);
    $db->SortBy($trackSqlColumns[TRACK_COLUMN_START_DATE], TRUE);
    $db->Limit(1);
    if (($val = $db->Query(SINGLE_VALUE, VALUES_FORMAT)) === FALSE) {
      error_log("Track::GetTracks(orgId=$orgId): Unable to select tracks from db: ".ORG_DB_NAME_PREFIX.$orgId);
      return FALSE;
    }

    return $val;
  }


  function GetId() {
    return $this->trackId;
  }

  
  function GetName() {
    return $this->trackName;
  }


  function SetName($newName) {
    //    $this->Where($this->trackSqlColumns[TRACK_COLUMN_ID].'='.$newTrackId);
    //    $this->Update(array($this->trackSqlColumns[TRACK_COLUMN_NAME] => $newName));
    $this->trackName = $newName;
  }


  // Assumes database connection already established.
  function GetContacts() {

    if (!$this->orgId) {
      error_log("Track::GetContacts(orgId=$orgId): Invalid organization ID");
      return FALSE;
    }
    
    $userSqlColumns = unserialize(USER_SQL_COLUMNS);
    
    //$db = new DatabaseObject(ORG_DB_NAME_PREFIX.$this->orgId);
    //$db->Open(ORG_DB_NAME_PREFIX.$this->orgId);
    $this->Select(array($userSqlColumns[USER_COLUMN_ID],
			$userSqlColumns[USER_COLUMN_FULL_NAME]),
		  USER_TABLE);
    $this->Where($userSqlColumns[USER_COLUMN_IS_CONTACT]."=1");
    
    return $this->Query(NAME_VALUES, KEYS_FORMAT);
  }
  
  
  function GetDirectory() {
    return $this->trackDirectory;
  }


  function SetDirectory($newDirectory) {

    // Make sure the user can not control paths
    $newDirectory = str_replace('.', '', $newDirectory);
    $newDirectory = str_replace('..', '', $newDirectory);

    // Protect ourselves from all manner of nastiness
    $newDirectory = escapeshellcmd($newDirectory);

    // Finally set the value
    $this->trackDirectory = $newDirectory;
  }


  function GetLogo() {
    return $this->trackLogo;
  }


  function GetNewsUrl() {
    return $this->newsUrl;
  }


  function UseMotif() {
    return $this->useMotif;
  }


  function GetMotif(&$topLeftBg, &$topMiddleBg, &$topRightBg, &$topLeftImg) {

    if (file_exists(APPLICATION_ROOT."/tracks/$this->trackDirectory/motif/header_middle.gif")) {
      $topLeftBg = URL_ROOT."/tracks/$this->trackDirectory/motif/header_middle.gif";
      $topMiddleBg = URL_ROOT."/tracks/$this->trackDirectory/motif/header_middle.gif";
    } else {
      $topLeftBg = URL_ROOT."/themes/".$_SESSION['uiTheme']."/presentation/header_middle.gif";
      $topMiddleBg = URL_ROOT."/themes/".$_SESSION['uiTheme']."/presentation/header_middle.gif";
    }

    if (file_exists(APPLICATION_ROOT."/tracks/$this->trackDirectory/motif/header_right.gif")) {
      $topRightBg = URL_ROOT."/tracks/$this->trackDirectory/motif/header_right.gif";
    } else {
      $topRightBg = URL_ROOT."/themes/".$_SESSION['uiTheme']."/presentation/header_right.gif";
    }

    if (file_exists(APPLICATION_ROOT."/tracks/$this->trackDirectory/motif/top_left_img.gif")) {
      $topLeftImg = URL_ROOT."/tracks/$this->trackDirectory/motif/top_left_img.gif";
    } else {
      $topLeftImg = URL_ROOT."/themes/".$_SESSION['uiTheme']."/presentation/wtp_logo.gif";
    }
  }


  function GetContactId() {
    return $this->contactId;
  }


  function SetContactId($newContactId) {
    
    if (empty($newContactId)) {
      $this->contactId = 'NULL';
    }

    if (!is_numeric($newContactId) || $newContactId <= 0) {
      error_log("Track.SetContactId($newContactId): invalid contact id");
      return;
    }

    $this->contactId = $newContactId;
    //return $this->Update(array($trackSqlColumns[TRACK_COLUMN_CONTACT_ID] => $this->contactId), TRACK_TABLE);
  }


  function GetDelinquencyNotifications() {
    if ($this->delinquencyNotification == 0)
      return '';
    return $this->delinquencyNotification;
  }

  
  function SetDelinquencyNotifications($newDelinquencies) {
    if ($newDelinquencies == '')
      $newDelinquencies = 0;
    if (!is_numeric($newDelinquencies) || $newDelinquencies < 0)
      return;
    $this->delinquencyNotification = $newDelinquencies;
  }


  function GetData($form) {

    $values = array();

    switch($form) {
    case TRACK_DATA_EDIT_DEPARTMENTS:
      $this->Select(array($this->departmentSqlColumns[DEPARTMENT_COLUMN_ID],
			  $this->departmentSqlColumns[DEPARTMENT_COLUMN_NAME]),
		    DEPARTMENT_TABLE);
      $values = $this->Query(NAME_VALUES);
      break;
    case TRACK_DATA_EDIT_DIVISIONS:
      $this->Select(array($this->divisionSqlColumns[DIVISION_COLUMN_ID],
			  $this->divisionSqlColumns[DIVISION_COLUMN_NAME]),
		    DIVISION_TABLE);
      $values = $this->Query(NAME_VALUES);
      break;
    case TRACK_DATA_EDIT_CUSTOM1:
      $this->Select(array($this->custom1SqlColumns[CUSTOM1_COLUMN_ID],
			  $this->custom1SqlColumns[CUSTOM1_COLUMN_NAME]),
		    CUSTOM1_TABLE);
      $values = $this->Query(NAME_VALUES);
      break;
    case TRACK_DATA_EDIT_CUSTOM2:
      $this->Select(array($this->custom2SqlColumns[CUSTOM2_COLUMN_ID],
			  $this->custom2SqlColumns[CUSTOM2_COLUMN_NAME]),
		    CUSTOM2_TABLE);
       $values = $this->Query(NAME_VALUES);
      break;
    case TRACK_DATA_EDIT_CUSTOM3:
      $this->Select(array($this->custom3SqlColumns[CUSTOM3_COLUMN_ID],
			  $this->custom3SqlColumns[CUSTOM3_COLUMN_NAME]),
		    CUSTOM3_TABLE);
      $values = $this->Query(NAME_VALUES);
      break;
    case TRACK_DATA_EDIT_CUSTOM_LABELS:
      $this->Select(array($this->customLabelSqlColumns[CUSTOM_LABEL_COLUMN_CUSTOM1],
			  $this->customLabelSqlColumns[CUSTOM_LABEL_COLUMN_CUSTOM2],
			  $this->customLabelSqlColumns[CUSTOM_LABEL_COLUMN_CUSTOM3]),
		    CUSTOM_LABEL_TABLE);
      $values = $this->Query(SINGLE_ROW, VALUES_FORMAT);
      break;
    case TRACK_DATA_EDIT_EMPLOYMENT_STATUS:
      $this->Select(array($this->employmentStatusSqlColumns[EMPLOYMENT_STATUS_COLUMN_ID],
			  $this->employmentStatusSqlColumns[EMPLOYMENT_STATUS_COLUMN_NAME]),
		    EMPLOYMENT_STATUS_TABLE);
      $values = $this->Query(NAME_VALUES);
      break;
    default:
      break;
    }

    return $values;
  }


  function SetData($form, $key, $value) {
    
    if (DEBUG & DEBUG_CALL_TRACE)
      error_log("Track->SetData(form = $form, key = $key, value = $value)");

    // Validation
    if ($key && (($form != 5 && !is_numeric($key)) || ($form == 5 && !is_array($key)))) {
      error_log("In Track->SetData a key=$key not numeric");
      return RC_INVALID_DATA_FORMAT;
    }

    $value = esql($value);
    $table = '';
    $indexCol = '';

    switch($form) {
    case TRACK_DATA_EDIT_DEPARTMENTS:
      $table = DEPARTMENT_TABLE;
      $indexCol = $this->departmentSqlColumns[DEPARTMENT_COLUMN_ID];
      $insertUpdateMap = array($this->departmentSqlColumns[DEPARTMENT_COLUMN_NAME] => $value);
      break;
    case TRACK_DATA_EDIT_DIVISIONS:
      $table = DIVISION_TABLE;
      $indexCol = $this->divisionSqlColumns[DIVISION_COLUMN_ID];
      $insertUpdateMap = array($this->divisionSqlColumns[DIVISION_COLUMN_NAME] => $value);
      break;
    case TRACK_DATA_EDIT_CUSTOM1:
      $table = CUSTOM1_TABLE;
      $indexCol = $this->custom1SqlColumns[CUSTOM1_COLUMN_ID];
      $insertUpdateMap = array($this->custom1SqlColumns[CUSTOM1_COLUMN_NAME] => $value);
      break;
    case TRACK_DATA_EDIT_CUSTOM2:
      $table = CUSTOM2_TABLE;
      $indexCol = $this->custom2SqlColumns[CUSTOM2_COLUMN_ID];
      $insertUpdateMap = array($this->custom2SqlColumns[CUSTOM2_COLUMN_NAME] => $value);
      break;
    case TRACK_DATA_EDIT_CUSTOM3:
      $table = CUSTOM3_TABLE;
      $indexCol = $this->custom3SqlColumns[CUSTOM3_COLUMN_ID];
      $insertUpdateMap = array($this->custom3SqlColumns[CUSTOM3_COLUMN_NAME] => $value);
      break;
    case TRACK_DATA_EDIT_CUSTOM_LABELS: // here the value is an array of 3 elements
      $table = CUSTOM_LABEL_TABLE;
      $indexCol = '';
      $insertUpdateMap = array($this->customLabelSqlColumns[CUSTOM_LABEL_COLUMN_CUSTOM1] => $key[0],
			       $this->customLabelSqlColumns[CUSTOM_LABEL_COLUMN_CUSTOM2] => $key[1],
			       $this->customLabelSqlColumns[CUSTOM_LABEL_COLUMN_CUSTOM3] => $key[2]);
      break;
    case TRACK_DATA_EDIT_EMPLOYMENT_STATUS:
      $table = EMPLOYMENT_STATUS_TABLE;
      $indexCol = $this->employmentStatusSqlColumns[EMPLOYMENT_STATUS_COLUMN_ID];
      $insertUpdateMap = array($this->employmentStatusSqlColumns[EMPLOYMENT_STATUS_COLUMN_NAME] => $value);
      break;
    default:
      error_log("Invalid form type of: $form specified when calling Track->SetData()");
      return;
    }

    if ($key) { // if key specified then we are updating or deleting a value
      if ($indexCol)
	$this->Where("$indexCol = $key");
      if ($value) { // an update
	$this->Update($insertUpdateMap, $table);
      } else { // a delete
	// The default user does not have permissions to delete records so we need to switch
	// and then change back if there is anything else to be done after this function executes
	$this->Open($_SESSION['dbName'], TRACK_ADMIN_USER, TRACK_ADMIN_PASS, DB_HOST);
	$this->Where("$indexCol = $key"); // Must set this up again since Open() wipes it out
	if (!$this->Delete($table)) {
	  error_log("Track.SetData($form,$key,$value): ".$this->GetErrorMessage());
	}
	$this->Open($_SESSION['dbName']);
      }
    } else {
      // An add operation
      return $this->Insert($insertUpdateMap, $table);
    }

    return RC_OK;
  }


  function GetColumnLabels() {
    return $this->trackListLabelColumns;
  }

  function GetFaqColumnLabels() {
    return unserialize(FAQ_LIST_LABELS);
  }


  // This returns the labels that are to be used for the column headers in
  // the question list display.
  function GetQueueColumnLabels() {
    return unserialize(QUEUE_LIST_LABELS);
  }


  // This returns the labels that are to be used for the column headers in
  // the question list display.
  function GetAssignmentColumnLabels() {
    return unserialize(ASSIGNMENT_LIST_LABELS);
  }


  // This returns the labels that are to be used for the column headers in
  // the question list display.
  function GetTrackParticipants() {
    $userSqlColumns = unserialize(USER_SQL_COLUMNS);
    $participantSqlColumns = unserialize(PARTICIPANT_SQL_COLUMNS);
    $this->Select(array($userSqlColumns[USER_COLUMN_FIRST_NAME], $userSqlColumns[USER_COLUMN_LAST_NAME],
			$userSqlColumns[USER_COLUMN_EMAIL]),
		  USER_TABLE);
    $this->LeftJoinUsing(PARTICIPANT_TABLE, $participantSqlColumns[PARTICIPANT_COLUMN_USER_ID]);
    $this->Where($participantSqlColumns[PARTICIPANT_COLUMN_TRACK_ID].'='.$this->trackId);
    return $this->Query(MANY_ROWS, VALUES_FORMAT);
  }


  // Purely an administrative function so hard setting the db is fine for now
  function GetTrackList($columnIds, $searchColumnId, $searchFor, $sortColumnId,
			$sortOrder, &$pageNumber, $rowsPerPage, $userId) {

    if (empty($columnIds)) {
      if (DEBUG & DEBUG_CLASS_FUNCTIONS)
	error_log("No columns selected for track list so immediately returning with empty result sets");
      return array();
    }

    if ($pageNumber <= 0) {
      if (DEBUG & DEBUG_CLASS_FUNCTIONS)
	error_log("Track.GetTrackList(): Invalid Page Number($pageNumber) passed, adjusting to 1");
      $pageNumber = 1;
    }

    if ($rowsPerPage <= 0) {
      if (DEBUG & DEBUG_CLASS_FUNCTIONS)
	error_log("Track.GetTrackList(): Invalid Rows Per Page($rowsPerPage) passed, adjusting to 1");
      $rowsPerPage = 1;
    }

    $trackListColumns = array();
    //$trackListSqlColumns = unserialize(TRACK_LIST_SQL_COLUMNS);
    $userSqlColumns = unserialize(USER_SQL_COLUMNS);
    $trackIds = array();
    $searchExact = FALSE;
    $joinArray = array();
    $usr = new User($this->orgId, $userId);

    if (!($this->Open($_SESSION['dbName'], DB_USER, DB_PASS))) {
      return FALSE;
    }

    // Now only select the columns currently selected in the interface for display
    for ($i = 0; $i < count($this->trackListSqlColumns); $i++) {
      if (1 << $i & $columnIds) {
	if ($this->trackListSqlColumns[1 << $i] == $this->trackListSqlColumns[TRACK_LIST_COLUMN_SUPERVISOR]) {
	  array_push($trackListColumns, USER_TABLE.'.'.$userSqlColumns[USER_COLUMN_FULL_NAME]);
	  $joinArray[USER_TABLE] = array(USER_TABLE.'.'.$userSqlColumns[USER_COLUMN_ID], '=',
					 TRACK_TABLE.'.'.$this->trackListSqlColumns[TRACK_LIST_COLUMN_SUPERVISOR]);
	} else {
	  array_push($trackListColumns, TRACK_TABLE.'.'.$this->trackListSqlColumns[1 << $i]);
	}
      }
    }

    array_push($trackListColumns, TRACK_TABLE.'.'.$this->trackListSqlColumns[TRACK_LIST_COLUMN_ID]);

    $this->Select($trackListColumns, TRACK_LIST_TABLE);
    $this->LeftJoinUsing($joinArray);
    if ($searchFor) {
      if ($searchColumnId == TRACK_COLUMN_ID)
	$searchExact = TRUE;
      if ($searchColumnId == TRACK_COLUMN_SUPERVISOR_ID)
	$this->SearchBy(USER_TABLE.'.'.$userSqlColumns[USER_COLUMN_FULL_NAME], $searchFor, $searchExact);
      else
	$this->SearchBy(TRACK_TABLE.'.'.$this->trackListSqlColumns[$searchColumnId], $searchFor, $searchExact);
    }
    // If this persons role  is one of limited scope like div admin or dept admin then limit list
    switch($usr->role) {
    case ROLE_DIVISION_ADMIN:
      $this->SearchBy(USER_TABLE.'.'.$userSqlColumns[USER_COLUMN_DIVISION_ID], $usr->divisionId, TRUE);
      break;
    case ROLE_DEPARTMENT_ADMIN:
      $this->SearchBy(USER_TABLE.'.'.$userSqlColumns[USER_COLUMN_DEPARTMENT_ID], $usr->departmentId, TRUE);
      break;
    case ROLE_CLASS_CONTACT_SUPERVISOR:
      $this->SearchBy(USER_TABLE.'.'.$userSqlColumns[USER_COLUMN_ID], $usr->userId, TRUE);
      break;
    case ROLE_CLASS_CONTACT_SUPERVISOR: // no additional restrictions for other roles
    case ROLE_ORG_ADMIN:
    default:
      break;
    }
    $this->SortBy($this->trackListSqlColumns[$sortColumnId], $sortOrder);
    $this->SetPage($pageNumber, $rowsPerPage);
    $pageCount = $this->GetPageCount($rowsPerPage);
    if ($pageNumber > $pageCount && $pageCount > 0) {
      error_log("Track.GetTrackList(): Page Number ($pageNumber) is greater than actual pages ($pageCount)");
      $pageNumber = $pageCount;
      $this->SetPage($pageNumber, $rowsPerPage);
    }
    $results = $this->Query(MANY_ROWS, VALUES_FORMAT);

    if ($results !== FALSE) {
      // Add actions for this listing
      for($i = 0; $i < count($results); $i++) {
	array_push($trackIds, (int)array_pop($results[$i]));
	$ref1 = new TplCaption('Edit');
	$ref1->href='track_edit.php';
	$ref1->onClick='parent.SetTab(3, 1);';
	$ref2 = new TplCaption('Delete');
	$ref2->href='track_list.php';
	$ref2->onClick="return AskDeleteRecord('$trackIds[$i]');";
	array_push($results[$i], array($ref1, $ref2));
      }
      
      return array($trackIds, $results);
    } else {
      return FALSE;
    }
  }


  // This function will assign the given questionId to this track
  function AssignQuestion($newQuestionId, $newGroupId = 0) {

    if (!is_numeric($newQuestionId) || !$newQuestionId) {
      error_log("Track.AssignQuestion($newQuestionId): Invalid argument for new question ID to assign to this track(".$this->trackId.')');
      return RC_INVALID_ARG;
    }

    $nextSeq = $this->GetNewSequenceNumber();

    if (!$newGroupId) {
      $rc = $this->Insert(array($this->queueSqlColumns[QUEUE_COLUMN_QUESTION_ID] => $newQuestionId,
				$this->queueSqlColumns[QUEUE_COLUMN_TRACK_ID] => $this->trackId,
				$this->queueSqlColumns[QUEUE_COLUMN_SEQUENCE] => $nextSeq),
			  QUEUE_TABLE);
    } else {
      $rc = $this->Insert(array($this->queueSqlColumns[QUEUE_COLUMN_QUESTION_ID] => $newQuestionId,
				$this->queueSqlColumns[QUEUE_COLUMN_TRACK_ID] => $this->trackId,
				$this->queueSqlColumns[QUEUE_COLUMN_SEQUENCE] => $nextSeq,
				$this->queueSqlColumns[QUEUE_COLUMN_GROUP_ID] => $newGroupId),
			  QUEUE_TABLE);
    }
    if ($rc !== TRUE) {
      error_log("Track.AssignQuestion($newQuestionId): Unable to assign question: ".$this->GetErrorMessage());
      return FALSE;
    }

    return $nextSeq;
  }


  // This function will assign the given questionId to this track
  function UnAssignQuestion($newQuestionId) {

    // Reconnect as user with permissions to delete
    $db = new DatabaseObject($_SESSION['dbName'], ORG_ADMIN_USER, ORG_ADMIN_PASS, DB_HOST);

    if (!is_numeric($newQuestionId) || !$newQuestionId) {
      error_log("Track.UnAssignQuestion($newQuestionId): Invalid argument for "
		."new question ID to assign to this track(".$this->trackId.')');
      return RC_INVALID_ARG;
    }

    // We need the sequence of this questionid in this track in the queue to
    // decrement the rest of the sequence numbers in the queue
    $seq = $this->GetSequenceByQuestionId($newQuestionId);
    
    if ($seq <= 0)
      return RC_QUERY_FAILED;

    $db->Where($this->queueSqlColumns[QUEUE_COLUMN_TRACK_ID].'='.$this->trackId);
    $db->Where($this->queueSqlColumns[QUEUE_COLUMN_QUESTION_ID].'='.$newQuestionId);
    $rc = $db->Delete(QUEUE_TABLE);

    if ($rc !== TRUE) {
      error_log("Track.UnAssignQuestion($newQuestionId): Unable to unassign question: ".$db->GetErrorMessage());
      $db->Close();
      return FALSE;
    }

    $db->Where($this->queueSqlColumns[QUEUE_COLUMN_TRACK_ID].' = '.$this->trackId);
    $db->Where($this->queueSqlColumns[QUEUE_COLUMN_SEQUENCE].'>'.$seq);
    $rc = $db->Update(array($this->queueSqlColumns[QUEUE_COLUMN_SEQUENCE] =>
			    $this->queueSqlColumns[QUEUE_COLUMN_SEQUENCE].'-1'),
		      QUEUE_TABLE);

    if ($rc !== TRUE) {
      error_log("Track.UnAssignQuestion($newQuestionId): Unable to update sequences: ".$db->GetErrorMessage());
      $db->Close();
      return FALSE;
    }

    $db->Close();
    return TRUE;
  }


  // This function removes all questions from the track specified
  function RemoveAllFromTrack() {

    // Reconnect as user with permissions to delete
    $db = new DatabaseObject($_SESSION['dbName'], ORG_ADMIN_USER, ORG_ADMIN_PASS, DB_HOST);
    $db->Where($this->queueSqlColumns[QUEUE_COLUMN_TRACK_ID].'='.$this->trackId);
    $rc = $db->Delete(QUEUE_TABLE);

    $db->Where($this->trackSqlColumns[TRACK_COLUMN_ID].'='.$this->trackId);
    $db->Update(array($this->trackSqlColumns[TRACK_COLUMN_LAST_QUESTION_NUMBER] => 0),
		TRACK_TABLE);

    
    if ($rc === FALSE)
      error_log("Track::RemoveAllFromTrack() return false after call to SQL delete statement");

    return $rc;
  }


  // Returns the next sequence number to be used in assignment in this track
  function GetNewSequenceNumber() {
    
    $this->Select('MAX('.$this->queueSqlColumns[QUEUE_COLUMN_SEQUENCE].')+1', QUEUE_TABLE);
    $this->Where($this->queueSqlColumns[QUEUE_COLUMN_TRACK_ID].'='.$this->trackId);
    $rc = $this->Query(SINGLE_VALUE, VALUES_FORMAT);
    if ($rc === FALSE) {
      error_log("Track.GetNextSequenceNumber(): Failed to return new sequence number: ".$this->GetErrorMessage());
    }

    // If there are no questions in the track then this function will return 0
    if ($rc == 0)
      $rc++;

    return $rc;
  }


  // This function returns the data to be displayed for each row of the list of questions.
  function GetQueueList($columnIds, $searchColumnId, $searchFor, $sortColumnId,
			     $sortOrder, &$pageNumber, $rowsPerPage, $showAll = FALSE) {
    
    if (empty($columnIds)) {
      if (DEBUG & DEBUG_CLASS_FUNCTIONS)
	error_log("No columns selected for track list so immediately returning with empty result sets");
      return array();
    }
    
    if ($pageNumber <= 0) {
      if (DEBUG & DEBUG_CLASS_FUNCTIONS)
	error_log("Track::GetQueueList(): Invalid Page Number($pageNumber) passed, adjusting to 1");
      $pageNumber = 1;
    }
    
    if ($rowsPerPage <= 0) {
      if (DEBUG & DEBUG_CLASS_FUNCTIONS)
	error_log("Track::GetQueueList(): Invalid Rows Per Page($rowsPerPage) passed, adjusting to 1");
      $rowsPerPage = 1;
    }
    
    $assignmentListColumns = array();
    $searchExact = FALSE;
    $searchField = '';
    $lastClass = 'trGroupEven';
    $lastGroupId = 0;
    $sortBy = array();
    $groupColumnIndex = -1;
    $sequenceColumnIndex = -1;
    $joinArray = array();
    $questionIds = array();
    $qIds = array();
    $questionIndex = -1;
    $indexCursor = 0;
    $where = '';
    $config = new Config($this->orgId);

    $questionSqlColumns = unserialize(QUESTION_SQL_COLUMNS);
    $languageSqlColumns = unserialize(LANGUAGE_SQL_COLUMNS);
    $domainSqlColumns = unserialize(DOMAIN_SQL_COLUMNS);
    $categorySqlColumns = unserialize(CATEGORY_SQL_COLUMNS);
    $assignmentListSqlColumns = unserialize(QUEUE_LIST_SQL_COLUMNS);
    
    // If descending also make group descending
    if ($sortColumnId != QUEUE_LIST_COLUMN_SEQUENCE &&
	$sortColumnId != QUEUE_LIST_COLUMN_SEQUENCE) {
      if ($sortOrder)
	$sortBy = array(QUEUE_TABLE.'.'.$this->queueSqlColumns[QUEUE_COLUMN_GROUP_ID].' DESC');
      else
	$sortBy = array(QUEUE_TABLE.'.'.$this->queueSqlColumns[QUEUE_COLUMN_GROUP_ID]);
    }

    switch($sortColumnId) {
    case QUEUE_LIST_COLUMN_QUESTION_ID:
      array_push($sortBy, QUESTION_TABLE.'.'.$this->queueSqlColumns[QUEUE_COLUMN_QUESTION_ID]);
      break;
    case QUEUE_LIST_COLUMN_SEQUENCE:
      array_push($sortBy, QUEUE_TABLE.'.'.$this->queueSqlColumns[QUEUE_COLUMN_SEQUENCE]);
      break;
    case QUEUE_LIST_COLUMN_GROUP_ID:
      array_push($sortBy, QUEUE_TABLE.'.'.$this->queueSqlColumns[QUEUE_COLUMN_GROUP_ID]);
      break;
    case QUEUE_LIST_COLUMN_QUESTION_TITLE:
      array_push($sortBy, QUESTION_TABLE.'.'.$questionSqlColumns[QUESTION_COLUMN_TITLE]);
      break;
    case QUEUE_LIST_COLUMN_QUESTION_CATEGORY_ID:
      array_push($sortBy, QUESTION_TABLE.'.'.$questionSqlColumns[QUESTION_COLUMN_CATEGORY_ID]);
    case QUEUE_LIST_COLUMN_QUESTION_LANGUAGE_ID:
      array_push($sortBy, QUESTION_TABLE.'.'.$questionSqlColumns[QUESTION_COLUMN_LANGUAGE_ID]);
      break;
    case QUEUE_LIST_COLUMN_QUESTION_DOMAIN_ID:
      array_push($sortBy, QUESTION_TABLE.'.'.$questionSqlColumns[QUESTION_COLUMN_DOMAIN_ID]);
      break;
    case QUEUE_LIST_COLUMN_QUESTION_VERSION_DATE:
      array_push($sortBy, QUESTION_TABLE.'.'.$questionSqlColumns[QUESTION_COLUMN_VERSION_DATE]);
      break;
    case QUEUE_LIST_COLUMN_CATEGORY_ID:
      array_push($sortBy, QUESTION_TABLE.'.'.$questionSqlColumns[QUESTION_COLUMN_CATEGORY_ID]);
      break;
    default:
      // Unknown column
      break;
    }
    //error_log("SORT COLUMNS: ".print_r($sortColumns, TRUE));

    // Now only select the columns currently selected in the interface for display
    for ($i = 0; $i < count($assignmentListSqlColumns); $i++) {
      if (1 << $i & $columnIds) {
	switch(1 << $i) {
	  //case QUEUE_LIST_COLUMN_QUESTION_ID:
	  //array_push($assignmentListColumns, QUESTION_TABLE.'.'.$questionSqlColumns[QUESTION_COLUMN_ID]);
	  //break;
	  //case QUEUE_LIST_COLUMN_QUESTION_TITLE:
	  //array_push($assignmentListColumns, QUESTION_TABLE.'.'.$questionSqlColumns[QUESTION_COLUMN_TITLE]);
	  //break;
	case QUEUE_LIST_COLUMN_QUESTION_LANGUAGE_ID:
	  array_push($assignmentListColumns, LANGUAGE_TABLE.'.'.$languageSqlColumns[LANGUAGE_COLUMN_NAME]);
	  $joinArray[LANGUAGE_TABLE] = array($assignmentListSqlColumns[(1 << $i)], '=',
					     LANGUAGE_TABLE.'.'.$languageSqlColumns[LANGUAGE_COLUMN_ID]);
	  $indexCursor++;
	  break;
	case QUEUE_LIST_COLUMN_QUESTION_CATEGORY_ID:
	  array_push($assignmentListColumns, CATEGORY_TABLE.'.'.$categorySqlColumns[CATEGORY_COLUMN_NAME]);
	  $joinArray[CATEGORY_TABLE] = array($assignmentListSqlColumns[(1 << $i)], '=',
					     CATEGORY_TABLE.'.'.$categorySqlColumns[CATEGORY_COLUMN_ID]);
	  $indexCursor++;
	  break;
	case QUEUE_LIST_COLUMN_QUESTION_DOMAIN_ID:
	  array_push($assignmentListColumns, DOMAIN_TABLE.'.'.$domainSqlColumns[DOMAIN_COLUMN_NAME]);
	  $joinArray[DOMAIN_TABLE] = array($assignmentListSqlColumns[(1 << $i)], '=',
					   DOMAIN_TABLE.'.'.$domainSqlColumns[DOMAIN_COLUMN_ID]);
	  $indexCursor++;
	  break;
	case QUEUE_LIST_COLUMN_QUESTION_VERSION_DATE:
	  array_push($assignmentListColumns, 'DATE('.$assignmentListSqlColumns[QUEUE_LIST_COLUMN_QUESTION_VERSION_DATE].')');
	  $indexCursor++;
	  break;
	case QUEUE_LIST_COLUMN_GROUP_ID:
	  array_push($assignmentListColumns, $assignmentListSqlColumns[(1 << $i)]);
	  $groupColumnIndex = $i;
	  $indexCursor++;
	  break;
	case QUEUE_LIST_COLUMN_SEQUENCE:
	  array_push($assignmentListColumns, $assignmentListSqlColumns[(1 << $i)]);
	  $sequenceColumnIndex = $i;
	  $indexCursor++;
	  break;
	default:
	  if (1 << $i == QUEUE_LIST_COLUMN_QUESTION_ID)
	    $questionIndex = $indexCursor;
	  array_push($assignmentListColumns, $assignmentListSqlColumns[(1 << $i)]);
	  $indexCursor++;
	  break;
	}
      }
    }

    array_push($assignmentListColumns, QUESTION_TABLE.'.'.$questionSqlColumns[QUESTION_COLUMN_ID],
	       QUEUE_TABLE.'.'.$this->queueSqlColumns[QUEUE_COLUMN_SEQUENCE],
	       QUEUE_TABLE.'.'.$this->queueSqlColumns[QUEUE_COLUMN_TRACK_ID]);

    $joinArray[QUEUE_TABLE] = array(QUESTION_TABLE.'.'.$questionSqlColumns[QUESTION_COLUMN_ID], '=',
				    QUEUE_TABLE.'.'.$this->queueSqlColumns[QUEUE_COLUMN_QUESTION_ID]." AND ".
				    QUEUE_TABLE.'.'.$this->queueSqlColumns[QUEUE_COLUMN_TRACK_ID].'='.$this->trackId);

    // build and execute the query
    $this->Select($assignmentListColumns, QUESTION_TABLE);
    if (!empty($joinArray))
      $this->LeftJoinUsing($joinArray);
    $this->SetupTrackSearch($searchColumnId, $searchFor, $searchExact);
    // Restrict show all in assignments to only those that are licensed
    $this->Where(QUESTION_TABLE.'.'.$questionSqlColumns[QUESTION_COLUMN_ID].'>399');
    $this->Where(QUESTION_TABLE.'.'.$questionSqlColumns[QUESTION_COLUMN_PARENT_ID].'=0');

    // Work with our question ranges
    $where = '(';
    if ($config->GetValue(CONFIG_LICENSED_HR_CONTENT) == 1)
      $where .= '('.QUESTION_TABLE.'.'.$questionSqlColumns[QUESTION_COLUMN_ID].'>=400 AND '.
	QUESTION_TABLE.'.'.$questionSqlColumns[QUESTION_COLUMN_ID].'< 5000)';
    if ($config->GetValue(CONFIG_LICENSED_AB1825_CONTENT) == 1) {
      if ($where != '(')
	$where .= ' OR ';
      $where .= '('.QUESTION_TABLE.'.'.$questionSqlColumns[QUESTION_COLUMN_ID].'>=50000 AND '.
	QUESTION_TABLE.'.'.$questionSqlColumns[QUESTION_COLUMN_ID].'< 55000)';
    }
    if ($config->GetValue(CONFIG_LICENSED_EDU_CONTENT) == 1) {
      if ($where != '(')
	$where .= ' OR ';
      $where .= '('.QUESTION_TABLE.'.'.$questionSqlColumns[QUESTION_COLUMN_ID].'>=90000 AND '.
	QUESTION_TABLE.'.'.$questionSqlColumns[QUESTION_COLUMN_ID].'< 95000)';
    }
    if ($where != '(')
      $where .= ' OR ';
    $where .= '('.QUESTION_TABLE.'.'.$questionSqlColumns[QUESTION_COLUMN_ID].'>=100000)';
    if ($where != '(')
      $this->Where($where.')');

    $this->SortBy($sortBy, $sortOrder, 1);
    $this->SetPage($pageNumber, $rowsPerPage);
    $pageCount = $this->GetPageCount($rowsPerPage);
    if ($pageNumber > $pageCount && $pageCount > 0) {
      error_log("Track::GetQueueList(): Page Number ($pageNumber) is greater than actual pages ($pageCount) (2nd call)");
      $pageNumber = $pageCount;
      $this->SetPage($pageNumber, $rowsPerPage);
    }

    $results = $this->Query(MANY_ROWS, VALUES_FORMAT);


    // Add actions for this listing
    for($i = 0; $i < count($results); $i++) {

      // Fix up the question number column to be a link that views the question
      if ($questionIndex >= 0) {
	$results[$i][$questionIndex] = new TplCaption($results[$i][$questionIndex]);
	$results[$i][$questionIndex]->href='question_view.php';
	$results[$i][$questionIndex]->params='return=assignment';
      }

      $trackId      = array_pop($results[$i]);
      $sequenceId   = array_pop($results[$i]);
      if ($showAll)
	$deleteRow = 0;
      else
	$deleteRow = 1;

      array_push($qIds, (int)array_pop($results[$i]));

      if ($this->isAb1825) {
	$ref1 = new TplCaption('View');
	$ref1->href='question_view.php';
	$ref1->params='return=assignment';
      } elseif ($trackId) {
	$ref1 = new TplCaption('Remove');
	//$ref1->params='action=remove';
	$ref1->href=$_SERVER['PHP_SELF']; //'assignment_list.php';
	$ref1->id="action100-$i";
	$ref1->isService=TRUE;
	$ref1->onClick="svcToggleClassAssignment('$trackId', '$qIds[$i]', $i, 100, $deleteRow)";
      } else {
	$ref1 = new TplCaption('Add');
	$ref1->href=$_SERVER['PHP_SELF']; //'assignment_list.php';
	$ref1->id="action100-$i";
	//$ref1->params='action=add';
	$ref1->isService=TRUE;
	$ref1->onClick="svcToggleClassAssignment('$this->trackId', '$qIds[$i]', $i, 100, 0)";
      }
      array_push($results[$i], array($ref1));

      if (!$this->isAb1825) { // Only allow group and sequence editing for non AB1825 tracks
	// In order for the group link to be displayed this question must
	// already be in a sequence
	if ($groupColumnIndex >= 0 && $sequenceId > 0) {
	  $results[$i][$groupColumnIndex] = new TplCaption($results[$i][$groupColumnIndex]);
	  $results[$i][$groupColumnIndex]->id = "GSeq$qIds[$i]";
	  if ($results[$i][$groupColumnIndex]->caption > 0) {
	    $results[$i][$groupColumnIndex]->href =
	      "javascript:SequencePopup('".$_SERVER['PHP_SELF']."', '".$this->trackId
	      ."', '$qIds[$i]', '".($results[$i][$groupColumnIndex]->caption)
	      ."', 'GSeq$qIds[$i]', false);";
	    if (($results[$i][$groupColumnIndex]->caption) != $lastGroupId) {
	      if ($lastClass == 'trGroupEven')
		$lastClass = 'trGroupOdd';
	      else
		$lastClass = 'trGroupEven';
	      $lastGroupId = ($results[$i][$groupColumnIndex]->caption);
	    }
	    $results[$i]['rowClass'] = $lastClass;
	  } else {
	    $results[$i][$groupColumnIndex]->href =
	      "javascript:SequencePopup('".$_SERVER['PHP_SELF']."', '".$this->trackId
	      ."', '$qIds[$i]', 0, 'GSeq$qIds[$i]', false);";
	    $results[$i][$groupColumnIndex]->caption = 'Assign to Group';
	  }
	}
	
	if ($sequenceColumnIndex >= 0 && $results[$i][$sequenceColumnIndex] > 0) {
	  $results[$i][$sequenceColumnIndex] = new TplCaption($results[$i][$sequenceColumnIndex]);
	  $results[$i][$sequenceColumnIndex]->href =
	    "javascript:SequencePopup('".$_SERVER['PHP_SELF']."', '".$this->trackId
	    ."', '$qIds[$i]', '".(int)$sequenceId."', 'Seq$qIds[$i]', true);";
	  $results[$i][$sequenceColumnIndex]->id = "Seq$qIds[$i]";
	}
      }
    }
    
    return array($qIds, $results);
  }


  // This function returns the data to be displayed for each row of the list of questions.
  function GetProgressList($columnIds, $searchColumnId, $searchFor, $sortColumnId,
			   $sortOrder, &$pageNumber, $rowsPerPage, $showAll = FALSE) {
    //error_log("COLUMN_IDS: ($columnIds): ".print_r($columnIds, TRUE));    
    if (empty($columnIds)) {
      if (DEBUG & DEBUG_CLASS_FUNCTIONS)
	error_log("No columns selected for track list so immediately returning with empty result sets");
      return array();
    }
    
    if ($pageNumber <= 0) {
      if (DEBUG & DEBUG_CLASS_FUNCTIONS)
	error_log("Track::GetQueueList(): Invalid Page Number($pageNumber) passed, adjusting to 1");
      $pageNumber = 1;
    }
    
    if ($rowsPerPage <= 0) {
      if (DEBUG & DEBUG_CLASS_FUNCTIONS)
	error_log("Track::GetQueueList(): Invalid Rows Per Page($rowsPerPage) passed, adjusting to 1");
      $rowsPerPage = 1;
    }

    $userAssignmentListColumns = array();
    $assignmentListColumns = array();
    $searchExact = FALSE;
    $searchField = '';
    $sortBy = array();
    $exitInfoIndex = -1;
    $questionNumberIndex = -1;
    $completionDateIndex = -1;
    $joinArray = array();
    $indexCursor = 0;

    // sql columns
    $userSqlColumns = unserialize(USER_SQL_COLUMNS);

    //$userAssignmentSqlColumns = unserialize(USER_ASSIGNMENT_SQL_COLUMNS);
    $assignmentSqlColumns = unserialize(ASSIGNMENT_SQL_COLUMNS);
    $languageSqlColumns = unserialize(LANGUAGE_SQL_COLUMNS);
    $domainSqlColumns = unserialize(DOMAIN_SQL_COLUMNS);

    // Setup sort list
    switch($sortColumnId) {
    case ASSIGNMENT_COLUMN_USER_ID:
      $sortBy = USER_TABLE.'.'.$userSqlColumns[USER_COLUMN_FULL_NAME];
      break;
    case ASSIGNMENT_COLUMN_LANGUAGE_ID:
      $sortBy = LANGUAGE_TABLE.'.'.$languageSqlColumns[LANGUAGE_COLUMN_NAME];
      break;
    case ASSIGNMENT_COLUMN_DOMAIN_ID:
      $sortBy = DOMAIN_TABLE.'.'.$domainSqlColumns[DOMAIN_COLUMN_NAME];
      break;
    default:
      if (!isset($assignmentSqlColumns[$sortColumnId])) {
	error_log("Undefined sort column id: $sortColumnId");
	$sortBy = $assignmentSqlColumns[DEFAULT_ASSIGNMENT_SORT_COLUMN];
      } else
	$sortBy = $assignmentSqlColumns[$sortColumnId];
      break;
    }


    // Now only select the columns currently selected in the interface for display
    for ($i = 0; $i < count($assignmentSqlColumns); $i++) {
      if (1 << $i & $columnIds) {
	switch(1 << $i) {
	case ASSIGNMENT_COLUMN_LANGUAGE_ID:
	  array_push($userAssignmentListColumns, LANGUAGE_TABLE.'.'.$languageSqlColumns[LANGUAGE_COLUMN_NAME]);
	  $joinArray[LANGUAGE_TABLE] = array(ASSIGNMENT_TABLE.'.'.$assignmentSqlColumns[(1 << $i)], '=',
					     LANGUAGE_TABLE.'.'.$languageSqlColumns[LANGUAGE_COLUMN_ID]);
	  $indexCursor++;
	  break;
	case ASSIGNMENT_COLUMN_USER_ID:
	  array_push($userAssignmentListColumns, USER_TABLE.'.'.$userSqlColumns[USER_COLUMN_FULL_NAME]);
	  $joinArray[USER_TABLE] = array(ASSIGNMENT_TABLE.'.'.$assignmentSqlColumns[(1 << $i)], '=',
					 USER_TABLE.'.'.$userSqlColumns[CATEGORY_COLUMN_ID]);
	  $indexCursor++;
	  break;
	case ASSIGNMENT_COLUMN_DOMAIN_ID:
	  array_push($userAssignmentListColumns, DOMAIN_TABLE.'.'.$domainSqlColumns[DOMAIN_COLUMN_NAME]);
	  $joinArray[DOMAIN_TABLE] = array(ASSIGNMENT_TABLE.'.'.$assignmentSqlColumns[(1 << $i)], '=',
					   DOMAIN_TABLE.'.'.$domainSqlColumns[DOMAIN_COLUMN_ID]);
	  $indexCursor++;
	  break;
	case ASSIGNMENT_COLUMN_DELIVERY_DATE:
	  array_push($userAssignmentListColumns, 'DATE('.ASSIGNMENT_TABLE.'.'.$assignmentSqlColumns[(1 << $i)].')');
	  $indexCursor++;
	  break;
	case ASSIGNMENT_COLUMN_COMPLETION_DATE:
	  array_push($userAssignmentListColumns, 'DATE('.ASSIGNMENT_TABLE.'.'.$assignmentSqlColumns[(1 << $i)].')');
	  $completionDateIndex = $indexCursor;
	  $indexCursor++;
	  break;
	case ASSIGNMENT_COLUMN_VERSION_DATE:
	  array_push($userAssignmentListColumns, 'DATE('.ASSIGNMENT_TABLE.'.'.$assignmentSqlColumns[(1 << $i)].')');
	  $indexCursor++;
	  break;
	default:
	  if (1 << $i == ASSIGNMENT_COLUMN_EXIT_INFO)
	    $exitInfoIndex = $indexCursor;
	  if (1 << $i == ASSIGNMENT_COLUMN_QUESTION_NUMBER)
	    $questionNumberIndex = $indexCursor;
	  array_push($userAssignmentListColumns, ASSIGNMENT_TABLE.'.'.$assignmentSqlColumns[(1 << $i)]);
	  $indexCursor++;
	  break;
	}
      }
    }

    // This ensures that joins are properly performed for search columns even when not selected
    switch($searchColumnId) {
    case ASSIGNMENT_COLUMN_USER_ID:
      $joinArray[USER_TABLE] = array(ASSIGNMENT_TABLE.'.'.$assignmentSqlColumns[ASSIGNMENT_COLUMN_USER_ID], '=',
				     USER_TABLE.'.'.$userSqlColumns[CATEGORY_COLUMN_ID]);
      break;
    case ASSIGNMENT_COLUMN_DOMAIN_ID:
      $joinArray[DOMAIN_TABLE] = array(ASSIGNMENT_TABLE.'.'.$assignmentSqlColumns[ASSIGNMENT_COLUMN_DOMAIN_ID], '=',
				       DOMAIN_TABLE.'.'.$domainSqlColumns[DOMAIN_COLUMN_ID]);
      break;
    case ASSIGNMENT_COLUMN_LANGUAGE_ID:
      $joinArray[LANGUAGE_TABLE] = array(ASSIGNMENT_TABLE.'.'.$assignmentSqlColumns[ASSIGNMENT_COLUMN_LANGUAGE_ID], '=',
					 LANGUAGE_TABLE.'.'.$languageSqlColumns[LANGUAGE_COLUMN_ID]);
      break;
    default:
      if (DEBUG & DEBUG_SQL)
	error_log("GetProgressList(searchColumnId=$searchColumnId): WARN search param specified does not need join");
      break;
    }

    // Pull params we need to build 
    if ($questionNumberIndex >= 0) {
      array_push($userAssignmentListColumns, ASSIGNMENT_TABLE.'.'.$assignmentSqlColumns[ASSIGNMENT_COLUMN_VERSION_DATE]);
      array_push($userAssignmentListColumns, ASSIGNMENT_TABLE.'.'.$assignmentSqlColumns[ASSIGNMENT_COLUMN_DOMAIN_ID]);
    }

    // build and execute the query
    $this->Select($userAssignmentListColumns, ASSIGNMENT_TABLE);
    if (!empty($joinArray))
      $this->LeftJoinUsing($joinArray);
    $this->SetupAssignmentSearch($searchColumnId, $searchFor, $searchExact);
    // Restrict show all in assignments to only those that are licensed
    $this->Where(ASSIGNMENT_TABLE.'.'.$assignmentSqlColumns[ASSIGNMENT_COLUMN_TRACK_ID].'='.$this->trackId);
    //    if ($searchColumnId == ASSIGNMENT_COLUMN_EXIT_INFO)
    //      $this->Where($assignmentSqlColumns[ASSIGNMENT_COLUMN_COMPLETION_DATE]."!=''");
    $this->SortBy($sortBy, $sortOrder, 1);
    $this->SetPage($pageNumber, $rowsPerPage);
    $pageCount = $this->GetPageCount($rowsPerPage);
    if ($pageNumber > $pageCount && $pageCount > 0) {
      error_log("Track::GetQueueList(): Page Number ($pageNumber) is greater than actual pages ($pageCount) (2nd call)");
      $pageNumber = $pageCount;
      $this->SetPage($pageNumber, $rowsPerPage);
    }

    $results = $this->Query(MANY_ROWS, VALUES_FORMAT);
    $exitInfoOptions = unserialize(EXIT_INFO_OPTIONS);

    // Post processing
    for ($i = 0; $i < count($results); $i++) {
      if ($exitInfoIndex >= 0)
	$results[$i][$exitInfoIndex] = $exitInfoOptions[$results[$i][$exitInfoIndex]];
      if ($completionDateIndex >= 0 && substr($results[$i][$completionDateIndex], 0, 10) == '0000-00-00')
	$results[$i][$completionDateIndex] = 'Not Finished';
      if ($questionNumberIndex >= 0) {
	$results[$i][$questionNumberIndex] = new TplCaption($results[$i][$questionNumberIndex]);
	$results[$i][$questionNumberIndex]->href = 'question_view.php';
	$results[$i][$questionNumberIndex]->params = 'questionId='.$results[$i][$questionNumberIndex]->caption
	  .'&domainId='.array_pop($results[$i]).'&versionDate='.array_shift(explode(' ', array_pop($results[$i]))).'&return=progress';
      }
    }

    return $results;
  }


  function SetupAssignmentSearch($searchColumnId, $searchFor, $searchExact) {
    
    if (DEBUG & DEBUG_CLASS_FUNCTIONS)
      error_log("Track.SetupAssignmentSearch($searchColumnId, $searchFor, $searchExact)");

    switch($searchColumnId) {
    case ASSIGNMENT_COLUMN_COMPLETION_DATE:
      if ($searchFor && strstr('not finished', strtolower($searchFor)) !== FALSE) {
	$searchExact = TRUE;
	$searchFor = 0;
      }
      $searchField = $this->GetAssignmentSearchFieldNameById($searchColumnId);
      $this->SearchBy($searchField, $searchFor, $searchExact);
      break;
    case ASSIGNMENT_COLUMN_EXIT_INFO:
      $searchExact = TRUE;
      if (strstr('understood', strtolower($searchFor)) !== FALSE)
	$searchFor = EXIT_INFO_UNDERSTOOD;
      else if (strstr('organization contact', strtolower($searchFor)) !== FALSE)
	$searchFor = EXIT_INFO_CONTACT;
      else if (strstr('incomplete', strtolower($searchFor)) !== FALSE)
	$searchFor = EXIT_INFO_INCOMPLETE;
      else
	$searchFor = (-1);
      $searchField = $this->GetAssignmentSearchFieldNameById($searchColumnId);
      $this->SearchBy($searchField, $searchFor, $searchExact);
      break;
    default:
      if (gettype($searchColumnId) == 'array') {
	for($i = 0; $i < count($searchColumnId); $i++) {
	  if ($searchFor[$i] || is_numeric($searchFor[$i])) {
	    if ($searchColumnId[$i] == ASSIGNMENT_COLUMN_QUESTION_NUMBER ||
		$searchColumnId[$i] == ASSIGNMENT_COLUMN_EXIT_INFO)
	      $searchExact = TRUE;
	    else
	      $searchExact = FALSE;
	    $searchField = $this->GetAssignmentSearchFieldNameById($searchColumnId[$i]);
	    $this->SearchBy($searchField, $searchFor[$i], $searchExact);
	  }
	}
      } else {
	if ($searchFor || is_numeric($searchFor)) {
	  if ($searchColumnId == ASSIGNMENT_COLUMN_QUESTION_NUMBER ||
	      $searchColumnId == ASSIGNMENT_COLUMN_EXIT_INFO)
	    $searchExact = TRUE;
	  else
	    $searchExact = FALSE;
	  $searchField = $this->GetAssignmentSearchFieldNameById($searchColumnId);
	  $this->SearchBy($searchField, $searchFor, $searchExact);
	}
      }
    }
  }
  

  function SetupTrackSearch($searchColumnId, $searchFor, $searchExact) {
    
    if (DEBUG & DEBUG_CLASS_FUNCTIONS) {
      $pSearchColumnId = $searchColumnId;
      $pSearchFor = $searchFor;
      $pSearchExact = $searchExact;
      if (is_array($searchColumnId))
	$pSearchColumnId = '|'.print_r($searchColumnId, TRUE).'|';
      if (is_array($searchFor))
	$pSearchFor = '|'.print_r($searchFor, TRUE).'|';
      error_log("Track.SetupTrackSearch($pSearchColumnId, $pSearchFor, $pSearchExact)");
    }

    if (gettype($searchColumnId) == 'array') {
      for($i = 0; $i < count($searchColumnId); $i++) {
        if ($searchFor[$i]) {
          if ($searchColumnId[$i] == QUEUE_LIST_COLUMN_QUESTION_ID ||
              $searchColumnId[$i] == QUEUE_LIST_COLUMN_SEQUENCE ||
              $searchColumnId[$i] == QUEUE_LIST_COLUMN_GROUP_ID ||
              $searchColumnId[$i] == QUEUE_LIST_COLUMN_TRACK_ID)
            $searchExact = TRUE;
          else
            $searchExact = FALSE;
          $searchField = $this->GetSearchFieldNameById($searchColumnId[$i]);
          $this->SearchBy($searchField, $searchFor[$i], $searchExact);
        }
      }
    } else {
      if ($searchFor) {
        if ($searchColumnId == QUEUE_LIST_COLUMN_QUESTION_ID ||
            $searchColumnId == QUEUE_LIST_COLUMN_SEQUENCE ||
            $searchColumnId == QUEUE_LIST_COLUMN_GROUP_ID ||
            $searchColumnId == QUEUE_LIST_COLUMN_TRACK_ID)
          $searchExact = TRUE;
        else
          $searchExact = FALSE;
        $searchField = $this->GetSearchFieldNameById($searchColumnId);
        $this->SearchBy($searchField, $searchFor, $searchExact);
      }
    }
  }


  // Returns the id of the next question_number to be delivered
  // if the track has ended and will not be running again (e.g. isLooping set to FALSE)
  // then this function will return 0 (an invalid question_number)
  function GetNextQuestionIdInSequence() {

    $nextQuestionNumber = 0;
    $nextQuestionSequence = 0;
    $nextQuestionGroup = 0;

    if (!GetQuestionNumberAndSequence($this->dbLink, $this->trackId, $this->lastQuestionNumber, $nextQuestionNumber,
				      $nextQuestionSequence, $nextQuestionGroup, $this->isLooping)) {
      error_log("Track::GetNextQuestionIdInSequence(): GetQuestionNumberAndSequnce("
		."trackId=$this->trackId, lastQuestionNumber=$this->lastQuestionNumber, nextQuestionNumber="
		."$nextQuestionNumber, nextQuestionSequence=$nextQuestionSequence, nextQuestionGroup="
		."$nextQuestionGroup, isLooping=$this->isLooping): returned FALSE, maybe no assignments in track?");
    }

    return $nextQuestionNumber;
  }
  

  // This returns the sequnce number of the given question id
  function GetSequenceByQuestionId($questionId) {
    
    if (!is_numeric($questionId))
      return RC_INVALID_ARG;
    
    $this->Select($this->queueSqlColumns[QUEUE_COLUMN_SEQUENCE], QUEUE_TABLE);
    $this->Where($this->queueSqlColumns[QUEUE_COLUMN_TRACK_ID].'='.$this->trackId);
    $this->Where($this->queueSqlColumns[QUEUE_COLUMN_QUESTION_ID].'='.$questionId);
    if (($seq = $this->Query(SINGLE_VALUE, VALUES_FORMAT)) === FALSE) {
      error_log("Track.GetSequenceByQuestionId: failed to obtain sequnce number for questionId=$questionId: "
		.$this->GetErrorMessage());
      return RC_QUERY_FAILED;
    }
    
    return $seq;
  }


  // Returns the maximum sequence id in this track
  function GetMaxSequenceId() {
    
    $this->Select('MAX('.$this->queueSqlColumns[QUEUE_COLUMN_SEQUENCE].')',
		  QUEUE_TABLE);
    $this->Where($this->queueSqlColumns[QUEUE_COLUMN_TRACK_ID].'='.$this->trackId);
    $rc = $this->Query(SINGLE_VALUE, VALUES_FORMAT);
    if ($rc === FALSE) {
      error_log("Track.GetMaxSequenceId(): Query failed: ".$this->GetErrorMessage());
      return RC_QUERY_FAILED;
    }

    return $rc;
  }


  // Returns the maximum group id in this track
  function GetMaxGroupId() {
    
    //$questionSqlColumns = unserialize(QUESTION_SQL_COLUMNS);
    
    $this->Select('MAX('.$this->queueSqlColumns[QUEUE_COLUMN_GROUP_ID].')',
		  QUEUE_TABLE);
    $this->Where($this->queueSqlColumns[QUEUE_COLUMN_TRACK_ID].'='.$this->trackId);
    $rc = $this->Query(SINGLE_VALUE, VALUES_FORMAT);
    if ($rc === FALSE) {
      error_log("Track.GetMaxGroupId(): Query failed: ".$this->GetErrorMessage());
      return RC_QUERY_FAILED;
    }
    
    return $rc;
  }
  
  
  // This will return a two element array containing the sequence number
  // and group id of the provided question id
  function GetQueueSequenceAndGroupIds($questionId) {
    
    $this->Select(array($this->queueSqlColumns[QUEUE_COLUMN_SEQUENCE],
			$this->queueSqlColumns[QUEUE_COLUMN_GROUP_ID]),
		  QUEUE_TABLE);
    $this->Where($this->queueSqlColumns[QUEUE_COLUMN_TRACK_ID].'='.$this->trackId);
    $this->Where($this->queueSqlColumns[QUEUE_COLUMN_QUESTION_ID].'='.$questionId);
    $rc = $this->Query(SINGLE_ROW, VALUES_FORMAT);
    
    if ($rc === FALSE) {
      error_log("Track.GetQueueSequenceId($questionId): Failed to obtain sequence id: ".$this->GetErrorMessage());
      return RC_QUERY_FAILED;
    }

    return $rc;
  }


  // Returns a two element array containing the first and last
  // sequence number for the given group id
  function GetFirstAndLastSequenceInGroup($groupId) {
    
    $this->Select(array('MIN('.$this->queueSqlColumns[QUEUE_COLUMN_SEQUENCE].')',
			'MAX('.$this->queueSqlColumns[QUEUE_COLUMN_SEQUENCE].')'),
		  QUEUE_TABLE);
    $this->Where($this->queueSqlColumns[QUEUE_COLUMN_TRACK_ID].'='.$this->trackId);
    $this->Where($this->queueSqlColumns[QUEUE_COLUMN_GROUP_ID].'='.$groupId);
    $rc = $this->Query(SINGLE_ROW, VALUES_FORMAT);

    if ($rc === FALSE) {
      error_log("Track.GetQueueSequenceId($groupId): Failed to obtain sequence id: ".$this->GetErrorMessage());
      return RC_QUERY_FAILED;
    }

    return $rc;
  }


  // Retrieve the group id based on the provided sequence id in the track that is this object
  function GetGroupIdBySequenceId($sequenceId) {
    
    if (!is_numeric($sequenceId))
      return RC_INVALID_ARG;
    
    $this->Select($this->queueSqlColumns[QUEUE_COLUMN_GROUP_ID], QUEUE_TABLE);
    $this->Where($this->queueSqlColumns[QUEUE_COLUMN_TRACK_ID].'='.$this->trackId);
    $this->Where($this->queueSqlColumns[QUEUE_COLUMN_SEQUENCE].'='.$sequenceId);
    if (($grp = $this->Query(SINGLE_VALUE, VALUES_FORMAT)) === FALSE) {
      error_log("Track.GetGroupIdBySequenceId: failed to obtain group id for sequenceId=$sequenceId: "
		.$this->GetErrorMessage());
      return RC_QUERY_FAILED;
    }
    
    return $grp;
  }


  // Retrieve the group id based on the provided question id in the track that is this object
  function GetGroupIdByQuestionId($questionId) {

    if (!is_numeric($questionId))
      return RC_INVALID_ARG;
    
    $this->Select($this->queueSqlColumns[QUEUE_COLUMN_GROUP_ID], QUEUE_TABLE);
    $this->Where($this->queueSqlColumns[QUEUE_COLUMN_TRACK_ID].'='.$this->trackId);
    $this->Where($this->queueSqlColumns[QUEUE_COLUMN_QUESTION_ID].'='.$questionId);
    if (($grp = $this->Query(SINGLE_VALUE, VALUES_FORMAT)) === FALSE) {
      error_log("Track.GetGroupIdByQuestionId: failed to obtain group id for questionId=$questionId: "
		.$this->GetErrorMessage());
      return RC_QUERY_FAILED;
    }
    
    return $grp;
  }


  // Retrieve the group id based on the provided question id in the track that is this object
  function GetSequenceIdByQuestionId($questionId) {

    if (!is_numeric($questionId))
      return RC_INVALID_ARG;
    
    $this->Select($this->queueSqlColumns[QUEUE_COLUMN_SEQUENCE], QUEUE_TABLE);
    $this->Where($this->queueSqlColumns[QUEUE_COLUMN_TRACK_ID].'='.$this->trackId);
    $this->Where($this->queueSqlColumns[QUEUE_COLUMN_QUESTION_ID].'='.$questionId);
    if (($seq = $this->Query(SINGLE_VALUE, VALUES_FORMAT)) === FALSE) {
      error_log("Track.GetSequenceIdByQuestionId: failed to obtain sequence id for questionId=$questionId: "
		.$this->GetErrorMessage());
      return RC_QUERY_FAILED;
    }
    
    return $seq;
  }


  // Sets the sequence number for the provided qeustion id and handles
  // all the many special cases when that question is grouped
  function SetQueueSequenceId($questionId, $newSequenceId) {

    $firstSequenceInGroup = 0;
    $lastSequenceInGroup = 0;
    $firstSequenceInDestGroup = 0;
    $lastSequenceInDestGroup = 0;
    $maxSequenceId = 0;
    $oldSequenceId = 0;
    $groupSize = 0;
    $groupId = 0;
    $difference = 0;
    $qty = 0;
    $dstGroupId = 0;
    $op = '';
    $swapInGroup = FALSE;

    // sequences <= 0 are not permitted
    if ($newSequenceId <= 0 || !is_numeric($newSequenceId))
      return RC_INVALID_SEQUENCE;

    $maxSequenceId = $this->GetMaxSequenceId();
    if ($newSequenceId > $maxSequenceId)
      return RC_INVALID_SEQUENCE;
    
    list($oldSequenceId, $groupId) = $this->GetQueueSequenceAndGroupIds($questionId);
    if ($oldSequenceId == $newSequenceId) {
      error_log("Track.SetQueueSequenceId($questionId,$newSequenceId): oldSequenceId=$oldSequenceId, no change, ignoring");
      RC_OK;
    }

    // We also need to know if the question having its sequence replaced is in a group
    // but not the same group as the oldSequenceId.  If it is then when the newSequenceId
    // is equivelant to the first sequence id in the group then the group is shifted down
    // otherwise, when the new sequence id lands in the middle of the group the new sequence
    // id is adjusted to go to the bottom of that group
    $destGroupId = $this->GetGroupIdBySequenceId($newSequenceId);
    if ($destGroupId > 0 && $destGroupId != $groupId) {
      if (DEBUG & DEBUG_CLASS_FUNCTIONS)
	error_log("Track.SetQueueSequenceId: Destination sequence id is part of alternate group, "
		  ."compensating for first and last sequence IDs of group");
      list($firstSequenceInDestGroup, $lastSequenceInDestGroup) = $this->GetFirstAndLastSequenceInGroup($destGroupId);

      // If the destination lands inside a group, just move the whole group!
      if ($oldSequenceId < $firstSequenceInDestGroup)
	$newSequenceId = $lastSequenceInDestGroup;
      else
	$newSequenceId = $firstSequenceInDestGroup;
    }

    // This is necessary because since we adjust the newSequenceId then after adjustment
    // it bemes impossible to tell if the original new sequence id was really in group or not
    if ($groupId == $destGroupId)
      $swapInGroup = TRUE;

    if ($groupId > 0) {
      // Get first and last sequences in the group to which this question belongs
      if (DEBUG & DEBUG_CLASS_FUNCTIONS)
	error_log("Track.SetQueueSequenceId: Getting first and last sequence ids in group($groupId)");
      list($firstSequenceInGroup, $lastSequenceInGroup) = $this->GetFirstAndLastSequenceInGroup($groupId);
      $groupSize = ($lastSequenceInGroup - $firstSequenceInGroup);
      if (DEBUG & DEBUG_CLASS_FUNCTIONS)
	error_log("Track.SetQueueSequenceId: Group size of source question=$groupSize");
    }

    // After having altered our new sequence ID we must always ensure that it does
    // run off the end.  So if the last sequence number exceeds maxSequenceId
    // then it must be reset to maxSequenceId less the current group size
    //error_log("MAX SEQUENCEID: $maxSequenceId");
    //error_log("***************($newSequenceId) BEFORE*******(".($maxSequenceId - $groupSize)."MAX-GRPSIZE**********");
    if ($newSequenceId > ($maxSequenceId - $groupSize)) {
      $newSequenceId = $maxSequenceId - $groupSize;
    }
    //error_log("***************($newSequenceId) AFTER*****************");
    
    // If this question is in a group then first reposition within that group
    if ($groupId > 0) {

      // If the new sequence number lands inside a group to which it is already assigned...
      // This also works when the group only consists of one member i.e. first and last group
      // sequence values are equal
      if ($swapInGroup || $groupSize == 0) {
	$this->ShiftQueueQuestion($questionId, $oldSequenceId, $newSequenceId);
      } else { //The question is certainly in a group but its destination lies outside that group
	$this->ShiftQueueGroup($groupId, $oldSequenceId, $newSequenceId, $firstSequenceInGroup, $lastSequenceInGroup);
      }

    } else {
      $this->ShiftQueueQuestion($questionId, $oldSequenceId, $newSequenceId);
    }
      
    return RC_OK;
  }


  // This function handles group assignment.  It is designed to be a public function and to take
  // and question already assigned to this track and manage its group membership.
  function SetQueueGroupId($questionId, $groupId) {

    $maxGroupId = 0;

    // Validate the groupID
    if ($groupId != 'New' && $groupId != 'Unassign' && !(is_numeric($groupId) && $groupId >= 0))
      return RC_INVALID_GROUP_ID;

    // 0 is valid and a convenient fast way to unassign a question
    if (is_numeric($groupId) && $groupId == 0)
      $groupId = 'Unassign';

    // Validate the group number against the maximum group in the system
    $maxGroupId = $this->GetMaxGroupId();
    if (is_numeric($groupId) && $groupId > $maxGroupId)
      return RC_INVALID_GROUP_ID;

    // All we need to complete this function is the last sequence of the new groupId
    // See if this question is already in a group
    $srcGroupId = $this->GetGroupIdByQuestionId($questionId);

    // We also need the sequence id of the question getting changed
    $sequenceId = $this->GetSequenceIdByQuestionId($questionId);

    // Make sure they are different and if same, just return
    if ($groupId == $srcGroupId) {
      error_log("Track.SetQueueGroupId($questionId,$groupId): old and new group ids are identical, skipping...");
      return RC_OK;
    }

    if ($groupId == 'New') {

      $setGroupId = ((int)$maxGroupId + 1);
      $setSequenceId = 0;
      
    } elseif ($groupId == 'Unassign') {

      // We need the last sequence number of the existing group because that is the spot this question will hold
      list($firstSequenceInGroup, $lastSequenceInGroup) = $this->GetFirstAndLastSequenceInGroup($srcGroupId);
      $setGroupId = 0;
      $setSequenceId = $lastSequenceInGroup;

    } elseif (is_numeric($groupId)) {

      // We need the last sequence number of the destination group because that is the spot this question will hold
      list($firstSequenceInGroup, $lastSequenceInGroup) = $this->GetFirstAndLastSequenceInGroup($groupId);
      $setGroupId = $groupId;
      $setSequenceId = $lastSequenceInGroup;
      // When sequence of source question is less than any sequences in the group
      // then the question gets placed one above the last question number
      // So really we only want to set the last sequence in the group if the
      // source question is smaller.  When it is bigger, it should be last sequence
      // plus one to make it the last question in the group.  This is necessary because
      // of the way sequence numbers are incremented and decremented while shifting
      // them inside the ShiftQueueXxxxx functions
      if ($sequenceId > $setSequenceId)
	$setSequenceId++;

    } else {

      error_log("Track.SetQueueGroupId($questionId,$groupId): Second argument to "
		."function invalid, valid args are: New, Unassign, <numeric>");
      return RC_INVALID_ARG;

    }

    if ($sequenceId == FALSE) {
      error_log("Track.SetQueueGroupId($questionId,$groupId): Failed to get sequence id of first arg question id: "
		.$this->GetErrorMessage());
      return RC_QUERY_FAILED;
    }

    // First shift the queue question where it should go but only if this is not a new group
    // When the assigned group id is 'New' the new sequence number is set to 0 to indicate
    // the sequence should not be updated
    if ($setSequenceId > 0)
      $this->ShiftQueueQuestion($questionId, $sequenceId, $setSequenceId);

    // Now update the question with the correct group information
    $this->Where($this->queueSqlColumns[QUEUE_COLUMN_TRACK_ID].' = '.$this->trackId);
    $this->Where($this->queueSqlColumns[QUEUE_COLUMN_QUESTION_ID].' = '.$questionId);
    $rc = $this->Update(array($this->queueSqlColumns[QUEUE_COLUMN_GROUP_ID] => $setGroupId),
			QUEUE_TABLE);
    if ($rc === FALSE) {
      error_log("Track.SetQueueGroupId($questionId,$groupId): Failed: ".$this->GetErrorMessage());
      return RC_QUERY_FAILED;
    }

    return RC_OK;

  } // End function SetQueueGroupId()


  // This will shift the question given in the queue from the old sequence to the new sequence
  // on safety checking is done her, it just does the raw functionality.  Use SetQueueSequenceId
  // above for boundery conditions and safety checking.
  function ShiftQueueQuestion($questionId, $oldSequenceId, $newSequenceId) {

    $difference = $oldSequenceId - $newSequenceId;

    if ($difference > 0) {
      $this->Where($this->queueSqlColumns[QUEUE_COLUMN_TRACK_ID].'='.$this->trackId);
      $this->Where($this->queueSqlColumns[QUEUE_COLUMN_SEQUENCE].'>='.$newSequenceId);
      $this->Where($this->queueSqlColumns[QUEUE_COLUMN_SEQUENCE].'<'.$oldSequenceId);
      $rc = $this->Update(array($this->queueSqlColumns[QUEUE_COLUMN_SEQUENCE] =>
				$this->queueSqlColumns[QUEUE_COLUMN_SEQUENCE].' + 1'),
			  QUEUE_TABLE);
    } else { // difference less than 0 indicating sequence smaller than dest (op already negative in number)
      $this->Where($this->queueSqlColumns[QUEUE_COLUMN_TRACK_ID].'='.$this->trackId);
      $this->Where($this->queueSqlColumns[QUEUE_COLUMN_SEQUENCE].'>'.$oldSequenceId);
      $this->Where($this->queueSqlColumns[QUEUE_COLUMN_SEQUENCE].'<='.$newSequenceId);
      $rc = $this->Update(array($this->queueSqlColumns[QUEUE_COLUMN_SEQUENCE] =>
				$this->queueSqlColumns[QUEUE_COLUMN_SEQUENCE].' - 1'),
			  QUEUE_TABLE);
    }
    if ($rc === FALSE) {
      error_log("Track.SetSequenceId($questionId,$newSequenceId): Failed shifting sequence numbers to accomodate change");
      return RC_QUERY_FAILED;
    }

    $this->Where($this->queueSqlColumns[QUEUE_COLUMN_TRACK_ID].'='.$this->trackId);
    $this->Where($this->queueSqlColumns[QUEUE_COLUMN_QUESTION_ID].'='.$questionId);
    $rc = $this->Update(array($this->queueSqlColumns[QUEUE_COLUMN_SEQUENCE] => $newSequenceId),
			QUEUE_TABLE);
    if ($rc === FALSE) {
      error_log("Track.SetSequenceId($questionId,$newSequenceId): Failed shifting sequence numbers to accomodate change");
      return RC_QUERY_FAILED;
    }

    return RC_OK;    
  }


  function ShiftQueueGroup($groupId, $oldSequenceId, $newSequenceId, $firstSequenceInGroup, $lastSequenceInGroup) {

    // Determine group size.  This will establish how far to move non group questions
    $groupSize = $lastSequenceInGroup - $firstSequenceInGroup + 1;
    // Determine if we are going up or down
    $difference = $newSequenceId - $firstSequenceInGroup;

    if (DEBUG & DEBUG_CLASS_FUNCTIONS)
      error_log("====> Track.ShiftQueueGroup($groupId...)");

    $this->Where($this->queueSqlColumns[QUEUE_COLUMN_TRACK_ID].'='.$this->trackId);
    //$this->Where($this->queueSqlColumns[QUEUE_COLUMN_GROUP_ID].'!='.$groupId);
    if ($difference < 0) { // Group decreasing in sequence (earlier delivery)
      $this->Where($this->queueSqlColumns[QUEUE_COLUMN_SEQUENCE].'>='.$newSequenceId);
      $this->Where($this->queueSqlColumns[QUEUE_COLUMN_SEQUENCE].'<'.$firstSequenceInGroup);
      $op = '+';
    } else { // Shift sequence of questions opposite direction by size of group
      $this->Where($this->queueSqlColumns[QUEUE_COLUMN_SEQUENCE].' > '.$lastSequenceInGroup);
      $this->Where($this->queueSqlColumns[QUEUE_COLUMN_SEQUENCE].' < '.($newSequenceId + $groupSize));
      $op = '-';
    }
    $rc = $this->Update(array($this->queueSqlColumns[QUEUE_COLUMN_SEQUENCE] =>
			      $this->queueSqlColumns[QUEUE_COLUMN_SEQUENCE]." $op $groupSize"),
			QUEUE_TABLE);
    if ($rc === FALSE) {
      error_log("Track.ShiftQueueGroup($groupId,$oldSequenceId,$newSequenceId,$firstSequenceInGroup,$lastSequenceInGroup): "
		."Failed shifting sequence numbers to accomodate change");
      return RC_QUERY_FAILED;
    }

    // Now we do the same thing except with the group instead and in the opposite direction
    $this->Where($this->queueSqlColumns[QUEUE_COLUMN_TRACK_ID].'='.$this->trackId);
    if ($difference < 0) {
      $this->Where($this->queueSqlColumns[QUEUE_COLUMN_GROUP_ID].'='.$groupId);
      $op = '';
    } else { // difference less than 0 indicating sequence smaller than dest (op already negative in number)
      $this->Where($this->queueSqlColumns[QUEUE_COLUMN_GROUP_ID].'='.$groupId);
      $op = '+';
    }
    $rc = $this->Update(array($this->queueSqlColumns[QUEUE_COLUMN_SEQUENCE] =>
			      $this->queueSqlColumns[QUEUE_COLUMN_SEQUENCE]." $op $difference"),
			QUEUE_TABLE);
    if ($rc === FALSE) {
      error_log("Track.ShiftQueueGroup($groupId,$oldSequenceId,$newSequenceId,$firstSequenceInGroup,$lastSequenceInGroup): "
		."Failed shifting group sequence numbers to accomodate change");
      return RC_QUERY_FAILED;
    }

    return RC_OK;    
  }


  function GetSearchFieldNameById($searchColumnId) {

    $questionSqlColumns = unserialize(QUESTION_SQL_COLUMNS);
    $languageSqlColumns = unserialize(LANGUAGE_SQL_COLUMNS);
    $domainSqlColumns = unserialize(DOMAIN_SQL_COLUMNS);
    $categorySqlColumns = unserialize(CATEGORY_SQL_COLUMNS);
    
    switch($searchColumnId) {
    case QUEUE_LIST_COLUMN_TRACK_ID:
      $searchField = QUEUE_TABLE.'.'.$this->queueSqlColumns[QUEUE_COLUMN_TRACK_ID];
      break;
    case QUEUE_LIST_COLUMN_QUESTION_ID:
      $searchField = QUEUE_TABLE.'.'.$this->queueSqlColumns[QUEUE_COLUMN_QUESTION_ID];
      break;
    case QUEUE_LIST_COLUMN_SEQUENCE:
      $searchField = QUEUE_TABLE.'.'.$this->queueSqlColumns[QUEUE_COLUMN_SEQUENCE];
      break;
    case QUEUE_LIST_COLUMN_GROUP_ID:
      $searchField = QUEUE_TABLE.'.'.$this->queueSqlColumns[QUEUE_COLUMN_GROUP_ID];
      break;
    case QUEUE_LIST_COLUMN_QUESTION_TITLE:
      $searchField = QUESTION_TABLE.'.'.$questionSqlColumns[QUESTION_COLUMN_TITLE];
      break;
    case QUEUE_LIST_COLUMN_QUESTION_LANGUAGE_ID:
      $searchField = LANGUAGE_TABLE.'.'.$languageSqlColumns[LANGUAGE_COLUMN_NAME];
      break;
    case QUEUE_LIST_COLUMN_QUESTION_DOMAIN_ID:
      $searchField = DOMAIN_TABLE.'.'.$domainSqlColumns[DOMAIN_COLUMN_NAME];
      break;
    case QUEUE_LIST_COLUMN_QUESTION_CATEGORY_ID:
      $searchField = CATEGORY_TABLE.'.'.$categorySqlColumns[CATEGORY_COLUMN_NAME];
      break;
    case QUEUE_LIST_COLUMN_QUESTION_VERSION_DATE:
      $searchField = QUESTION_TABLE.'.'.$questionSqlColumns[QUESTION_COLUMN_VERSION_DATE];
      break;
    default:
      $searchField = '';
      error_log("Track.GetSearchFieldNameById($searchColumnId): Unknown search column specified in first arg");
      break;
    }

    return $searchField;
  }



  function GetAssignmentSearchFieldNameById($searchColumnId) {

    $assignmentSqlColumns = unserialize(ASSIGNMENT_SQL_COLUMNS);
    $languageSqlColumns = unserialize(LANGUAGE_SQL_COLUMNS);
    $domainSqlColumns = unserialize(DOMAIN_SQL_COLUMNS);
    $userSqlColumns = unserialize(USER_SQL_COLUMNS);
    
    switch($searchColumnId) {
    case ASSIGNMENT_COLUMN_USER_ID:
      $searchField = USER_TABLE.'.'.$userSqlColumns[USER_COLUMN_FULL_NAME];
      break;
    case ASSIGNMENT_COLUMN_DOMAIN_ID:
      $searchField = DOMAIN_TABLE.'.'.$domainSqlColumns[DOMAIN_COLUMN_NAME];
      break;
    case ASSIGNMENT_COLUMN_LANGUAGE_ID:
      $searchField = LANGUAGE_TABLE.'.'.$languageSqlColumns[LANGUAGE_COLUMN_NAME];
      break;
    default:
      if (!isset($assignmentSqlColumns[$searchColumnId])) {
	error_log("Track.GetAssignmentSearchFieldNameById($searchColumnId): Unknown search column specified in first arg");
	$searchField = $assignmentSqlColumns[DEFAULT_ASSIGNMENT_SEARCH_COLUMN];
      } else
	$searchField = $assignmentSqlColumns[$searchColumnId];
      break;
    }

    return $searchField;
  }



  function CreateTrack() {

    // Function local vars
    $oldUmask = 0;
    $cmd = 'mysql';
    $this->rc = 0;
    $mysqlOutpt = '';

    // Sanity checks, this is a major operation
    if (!file_exists(TRACK_SQL_FILE))
      return RC_TRACK_SQL_FILE_NOT_FOUND;
      
    if (file_exists($this->trackDirectory))
      return RC_TRACK_DIRECTORY_EXISTS;

    // Make sure the directory where we create is writable
    if (!is_writable("../tracks"))
      return RC_CREATE_DIRECTORY_FAILED;

    // Create the directory
    $oldUmask = umask(0);
    if (!mkdir(APPLICATION_ROOT."/tracks/$this->trackDirectory", 0775)) {
      umask($oldUmask);
      return RC_CREATE_DIRECTORY_FAILED;
    }
    
    if (!mkdir(APPLICATION_ROOT."/tracks/$this->trackDirectory/email", 0775)) {
      umask($oldUmask);
      return RC_CREATE_DIRECTORY_FAILED;
    }
    
    if (!mkdir(APPLICATION_ROOT."/tracks/$this->trackDirectory/motif", 0775)) {
      umask($oldUmask);
      return RC_CREATE_DIRECTORY_FAILED;
    }
    
    if (!mkdir(APPLICATION_ROOT."/tracks/$this->trackDirectory/news", 0775)) {
      umask($oldUmask);
      return RC_CREATE_DIRECTORY_FAILED;
    }
    
    if (!mkdir(APPLICATION_ROOT."/tracks/$this->trackDirectory/reports", 0775)) {
      umask($oldUmask);
      return RC_CREATE_DIRECTORY_FAILED;
    }

    // Restore old umask value
    umask($oldUmask);

    // Close any existing connections and reopen to the master database
    $this->Open(AUTH_DB_NAME, SA_DB_USER, SA_DB_PASS);

    // Add new track to master track table to get new track ID
    $this->trackId =
      $this->Insert(array($this->trackListSqlColumns[TRACK_LIST_COLUMN_NAME] => esql($_REQUEST['trackName'])),
		    TRACK_LIST_TABLE);

    // See if insert statement failed
    if (!$this->trackId) {
      $this->cacheError = $this->GetErrorMessage();
      system("rm -rf ".APPLICATION_ROOT."/tracks/$this->trackDirectory");
      return RC_CREATE_ID_FAILED;
    }

    // Assign our new directory name
    $this->dbName = TRACK_DB_NAME_PREFIX.$this->trackId;

    // Now we have to go back and update the same table with the new database name
    // This is a lame architecture and at some point needs to be gutted!
    // TODO: redesign the whole HR_Tools_Authentication and HR_Tools_Track
    // master database concepts
    $this->Where($this->trackListSqlColumns[TRACK_LIST_COLUMN_ID]."=".$this->trackId);
    if (!$this->Update(array($this->trackListSqlColumns[TRACK_LIST_COLUMN_DB_NAME] => $this->dbName),
		       TRACK_LIST_TABLE)) {
      $this->cacheError = $this->GetErrorMessage();
      $this->Where($this->trackListSqlColumns[TRACK_LIST_COLUMN_ID]."=".$this->trackId);
      $this->Delete(TRACK_LIST_TABLE);
      system("rm -rf ".APPLICATION_ROOT."/tracks/$this->trackDirectory");
      return RC_UPDATE_DB_NAME_FAILED;
    }

    // Create the new track database
    if (!$this->CreateDb(TRACK_DB_NAME_PREFIX.$this->trackId)) {
      $this->cacheError = $this->GetErrorMessage();
      $this->Where($this->trackListSqlColumns[TRACK_LIST_COLUMN_ID]."=".$this->trackId);
      $this->Delete(TRACK_LIST_TABLE);
      system("rm -rf ".APPLICATION_ROOT."/tracks/$this->trackDirectory");
      return RC_CREATE_DB_FAILED;
    }
    
    // Execute the import SQL script
    if (MYSQL_CLIENT_PATH && file_exists(MYSQL_CLIENT_PATH) &&
	is_executable(MYSQL_CLIENT_PATH)) {
      $cmd = MYSQL_CLIENT_PATH;
    } else {
      // Otherwise, try to find it in th path
      $cmd = 'mysql';
      $cmd = `which $cmd 2> /dev/null`;
      if (!$cmd) {
	$this->Where($this->trackListSqlColumns[TRACK_LIST_COLUMN_ID]."=".$this->trackId);
	$this->Delete(TRACK_LIST_TABLE);
	system("rm -rf ".APPLICATION_ROOT."/tracks/$this->trackDirectory");
	$this->DropDb($this->dbName);
	return RC_MYSQL_CLI_NOT_FOUND;
      }
    }

    // Execute the SQL sript
    // At first glance, the & at the end of this command may seem entirely unnecessary.
    // The problem is this.  If it is left off,  the system call executes the command
    // in a shell and keeps it in a shell until finished.  The return is designed to
    // be examined using some of the MACROS available in other languages see man
    // system for more info.  Basically the upper 2 bits or so of the return code are
    // used to signify things other than the programs actual return code.  So for a
    // return of 0 in a program you will actually see something like 128.  Using the &
    // drops the shell when the command is backgrounded and upon return you get the
    // actual unmodified return code.  Coincidentally, because the shell process is
    // dropped this is also a tad bit more efficient.
    $cmd .= escapeshellcmd($cmd." -h ".DB_HOST." -u ".SA_DB_USER." -p".SA_DB_PASS." ".$this->dbName)
      ." < ".TRACK_SQL_FILE." 2>&1 &";
    $this->mysqlOutput = system($cmd, $this->rc);

    // Make sure all went well
    if ($this->rc || $this->mysqlOutput) {
      $this->mysqlOutput = "Command($cmd) produced: ".$this->mysqlOutput;
      $this->Where($this->trackListSqlColumns[TRACK_LIST_COLUMN_ID]."=".$this->trackId);
      $this->Delete(TRACK_LIST_TABLE);
      system("rm -rf ".APPLICATION_ROOT."/tracks/$this->trackDirectory");
      $this->DropDb($this->dbName);
      return RC_MYSQL_SCRIPT_FAILED;
    }

    // Make this new track the track for this object
    if (!$this->SetDb($this->dbName)) {
      $this->cacheError = $this->GetErrorMessage();
      $this->SetDb(AUTH_DB_NAME);
      $this->Where($this->trackListSqlColumns[TRACK_LIST_COLUMN_ID]."=".$this->trackId);
      $this->Delete(TRACK_TABLE);
      system("rm -rf ".APPLICATION_ROOT."/tracks/$this->trackDirectory");
      $this->DropDb($this->dbName);

      return RC_SELECT_DB_FAILED;
    }

    // Setup the Track table for this track
    if ($this->Insert(array($this->trackSqlColumns[TRACK_COLUMN_NAME] => $this->trackName,
			     $this->trackSqlColumns[TRACK_COLUMN_DIRECTORY] => $this->trackDirectory),
		       TRACK_TABLE) === FALSE) {
      $this->cacheError = $this->GetErrorMessage();
      $this->SetDb(AUTH_DB_NAME);
      $this->Where($this->trackListSqlColumns[TRACK_LIST_COLUMN_ID]."=".$this->trackId);
      $this->Delete(TRACK_TABLE);
      system("rm -rf ".APPLICATION_ROOT."/tracks/$this->trackDirectory");
      $this->DropDb($this->dbName);

      return RC_TRACK_INIT_DATA_FAILED;
    }
	
    // Make this new track the track for this object
    $this->SetTrackId($this->trackId);

    return RC_OK;
  }


  function DeleteTrack() {
    
    $rc = 0;
    $this->cacheError = '';
    $queueSqlColumns = unserialize(QUEUE_SQL_COLUMNS);
    $participantSqlColumns = unserialize(PARTICIPANT_SQL_COLUMNS);

    $oldDbName = $this->dbName;
    if (!$this->Open($this->dbName, ORG_ADMIN_USER, ORG_ADMIN_PASS, DB_HOST))
      return FALSE;

    // Remove all users from this track
    $this->Where($participantSqlColumns[PARTICIPANT_COLUMN_TRACK_ID]."=".$this->trackId);
    if ($this->Delete(PARTICIPANT_TABLE) === FALSE) {
      $this->cacheError .= "Track.DeleteTrack(): Failed to update users to trackid of null in this track("
	.$this->trackId."): ".$this->GetErrorMessage();
      error_log($this->cacheError);
      return RC_DELETE_FAILED;
    }

    // Remove all queue entries associated with this track
    $this->Where($queueSqlColumns[QUEUE_COLUMN_TRACK_ID]."=".$this->trackId);
    if ($this->Delete(QUEUE_TABLE) === FALSE) {
      $this->cacheError .= "Track.DeleteTrack(): Failed to delete queue entries for track("
	.$this->trackId."): ".$this->GetErrorMessage();
      error_log($this->cacheError);
      return RC_DELETE_FAILED;
    }

    // Actually remove the track from the track table
    $this->Where($this->trackSqlColumns[TRACK_COLUMN_ID]."=".$this->trackId);
    if ($this->Delete(TRACK_TABLE) === FALSE) {
      $this->cacheError .= "Failed to delete track: ".$this->GetErrorMessage();
      error_log($this->cacheError);
      return RC_DELETE_FAILED;
    }

    return RC_OK;
  }


  function UpdateInfo($globalGroupId = 0) {
    
    $qId = 0;
    $questionSqlColumns = unserialize(QUESTION_SQL_COLUMNS);

    //error_log("CALLED: UPDATEINFO(): Where clase is: ".$this->where);
    if ($this->trackId) {
      $this->Where($this->trackSqlColumns[TRACK_COLUMN_ID]." = ".$this->trackId);
      $rc = $this->Update(array($this->trackSqlColumns[TRACK_COLUMN_NAME] => $this->trackName,
				$this->trackSqlColumns[TRACK_COLUMN_SUPERVISOR_ID] => $this->supervisorId,
				$this->trackSqlColumns[TRACK_COLUMN_IS_MANAGEMENT] => (int)$this->isManagement,
				$this->trackSqlColumns[TRACK_COLUMN_IS_BINDING] => (int)$this->isBinding,
				$this->trackSqlColumns[TRACK_COLUMN_IS_LOOPING] => (int)$this->isLooping,
				$this->trackSqlColumns[TRACK_COLUMN_EMAIL_REPLY_TO] => $this->emailReplyTo,
				$this->trackSqlColumns[TRACK_COLUMN_RECURRENCE] => $this->recurrence,
				$this->trackSqlColumns[TRACK_COLUMN_START_DATE] => $this->startDate,
				$this->trackSqlColumns[TRACK_COLUMN_DELINQUENCY_NOTIFICATION] => $this->delinquencyNotification),
			  TRACK_LIST_TABLE);
      //$this->trackSqlColumns[TRACK_COLUMN_TRACK_NEWS_URL] => $this->trackNewsUrl),
      if ($rc === FALSE) {
	$this->cacheError = $this->GetErrorMessage();
	return RC_UPDATE_INFO_FAILED;
      }

    } else {
      // Insert new track data here
      $this->trackId =
	$this->Insert(array($this->trackSqlColumns[TRACK_COLUMN_NAME] => $this->trackName,
			    $this->trackSqlColumns[TRACK_COLUMN_SUPERVISOR_ID] => $this->supervisorId,
			    $this->trackSqlColumns[TRACK_COLUMN_IS_MANAGEMENT] => (int)$this->isManagement,
			    $this->trackSqlColumns[TRACK_COLUMN_IS_BINDING] => (int)$this->isBinding,
			    $this->trackSqlColumns[TRACK_COLUMN_IS_LOOPING] => (int)$this->isLooping,
			    $this->trackSqlColumns[TRACK_COLUMN_IS_AB1825] => (int)$this->isAb1825,
			    $this->trackSqlColumns[TRACK_COLUMN_EMAIL_REPLY_TO] => $this->emailReplyTo,
			    $this->trackSqlColumns[TRACK_COLUMN_RECURRENCE] => $this->recurrence,
			    $this->trackSqlColumns[TRACK_COLUMN_START_DATE] => $this->startDate,
			    $this->trackSqlColumns[TRACK_COLUMN_DELINQUENCY_NOTIFICATION] => $this->delinquencyNotification),
		      TRACK_LIST_TABLE);
      //$this->trackSqlColumns[TRACK_COLUMN_TRACK_NEWS_URL] => $this->trackNewsUrl),
      if ($this->trackId === FALSE) {
	$this->cacheError = $this->GetErrorMessage();
	return RC_UPDATE_INFO_FAILED;
      }
      
      // If an ab1825 track then add all ab1825 questions to the track by question number order
      if ($this->isAb1825) {
	$this->Select($questionSqlColumns[QUESTION_COLUMN_ID], QUESTION_TABLE);
	$this->Where($questionSqlColumns[QUESTION_COLUMN_ID].' >= 50000');
	$this->Where($questionSqlColumns[QUESTION_COLUMN_ID].' < 55000');
	$this->Where($questionSqlColumns[QUESTION_COLUMN_PARENT_ID].' = 0');
	$this->SortBy($questionSqlColumns[QUESTION_COLUMN_ID]); // ascending
	$rc = $this->Query(MANY_VALUES, VALUES_FORMAT);
	if ($rc === FALSE) {
	  error_log("Failed to get max ab1825 question number to add to new track".$this->trackId);
	} elseif ($rc >= 50000) {
	  // Loop through assignment ids and if 
	  foreach($rc as $qId)
	    $this->AssignQuestion((int)$qId, $globalGroupId);
	} else {
	  error_log("***** ERROR: An AB1825 class was created but there are no AB1825 questions (50000-549999) in question database");
	  $this->DeleteTrack();
	  return RC_CLASS_CREATE_NO_AB1825_CONTENT;
	}
      }

    }
    
    // Make sure we update the verbose recurrence string for updated display
    $obj = new RecurrenceObject();
    $obj->ParseTokenString($this->recurrence);
    $this->strRecurrence = $obj->CreateVerboseTokenString();
    
    //$this->Close();

    return RC_OK;
  }


  function UpdateContact() {
    
    if (!$this->Update(array($this->trackSqlColumns[TRACK_COLUMN_CONTACT_ID] => $this->contactId,
			     $this->trackSqlColumns[TRACK_COLUMN_DELINQUENCY_NOTIFICATION] => $this->delinquencyNotification),
		       TRACK_TABLE)) {
      $this->cacheError = $this->GetErrorMessage();
      return RC_UPDATE_INFO_FAILED;
    }

    return RC_OK;
  }


  // This will remove all entries from the queue and adjust sequence numbers accordingly
  function GetTrackIdsWithAssignedQuestion($questionId) {

    // Data validation
    if (!is_numeric($questionId) || $questionId <= 0)
      return RC_INVALID_ARG;

    $this->Select('DISTINCT('.$this->queueSqlColumns[QUEUE_COLUMN_TRACK_ID].')',
		QUEUE_TABLE);
    $this->Where($this->queueSqlColumns[QUEUE_COLUMN_QUESTION_ID].' = '.$questionId);
    $trackList = $this->Query(MANY_VALUES, VALUES_FORMAT);

    if ($trackList === FALSE) {
      error_log("Track::GetTrackListWithQuestionInQueue($db,$questionId): Distinct query failed: "
		.$this->GetErrorMessage());
      return RC_QUERY_FAILED;
    }

    return $trackList;
  }


}
