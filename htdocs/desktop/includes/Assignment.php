<?php

require_once('common.php');
require_once('tcpdf/tcpdf.php');
require_once('Question.php');

class Assignment extends Question {

  //var $assignmentListLabelColumns = array();
  var $assignmnetSqlColumns = array();
  var $userId = 0;
  var $trackId = 0;
  var $assignmentId = 0;
  var $parentAssignmentId = 0;
  var $assignmentLanguageId = 1;
  var $assignmentDomainId = 1;
  var $assignmentVersionDate = '';

  var $nextAssignmentId = 0;
  var $deliveryDate = '';
  var $dueDate = '';
  var $completionDate = '';
  var $firstAnswer = 0;
  var $finalAnswer = 0;
  var $answeredCorrectly = FALSE;
  var $answeredLate = FALSE;
  var $exitInfo = EXIT_INFO_INCOMPLETE;
  var $departmentId = 0;
  var $divisionId = 0;
  var $completedSeconds = 0;
  var $noSelectMsg = 0;

  // Parent child question feature support
  var $progress = 0;

  var $assignmentSqlColumns = array();


  function Assignment($orgId, $newAssignmentId = 0) {
    
    //$this->assignmentListLabelColumns = unserialize(ASSIGNMENT_LIST_LABELS);
    $this->assignmentSqlColumns = unserialize(ASSIGNMENT_SQL_COLUMNS);
    if (!$this->Question((int)$orgId))
	return FALSE;

    if ($newAssignmentId > 0) {
      if ($this->SetAssignmentId($newAssignmentId) == RC_OK)
	return TRUE;
    }

    return FALSE;
  }


  // This retrieves information from the database and populates the properties of this
  // object with values and information from the database.
  function SetAssignmentId($newAssignmentId) {
    
    if (!is_numeric($newAssignmentId)) {
      error_log("Assignment::SetAssignmentId(newAssignmentId=$newAssignmentId): invalid non-numeric passed to function");
      return RC_INVALID_ARG;
    }      

    $newAssignmentId = (int)$newAssignmentId;

    if ($newAssignmentId <= 0) {
      error_log("Assignment::SetAssignmentId(newAssignmentId=$newAssignmentId): invalid numeric<=0 passed to function");
      return RC_INVALID_ARG;
    }

    $key = '';
    $val = '';
    $selectColumns = array();
    $values = array();
    
    if ($this->assignmentId != $newAssignmentId) {
      $this->assignmentId = $newAssignmentId;
      foreach ($this->assignmentSqlColumns as $key => $val) {
      	//	if ($key == ASSIGNMENT_COLUMN_VERSION_DATE || $key == ASSIGNMENT_COLUMN_DELIVERY_DATE ||
      	//	    $key == ASSIGNEMNT_COLUMN_DUE_DATE || $key == ASSIGNMENT_COLUMN_COMPLETION_DATE)
      	//	  array_push($selectColumns, "DATE($val)");
      	//	else
      	array_push($selectColumns, $val);
      }
      $this->Select($selectColumns, ASSIGNMENT_TABLE);
      $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ID]." = ".$this->assignmentId);
      $values = $this->Query(SINGLE_ROW);
      
      if ($values === FALSE) {
	error_log("Assignment::SetAssignmentId(newAssignmentId=$newAssignmentId): "
		  .$this->GetErrorMessage());
	return RC_QUERY_FAILED;
      }
      $this->assignmentDomainId = $values[$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_DOMAIN_ID]];
      $this->assignmentVersionDate = $values[$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_VERSION_DATE]];
      $trimmedAssignmentVersionDate = explode(' ', $this->assignmentVersionDate);
      $trimmedAssignmentVersionDate = $trimmedAssignmentVersionDate[0]; // Just get the date part

      // Get the question if all 3 pieces of info required to get question are present
      //      error_log("+_+_+_+_+_+qnumber: ".$values[$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_QUESTION_NUMBER]].
      //		", vdate: ".$trimmedAssignmentVersionDate.", domainId: ".$this->assignmentDomainId);
	// && $trimmedAssignmentVersionDate && $this->assignmentDomainId > 0)
      if ($values[$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_QUESTION_NUMBER]] > 0)
	$this->Question($this->orgId, $values[$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_QUESTION_NUMBER]],
			$this->assignmentDomainId, 0); //$this->assignmentVersionDate (no longer use version date to get a question)
      else
	error_log("Assignment::SetAssignmentId(newAssignmentId=$newAssignmentId): invalid version date("
		  ."$trimmedAssignmentVersionDate) or domain id(".$this->assignmentDomainId.")"
		  ."while checking for params of Question() contructor");

      $this->userId = $values[$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_USER_ID]];
      $this->trackId = $values[$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_TRACK_ID]];
      $this->parentAssignmentId = (int)$values[$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_PARENT_ID]];
      $this->assignmentLanguageId = $values[$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_LANGUAGE_ID]];
      $this->deliveryDate = $values[$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_DELIVERY_DATE]];
      $this->dueDate = $values[$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_DUE_DATE]];
      $this->completionDate = $values[$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_COMPLETION_DATE]];
      $this->firstAnswer = $values[$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_FIRST_ANSWER]];
      $this->finalAnswer = $values[$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_FINAL_ANSWER]];
      $this->answeredCorrectly = $values[$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ANSWERED_CORRECTLY]];
      $this->answeredLate = $values[$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ANSWERED_LATE]];
      $this->exitInfo = $values[$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_EXIT_INFO]];
      $this->departmentId = $values[$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_DEPARTMENT_ID]];
      $this->divisionId = $values[$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_DIVISION_ID]];
      $this->completedSeconds = $values[$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_COMPLETED_SECONDS]];
      
      $this->questionSetSize = $this->GetAssignmentSetSize();
      if ($this->questionSetSize > 1) {
	$completed = $this->GetCompletedSetSize();
	$this->progress = (int)(($completed / $this->questionSetSize) * 100.0);
	if ($this->completionDate) // handle getting next id for a completed question
	  $this->nextAssignmentId = $this->GetNextCompleteId();
	else
	  $this->nextAssignmentId = $this->GetNextIncompleteId();
      }
    }
    
    return RC_OK;
  }


  // This function gets all the assignments from all the tracks that this user belongs to and returns them
  // in the form of a list of associative arrays.
  // i.e. array[0] = array('id' => assignmentId, 'title' => questionTitle);
  function GetIncompleteAssignments($searchUserId, $searchDivisionId = 0, $searchDepartmentId = 0) {
    
    // validation checks
    if (!isset($searchUserId) || !is_numeric($searchUserId) || $searchUserId <= 0) {
      error_log("Assignment::GetIncompleteAssignments(searchUserId=$searchUserId,".
		"searchDivisionId=$searchDivisionId,searchDepartmentId=$searchDepartmentId): ".
		"Invalid argument for searchUserId, not numerica or <= 0");
      return RC_INVALID_ARG;
    }
    if (!isset($searchDivisionId) || !is_numeric($searchDivisionId) || $searchDivisionId < 0) {
      error_log("Assignment::GetIncompleteAssignments(searchUserId=$searchUserId,".
		"searchDivisionid=$searchDivisionId,searchDepartmentId=$searchDepartmentId): ".
		"Invalid argument for searchDivisionId, not numeric or less than 0");
      return RC_INVALID_ARG;
    }
    if (!is_numeric($searchDepartmentId) || $searchDepartmentId < 0) {
      error_log("Assignment::GetIncompleteAssignments(searchUserId=$searchUserId,".
		"searchDivisionid=$searchDivisionId,searchDepartmentId=$searchDepartmentId): ".
		"Invalid argument for searchDepartmentId, not numeric or less than 0");
      return RC_INVALID_ARG;
    }

    $this->Select(array($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ID],
			QUESTION_TABLE.'.'.$this->questionSqlColumns[QUESTION_COLUMN_TITLE]),
		  ASSIGNMENT_TABLE);
    $this->LeftJoinUsing(QUESTION_TABLE, $this->questionSqlColumns[QUESTION_COLUMN_ID]);
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_USER_ID].' = '.esql($searchUserId));
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_EXIT_INFO].' = 0');
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_PARENT_ID].' = 0');
    if ($searchDivisionId == 0 && $searchDepartmentId == 0) {
      $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_DIVISION_ID]." = 0");
      $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_DEPARTMENT_ID]." = 0");
    } elseif ($searchDivisionId) {
      $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_DIVISION_ID]." = $searchDivisionId");
    } elseif ($searchDepartmentId) {
      $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_DEPARTMENT_ID]." = $searchDepartmentId");
    }
    $this->SortBy(array($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_DELIVERY_DATE],
			$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ID]), TRUE);
    $values = $this->Query(MANY_ROWS, KEYS_FORMAT);
    if ($values === FALSE)
      return RC_QUERY_FAILED;
    
    return $values;
  }
  

  // This function gets all the assignments from all the tracks that this user belongs to and returns them
  // in the form of a list of associative arrays.
  // i.e. array[0] = array('id' => assignmentId, 'title' => questionTitle);
  //  function GetCompletedAssignments($searchUserId, $searchDivisionId = 0, $searchDepartmentId, $maxReturned = 10)
  //  function GetCompletedAssignments($searchUserId, $maxReturned = 10)
  function GetCompletedAssignments($searchUserId) {
    
    // validation check
    if (!isset($searchUserId) || !is_numeric($searchUserId) || $searchUserId <= 0) {
      error_log("Assignment::GetAssignments(searchUserId=$searchUserId): invalid argument passed to method");
      return RC_INVALID_ARG;
    }

    $this->Select(array($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ID],
			QUESTION_TABLE.'.'.$this->questionSqlColumns[QUESTION_COLUMN_TITLE]),
		  ASSIGNMENT_TABLE);
    $this->LeftJoinUsing(QUESTION_TABLE, $this->questionSqlColumns[QUESTION_COLUMN_ID]);
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_USER_ID].' = '.esql($searchUserId));
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_COMPLETION_DATE].' != 0');
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_EXIT_INFO].' != 0');
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_PARENT_ID].' = 0');
    $this->SortBy($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_COMPLETION_DATE], TRUE);
    //    $this->Limit($maxReturned);
    $values = $this->Query(MANY_ROWS, KEYS_FORMAT);
    if ($values === FALSE)
      return RC_QUERY_FAILED;
    
    return $values;
  }
  

  function Completed($locFirstAnswer, $locFinalAnswer, $locCorrectAnswer, $locExitInfo, $completedSeconds) {

    // Make sure we have a valid instance of this object
    if ($this->assignmentId <= 0) {
      error_log("Assignment::Completed(locFirstAnswer=$locFirstAnswer,locFinalAnswer=$locFinalAnswer,".
		"locCorrectAnswer=$locCorrectAnswer,locExitInfo=$locExitInfo): ".
		"Invalid object, local assignmentId=".$this->assignmentId);
      return RC_OBJECT_INSTANCE_REQUIRED;
    }

    // validate numeric format of args
    if (!is_numeric($locFirstAnswer) || !is_numeric($locFinalAnswer) || !is_numeric($locExitInfo)) {
      error_log("Assignment::Completed(locFirstAnswer=$locFirstAnswer,locFinalAnswer=$locFinalAnswer,".
		"locCorrectAnswer=$locCorrectAnswer,locExitInfo=$locExitInfo): ".
		"Invalid argument, all args required to be nonnegative numeric values");
      return RC_INVALID_ARG;
    }

    $updateArr = array($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_FIRST_ANSWER] => $locFirstAnswer,
		       $this->assignmentSqlColumns[ASSIGNMENT_COLUMN_FINAL_ANSWER] => $locFinalAnswer,
		       $this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ANSWERED_CORRECTLY] => $locCorrectAnswer,
		       $this->assignmentSqlColumns[ASSIGNMENT_COLUMN_EXIT_INFO] => $locExitInfo,
		       $this->assignmentSqlColumns[ASSIGNMENT_COLUMN_COMPLETION_DATE] => 'NOW()',
		       $this->assignmentSqlColumns[ASSIGNMENT_COLUMN_COMPLETED_SECONDS] => ($this->completedSeconds + $completedSeconds),
		       $this->assignmentSqlColumns[ASSIGNMENT_COLUMN_VERSION_DATE] => $this->versionDate);
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ID].' = '.$this->assignmentId);
    if ($this->Update($updateArr, ASSIGNMENT_TABLE) === FALSE) {
      error_log("Assignment::Completed(locFirstAnswer=$locFirstAnswer,locFinalAnswer=$locFinalAnswer,".
		"locCorrectAnswer=$locCorrectAnswer,locExitInfo=$locExitInfo): ".
		"Update assignment completion failed");
      return RC_QUERY_FAILED;
    }

    return RC_OK;
  }

  
  function CheckAb1825IncompleteCount() {

    $this->Select('COUNT('.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ID].')', ASSIGNMENT_TABLE);
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_TRACK_ID].'='.$this->trackId);
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_COMPLETION_DATE].'=0');
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_USER_ID].'='.$this->userId);
    $cnt = $this->Query(SINGLE_VALUE, VALUES_FORMAT);
    return $cnt;
  }


  function GetIncorrectInSet() {

    $this->Select('COUNT('.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ID].')', ASSIGNMENT_TABLE);
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ANSWERED_CORRECTLY].'='.RESULT_INCORRECT);
    if ($this->parentAssignmentId)
      $this->Where('('.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_PARENT_ID].'='.$this->parentAssignmentId.' OR '.
		   $this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ID].'='.$this->parentAssignmentId.')');
    else
      $this->Where('('.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ID].'='.$this->assignmentId.' OR '.
		   $this->assignmentSqlColumns[ASSIGNMENT_COLUMN_PARENT_ID].'='.$this->assignmentId.')');
    $cnt = $this->Query(SINGLE_VALUE, VALUES_FORMAT);
    return $cnt;
  }


  function GetIncompleteInSet() {

    $this->Select('COUNT('.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ID].')', ASSIGNMENT_TABLE);
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_COMPLETION_DATE].'=0');
    if ($this->parentAssignmentId)
      $this->Where('('.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_PARENT_ID].'='.$this->parentAssignmentId.' OR '.
		   $this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ID].'='.$this->parentAssignmentId.')');
    else
      $this->Where('('.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ID].'='.$this->assignmentId.' OR '.
		   $this->assignmentSqlColumns[ASSIGNMENT_COLUMN_PARENT_ID].'='.$this->assignmentId.')');
    $cnt = $this->Query(SINGLE_VALUE, VALUES_FORMAT);
    return $cnt;
  }

  function GetAb1825LastCompleted($trackId) {

    $this->Select($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ID], ASSIGNMENT_TABLE);
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_COMPLETION_DATE].'!=0');
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_QUESTION_NUMBER].'=50900');
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_PARENT_ID].'=0');
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_TRACK_ID]."=$trackId");
    $this->SortBy($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_COMPLETION_DATE]);
    $this->Limit(1);
    $aID = $this->Query(SINGLE_VALUE, VALUES_FORMAT);
    //error_log("+_+_++_+_+_+_+_++: assignmentID is: $aID");
    if ($aID === FALSE)
      return 0;
    else {
      $this->SetAssignmentId($aID);
      return $aID;
    }
  }

  function GetScorecard($userId, $daysHistory) {
    
    $stats = array();

    $this->Select('COUNT('.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ID].')', ASSIGNMENT_TABLE);
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_COMPLETION_DATE].
		 " > DATE_SUB(NOW(), INTERVAL $daysHistory DAY)");
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ANSWERED_CORRECTLY].'='.RESULT_CORRECT);
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_USER_ID]."=$userId");
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_PARENT_ID].'=0');
    $stats['correct'] = $this->Query(SINGLE_VALUE, VALUES_FORMAT);

    $this->Select('COUNT('.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ID].')', ASSIGNMENT_TABLE);
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_COMPLETION_DATE].
		 " > DATE_SUB(NOW(), INTERVAL $daysHistory DAY)");
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ANSWERED_CORRECTLY].'='.RESULT_INCORRECT);
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_USER_ID]."=$userId");
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_PARENT_ID].'=0');
    $stats['incorrect'] = $this->Query(SINGLE_VALUE, VALUES_FORMAT);
    
    if ($stats['correct'] === FALSE || $stats['incorrect'] === FALSE) {
      error_log("Assignment::GetScorecard(userId=$userId,daysHistory=$daysHistory): Failed to get status from db");
      return FALSE;
    }

    $stats['totalAnswered'] = $stats['correct'] + $stats['incorrect'];

    return $stats;
  }


  function UpdateAssignmentSetExitInfo($exitInfo) {

    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ID].'='.$this->parentAssignmentId);
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_PARENT_ID].'='.$this->parentAssignmentId, ' OR ');
    $res = $this->Update(array($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_EXIT_INFO] => $exitInfo),
			 ASSIGNMENT_TABLE);
    if ($res === FALSE) {
      error_log("Assignment::UpdateQuestionSetExitInfo(exitInfo=$exitInfo): ".
		"Failed to update question set with parent assignment id ".$a->parentAssignmentId);
      return RC_QUERY_FAILED;
    }

    return TRUE;
  }


  function GetAssignmentSetSize() {
    if ($this->parentAssignmentId) {
      $this->Select('COUNT('.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ID].')', ASSIGNMENT_TABLE);
      $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_PARENT_ID].'='.$this->parentAssignmentId);
      $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ID].'='.$this->parentAssignmentId, ' OR ');
    } else {
      $this->Select('COUNT('.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ID].')', ASSIGNMENT_TABLE);
      $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_PARENT_ID].'='.$this->assignmentId);
      $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ID].'='.$this->assignmentId, ' OR ');
    }
    $cnt = $this->Query(SINGLE_VALUE, VALUES_FORMAT);
    if ($cnt === FALSE) {
      error_log('*** ERROR Assignment::GetAssignmentSetSize(): Failed to execute query on assignment('.$this->assignmentId.')');
      return RC_QUERY_FAILED;
    }
    return $cnt;
  }

  
  function GetCompletedSetSize() {
    if ($this->parentAssignmentId) {
      $this->Select('COUNT('.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ID].')', ASSIGNMENT_TABLE);
      $this->Where('('.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_PARENT_ID].'='.$this->parentAssignmentId);
      $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ID].'='.$this->parentAssignmentId.')', ' OR ');
    } else {
      $this->Select('COUNT('.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ID].')', ASSIGNMENT_TABLE);
      $this->Where('('.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_PARENT_ID].'='.$this->assignmentId);
      $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ID].'='.$this->assignmentId.')', ' OR ');
    }
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_COMPLETION_DATE].'!=0');
    $cnt = $this->Query(SINGLE_VALUE, VALUES_FORMAT);
    if ($cnt === FALSE) {
      error_log('*** ERROR Assignment::GetCompletedSetSize(): Failed to execute query on assignment('.$this->assignmentId.')');
      return RC_QUERY_FAILED;
    }
    return $cnt;
  }

  
  function GetNextIncompleteId() {
    $this->Select($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ID], ASSIGNMENT_TABLE);
    if ($this->parentAssignmentId)
      $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_PARENT_ID].'='.$this->parentAssignmentId);
    else
      $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_PARENT_ID].'='.$this->assignmentId);
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_COMPLETION_DATE].'=0');
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ID].'!='.$this->assignmentId);
    $this->SortBy($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ID]);
    $this->Limit(1);
    $next = $this->Query(SINGLE_VALUE, VALUES_FORMAT);
    if ($next === FALSE) {
      error_log('*** ERROR Assignment::GetNextIncompleteId(): Failed to execute query on assignment('.$this->assignmentId.')');
      return RC_QUERY_FAILED;
    }
    if (!$next)
      $next = 0;
    return $next;
  }

  
  function GetNextCompleteId() {
    $this->Select($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ID], ASSIGNMENT_TABLE);
    if ($this->parentAssignmentId)
      $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_PARENT_ID].'='.$this->parentAssignmentId);
    else
      $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_PARENT_ID].'='.$this->assignmentId);
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ID].'>'.$this->assignmentId);
    $this->SortBy($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ID]);
    $this->Limit(1);
    $next = $this->Query(SINGLE_VALUE, VALUES_FORMAT);
    if ($next === FALSE) {
      error_log('*** ERROR Assignment::GetNextIncompleteId(): Failed to execute query on assignment('.$this->assignmentId.')');
      return RC_QUERY_FAILED;
    }
    if (!$next)
      $next = 0;
    return $next;
  }


  function ResetAssignmentSet() {
    
    $ret1 = FALSE;
    $ret2 = FALSE;

    if ($this->parentAssignmentId > 0) {
      $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_PARENT_ID].'='.$this->parentAssignmentId);
      $ret1 = $this->Update(array($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_COMPLETION_DATE] => 0,
				 $this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ANSWERED_CORRECTLY] => 0,
				 $this->assignmentSqlColumns[ASSIGNMENT_COLUMN_FIRST_ANSWER] => 0,
				 $this->assignmentSqlColumns[ASSIGNMENT_COLUMN_FINAL_ANSWER] => 0,
				 $this->assignmentSqlColumns[ASSIGNMENT_COLUMN_EXIT_INFO] => 0),
			   ASSIGNMENT_TABLE);
      $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ID].'='.$this->parentAssignmentId);
      $ret2 = $this->Update(array($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_EXIT_INFO] => 0), ASSIGNMENT_TABLE);
      return ($ret1 && $ret2);
    } else {
      error_log('!!! CRITICAL ERROR: ResetAssignmentSet() called on assignment with ID='.$this->assignmentId.', with parent ID='.$this->parentAssignmentId);
    }
  }


  function DeleteUnansweredAssignments($questionNumber) {
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_QUESTION_NUMBER].' = '.$questionNumber);
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_COMPLETION_DATE].' = 0');
    $this->Delete(ASSIGNMENT_TABLE);
  }


  function GenerateAb1825Certificate($u) {
    // new PDF document with landscape orientation, inche units
    // format is last argument and hopefully letter is the default
    $timeString = '';
    $months = array('', 'January', 'February', 'March', 'April', 'May', 'June',
		    'July', 'August', 'September', 'October', 'November', 'December');

    $this->Select('SUM('.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_COMPLETED_SECONDS].')', ASSIGNMENT_TABLE);
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_TRACK_ID].'='.$this->trackId);
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_USER_ID].'='.$u->userId);
    $seconds = $this->Query(SINGLE_VALUE, VALUES_FORMAT);
    if ($seconds === FALSE) {
      error_log('Assignment::GenerateAb1825Certificate(<userobject id='.$u->userId.'>): Failed to completed '.
		'training time from data base for specified user');
    }

    // Calculate hours and minutes from seconds stored in db
    $compHrs = (int)($seconds/60/60);
    $compMts = (int)(($seconds/60)%60);

    // Build the string containing the length of time completed in training
    if ($compHrs > 0) {
      $timeString .= $compHrs;
      if ($compHrs > 1)
	$timeString .= ' Hours, ';
      else
	$timeString .= ' Hour, ';
    }
    $timeString .= $compMts;
    if ($compMts == 1)
      $timeString .= ' Minute';
    else
      $timeString .= ' Minute(s)';

    $dateComponents = explode(' ', $this->completionDate);
    // If the completion date has not yet been updated in the database it soon will be
    // so using the current date is a safe bet.
    if ($dateComponents[0] == '0000-00-00') {
      $tempDate = getdate();
      $dateComponents[0] = $tempDate['year'].'-'.$tempDate['mon'].'-'.$tempDate['mday'];
    }
    $dateParts = explode('-', $dateComponents[0]);
    $dateParts[1] = $months[(int)$dateParts[1]];
    if (substr($dateParts[2], -1) == '1')
      $dateParts[2] = (int)$dateParts[2].'st';
    elseif (substr($dateParts[2], -1) == '2')
      $dateParts[2] = (int)$dateParts[2].'nd';
    elseif (substr($dateParts[2], -1) == '3')
      $dateParts[2] = (int)$dateParts[2].'rd';
    else
      $dateParts[2] = (int)$dateParts[2].'th';
    
    $pdf = new tcpdf('L');
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(false);
    $pdf->SetCreator('Training Advisor');
    $pdf->SetAuthor('Training Advisor System');
    $pdf->SetSubject('AB1825 Certificate of Completion');
    $pdf->SetTitle('Training Advisor Report');
    $pdf->SetKeywords('training, certificate, ab1825, complete, completion');
    $pdf->SetHeaderData('', 0, '', '');
    $pdf->SetMargins(0, 0, 0, 0);
    $pdf->SetHeaderMargin(0);
    $pdf->SetFooterMargin(0);
    $pdf->SetAutoPageBreak(TRUE, 0);
    $pdf->setImageScale(1.535);

    $pdf->AddPage();
    $pdf->SetDisplayMode('real', 'default');
    $pdf->Image('../themes/Default/presentation/ab1825cert.png', 13, 0, 0, 0, 'png');
    $pdf->SetFontSize(10);
    $pdf->Text(100, 84.5, $dateParts[1].' '.$dateParts[2].', '.$dateParts[0]);
    $pdf->Text(100, 88.5, $timeString);
    $pdf->Text(65, 139.5, $u->firstName);
    $pdf->SetFontSize(11);
    $pdf->Text(80, 160, $dateParts[1].' '.$dateParts[2].', '.$dateParts[0]);
    $pdf->Text(80, 170, $u->firstName.' '.$u->lastName);
    
    $pdf->Output('certificate.pdf', 'D');
  }

}
