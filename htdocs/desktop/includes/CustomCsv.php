<?php

// Global definitions for use with this class and report oriented html query forms


class CustomCsv {

  // Instance variables
  var $fileName;
  var $fh;
  var $version = 0;
  var $majorVersion = 0;
  var $minorVersion = 0;
  var $revisionVersion = 0;
  var $lineNumber = 0;

  // Class methods
  function CustomCsv($filename = 'php://output', $perm = 'w') {

    $this->version = phpversion();
    $versionParts = explode('.', $this->version);
    $this->majorVersion = $versionParts[0];
    $this->minorVersion = $versionParts[1];
    if (isset($versionParts[2]))
      $this->revisionVersion = $versionParts[2];

    if (!strstr($filename, 'php://') && !file_exists($filename)) {
      error_log("*** CustomCsv::CustomCsv(filename=$filename, perm=$perm): ERROR File does not exist: ".$filename);
      return FALSE;
    }
    
    $this->fileName = $filename;
    $this->fh = fopen($this->fileName, $perm);
    if ($this->fh === FALSE) {
      error_log("*** ERROR: CustomCsv::CustomCsv: Failed to open output stream: ".$this->fileName);
      return FALSE;
    }
  }


  // Write HTTP Headers
  // This function will output the headers appropriate for CSV rendering as attachment on web page
  function WriteHeaders($fileName = 'report.csv', $saveAs = TRUE) {
    header('Expires: 0');
    header('Pragma: no-cache');
    header('Cache-Control: private');
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    header('Content-Description: File Transfer');
    header('Content-Type: application/csv');
    if ($saveAs)
      header("Content-Disposition: attachment; filename=$fileName");
    else
      header("Content-Disposition: $fileName");
  }
  

  // Write CSV output
  // Param(array): $fields: list of fields to be output in CSV format
  // Param(format): $newLineFormat:  output either microsoft or unix newlines, default='ms'
  function WriteCsv($fields, $newLineFormat = 'ms') {
    
    if ($this->majorVersion == 5) {
      $response = fputcsv($this->fh, $fields);
    } else { // Roll our own
      // First escape fields that contain commas or double quotes
      for($i = 0; $i < count($fields); $i++) {
	if (strpos($fields[$i], ',') !== FALSE || strpos($fields[$i], '"') !== FALSE) {
	  $fields[$i] = str_replace('"', '""', $fields[$i]);
	  $fields[$i] = '"'.$fields[$i].'"';
	}
      }
      // Now combine
      $output = implode(',', $fields);
      if ($newLineFormat == 'ms')
	$output .= "\r";
      $output .= "\n";
      $response = fwrite($this->fh, $output);
    }

    return $response;
  }


  function ReadCsv($expectedColumnCount=0) {

    $rows = array();

    while ($fields = $this->ReadCsvLine($expectedColumnCount)) {
      array_push($rows, $fields);
      $this->lineNumber++;
    }

    return $rows;
  }


  function ReadCsvLine($expectedColumnCount=0) {
    
    $csvFields = array();

    if ($expectedColumnCount) {
      do {
	$colCount = $this->ReadCsvHelper($csvFields);
	if ($expectedColumnCount) {
	  if (count($csvFields) < $expectedColumnCount)
	    error_log("*** WARNING: CustomCsv::ReadCsvLine(expectedColumnCount=$expectedColumnCount): Column count of ".count($csvFields).
		      " at line $this->lineNumber is less than expected count, attempting to compensate by appending next row columns");
	  if (count($csvFields) > $expectedColumnCount)
	    error_log("*** ERROR: CustomCsv::ReadCsvLine(expectedColumntCount=$expectedColumnCount): Overrun of colums, count of ".count($csvFields).
		      " at line $this->lineNumber exceeded expected count, check format for errors or unqouted strings");
	}
      } while ($colCount !== FALSE && $colCount < $expectedColumnCount);
      //      error_log("*_*_)*_)_*)_)*_*)_)*_*)_*)_*) Read columns: ".print_r($csvFields, TRUE));
    } else {
	$colCount = $this->ReadCsvHelper($csvFields);
	if ($expectedColumnCount) {
	  if (count($csvFields) < $expectedColumnCount)
	    error_log("*** WARNING: CustomCsv::ReadCsvLine(expectedColumnCount=$expectedColumnCount): Column count of ".count($csvFields).
		      " at line $this->lineNumber is less than expected count, attempting to compensate by appending next row columns");
	  if (count($csvFields) > $expectedColumnCount)
	    error_log("*** ERROR: CustomCsv::ReadCsvLine(expectedColumntCount=$expectedColumnCount): Overrun of colums, count of ".count($csvFields).
		      " at line $this->lineNumber exceeded expected count, check format for errors or unqouted strings");
	}
    }

    //    error_log("+_+_+_+_+_+_+_+_+_+ PARSED INTO: ".print_r($csvFields, TRUE));
    return $csvFields;
  }


  function ReadCsvHelper(&$csvFields) {

    $line = '';
    $fields = array();

    $line = $this->GetLine();
    if ($line === FALSE) {
      error_log("Info: CustomCsv::ReadCsvHelper: end of file reached at line $this->lineNumber");
      return FALSE;
    }
    $fields = explode(',', $line);
    $this->ParseFields($fields, $csvFields);

    return count($csvFields);
  }


  function GetLine() {

    $line = '';

    do {
      $line = fgets($this->fh);
    } while (!feof($this->fh) && (trim($line) == '' || preg_match("/^\s*\,+\s*$/", $line)));

    if (feof($this->fh))
      return FALSE;

    return trim($line);
  }


  function ParseFields($fields, &$csvFields) {

    $quoteStarted = FALSE;
    $continuationFlag = FALSE;
    $startQuote = '';
    $tmp = '';
    $i = 0;
    $j = 0;

    if (count($csvFields)) {
      $j = count($csvFields) - 1;
      $quoteStarted = TRUE;
      $continuationFlag = TRUE;
    }

    for ($i = 0; $i < count($fields); $i++) {
      $tmp = trim($fields[$i]);

      if (!$quoteStarted && substr($tmp, 0, 1) == '"') {
	$quoteStarted = TRUE;
	$startQuote = $fields[$i];
	array_push($csvFields, $fields[$i]);
      }

      if ($quoteStarted) {
	if ($startQuote != $fields[$i]) {
	  if ($i == 0 && $continuationFlag) {
	    $csvFields[$j] .= " $fields[$i]";
	    $continuationFlag = FALSE;
	  } else
	    $csvFields[$j] .= ",$fields[$i]";
	}
	if (substr($tmp, -1) == '"' && substr($tmp, -2) != '\"' &&
	    (substr($tmp, -2) != '""' || substr($tmp, -3) == '"""' || substr($tmp, -3) == '\""')) {
	  $quoteStarted = FALSE;
	  // Strip quotes from quoted field
	  $csvFields[$j] = substr(trim($csvFields[$j]), 1, -1);
	  $csvFields[$j] = str_replace('\"', '"', $csvFields[$j]);
	  $csvFields[$j] = str_replace('""', '"', $csvFields[$j]);
	  $j++;
	}
      } else {
	array_push($csvFields, $fields[$i]);
	$j++;
      }

    }
  }


  function CloseCsv() {
    fclose($this->fh);
  }

}
