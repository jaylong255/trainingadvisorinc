<?php

require_once("DatabaseObject.php");

class Faq extends DatabaseObject {

  var $faqListSqlColumns = array();
  var $faqListLabelColumns = array();
  var $faqSqlColumns = array();
  var $faqId = '';
  var $trackId = '';
  var $dbName = '';
  var $orgId = 0;
  var $question = '';
  var $answer = '';
  var $cacheError = '';
  var $rc = 0;


  function Faq($orgId, $newTrackId = 0, $newFaqId = 0) {

    if (DEBUG & DEBUG_CLASS_FUNCTIONS) {
      error_log("Creating new Faq object: orgId=$orgId, trackId=$newTrackId, faqId=$newFaqId");
    }

    $this->faqListSqlColumns = unserialize(FAQ_LIST_SQL_COLUMNS);
    $this->faqListLabelColumns = unserialize(FAQ_LIST_LABELS);
    $this->faqSqlColumns = unserialize(FAQ_SQL_COLUMNS);

    $this->orgId = (int)$orgId;
    $this->trackId = (int)$newTrackId;

    if ($newTrackId && $newFaqId) {
      $this->SetFaqId((int)$newFaqId);
    } else {
      // Grab the database connection
      $this->DatabaseObject(ORG_DB_NAME_PREFIX.$this->orgId);
    }
  }


  function SetFaqId($newFaqId) {
    
    $values = array();
    
    if (!$newFaqId) {
      error_log("Faq.SetFaqId(newFaqId=$newFaqId): Invalid FAQ ID");
      return;
    }

    if ($this->faqId != $newFaqId) {
      $this->faqId = (int)$newFaqId;
      // Get DB name from HR_Tools_Track db or table
      $this->Open(ORG_DB_NAME_PREFIX.$this->orgId);
      $this->Select('*', FAQ_TABLE);
      $this->Where($this->faqSqlColumns[FAQ_COLUMN_ID].'='.$newFaqId);
      $values = $this->Query(SINGLE_ROW, KEYS_FORMAT);
      $this->question = $values[$this->faqSqlColumns[FAQ_COLUMN_QUESTION]];
      $this->answer = $values[$this->faqSqlColumns[FAQ_COLUMN_ANSWER]];
    }
  }


  function GetId() {
    return $this->faqId;
  }


  function GetTrackId() {
    return $this->trackId;
  }

  
  function GetQuestion() {
    return $this->question;
  }


  function SetQuestion($newQuestion) {
    $this->question = $newQuestion;
  }


  function GetAnswer() {
    return $this->answer;
  }


  function SetAnswer($newAnswer) {
    $this->answer = $newAnswer;
  }


  function GetColumnLabels() {
    return $this->faqListLabelColumns;
  }


  // Purely an administrative function so hard setting the db is fine for now
  function GetFaqList($columnIds, $searchColumnId, $searchFor, $sortColumnId,
		      $sortOrder, &$pageNumber, $rowsPerPage) {
    
    if (empty($columnIds)) {
      if (DEBUG & DEBUG_CLASS_FUNCTIONS)
	error_log("No columns selected for faq list so immediately returning with empty result sets");
      return array();
    }

    if ($pageNumber <= 0) {
      if (DEBUG & DEBUG_CLASS_FUNCTIONS)
	error_log("Faq.GetFaqList(): Invalid Page Number($pageNumber) passed, adjusting to 1");
      $pageNumber = 1;
    }

    if ($rowsPerPage <= 0) {
      if (DEBUG & DEBUG_CLASS_FUNCTIONS)
	error_log("Faq.GetFaqList(): Invalid Rows Per Page($rowsPerPage) passed, adjusting to 1");
      $rowsPerPage = 1;
    }

    $faqListColumns = array();
    $faqIds = array();
    $searchExact = FALSE;

    if (!($this->Open($_SESSION['dbName'], DB_USER, DB_PASS))) {
      return FALSE;
    }

    /*
    // IDs must always be returned no matter what.  They are critical in
    // that they are used as parameters to perform other actions like edit delete
    $this->Select($this->faqListSqlColumns[FAQ_LIST_COLUMN_ID], FAQ_LIST_TABLE);
    $this->Where($this->faqSqlColumns[FAQ_COLUMN_TRACK_ID].'='.$this->trackId);
    if ($searchFor) {
      if ($searchColumnId == FAQ_LIST_COLUMN_ID)
	$searchExact = TRUE;
      $this->SearchBy($this->faqListSqlColumns[$searchColumnId], $searchFor, $searchExact);
      //if (DEBUG & DEBUG_CLASS_FUNCTIONS)
      //error_log("SearchBy(".$this->FaqListSqlColumns[$searchColumnId].", $searchFor, $searchExact)");
    }

    $this->SortBy($this->faqListSqlColumns[$sortColumnId], $sortOrder);
    $this->SetPage($pageNumber, $rowsPerPage);
    $pageCount = $this->GetPageCount($rowsPerPage);
    if ($pageNumber > $pageCount && $pageCount > 0) {
      error_log("Faq.GetFaqList(): Page Number ($pageNumber) is greater than actual pages ($pageCount)");
      $pageNumber = $pageCount;
      $this->SetPage($pageNumber, $rowsPerPage);
    }
    $faqIds = $this->Query(MANY_VALUES, VALUES_FORMAT);
    */

    //error_log("FAQ IDS: ".print_r($faqIds, TRUE));

    // Now only select the columns currently selected in the interface for display
    for ($i = 0; $i < count($this->faqListSqlColumns); $i++) {
      if (1 << $i & $columnIds)
	array_push($faqListColumns, FAQ_TABLE.'.'.$this->faqListSqlColumns[1 << $i]);
    }
    // Always select the ids
    array_push($faqListColumns, FAQ_TABLE.'.'.$this->faqListSqlColumns[FAQ_LIST_COLUMN_ID]);

    $this->Select($faqListColumns, FAQ_LIST_TABLE);
    $this->Where($this->faqSqlColumns[FAQ_COLUMN_TRACK_ID].'='.$this->trackId);

    if ($searchFor) {
      if ($searchColumnId == FAQ_COLUMN_ID)
	$searchExact = TRUE;
      $this->SearchBy($this->faqListSqlColumns[$searchColumnId], $searchFor, $searchExact);
    }

    $this->SortBy($this->faqListSqlColumns[$sortColumnId], $sortOrder);
    $this->SetPage($pageNumber, $rowsPerPage);
    $pageCount = $this->GetPageCount($rowsPerPage);
    if ($pageNumber > $pageCount && $pageCount > 0) {
      error_log("Faq.GetFaqList(): Page Number ($pageNumber) is greater than actual pages ($pageCount)");
      $pageNumber = $pageCount;
      $this->SetPage($pageNumber, $rowsPerPage);
    }

    $results = $this->Query(MANY_ROWS, VALUES_FORMAT);

    // Add actions for this listing
    for($i = 0; $i < count($results); $i++) {
      array_push($faqIds, array_pop($results[$i]));
      $ref1 = new TplCaption('Edit');
      $ref1->href='faq_edit.php';
      $ref2 = new TplCaption('Delete');
      $ref2->href='faq_list.php';
      $ref2->onClick="return AskDeleteRecord('$faqIds[$i]');";
      array_push($results[$i], array($ref1, $ref2));
    }

    return array($faqIds, $results);
  }



  function DeleteFaq($faqId) {

    if (!$this->Open($this->dbName, ORG_ADMIN_USER, ORG_ADMIN_PASS, DB_HOST))
      return FALSE;

    $this->Where($this->faqSqlColumns[FAQ_COLUMN_ID].'='.$faqId);
    $rc = $this->Delete(FAQ_TABLE);

    $this->Open($this->dbName);

    return $rc;
  }


  function UpdateInfo() {
    
    $this->Open(ORG_DB_NAME_PREFIX.(int)$this->orgId);

    // Brand new faq so insert
    if ($this->faqId) {
      $this->Where($this->faqSqlColumns[FAQ_COLUMN_ID].'='.$this->faqId);
      if (!$this->Update(array($this->faqSqlColumns[FAQ_COLUMN_QUESTION] => $this->question,
			       $this->faqSqlColumns[FAQ_COLUMN_ANSWER] => $this->answer),
			 FAQ_TABLE)) {
	$this->cacheError = $this->GetErrorMessage();
	$this->Close();
	return RC_UPDATE_INFO_FAILED;
      }
    } else { // else existing faq so update
      $this->faqId = $this->Insert(array($this->faqSqlColumns[FAQ_COLUMN_TRACK_ID] => $this->trackId,
				   $this->faqSqlColumns[FAQ_COLUMN_QUESTION] => $this->question,
				   $this->faqSqlColumns[FAQ_COLUMN_ANSWER] => $this->answer),
			     FAQ_TABLE);
      if (!$this->faqId) {
	$this->cacheError = $this->GetErrorMessage();
	$this->Close();
	return RC_UPDATE_INFO_FAILED;
      }
    }

    $this->Close();

    return RC_OK;
  }


  function GetFaqForUser($userId, $trackId = 0) {
    $participantSqlColumns = unserialize(PARTICIPANT_SQL_COLUMNS);
    
    $this->Select(array($this->faqSqlColumns[FAQ_COLUMN_QUESTION],
			$this->faqSqlColumns[FAQ_COLUMN_ANSWER]),
		  FAQ_TABLE);
    $this->LeftJoinUsing(PARTICIPANT_TABLE, $this->faqSqlColumns[FAQ_COLUMN_TRACK_ID]);
    $this->Where($participantSqlColumns[PARTICIPANT_COLUMN_USER_ID].'='.$userId);
    if ($trackId)
      $this->Where(PARTICIPANT_TABLE." ".$participantSqlColumns[PARTICIPANT_COLUMN_TRACK_ID].'='.$this->trackId);
    $results = $this->Query(MANY_ROWS, KEYS_FORMAT);
    if ($results === FALSE) {
      error_log("FAQ::GetFaqForUser(userId=$userId): failed to obtain FAQ for user");
      return RC_QUERY_FAILED;
    }
    return $results;
  }


  function GetError() {
    return $this->cacheError;
  }

}
