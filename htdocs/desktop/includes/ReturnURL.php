<?php
/////////////////////////////////////////////////////////////////////////////
//	ReturnURL.php
//		Methods used to assist in encoding and decoding return URL paths
//		used to inform a calling page of it's caller and the callers's parameters.

/////////////////////////////////////////////////////////////////////////////
//	DecodeReturnURL
//		Decode a string previously encoded by EncodeReturnURL. This method
//		also respects embedded return URL's embedded within each other.
function DecodeReturnURL($strURL)
{
	$outer = 0;
	$len = strlen($strURL);

	for ($i = 0; $i < $len; $i++) {

		if ($strURL[$i] == '(') {
			if ($outer == 0)
				$strURL[$i] = ' ';
			$outer++;
		}
		else if ($strURL[$i] == ')') {
			$outer--;
			if ($outer == 0)
				$strURL[$i] = ' ';
		}
		else if ($outer == 1) {
			if ($strURL[$i] == '*')
				$strURL[$i] = '?';
			else if ($strURL[$i] == '|')
				$strURL[$i] = '&';
		}
	}
	$strURL = trim($strURL);
	return $strURL;
}

/////////////////////////////////////////////////////////////////////////////
//	EncodeReturnURL
//		Encode the specified URL so that we can pass it in a GET statement to
//		another page without conflicting the GET parameters contained in strURL.
function EncodeReturnURL($strURL)
{
	$search = array('?', '&');
	$replace = array('*', '|');
	$strURL = str_replace($search, $replace, $strURL);

	$strURL = "($strURL)";
	return $strURL;
}
?>