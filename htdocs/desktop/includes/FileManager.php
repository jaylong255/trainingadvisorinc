<?php

// This value limits the number of files that can be managed in any given
// directory through the web based file management interface
define('MAX_FILES_PER_DIR', 1000);


class FileManager {

  // This is the list of folders to skip when generating the list to
  // display in the interface.
  var $buildTreeSkipList = array('.', '..', '.svn', 'CVS');

  // This list defines the predefined list of application folders needed
  // for proper functioning of the application.  This is the list of
  // folders that are or should be owned by the app and not the customer.
  var $buildTreeSpecialList = array('email', 'reports', 'news', 'motif');

  //var $level = 0;
  var $entryCount = 0;
  var $dirRoot = '';
  //var $folderList = array('<Home>');


  function FileManager($dirRoot) {

    $this->dirRoot = $this->FilterPath($dirRoot);
    chdir($this->dirRoot);
  }


  function BuildTree($dir = '') {

    // Force object creation
    if (!isset($this)) {
      error_log("FileManager.BuildTree($dir): Object must be instantiated to use this function");
      return array();
    }

    // If the directory is empty then assume the dir root with which object was created
    if ($dir == '') {

      // Make sure the previously specified directory root is still clean
      $dir = $this->FilterPath($this->dirRoot);

      // Make sure nothing bad happens anywhere else in our tree
      //chdir($this->dirRoot);

      //if (DEBUG & DEBUG_CLASS_FUNCTIONS)
	//error_log('Directory argument empty using currently specified dir root: '.$this->dirRoot);

    } else {

      $dir = $this->FilterPath($dir);
      //error_log("Opening: $dir");
    }

    // Local variables
    $dirHandle = opendir($dir);
    $isDir = FALSE;

    // If directory specified but is not a directory error and return
    if ($dir && !is_dir($dir)) {
      error_log("FileManager.BuildTree($dir): not a directory");
      return array();
    }

    $parentNode = $this->entryCount;
    $localEntryCount = 0;
    $localEntries = array();
    //error_log("Before while loop, parentNode=$parentNode, localEntryCount=0");
    //array_push($this->folderList, $dir);

    while ($entry = readdir($dirHandle)) {
      
      if (array_search($entry, $this->buildTreeSkipList) !== FALSE) {
      	//error_log("Skipping entry ($entry) since match on buildTreeSkipList()");
      	continue;
      }
      $isDir = FALSE;
      //error_log("Processing $entry");
      $this->entryCount++;
      $localEntryCount++;
      
      if (is_dir("$dir/$entry"))
	$isDir = TRUE;

      array_push($localEntries, array('parent' => $parentNode, 'name' => str_replace("'", "\\'", $entry),
				      'path' => str_replace($this->dirRoot, '', $this->FilterPath("$dir/$entry")),
				      'isDir' => $isDir, 'url' => 'file_man_edit.php?path='
				      .urlencode(str_replace($this->dirRoot, '', $this->FilterPath("$dir/$entry")))));
		      //	      'url' => 'javascript:loadIFrame("file_man_edit", "file_man_edit.php?path='.
      if (is_dir($this->FilterPath("$dir/$entry"))) {
	//error_log("Adding directory: $dir/$entry to array with parent ".$parentNode);
	//$this->level++;
	$localEntries = array_merge($localEntries, $this->BuildTree("$dir/$entry", count($localEntries)));
	//$this->level--;
      }
      
      // Make sure the entry count does not exceed some reasonably sane value
      if ($localEntryCount > MAX_FILES_PER_DIR) {
	error_log('Hard limit on files per directory reached at($localEntryCount) with max set to: '.MAX_FILES_PER_DIR);
      	break;
      }
    }

    if (empty($localEntries)) {
      array_push($localEntries, array('parent' => $parentNode, 'name' => '&lt;empty&gt;',
				      'url' => '', 'isDir' => FALSE));
      $this->entryCount++;
      $localEntryCount++;
      //				      .urlencode($this->FilterPath($dir))));
      //error_log("PARENT: $parentNode, DIR: $dir found to be empty");
    }

    return $localEntries;
  }


  function MoveUpload($src, $dst) {

    if (!move_uploaded_file($src, $dst)) {
      error_log("FileManager.MoveUpload($src, $dst)");
      return FALSE;
    }

    return TRUE;
  }


  function Unlink($path) {
    
    // Make sure the path exists
    if(!file_exists($path)) return FALSE;
    
    // If it is a file or link, just delete it
    if(is_file($path) || is_link($path)) return unlink($path);
    
    // Scan the dir and recursively unlink
    $files = scandir($path);

    foreach($files as $filename) {

      if($filename == '.' || $filename == '..') continue;
      $file = str_replace('//','/',$path.'/'.$filename);
      self::unlink($file);
    } //end foreach
    
    // Remove the parent dir
    if(!rmdir($path)) return FALSE;

    return TRUE;

  } //end function unlink
  
  
  function IsDirectory($path) {
    
    if (is_dir($this->dirRoot.'/'.$this->FilterPath($path)))
      return TRUE;
    return FALSE;
  }


  function IsEditable($path) {

    $fileName = basename($this->FilterPath($path));
    $comps = explode('.', $fileName);
    $exten = array_pop($comps);
    if ($exten == 'txt' || $exten == 'text' ||
	$exten == 'htm' || $exten == 'html' ||
	$exten == 'xml' || $exten == 'xhtml')
      return TRUE;
    return FALSE;
  }


  function IsImmutable($path) {
    
    if (empty($path) || $path == 'email' ||
	$path == 'news' || $path == 'motif' ||
	$path == 'motif')
      return TRUE;
    return FALSE;
  }


  function FilterPath($path, $noRoot = FALSE) {
    
    // Strip harmful characters out
    $path = str_replace("\\", "/", $path);
    
    $path = str_replace("/..", "/", $path);
    $path = str_replace("/.", "/", $path);
    $path = str_replace("//", "/", $path);
    
    // No leading / characters allowed
    if ($noRoot)
      $path = preg_replace("/^\//", '', $path);

    return $path;
  }

  
  function WriteFile($path, &$contents) {
    
    $fh = fopen($path, 'w');
    
    if (!$fh) {
      error_log("Unable to open file: $path");
      return FALSE;
    }

    if (fwrite($fh, $contents) === FALSE) {
      error_log("FileManager::WriteFile: File($path) successfully opened but write failed");
      fclose($fh);
      return FALSE;
    }

    fclose($fh);
    return TRUE;
  }

}