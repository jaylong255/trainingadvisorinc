<?php
/////////////////////////////////////////////////////////////////////////////
//	DateObject
//		Object used for date manipulation and comparision.
class DateObject
{
	var		$strDate;		// i.e. 2003-02-21 = February 21, 2003
	var		$strWeekday;	// Sunday, Monday...
	var		$strMonth;		// January, February, March...
	var		$nWeekday;		// 1 = Sunday, 2 = Monday..... 7 = Saturday
	var		$nDay;			// 1...31 (day of the month)
	var		$nDayOfYear;	// 1...366 (day of year)
	var		$nWeek;			// 0 to 53 (week of year)
	var		$nMonth;		// 1 = January, 2 = February...
	var		$nYear;			// 2003, 2004...
	var		$nBaseDays;		// Days since the year 0 (doesn't work for dates earlier than 1582)
	var		$nBaseWeeks;	// Weeks since the year 0 (doesn't work for dates earlier than 1582)
	var		$nBaseMonths;	// Months since the year 0

	function DateObject($strDate = NULL)
	{
		if (!$strDate)
			$this->SetToday();
		else
			$this->SetDate($strDate);
	}
											// Data assignment methods
	function SetToday()
	{
		$result = mysql_query("SELECT CURDATE() as date");
		$this->strDate = mysql_result($result, 0, "date");
		$this->SetDate($this->strDate);
	}

	function SetDate($strDate)
	{
		$this->strDate = $strDate;

		$result = mysql_query("SELECT DAYNAME('$this->strDate') as day");
		$this->strWeekday = mysql_result($result, 0);

		$result = mysql_query("SELECT MONTHNAME('$this->strDate') as month");
		$this->strMonth = mysql_result($result, 0);

		$result = mysql_query("SELECT DAYOFWEEK('$this->strDate') as weekday");
		$this->nWeekday = (int)mysql_result($result, 0);

		$result = mysql_query("SELECT DAYOFMONTH('$this->strDate') as day");
		$this->nDay = (int)mysql_result($result, 0);

		$result = mysql_query("SELECT DAYOFYEAR('$this->strDate') as day");
		$this->nDayOfYear = (int)mysql_result($result, 0);

		$result = mysql_query("SELECT WEEK('$this->strDate') as week");
		$this->nWeek = (int)mysql_result($result, 0);

		$result = mysql_query("SELECT MONTH('$this->strDate') as month");
		$this->nMonth = (int)mysql_result($result, 0);

		$result = mysql_query("SELECT YEAR('$this->strDate') as year");
		$this->nYear = (int)mysql_result($result, 0);

		$result = mysql_query("SELECT TO_DAYS('$this->strDate') as days");
		$this->nBaseDays = (int)mysql_result($result, 0);

		$n = $this->nWeekday - 1;
		$result = mysql_query("SELECT DATE_SUB('$this->strDate', INTERVAL '$n' DAY)");
		$date = mysql_result($result, 0);
		$result = mysql_query("SELECT TO_DAYS('$date') as days");
		$this->nBaseWeeks = (int)mysql_result($result, 0);
		$this->nBaseWeeks = (int)($this->nBaseWeeks / 7);

		$this->nBaseMonths = (int)(($this->nYear * 12) + $this->nMonth);
	}
}
?>