<?php

require_once('Report.php');


class ReportAccuracyByTrack extends Report {

  var $allTracks = array();
  var $reportData = array();
  //var $departmentId = 0;
  //var $divisionId = 0;
  //var $selectColumns = array();


  // Returns an array structured like the following:
  // (<track_id> => (
  //                 (<track_name>, <delivered>, <completed>, <correct>, <incorrect>, <understood>, <contact>)
  //                 (
  //
  function ReportAccuracyByTrack($orgId, $trackIds, $questionId, $dateFrom, $dateTo, $divId, $deptId) {

    $this->Report($orgId);
    $allTracks = $this->GetDistinctTracks();
    //$selectColumns = array();
    $idx = 0;

    //$this->departmentId = $deptId;
    //$this->divisionId = $divId;
    //array_push($selectColumns, $this->assignmentSqlColumns[ASSIGNMENT_COLUMN_QUESTION_NUMBER]);
    //array_push($selectColumns, 'DISTINCT('.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_DELIVERY_DATE].')');
    //array_push($selectColumns, QUESTION_TABLE.'.'.$this->questionSqlColumns[QUESTION_COLUMN_TITLE]);
    //array_push($selectColumns, QUESTION_TABLE.'.'.$this->questionSqlColumns[QUESTION_COLUMN_CONTENT]);

    // Start by going through all the tracks and track ids in the system
    foreach($trackIds as $trackId) {
      $tempAssigned = $this->GetAssigned($trackId, $questionId, $dateFrom, $dateTo, $divId, $deptId);

      // If there were no assignments skip the track and gathering any reporting data for it
      if ($tempAssigned == 0)
	continue;

      // Create the report header for this particular track
      $idx = array_push($this->reportData, array(0 => array($trackId, $allTracks[$trackId], $tempAssigned,
							    $this->GetCompleted($trackId, $questionId, $dateFrom, $dateTo, $divId, $deptId),
							    $this->GetUnderstood($trackId, $questionId, $dateFrom, $dateTo, $divId, $deptId),
							    $this->GetCorrect($trackId, $questionId, $dateFrom, $dateTo, $divId, $deptId))));

      
      // Get the distinct delivery dates of questions that went out
      $this->Select('DISTINCT('.ASSIGNMENT_TABLE.'.'.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_DELIVERY_DATE].')', ASSIGNMENT_TABLE);
      $this->LeftJoinUsing(USER_TABLE, $this->assignmentSqlColumns[ASSIGNMENT_COLUMN_USER_ID]);
      $this->ReportConstraints($trackId, $questionId, $dateFrom, $dateTo, $divId, $deptId);
      $this->SortBy($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_DELIVERY_DATE]);
      //      $this->LeftJoinUsing(QUESTION_TABLE, $this->assignmentSqlColumns[ASSIGNMENT_COLUMN_QUESTION_NUMBER]);
      //      $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_DELIVERY_DATE].">='".MIGRATION_DATE);
      $results = $this->Query(MANY_VALUES, VALUES_FORMAT);
      foreach ($results as $deliveryDate) {
	$this->Select('DISTINCT('.ASSIGNMENT_TABLE.'.'.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_QUESTION_NUMBER].')', ASSIGNMENT_TABLE);
	$this->LeftJoinUsing(USER_TABLE, $this->assignmentSqlColumns[ASSIGNMENT_COLUMN_USER_ID]);
	$this->ReportConstraints($trackId, $questionId, $deliveryDate, $deliveryDate, $divId, $deptId);
	$questionResults = $this->Query(MANY_VALUES, VALUES_FORMAT);
	foreach($questionResults as $questionNumber) {
	  $this->Select($this->questionSqlColumns[QUESTION_COLUMN_TITLE], QUESTION_TABLE);
	  $this->Where($this->questionSqlColumns[QUESTION_COLUMN_ID].'='.$questionNumber);
	  $title = $this->Query(SINGLE_VALUE, VALUES_FORMAT);
	  $dateArr = explode(' ', $deliveryDate);
	  $this->reportData[$idx-1][] = array($questionNumber, $title, $dateArr[0],
					       $this->GetAssigned($trackId, $questionNumber, $deliveryDate, $deliveryDate, $divId, $deptId),
					       $this->GetCompleted($trackId, $questionNumber, $deliveryDate, $deliveryDate, $divId, $deptId),
					       $this->GetUnderstood($trackId, $questionNumber, $deliveryDate, $deliveryDate, $divId, $deptId),
					       $this->GetCorrect($trackId, $questionNumber, $deliveryDate, $deliveryDate, $divId, $deptId));
	}
      }
    }

    return $this->reportData;
  }


  function GetAssigned($trackId, $questionId, $dateFrom, $dateTo, $divId, $deptId) {

    $this->Select('COUNT('.ASSIGNMENT_TABLE.'.'.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ID].')', ASSIGNMENT_TABLE);
    $this->LeftJoinUsing(USER_TABLE, $this->assignmentSqlColumns[ASSIGNMENT_COLUMN_USER_ID]);
    $this->ReportConstraints($trackId, $questionId, $dateFrom, $dateTo, $divId, $deptId);
    return $this->Query(SINGLE_VALUE, VALUES_FORMAT);

  }


  function GetCompleted($trackId, $questionId, $dateFrom, $dateTo, $divId, $deptId) {

    $this->Select('COUNT('.ASSIGNMENT_TABLE.'.'.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ID].')', ASSIGNMENT_TABLE);
    $this->LeftJoinUsing(USER_TABLE, $this->assignmentSqlColumns[ASSIGNMENT_COLUMN_USER_ID]);
    $this->ReportConstraints($trackId, $questionId, $dateFrom, $dateTo, $divId, $deptId);
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_COMPLETION_DATE].'!=0');
    return $this->Query(SINGLE_VALUE, VALUES_FORMAT);

  }

  
  function GetUnderstood($trackId, $questionId, $dateFrom, $dateTo, $divId, $deptId) {

    $this->Select('COUNT('.ASSIGNMENT_TABLE.'.'.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ID].')', ASSIGNMENT_TABLE);
    $this->LeftJoinUsing(USER_TABLE, $this->assignmentSqlColumns[ASSIGNMENT_COLUMN_USER_ID]);
    $this->ReportConstraints($trackId, $questionId, $dateFrom, $dateTo, $divId, $deptId);
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_EXIT_INFO].'='.EXIT_INFO_UNDERSTOOD);
    return $this->Query(SINGLE_VALUE, VALUES_FORMAT);

  }


  function GetContact($trackId, $questionId, $dateFrom, $dateTo, $divId, $deptId) {

    $this->Select('COUNT('.ASSIGNMENT_TABLE.'.'.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ID].')', ASSIGNMENT_TABLE);
    $this->LeftJoinUsing(USER_TABLE, $this->assignmentSqlColumns[ASSIGNMENT_COLUMN_USER_ID]);
    $this->ReportConstraints($trackId, $questionId, $dateFrom, $dateTo, $divId, $deptId);
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_EXIT_INFO].'='.EXIT_INFO_CONTACT);
    return $this->Query(SINGLE_VALUE, VALUES_FORMAT);

  }


  function GetCorrect($trackId, $questionId, $dateFrom, $dateTo, $divId, $deptId) {

    $this->Select('COUNT('.ASSIGNMENT_TABLE.'.'.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ID].')', ASSIGNMENT_TABLE);
    $this->LeftJoinUsing(USER_TABLE, $this->assignmentSqlColumns[ASSIGNMENT_COLUMN_USER_ID]);
    $this->ReportConstraints($trackId, $questionId, $dateFrom, $dateTo, $divId, $deptId);
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ANSWERED_CORRECTLY].'='.RESULT_CORRECT);
    return $this->Query(SINGLE_VALUE, VALUES_FORMAT);

  }


  function GetIncorrect($trackId, $questionId, $dateFrom, $dateTo, $divId, $deptId) {

    $this->Select('COUNT('.ASSIGNMENT_TABLE.'.'.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ID].')', ASSIGNMENT_TABLE);
    $this->LeftJoinUsing(USER_TABLE, $this->assignmentSqlColumns[ASSIGNMENT_COLUMN_USER_ID]);
    $this->ReportConstraints($trackId, $questionId, $dateFrom, $dateTo, $divId, $deptId);
    $this->Where($this->assignmentSqlColumns[ASSIGNMENT_COLUMN_ANSWERED_CORRECTLY].'='.RESULT_INCORRECT);
    return $this->Query(SINGLE_VALUE, VALUES_FORMAT);

  }


  function ReportConstraints($trackId, $questionId, $dateFrom, $dateTo, $divId, $deptId) {

    if (!$trackId) {
      error_log("ReportAccuracyByTrack::GetAssigned(trackId=$trackId,questionId=$questionId,dateFrom=$dateFrom,dateTo=$dateTo): ".
		"Invalid track ID passed to function, track ID must be specified and non-zero");
      return FALSE;
    }

    $this->Where(ASSIGNMENT_TABLE.'.'.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_TRACK_ID].'='.$trackId);
    if ($questionId)
      $this->Where(ASSIGNMENT_TABLE.'.'.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_QUESTION_NUMBER].'='.$questionId);
    if ($dateFrom)
      $this->Where(ASSIGNMENT_TABLE.'.'.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_DELIVERY_DATE].">='".$dateFrom."'");
    if ($dateTo)
      $this->Where(ASSIGNMENT_TABLE.'.'.$this->assignmentSqlColumns[ASSIGNMENT_COLUMN_DELIVERY_DATE]."<='".$dateTo."'");
    if (is_numeric($divId) && (int)$divId > 0)
      $this->Where(USER_TABLE.'.'.$this->userSqlColumns[USER_COLUMN_DIVISION_ID]."=$divId");
    if (is_numeric($deptId) && (int)$deptId > 0)
      $this->Where(USER_TABLE.'.'.$this->userSqlColumns[USER_COLUMN_DEPARTMENT_ID]."=$deptId");
  }


  function GetReportData() {
    return $this->reportData;
  }


  function CreatePdf($results, $resultFormatNumbers, $resultFormatPercentages) {
    // new PDF document with landscape orientation, inche units
    // format is last argument and hopefully letter is the default
    $html = '';
    $pdf = new tcpdf('L');
    $pdf->SetCreator('Training Advisor');
    $pdf->SetAuthor('Training Advisor System');
    $pdf->SetSubject('Question Accuracy By Class or Date Range');
    $pdf->SetTitle('Training Advisor Report');
    $pdf->SetKeywords('report, question, accuracy, date range');
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $pdf->SetHeaderData(PDF_HEADER_LOGO, 50, 'Training Advisor Report:', 'Question Accuracy By Class or Date Range');
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetMargins(PDF_MARGIN_LEFT, 23, PDF_MARGIN_RIGHT);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    //    $pdf->UseTableHeader();
    //$pdf->UseCSS();
    //$pdf->SetFont('Arial','',12);

    foreach ($results as $trackData) {
      $html = '<span class="OrgInfoText">Report for Class: '.$trackData[0][1].'</span><BR>
  <table border="0" align="center" bgcolor="#FFFFFF">
    <tr bgcolor="#DDDDDD">
      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #000000; width: 0.6em; font-weight: bold;" align="left">Question #</td>
      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #000000; width: 3.5em; font-weight: bold;" align="left">Topic</td>
      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #000000; width: 0.6em; font-weight: bold;" align="left">Delivery Date</td>
      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #000000; width: 0.6em; font-weight: bold;">Assigned</td>
      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #000000; width: 0.6em; font-weight: bold;">Completed</td>
      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #000000; width: 0.6em; font-weight: bold;">Understood</td>
      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #000000; width: 0.6em; font-weight: bold;">Correct</td>
    </tr>';
      for($i = 1; $i < count($trackData); $i++) {
	$html .= '<tr bgcolor="#FFFFFF">
      <td style="font-size: 10pt; width: 0.6em;" align="left">'.$trackData[$i][0].'</td>
      <td style="font-size: 10pt; width: 3.5em;" align="left">'.$trackData[$i][1].'</td>
      <td style="font-size: 10pt; width: 0.6em;" align="left">&nbsp;'.$trackData[$i][2].'</td>
      <td style="font-size: 10pt; width: 0.6em;" align="center">'.$trackData[$i][3].'</td>
      <td style="font-size: 10pt; width: 0.6em;" align="center">';
	if ($resultFormatNumbers)
	  $html .= $trackData[$i][4];
	if ($resultFormatPercentages) {
	  if ($resultFormatNumbers)
	    $html .= ' (';
	  $html .= sprintf("%.1f", $trackData[$i][4] / $trackData[$i][3] * 100.0).'%';
	  if ($resultFormatNumbers)
	    $html .= ')';
	}
	$html .= '      </td>
      <td style="font-size: 10pt; width: 0.6em" align="center">';
	if ($resultFormatNumbers)
	  $html .= $trackData[$i][5];
	if ($resultFormatPercentages) {
	  if ($resultFormatNumbers)
	    $html .= ' (';
	  $html .= sprintf("%.1f", $trackData[$i][5] / $trackData[$i][4] * 100.0).'%';
	  if ($resultFormatNumbers)
	    $html .= ')';
	}
	$html .= '      </td>
      <td style="font-size: 10pt; width: 0.6em" align="center">';
	if ($resultFormatNumbers)
	  $html .= $trackData[$i][6];
	if ($resultFormatPercentages) {
	  if ($resultFormatNumbers)
	    $html .= ' (';
	  $html .= sprintf("%.1f", $trackData[$i][6] / $trackData[$i][4] * 100.0).'%';
	  if ($resultFormatNumbers)
	    $html .= ')';
	}
	$html .= '      </td>
    </tr>';
      }
      $html .=    '<tr bgcolor="#DDDDDD">
      <td colspan="3" style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #000000; width: 1.56em; font-weight: bold;" align="left"><b>Summary Totals:</b></td>
      <td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #000000; width: 0.6em; font-weight: bold;"><b>'.$trackData[0][2].'</b></td>
      <td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #000000; width: 0.6em; font-weight: bold;"><b>';
      if ($resultFormatNumbers)
	$html .= $trackData[0][3];
      if ($resultFormatPercentages) {
	if ($resultFormatNumbers)
	  $html .= ' (';
	$html .= sprintf("%.1f", $trackData[0][3] / $trackData[0][2] * 100.0).'%';
	if ($resultFormatNumbers)
	  $html .= ')';
      }
      $html .= '</b></td>
      <td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #000000; width: 0.6em; font-weight: bold;"><b>';
      if ($resultFormatNumbers)
	$html .= $trackData[0][4];
      if ($resultFormatPercentages) {
	if ($resultFormatNumbers)
	  $html .= ' (';
	$html .= sprintf("%.1f", $trackData[0][4] / $trackData[0][3] * 100.0).'%';
	if ($resultFormatNumbers)
	  $html .= ')';
      }
      $html .= '</b></td>
      <td align="center" style="font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #000000; width: 0.6em; font-weight: bold;"><b>';
      if ($resultFormatNumbers)
	$html .= $trackData[0][5];
      if ($resultFormatPercentages) {
	if ($resultFormatNumbers)
	  $html .= ' (';
	$html .= sprintf("%.1f", $trackData[0][5] / $trackData[0][3] * 100.0).'%';
	if ($resultFormatNumbers)
	  $html .= ')';
      }
      $html .= '</b></td>
    </tr>
  </table>
';
      $pdf->AddPage('L');
      $pdf->WriteHTML($html);
    }
    //    return $html;

    //    $this->ImprovedTable($pdf, array('col1', 'col2', 'col3', 'col4', 'col5'),
    //    //		      array(array('r1c1 fjdsalk fjdsalk djsalfdsa', 'r1c2', 'r1c3', 'r1c4', 'r1c5'),
    //			    array('r2c1', 'r2c2', 'r2c3', 'r2c4', 'r2c5'),
    //		    array('r3c1', 'r3c2', 'r3c3', 'r3c4', 'r3c5')));
    /*

    $pdf->SetAuthor('Training Advisor Inc.');
    $pdf->SetTitle($title);
    $pdf->SetFont('Helvetica', 'B', 20);
    $pdf->SetTextColor(50, 60, 100);

    foreach($results as $trackResults) {
      $pdf->AddPage('L');
      $pdf->SetDisplayMode('real', 'default');
      //$pdf->Image('<filename>', 10, 20, 30, 0, 'url');
      //$pdf->Link(10, 20, 33, 33, 'url');
      $pdf->SetFontSize(13);
      $pdf->Write(0, 'Report for Class: '.$trackResults[0][1]."\n");

      $pdf->SetFontSize(11);
      $pdf->SetXY(5, 18);
      //    $pdf->SetDrawColor(50, 60, 100);
      $pdf->Cell(0, 0, 'Question #', 1, 0);
      $pdf->Cell(0, 0, 'Topic', 1, 0);

      //$pdf->Cell(0, 0, 'Delivery Date', 'L1', 0);
      //$pdf->Cell(0, 0, 'Assigned', 'L1', 0);
      //$pdf->Cell(0, 0, 'Completed', 'L1', 0);
      //$pdf->Cell(0, 0, 'Understood', 'L1', 0);
      //$pdf->Cell(0, 0, 'Correct', 'L1', 1);
      //for($i = 0; $i < count($trackResults); $i++) {
	//    $pdf->SetFontSize(10);
	//    $pdf->Write(5, 'Congratulations! You have generated a PDF. ');
	//foreach($trackResults[$i] as $key => $field) {
	 // if ($key == count($trackResults[$i]) - 1)
	  //  $pdf->Cell(0, 0, $field, 'L1', 1);
	  //else
	  //  $pdf->Cell(0, 0, $field, 'L1', 0);
	//}
      //}

      }
    */
    $pdf->Output('Class_Accuracy_Report.pdf', 'D');
  }


  function CreateCsv($results, $resultFormatNumbers, $resultFormatPercentages) {

    if (empty($results)) {
      error_log("Report::CreateCsv(): empty results data passed to CSV function");
      return;
    }

    $outputArr = array();
    //$fp = fopen('php://output', 'w');
    $csv = new CustomCsv('php://output', 'w');

    // Output headers
    $csv->WriteHeaders('Class_Accuracy_Report.csv');

    // Output CSV header information:
    $outputArr = array('Class', 'Question #', 'Topic', 'Delivery Date', 'Assigned');
    if ($resultFormatNumbers)
      $outputArr[] = "Number Completed";
    if ($resultFormatPercentages)
      $outputArr[] = "Percentage Completed";
    if ($resultFormatNumbers)
      $outputArr[] = "Number Understood";
    if ($resultFormatPercentages)
      $outputArr[] = "Percentage Understood";
    if ($resultFormatNumbers)
      $outputArr[] = "Number Correct";
    if ($resultFormatPercentages)
      $outputArr[] = "Percentage Correct";
    $csv->WriteCsv($outputArr);

    foreach ($results as $trackData) {
      for($i = 1; $i < count($trackData); $i++) {
	$outputArr = array($trackData[0][1], $trackData[$i][0], $trackData[$i][1], $trackData[$i][2], $trackData[$i][3]);
	if ($resultFormatNumbers)
	  $outputArr[] = $trackData[$i][4];
	if ($resultFormatPercentages)
	  $outputArr[] = sprintf('%.1f', ($trackData[$i][4] / $trackData[$i][3] * 100.0));
	if ($resultFormatNumbers)
	  $outputArr[] = $trackData[$i][5];
	if ($resultFormatPercentages)
	  $outputArr[] = sprintf('%.1f', ($trackData[$i][5] / $trackData[$i][4] * 100.0));
	if ($resultFormatNumbers)
	  $outputArr[] = $trackData[$i][6];
	if ($resultFormatPercentages)
	  $outputArr[] = sprintf('%.1f', ($trackData[$i][6] / $trackData[$i][4] * 100.0));
	$csv->WriteCsv($outputArr);
      }
    }

    $csv->CloseCsv();

    return;
  }

}



