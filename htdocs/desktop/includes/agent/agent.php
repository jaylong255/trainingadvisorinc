<?php

// This file is a collection of helper functions for the agent that gives assignments and sends notification emails



// Function:
// Purpose:    
// Args:      
// Pre:       
// Post:      
function AssignQuestionHelper($db, $Question_Number, $User_ID, $Track_ID,
			      &$nAssignments, $Supervisor, $parentAssignment = 0) {

  global $DEBUG;
  $childQuestionNumbers = array();
  $childQuestionNumber = 0;
  $totalSeconds = 0;
  $frameTypeId = 0;
  $isAb1825 = 0;
  $languageId = 0;
  $domainId = 0;
  $versionDate = '';
  $insert = 0;

  // Ensure the same assignment does not already exist
  $sqlStr = "SELECT User_ID FROM User_Assignment WHERE "
    ."Question_Number='$Question_Number' AND User_ID='$User_ID' AND "
    ."Track_ID='$Track_ID' AND Delivery_Date=CURDATE() LIMIT 1";
  $ua_exists = mysql_query($sqlStr, $db);
  $sqlStr = "SELECT User_ID FROM Assignment WHERE "
    ."Question_Number='$Question_Number' AND User_ID='$User_ID' AND "
    ."Track_ID='$Track_ID' AND Delivery_Date=CURDATE() LIMIT 1";
  $a_exists = mysql_query($sqlStr, $db);

  if (($ua_exists == 0 || mysql_num_rows($ua_exists) == 0) &&
      ($a_exists == 0 || mysql_num_rows($a_exists) == 0)) {
    
    // Get any child question numbers
    $sqlStr = "SELECT Question_Number FROM Question WHERE Parent_Question_Number='$Question_Number' ORDER BY Question_Number";
    $res = mysql_query($sqlStr, $db);
    while ($row = mysql_fetch_row($res)) {
      array_push($childQuestionNumbers, $row[0]);
    }

    $sqlStr = "SELECT Is_Ab1825 FROM Track WHERE Track_ID=$Track_ID";
    $res = mysql_query($sqlStr, $db);
    $row = mysql_fetch_row($res);
    $isAb1825 = $row[0];
    if ($isAb1825) {
      if (defined('STDIN') && getenv('TERM'))
	output("- NOTICE: AB1825 Track found, processing...<BR>\n");
      else
	error_log("- NOTICE: AB1825 Track found, processing...\n");
      $sqlStr = "SELECT SUM(Completed_Seconds) FROM Assignment WHERE Track_ID=$Track_ID AND User_ID=$User_ID";
      $res = mysql_query($sqlStr, $db);
      $row = mysql_fetch_row($res);
      $totalSeconds = $row[0];
      if (defined('STDIN') && getenv('TERM'))
	output("- Training Completed so far by User_ID=$User_ID is $totalSeconds seconds...<BR>\n");
      else
	error_log("- Training Completed so far by User_ID=$User_ID is $totalSeconds seconds...\n");
    }

    // Get Version date, domain, and language from question
    $sqlStr = "SELECT Version_Date, Domain_ID, Language_ID, Frametype_ID FROM Question WHERE Question_Number='$Question_Number'";
    if (defined('STDIN') && getenv('TERM'))    
      output("- SQL to get version date: $sqlStr<BR>\n");
    else
      error_log("- SQL to get version date: $sqlStr\n");
    $res = mysql_query($sqlStr, $db);
    if ($res === FALSE) {
      if (defined('STDIN') && getenv('TERM'))    
	output('- ERROR ('.mysql_errno($db).'): '.mysql_error($db)."<BR>\n");
      else
	error_log('- ERROR ('.mysql_errno($db).'): '.mysql_error($db)."\n");
      return FALSE;
    }
    $row = mysql_fetch_row($res);
    $versionDate = 0; //$row[0];
    $domainId = (int)$row[1];
    $languageId = (int)$row[2];
    $frameTypeId = (int)$row[3];

    if ($isAb1825 && $frameTypeId == 6 && $totalSeconds >= 7200) {
      if (defined('STDIN') && getenv('TERM'))    
	output("- Skipping question $Question_Number in Track $Track_ID for user $User_ID since frametype optional and ab1825 requirement met at $totalSeconds seconds...<BR>\n");
      else
	error_log("- Skipping question $Question_Number in Track $Track_ID for user $User_ID since frametype optional and ab1825 requirement met at $totalSeconds seconds...\n");
      return TRUE;
    }

    $sqlStr = "SELECT User.Department_ID, User.Division_ID, User.Role FROM User, Track "
      ."WHERE User.User_ID = Track.Supervisor_ID AND Track.Track_ID";
    $res = mysql_query($sqlStr, $db);
    $row = mysql_fetch_row($res);
    $departmentId = (int)$row[0];
    $divisionId = (int)$row[1];
    $role = (int)$row[2];
   
    // Create the new question assignment(s)
    $sqlStr = "INSERT INTO Assignment SET "
      ."Assignment_Parent_ID='$parentAssignment', Question_Number='$Question_Number', Version_Date='$versionDate', "
      ."Domain_ID=$domainId, Language_ID=$languageId, "
      ."User_ID='$User_ID', Track_ID='$Track_ID', Delivery_Date=CURDATE()";
    if ($role == 2 || $role == 4)
      $sqlStr .= ", Department_ID=$departmentId, Division_ID=$divisionId";
    if (defined('STDIN') && getenv('TERM'))
      output("- Assigning question: SQL: $sqlStr<BR>\n");
    else
      error_log("- Assigning question: SQL: $sqlStr\n");
    $insert = mysql_query($sqlStr, $db);
    if ($insert != 0) {
      $nAssignments++;
    } else {
      $outMsg = "- WARNING: Unable to insert new record into Assignment for "
	."User: $User_ID, Track: $Track_ID, Question: $Question_Number<BR>\n";
      if (defined('STDIN') && getenv('TERM'))
	output($outMsg);
      else
	error_log($outMsg);
      return FALSE;
    }
    if (!empty($childQuestionNumbers)) {
      $insert = mysql_insert_id($db);
      if ($insert === FALSE) {
	error_log("agent.php: Failed to obtain last insert id on assignment of question: $Question_Number to User: $User_ID in Track: $Track_ID");
      } else {
	foreach($childQuestionNumbers as $childQuestionNumber) {
	  AssignQuestionHelper($db, $childQuestionNumber, $User_ID, $Track_ID,
			       $nAssignments, $Supervisor, $insert);
	}
      }
    }
  } else {
    if (defined('STDIN') && getenv('TERM'))
      output("- NOTICE: Duplicate assignment already exists\n");
    else
      error_log("- NOTICE: Duplicate assignment already exists\n");
    return FALSE;
  }
  
  if (defined('STDIN') && getenv('TERM'))
    output("<br>\n");
  
  return TRUE;

} // end function()





// Function:   
// Purpose:    
// Args:        
// Pre:        
// Post:        
function GetQuestionNumberAndSequence($db, $Track_ID, $Last_Question_Number, &$Question_Number,
				      &$Sequence, &$Group, $Is_Looping) {

  // Local variable initialization
  $Last_Sequence = 0;
  $Last_Group    = 0;
    
  // Check for Last_Question_Number
  if ($Last_Question_Number > 0) {

    $sqlStr = "SELECT Sequence, GroupID FROM Queue WHERE Track_ID='$Track_ID' "
      ."AND Question_Number='$Last_Question_Number' LIMIT 1";

    $result = mysql_query($sqlStr, $db);
    if ($result && mysql_num_rows($result) > 0) {

      $Last_Sequence = mysql_result($result, 0, "Sequence");
      $Last_Group = mysql_result($result, 0, "GroupID");

    } else { // end if result valid and rows returned
      
      $outMsg = "Unable to obtain Sequence or GroupID from Queue, "
	."Track_ID: $Track_ID, Last_Question_Number: $Last_Question_Number, "
	."Question_Number: $Question_Number, Sequence: $Sequence, Looping: "
	."$Is_Looping<BR>\n";
      //output($outMsg);
      //      return FALSE;
      
    } // end if mysql query was unsuccessful

    $sqlStr = "SELECT MAX(Sequence) FROM Queue WHERE Track_ID='$Track_ID'";
    $result = mysql_query($sqlStr, $db);
    if ($result && mysql_num_rows($result) > 0) {
      $row = mysql_fetch_array($result);
      $maxSequence = $row[0];
      if (defined('STDIN') && getenv('TERM'))
	output("- Max sequence in this track is: $maxSequence<BR>\n");
    }

  } // end if Last_Question_Number


    // Determine the next question to be assigned
  $query = "SELECT * FROM Queue WHERE Track_ID=$Track_ID";
  
  if ($Last_Sequence > 0)
    $query .= " AND Sequence > $Last_Sequence";
  
  // If the Last_Group was > 0 meaning that the last question was
  // part of a question group then we need to make sure we skip
  // any questions that were in that group as well as the next
  // sequence number (this is mostly for safety and completeness)
  if ($Last_Group > 0)
    $query .= " AND GroupID != $Last_Group";

  // Complete the query with the order by SQL clause
  $query .= " ORDER BY Sequence LIMIT 1";
  
  //echo "Query: $query<BR>\n";
  // Execute query for next assigned question and override currently set values
  $result = mysql_query($query, $db);
  if ($result) {

    if (mysql_num_rows($result) > 0) {
      $Question_Number = mysql_result($result, 0, "Question_Number");
      $Sequence        = mysql_result($result, 0, "Sequence");
      $Group           = mysql_result($result, 0, "GroupID");
    } else {
      if (defined('STDIN') && getenv('TERM')) {
	output("- Track appears at end with previous delivery of Question Number: $Last_Question_Number<BR>\n");
	output("- Performing looping configuration check...<BR>\n");
      }
    }

  } else { // end if result set valid and rows returned
    
    $outMsg = "Unable to obtain Question_Number, Sequence, or GroupID during "
      ."GetQuestionNumberAndSequence, Track: $Track_ID, Last_Question_Number: "
      ."$Last_Question_Number, Looping: $Is_Looping<BR>\n";
    if (defined('STDIN') && getenv('TERM'))
      output ($outMsg);
    return FALSE;
  }
  

  // Dont progress if track is not looping
  if ($Last_Sequence == $maxSequence) {

    if (!$Is_Looping) {
      if (defined('STDIN') && getenv('TERM'))
	output("- Question sequence for Track $Track_ID has come to an end and track is not configured as looping.<BR>\n");
      return TRUE;
    }

    $sqlStr = "SELECT * FROM Queue WHERE Track_ID=$Track_ID AND Sequence = '000001' LIMIT 1";
    $result = mysql_query($sqlStr, $db);
    if ($result && mysql_num_rows($result) != 0) {
      
      $Question_Number = mysql_result($result, 0, "Question_Number");
      $Sequence        = mysql_result($result, 0, "Sequence");
      $Group           = mysql_result($result, 0, "Group");
      
      $outMsg = "Reached end of assigned questions, looping around to first question.<br>\n";
      //output($outMsg);
      
    } else { // end if result set valid and rows returned
      
      $outMsg = "Unable to select info from Queue in GetQuestionNumberAndSequence, "
	."Track: $Track_ID, Last_Question_Number: $Last_Question_Number, Looping: "
	."$Is_Looping<BR>\n";
      //output($outMsg);
      return FALSE;
      
    } // end if mysql query failed
    
  } // end if Question_Number is valid and looping is true
  
  return TRUE;


} // end function GetQuestionNumberAndSequence()



/////////////////////////////////////////////////////////////////////////////
//	SendEmail
//		Attempt to send the specified email.
function SendEmail($sendto, $replyto, $subject, $message, $header = "") {

  global	$disable_email;
  global        $DEBUG;
  global        $devEmail;
  global        $devEmailArr;
  $mboundary = uniqid('tainc-'.substr($sendto, 0, strpos($sendto, "@")));

  if ($disable_email == true) {
    if (defined('STDIN') && getenv('TERM'))
      output("Sending of email has been disabled to email to $sendto will not be delivered<BR>\n");
    else
      error_log("Sending of email has been disabled to email to $sendto will not be delivered\n");
    return true;
  }

  // Attempt to send the email
  if ($replyto) {
    $header = 'From: '.substr($replyto, 0, strpos($replyto, '<')).'<'.AGENT_ENVELOPE_SENDER_ADDRESS.">\r\n".$header;
    $header .= "Reply-To: $replyto\r\n";
  }
  //."MIME-Version: 1.0\r\n"."Content-Type:text/html\r\n";
  // When using SMTP we need to include the To: field, but with qmail this causes a duplicate
  // To: field so we will just have to live with leaving it off.
  //$header = "To: $sendto\r\n"."From: $replyto\r\n"."Reply-To: $replyto\r\n";
  //	else
  //		$header = "MIME-Version: 1.0\r\n"."Content-Type:text/html\r\n";
  
  // Do an idiotic check to see if this should be html email or not
  if (defined('STDIN') && getenv('TERM'))
    output("Checking to see if message has key html elements<BR>\n");
  else
    error_log("Checking to see if message has key html elements\n");
  if (stristr($message, "<html") || stristr($message, "<head") || stristr($message, "<body")) {
    if (defined('STDIN') && getenv('TERM'))
      output("HTML elements found, Checking to see if multipart header exists<BR>\n");
    else
      error_log("HTML elements found, Checking to see if multipart header exists\n");
    if (preg_match("/__multipart_boundary__/i", $message)) {
      if (defined('STDIN') && getenv('TERM'))
	output("EMAIL using multipart alternative mime type<BR>\n");
      else
	error_log("EMAIL using multipart alternative mime type\n");
      $header .= "MIME-Version: 1.0\r\nContent-Type: multipart/alternative;charset=UTF-8;boundary=$mboundary\r\n";
      $message = str_replace("__MULTIPART_BOUNDARY__", $mboundary, $message);
    } else {
      if (defined('STDIN') && getenv('TERM'))
	output("EMAIL NOT using multipart alternative mime type<BR>\n");
      else
	error_log("EMAIL NOT using multipart alternative mime type in message\n");
      $header .= "MIME-Version: 1.0\n"."Content-Type: text/html";
    }
  } else {
    if (defined('STDIN') && getenv('TERM'))
      output("EMAIL no html elements found in email<BR>\n");
    else
      error_log("EMAIL no html elements found in email\n");
    $header .= "MIME-Version: 1.0\n"."Content-Type: text/plain";
  }

  // If debugging then override the normal destination email address
  if ($DEBUG > 0) {
    
    //$outMsg = "Sending email addess under $devEmail instead of $sendto<BR>\n";
    //output($outMsg);
    //if (array_search($sendto, $devEmailArr) === FALSE)
    //  return true;
    $sendto = 'general@itpsg.com';

  } // end if debugging code enabled


  if (mail($sendto, $subject, $message, $header, '-f '.AGENT_ENVELOPE_SENDER_ADDRESS))
    return true;
  
  return false;
  
} // end function SendEmail()


?>
