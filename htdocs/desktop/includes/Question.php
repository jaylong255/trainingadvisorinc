<?php

require_once('common.php');
require_once('DatabaseObject.php');
require_once('Config.php');
require_once('CustomCsv.php');
require_once('Track.php');

class Question extends DatabaseObject {

  var $questionListLabelColumns = array();
  var $questionListSqlColumns = array();
  var $questionSqlColumns = array();
  var $orgId = 0; // The organization ID to which the user belongs
  var $questionId = 0;
  var $parentQuestionId = 0;
  var $languageId = 1;
  var $domainId = 1;
  var $categoryId = 0;
  var $frameTypeId = 0;
  var $versionDate = '';
  var $isManagement = 0;
  var $author = '';
  var $notes = '';
  var $title = '';
  var $supportInfo = '';
  var $capConfirmThanks = '';
  var $requireCorrectAnswer = FALSE;
  var $ownerUserId = 0;

  // Individual fields parsed from the content field
  var $question = '';
  var $multipleChoice1 = '';
  var $multipleChoice2 = '';
  var $multipleChoice3 = '';
  var $multipleChoice4 = '';
  var $multipleChoice5 = '';
  var $feedbackChoice1 = '';
  var $feedbackChoice2 = '';
  var $feedbackChoice3 = '';
  var $feedbackChoice4 = '';
  var $feedbackChoice5 = '';
  var $correctAnswer = 0;
  var $purpose = '';
  var $learningPoints = '';
  var $mediaFilePath = '';
  var $mediaDurationSeconds = 0;
  
  var $displayChoices = 'default';

  // Added for AB1825 functionality
  var $questionSetSize = 0;
  var $nextAssignmentId = 0;

  // These were carried over from the Organization
  // Class and could very well apply here as well
  var $cacheError = '';
  var $rc = 0;
  var $myqslOutput = '';


  function Question($orgId, $newQuestionId = 0, $newDomainId = 0, $newVersionDate = '') {

    if (DEBUG & DEBUG_CLASS_FUNCTIONS)
      error_log("Question::Question(orgId=$orgId,newQuestionId=$newQuestionId,newDomainId=$newDomainId,newVersionDate=$newVersionDate): constructor called");

    $this->questionListLabelColumns = unserialize(QUESTION_LIST_LABELS);
    $this->questionListSqlColumns = unserialize(QUESTION_LIST_SQL_COLUMNS);
    $this->questionSqlColumns = unserialize(QUESTION_SQL_COLUMNS);
    $this->languageSqlColumns = unserialize(LANGUAGE_SQL_COLUMNS);
    $this->domainSqlColumns = unserialize(DOMAIN_SQL_COLUMNS);
    $this->frameTypeSqlColumns = unserialize(FRAMETYPE_SQL_COLUMNS);
    $this->categorySqlColumns = unserialize(CATEGORY_SQL_COLUMNS);
    $this->orgId = (int)$orgId;
    $this->versionDate = date('Y-m-d').' 00:00:00';
    $this->DatabaseObject(ORG_DB_NAME_PREFIX.$this->orgId);
    
    if (!$this->initialized)
      return FALSE;

    if (is_numeric($newDomainId) && $newDomainId && $newVersionDate) {
      if ($this->SetQuestionId($newQuestionId) != RC_OK)
	return FALSE;
      if ($this->questionId == 0 || $this->domainId != $newDomainId ||
	  strncmp($this->versionDate, $newVersionDate, strlen($newVersionDate))) {
	error_log("Getting Archived questions since no matching question in Question table "
		  ."with questionId=$newQuestionId, domainId=$newDomainId, versionDate=$newVersionDate");
	// Must reset/invalidate this questionID so that the following function will execute its query
	$this->questionId = 0;
	$this->SetArchiveQuestionId($newQuestionId, $newDomainId, $newVersionDate);
      }
      return FALSE;
    } elseif (is_numeric($newQuestionId) && $newQuestionId) {
      if ($this->SetQuestionId($newQuestionId) != RC_OK)
	if ($this->SetArchiveQuestionId($newQuestionId, $newDomainId, $newVersionDate) != RC_OK)
	  return FALSE;
    }

    return TRUE;
  }


  // This retrieves information from the database and populates the properties of this
  // object with values and information from the database.
  function SetQuestionId($newQuestionId) {
    
    if (!is_numeric($newQuestionId)) {
      error_log("Question::SetQuestionId(newQuestionId=$newQuestionId): invalid non-numeric "
		."passed to function...returning RC_INVALID_ARG");
      return RC_INVALID_ARG;
    }

    $newQuestionId = (int)$newQuestionId;

    if ($newQuestionId <= 0) {
      error_log("Question::SetQuestionId($newQuestionId): invalid numeric<=0 passed to "
		."function...returning RC_INVALID_ARG");
      return RC_INVALID_ARG;
    }

    $values = array();
    
    if ($this->questionId != $newQuestionId) {
      $this->questionId = $newQuestionId;
      $this->Select('*', QUESTION_TABLE);
      $this->Where($this->questionSqlColumns[QUESTION_COLUMN_ID]." = ".$this->questionId);
      $values = $this->Query(SINGLE_ROW);
      
      if ($values === FALSE) {
	error_log("Question.SetQuestionId($newQuestionId): "
		  .$this->GetErrorMessage());
	return FALSE;
      }

      if (!empty($values)) {
	$this->parentQuestionId = (int)$values[$this->questionSqlColumns[QUESTION_COLUMN_PARENT_ID]];
	$this->languageId = $values[$this->questionSqlColumns[QUESTION_COLUMN_LANGUAGE_ID]];
	$this->domainId = $values[$this->questionSqlColumns[QUESTION_COLUMN_DOMAIN_ID]];
	$this->categoryId = $values[$this->questionSqlColumns[QUESTION_COLUMN_CATEGORY_ID]];
	$this->frameTypeId = $values[$this->questionSqlColumns[QUESTION_COLUMN_FRAMETYPE_ID]];
	$this->versionDate = $values[$this->questionSqlColumns[QUESTION_COLUMN_VERSION_DATE]];
	$this->isManagement = $values[$this->questionSqlColumns[QUESTION_COLUMN_IS_MANAGEMENT]];
	$this->author = $values[$this->questionSqlColumns[QUESTION_COLUMN_AUTHOR]];
	$this->notes = $values[$this->questionSqlColumns[QUESTION_COLUMN_NOTES]];
	$this->title = $values[$this->questionSqlColumns[QUESTION_COLUMN_TITLE]];
	$this->supportInfo = $values[$this->questionSqlColumns[QUESTION_COLUMN_SUPPORT_INFO]];
	$this->capConfirmThanks = $values[$this->questionSqlColumns[QUESTION_COLUMN_CAP_CONFIRM_THANKS]];
	$this->requireCorrectAnswer = $values[$this->questionSqlColumns[QUESTION_COLUMN_REQUIRE_CORRECT_ANSWER]];
	$this->question = $values[$this->questionSqlColumns[QUESTION_COLUMN_QUESTION]];
	$this->multipleChoice1 = $values[$this->questionSqlColumns[QUESTION_COLUMN_MULTIPLE_CHOICE1]];
	$this->multipleChoice2 = $values[$this->questionSqlColumns[QUESTION_COLUMN_MULTIPLE_CHOICE2]];
	$this->multipleChoice3 = $values[$this->questionSqlColumns[QUESTION_COLUMN_MULTIPLE_CHOICE3]];
	$this->multipleChoice4 = $values[$this->questionSqlColumns[QUESTION_COLUMN_MULTIPLE_CHOICE4]];
	$this->multipleChoice5 = $values[$this->questionSqlColumns[QUESTION_COLUMN_MULTIPLE_CHOICE5]];
	$this->feedbackChoice1 = $values[$this->questionSqlColumns[QUESTION_COLUMN_FEEDBACK_CHOICE1]];
	$this->feedbackChoice2 = $values[$this->questionSqlColumns[QUESTION_COLUMN_FEEDBACK_CHOICE2]];
	$this->feedbackChoice3 = $values[$this->questionSqlColumns[QUESTION_COLUMN_FEEDBACK_CHOICE3]];
	$this->feedbackChoice4 = $values[$this->questionSqlColumns[QUESTION_COLUMN_FEEDBACK_CHOICE4]];
	$this->feedbackChoice5 = $values[$this->questionSqlColumns[QUESTION_COLUMN_FEEDBACK_CHOICE5]];
	$this->correctAnswer = $values[$this->questionSqlColumns[QUESTION_COLUMN_CORRECT_ANSWER]];
	$this->purpose = $values[$this->questionSqlColumns[QUESTION_COLUMN_PURPOSE]];
	$this->learningPoints = $values[$this->questionSqlColumns[QUESTION_COLUMN_LEARNING_POINTS]];
	$this->mediaFilePath = $values[$this->questionSqlColumns[QUESTION_COLUMN_MEDIA_FILE_PATH]];
	$this->mediaDurationSeconds = $values[$this->questionSqlColumns[QUESTION_COLUMN_MEDIA_DURATION_SECONDS]];
	$this->displayChoices = $values[$this->questionSqlColumns[QUESTION_COLUMN_DISPLAY_CHOICES]];
	$this->ownerUserId = $values[$this->questionSqlColumns[QUESTION_COLUMN_OWNER_USER_ID]];
      }

      $this->questionSetSize = $this->GetQuestionSetSize();
      if ($this->questionSetSize > 1) {
	$this->nextAssignmentId = (int)$this->GetNextChildQuestionId();
      }

    }

    return RC_OK;
  }


  // This retrieves information from the database and populates the properties of this
  // object with values and information from the database.
  function SetArchiveQuestionId($newQuestionId, $newDomainId, $newVersionDate) {
    
    $newQuestionId = (int)$newQuestionId;
    $newDomainId = (int)$newDomainId;

    if (DEBUG & DEBUG_CLASS_FUNCTIONS)
      error_log("-----------> FUNCTION CALL: Question::SetArchiveQuestionId(newQuestionId=$newQuestionId, ".
		"newDomainId=$newDomainId, newVersionDate=$newVersionDate)");

    if ($newQuestionId <= 0 || $newDomainId <= 0) {
      error_log("Question::SetArchiveQuestionId($newQuestionId,$newDomainId,$newVersionDate): "
		."invalid non-numeric passed to SetQuestionId");
      return RC_INVALID_ARG;
    }

    $values = array();
    
    if ($this->questionId != $newQuestionId) {
      $this->questionId = $newQuestionId;
      $this->Select('*', QUESTION_ARCHIVE_TABLE);
      $this->Where($this->questionSqlColumns[QUESTION_COLUMN_ID]." = ".$this->questionId);
      if ($newDomainId)
	$this->Where($this->questionSqlColumns[QUESTION_COLUMN_DOMAIN_ID]." = ".$newDomainId);
      if ($newVersionDate)
	$this->Where($this->questionSqlColumns[QUESTION_COLUMN_VERSION_DATE]." = '".$newVersionDate."'");
      else {
	$this->OrderBy($this->questionSqlColumns[QUESTION_COLUMN_VERSION_DATE], TRUE);
	$this->Limit(1);
      }

      $values = $this->Query(SINGLE_ROW);
      
      if ($values === FALSE) {
	error_log("Question.SetArchiveQuestionId($newQuestionId,$newDomainId,$newVersionDate): "
		  .$this->GetErrorMessage());
	return FALSE;
      }
      // Warn if there were no results
      if (count($values) == 0) {
	error_log("WARN: Question.SetArchiveQuestionId(newQuestionId=$newQuestionId,newDomainId=$newDomainId,newVersionDate=$newVersionDate): ".
		  "No archive questions found with given id, domain, and version date.  User may be seeing blank pages!\n");
      }
      $this->parentQuestionId = (int)$values[$this->questionSqlColumns[QUESTION_COLUMN_PARENT_ID]];
      $this->languageId = $values[$this->questionSqlColumns[QUESTION_COLUMN_LANGUAGE_ID]];
      $this->domainId = $values[$this->questionSqlColumns[QUESTION_COLUMN_DOMAIN_ID]];
      $this->categoryId = $values[$this->questionSqlColumns[QUESTION_COLUMN_CATEGORY_ID]];
      $this->frameTypeId = $values[$this->questionSqlColumns[QUESTION_COLUMN_FRAMETYPE_ID]];
      $this->versionDate = $values[$this->questionSqlColumns[QUESTION_COLUMN_VERSION_DATE]];
      $this->isManagement = $values[$this->questionSqlColumns[QUESTION_COLUMN_IS_MANAGEMENT]];
      $this->author = $values[$this->questionSqlColumns[QUESTION_COLUMN_AUTHOR]];
      $this->notes = $values[$this->questionSqlColumns[QUESTION_COLUMN_NOTES]];
      $this->title = $values[$this->questionSqlColumns[QUESTION_COLUMN_TITLE]];
      $this->supportInfo = $values[$this->questionSqlColumns[QUESTION_COLUMN_SUPPORT_INFO]];
      $this->capConfirmThanks = $values[$this->questionSqlColumns[QUESTION_COLUMN_CAP_CONFIRM_THANKS]];
      $this->requireCorrectAnswer = $values[$this->questionSqlColumns[QUESTION_COLUMN_REQUIRE_CORRECT_ANSWER]];
      $this->question = $values[$this->questionSqlColumns[QUESTION_COLUMN_QUESTION]];
      $this->multipleChoice1 = $values[$this->questionSqlColumns[QUESTION_COLUMN_MULTIPLE_CHOICE1]];
      $this->multipleChoice2 = $values[$this->questionSqlColumns[QUESTION_COLUMN_MULTIPLE_CHOICE2]];
      $this->multipleChoice3 = $values[$this->questionSqlColumns[QUESTION_COLUMN_MULTIPLE_CHOICE3]];
      $this->multipleChoice4 = $values[$this->questionSqlColumns[QUESTION_COLUMN_MULTIPLE_CHOICE4]];
      $this->multipleChoice5 = $values[$this->questionSqlColumns[QUESTION_COLUMN_MULTIPLE_CHOICE5]];
      $this->feedbackChoice1 = $values[$this->questionSqlColumns[QUESTION_COLUMN_FEEDBACK_CHOICE1]];
      $this->feedbackChoice2 = $values[$this->questionSqlColumns[QUESTION_COLUMN_FEEDBACK_CHOICE2]];
      $this->feedbackChoice3 = $values[$this->questionSqlColumns[QUESTION_COLUMN_FEEDBACK_CHOICE3]];
      $this->feedbackChoice4 = $values[$this->questionSqlColumns[QUESTION_COLUMN_FEEDBACK_CHOICE4]];
      $this->feedbackChoice5 = $values[$this->questionSqlColumns[QUESTION_COLUMN_FEEDBACK_CHOICE5]];
      $this->correctAnswer = $values[$this->questionSqlColumns[QUESTION_COLUMN_CORRECT_ANSWER]];
      $this->purpose = $values[$this->questionSqlColumns[QUESTION_COLUMN_PURPOSE]];
      $this->learningPoints = $values[$this->questionSqlColumns[QUESTION_COLUMN_LEARNING_POINTS]];
      $this->mediaFilePath = $values[$this->questionSqlColumns[QUESTION_COLUMN_MEDIA_FILE_PATH]];
      $this->mediaDurationSeconds = $values[$this->questionSqlColumns[QUESTION_COLUMN_MEDIA_DURATION_SECONDS]];
      $this->displayChoices = $values[$this->questionSqlColumns[QUESTION_COLUMN_DISPLAY_CHOICES]];
      $this->ownerUserId = $values[$this->questionSqlcolumns[QUESTION_COLUMN_OWNER_USER_ID]];
    }
  }


  // This returns the labels that are to be used for the column headers in
  // the question list display.
  function GetColumnLabels() {
    return $this->questionListLabelColumns;
  }


  // This returns the summary list labels that are to be used for the column 
  // headers in the summary list display.
  function GetSummaryColumnLabels($configIncludeUserColumn = FALSE) {

    $columns = unserialize(SUMMARY_LIST_LABELS);
    if (!$configIncludeUserColumn)
      unset($columns[SUMMARY_LIST_COLUMN_USER_ID]);

    return $columns;
  }


  // This is used to populate the select dropdown box of languages
  function GetLanguages() {
    
    $this->Select(array($this->languageSqlColumns[LANGUAGE_COLUMN_ID],
			$this->languageSqlColumns[LANGUAGE_COLUMN_NAME]),
		  LANGUAGE_TABLE);
    $languages = $this->Query(NAME_VALUES);
    if ($languages === FALSE) {
      error_log("Question.GetLanguages(): Failed to get language list from database");
      return array();
    }

    return $languages;
  }

  
  // This is used to populate the select dropdown box of domains (locations/states/etc...)
  function GetDomains() {
    
    $this->Select(array($this->domainSqlColumns[DOMAIN_COLUMN_ID],
			$this->domainSqlColumns[DOMAIN_COLUMN_NAME]),
		  DOMAIN_TABLE);
    $domains = $this->Query(NAME_VALUES);
    if ($domains === FALSE) {
      error_log("Question.GetDomains(): Failed to get domain list from database");
      return array();
    }
    
    return $domains;
  }
  
  
  // This is used to populate the select dropdown box of frame types (question types, mc text, mc video, etc...)
  function GetFrameTypes() {

    $this->Select(array($this->frameTypeSqlColumns[FRAMETYPE_COLUMN_ID],
			$this->frameTypeSqlColumns[FRAMETYPE_COLUMN_ITEM_NAME]),
		  FRAMETYPE_TABLE);
    $frameTypes = $this->Query(NAME_VALUES);
    if ($frameTypes === FALSE) {
      error_log("Question.GetFrameTypes(): Failed to get domain list from database");
      return array();
    }
    
    return $frameTypes;
  }


  // This is used to populate the select dropdown box of categories (locations/states/etc...)
  function GetCategories() {
    
    $this->Select(array($this->categorySqlColumns[CATEGORY_COLUMN_ID],
			$this->categorySqlColumns[CATEGORY_COLUMN_NAME]),
		  CATEGORY_TABLE);
    $categories = $this->Query(NAME_VALUES);
    if ($categories === FALSE) {
      error_log("Question.GetCategories(): Failed to get category list from database");
      return array();
    }
    
    return $categories;
  }


  // This is used to get the next question number for insert
  // These are managed manually since the original authors of this system decided
  // to segment out the question ID space into administrative use and end user use.
  function GetNextQuestionId($low, $high) {
    
    $this->Select('MAX('.$this->questionSqlColumns[QUESTION_COLUMN_ID].')+1 AS last',
		  QUESTION_TABLE);
    $this->Where($this->questionSqlColumns[QUESTION_COLUMN_ID].'>='.$low);
    $this->Where($this->questionSqlColumns[QUESTION_COLUMN_ID].'<'.$high);
    /*
    $cnt = $this->QueryCount();
    if ($cnt === FALSE) {
      error_log("Question.GetNextQuestionId($low,$high): Unable to execute query count for next questionId");
      return 0;
    }
    if ($cnt == 0)
      return $low;
    */
    $id = $this->Query(SINGLE_VALUE, VALUES_FORMAT);
    if ($id === FALSE) {
      error_log("Question.GetNextQuestionId($low,$high): Unable to execute query for next questionId");
      return RC_QUERY_FAILED; // Indicates an error and something bad happened
    }

    if ($id == 0) // SQL representation of NULL value from db
      return $low;
    
    if ($id > $high)
      return RC_NO_MORE_NUMBERS;

    return $id;
  }
  
  
  // This function will test each variable value to determine if the question
  // has changed and requires an update
  function UpdateRequired($qOld) {
    if ($this->questionId != $qOld->questionId ||
	$this->languageId != $qOld->languageId ||
	$this->domainId != $qOld->domainId ||
	$this->categoryId != $qOld->categoryId ||
	$this->frameTypeId != $qOld->frameTypeId ||
	$this->versionDate != $qOld->versionDate ||
	$this->isManagement != $qOld->isManagement ||
	$this->author != $qOld->author ||
	$this->notes != $qOld->notes ||
	$this->title != $qOld->title ||
	$this->supportInfo != $qOld->supportInfo ||
	$this->capConfirmThanks != $qOld->capConfirmThanks ||
	$this->requireCorrectAnswer != $qOld->requireCorrectAnswer ||
	$this->question != $qOld->question ||
	$this->multipleChoice1 != $qOld->multipleChoice1 ||
	$this->multipleChoice2 != $qOld->multipleChoice2 ||
	$this->multipleChoice3 != $qOld->multipleChoice3 ||
	$this->multipleChoice4 != $qOld->multipleChoice4 ||
	$this->multipleChoice5 != $qOld->multipleChoice5 ||
	$this->feedbackChoice1 != $qOld->feedbackChoice1 ||
	$this->feedbackChoice2 != $qOld->feedbackChoice2 ||
	$this->feedbackChoice3 != $qOld->feedbackChoice3 ||
	$this->feedbackChoice4 != $qOld->feedbackChoice4 ||
	$this->feedbackChoice5 != $qOld->feedbackChoice5 ||
	$this->correctAnswer != $qOld->correctAnswer ||
	$this->purpose != $qOld->purpose ||
	$this->learningPoints != $qOld->learningPoints ||
	$this->mediaFilePath != $qOld->mediaFilePath)
      return TRUE;

    return FALSE;
  }


  // This function will test each variable value to determine if the question
  // has changed and requires an updat
  function FullUpdateRequired($qOld) {
    if ($this->questionId == $qOld->questionId &&
	$this->languageId == $qOld->languageId &&
	$this->domainId == $qOld->domainId &&
	$this->categoryId == $qOld->categoryId &&
	$this->frameTypeId == $qOld->frameTypeId &&
	$this->versionDate == $qOld->versionDate &&
	$this->isManagement == $qOld->isManagement &&
	$this->author == $qOld->author &&
	$this->title == $qOld->title &&
	$this->question == $qOld->question &&
	$this->multipleChoice1 == $qOld->multipleChoice1 &&
	$this->multipleChoice2 == $qOld->multipleChoice2 &&
	$this->multipleChoice3 == $qOld->multipleChoice3 &&
	$this->multipleChoice4 == $qOld->multipleChoice4 &&
	$this->multipleChoice5 == $qOld->multipleChoice5 &&
	$this->feedbackChoice1 == $qOld->feedbackChoice1 &&
	$this->feedbackChoice2 == $qOld->feedbackChoice2 &&
	$this->feedbackChoice3 == $qOld->feedbackChoice3 &&
	$this->feedbackChoice4 == $qOld->feedbackChoice4 &&
	$this->feedbackChoice5 == $qOld->feedbackChoice5 &&
	$this->correctAnswer == $qOld->correctAnswer &&
	$this->purpose == $qOld->purpose &&
	$this->learningPoints == $qOld->learningPoints &&
	$this->mediaFilePath == $qOld->mediaFilePath)
      return FALSE;

    return TRUE;
  }


  // This function returns simply a list of all existing question numbers
  function GetParentQuestions() {

    $results = array();

    $this->Select(array($this->questionSqlColumns[QUESTION_COLUMN_ID],
			$this->questionSqlColumns[QUESTION_COLUMN_TITLE]),
		  QUESTION_TABLE);
    $this->Where($this->questionSqlColumns[QUESTION_COLUMN_PARENT_ID].'=0');
    $this->Where($this->questionSqlColumns[QUESTION_COLUMN_ID].'!='.$this->questionId);
    $this->SortBy($this->questionSqlColumns[QUESTION_COLUMN_ID]);
    $results = $this->Query(NAME_VALUES);
    return $results;
  }


  // This function returns the data to be displayed for each row of the list of questions.
  function GetQuestionList($columnIds, $searchColumnId, $searchFor, $sortColumnId,
			   $sortOrder, &$pageNumber, $rowsPerPage, $userId, $archived = FALSE) {
    
    if (empty($columnIds)) {
      if (DEBUG & DEBUG_CLASS_FUNCTIONS)
	error_log("No columns selected for track list so immediately returning with empty result sets");
      return array();
    }

    if ($pageNumber <= 0) {
      if (DEBUG & DEBUG_CLASS_FUNCTIONS)
	error_log("Question.GetQuestionList(): Invalid Page Number($pageNumber) passed, adjusting to 1");
      $pageNumber = 1;
    }

    if ($rowsPerPage <= 0) {
      if (DEBUG & DEBUG_CLASS_FUNCTIONS)
	error_log("Question.GetQuestionList(): Invalid Rows Per Page($rowsPerPage) passed, adjusting to 1");
      $rowsPerPage = 1;
    }

    $questionListColumns = array();
    $searchExact = FALSE;
    $sortBy = '';
    $table = '';
    $joinsArray = array();
    $qIds = array();
    $questionNumberIndex = -1;
    $parentNumberIndex = -1;
    $columnIndex = 0;
    $where = '';
    $config = new Config($this->orgId);

    $table = $archived ? QUESTION_ARCHIVE_TABLE : QUESTION_TABLE;

    switch($sortColumnId) {
    case QUESTION_LIST_COLUMN_DOMAIN:
      $sortBy = array(DOMAIN_TABLE.'.'.$this->domainSqlColumns[DOMAIN_COLUMN_NAME],
		      QUESTION_LIST_TABLE.'.'.$this->questionListSqlColumns[QUESTION_LIST_COLUMN_NUMBER],
		      QUESTION_LIST_TABLE.'.'.$this->questionListSqlColumns[QUESTION_LIST_COLUMN_VERSION_DATE]);
      break;
    case QUESTION_LIST_COLUMN_CATEGORY:
      $sortBy = array(CATEGORY_TABLE.'.'.$this->categorySqlColumns[CATEGORY_COLUMN_NAME],
		      QUESTION_LIST_TABLE.'.'.$this->questionListSqlColumns[QUESTION_LIST_COLUMN_NUMBER],
		      QUESTION_LIST_TABLE.'.'.$this->questionListSqlColumns[QUESTION_LIST_COLUMN_VERSION_DATE]);
      break;
    case QUESTION_LIST_COLUMN_LANGUAGE:
      $sortBy = array(LANGUAGE_TABLE.'.'.$this->languageSqlColumns[LANGUAGE_COLUMN_NAME],
		      QUESTION_LIST_TABLE.'.'.$this->questionListSqlColumns[QUESTION_LIST_COLUMN_NUMBER],
		      QUESTION_LIST_TABLE.'.'.$this->questionListSqlColumns[QUESTION_LIST_COLUMN_VERSION_DATE]);
      break;
    default:
      // Sort Column
      if ($sortColumnId != QUESTION_LIST_COLUMN_VERSION_DATE) {
	$sortBy = array($this->questionListSqlColumns[$sortColumnId]);
	if ($sortColumnId != QUESTION_LIST_COLUMN_NUMBER)
	  array_push($sortBy, $this->questionListSqlColumns[QUESTION_LIST_COLUMN_NUMBER]);
	array_push($sortBy, $this->questionListSqlColumns[QUESTION_LIST_COLUMN_VERSION_DATE]);
      } else {
	$sortBy = array($this->questionListSqlColumns[$sortColumnId],
			$this->questionListSqlColumns[QUESTION_LIST_COLUMN_NUMBER]);
      }
      break;
    }

    // Now only select the columns currently selected in the interface for display
    for ($i = 0; $i < count($this->questionListSqlColumns); $i++) {
      //      error_log("+_+_+_+_+_ Checking to see if column ".$this->questionListSqlColumns[$i]."($i) is selected\n");
      if (1 << $i & $columnIds) {
	//	error_log("+_+_++_+_+_+_+_+_+_    column ".$this->questionListSqlColumns[$i]."($i) is selected\n");
	switch(1 << $i) {
	case QUESTION_LIST_COLUMN_LANGUAGE:
	  array_push($questionListColumns, LANGUAGE_TABLE.'.'.$this->languageSqlColumns[LANGUAGE_COLUMN_NAME]);
	  $joinArray[LANGUAGE_TABLE] = array($table.'.'.$this->questionListSqlColumns[QUESTION_LIST_COLUMN_LANGUAGE],
					     '=', LANGUAGE_TABLE.'.'.$this->languageSqlColumns[LANGUAGE_COLUMN_ID]);
	  $columnIndex++;
	  break;
	case QUESTION_LIST_COLUMN_CATEGORY:
	  array_push($questionListColumns, CATEGORY_TABLE.'.'.$this->categorySqlColumns[CATEGORY_COLUMN_NAME]);
	  $joinArray[CATEGORY_TABLE] = array($table.'.'.$this->questionListSqlColumns[QUESTION_LIST_COLUMN_CATEGORY],
					     '=', CATEGORY_TABLE.'.'.$this->categorySqlColumns[CATEGORY_COLUMN_ID]);
	  $columnIndex++;
	  break;
	case QUESTION_LIST_COLUMN_DOMAIN:
	  array_push($questionListColumns, DOMAIN_TABLE.'.'.$this->domainSqlColumns[DOMAIN_COLUMN_NAME]);
	  $joinArray[DOMAIN_TABLE] = array($table.'.'.$this->questionListSqlColumns[QUESTION_LIST_COLUMN_DOMAIN],
					   '=', DOMAIN_TABLE.'.'.$this->domainSqlColumns[DOMAIN_COLUMN_ID]);
	  $columnIndex++;
	  break;
	case QUESTION_LIST_COLUMN_VERSION_DATE:
	  array_push($questionListColumns, 'DATE('.$table.'.'.$this->questionListSqlColumns[1 << $i].')');
	  $columnIndex++;
	  break;
	default:
	  if (1 << $i == QUESTION_LIST_COLUMN_NUMBER)
	    $questionNumberIndex = $columnIndex;
	  if (1 << $i == QUESTION_LIST_COLUMN_PARENT_NUMBER)
	    $parentNumberIndex = $columnIndex;
	  array_push($questionListColumns, $table.'.'.$this->questionListSqlColumns[1 << $i]);
	  $columnIndex++;
	  break;
	}
      }
    }

    // Always select the necessary fields to build the links.  These just get popped off of the result
    // set so that they are not sent to the template.
    array_push($questionListColumns, $table.'.'.$this->questionListSqlColumns[QUESTION_LIST_COLUMN_VERSION_DATE]);
    array_push($questionListColumns, $table.'.'.$this->questionListSqlColumns[QUESTION_LIST_COLUMN_DOMAIN]);
    array_push($questionListColumns, $table.'.'.$this->questionListSqlColumns[QUESTION_LIST_COLUMN_NUMBER]);
    array_push($questionListColumns, $table.'.'.$this->questionSqlColumns[QUESTION_COLUMN_OWNER_USER_ID]);
    array_push($questionListColumns, $table.'.'.$this->questionSqlColumns[QUESTION_COLUMN_CATEGORY_ID]);
    array_push($questionListColumns, $table.'.'.$this->questionSqlColumns[QUESTION_COLUMN_LANGUAGE_ID]);

    /*
    error_log("COLUMN: ".$this->questionListSqlColumns[QUESTION_LIST_COLUMN_VERSION_DATE]);
    error_log("COLUMN: ".$this->questionListSqlColumns[QUESTION_LIST_COLUMN_DOMAIN]);
    error_log("COLUMN: ".$this->questionListSqlColumns[QUESTION_LIST_COLUMN_NUMBER]);
    */

    // Begin building query
    $this->Select($questionListColumns, $table);
    if (!empty($joinArray))
      $this->LeftJoinUsing($joinArray);

    if ($searchFor) {
      if ($searchColumnId == QUESTION_COLUMN_ID || $searchColumnId == QUESTION_COLUMN_PARENT_ID)
	$searchExact = TRUE;
      switch($searchColumnId) {
      case QUESTION_LIST_COLUMN_CATEGORY:
	$this->SearchBy(CATEGORY_TABLE.'.'.$this->categorySqlColumns[CATEGORY_COLUMN_NAME], $searchFor, $searchExact);
	break;
      case QUESTION_LIST_COLUMN_LANGUAGE:
	$this->SearchBy(LANGUAGE_TABLE.'.'.$this->languageSqlColumns[LANGUAGE_COLUMN_NAME], $searchFor, $searchExact);
	break;
      case QUESTION_LIST_COLUMN_DOMAIN:
	$this->SearchBy(DOMAIN_TABLE.'.'.$this->domainSqlColumns[DOMAIN_COLUMN_NAME], $searchFor, $searchExact);
	break;
      default:
	$this->SearchBy(QUESTION_LIST_TABLE.'.'.$this->questionListSqlColumns[$searchColumnId], $searchFor, $searchExact);
	break;
      }
    }

    // Work with question ranges for different types of content
    $this->Where($table.'.'.$this->questionSqlColumns[QUESTION_COLUMN_ID].'>399');
    $where = '(';
    if ($config->GetValue(CONFIG_LICENSED_HR_CONTENT) == 1)
      $where .= '('.$table.'.'.$this->questionSqlColumns[QUESTION_COLUMN_ID].'>=400 AND '.
	$table.'.'.$this->questionSqlColumns[QUESTION_COLUMN_ID].'< 5000)';
    if ($config->GetValue(CONFIG_LICENSED_AB1825_CONTENT) == 1) {
      if ($where != '(')
	$where .= ' OR ';
      $where .= '('.$table.'.'.$this->questionSqlColumns[QUESTION_COLUMN_ID].'>=50000 AND '.
	$table.'.'.$this->questionSqlColumns[QUESTION_COLUMN_ID].'< 55000)';
    }
    if ($config->GetValue(CONFIG_LICENSED_EDU_CONTENT) == 1) {
      if ($where != '(')
	$where .= ' OR ';
      $where .= '('.$table.'.'.$this->questionSqlColumns[QUESTION_COLUMN_ID].'>=90000 AND '.
	$table.'.'.$this->questionSqlColumns[QUESTION_COLUMN_ID].'< 95000)';
    }
    if ($where != '(')
      $where .= ' OR ';
    $where .= '('.$table.'.'.$this->questionSqlColumns[QUESTION_COLUMN_ID].'>=100000)';
    if ($where != '(')
      $this->Where($where.')');

    $this->SortBy($sortBy, $sortOrder);
    $this->SetPage($pageNumber, $rowsPerPage);
    $pageCount = $this->GetPageCount($rowsPerPage);
    if ($pageNumber > $pageCount && $pageCount > 0) {
      error_log("Question.GetQuestionList(): Page Number ($pageNumber) is greater than actual pages ($pageCount)");
      $pageNumber = $pageCount;
      $this->SetPage($pageNumber, $rowsPerPage);
    }
    $results = $this->Query(MANY_ROWS, VALUES_FORMAT);

    // Add actions for this listing
    for($i = 0; $i < count($results); $i++) {
      $languageId = (int)array_pop($results[$i]);
      $categoryId = (int)array_pop($results[$i]);
      $ownerUserId = (int)array_pop($results[$i]);
      $questionId = (int)array_pop($results[$i]);
      $domainId = (int)array_pop($results[$i]);
      $versionDate = array_pop($results[$i]);
      if ($questionNumberIndex >= 0) {
	$results[$i][$questionNumberIndex] = new TplCaption($results[$i][$questionNumberIndex]);
	$results[$i][$questionNumberIndex]->href='question_view.php';
	if ($table == QUESTION_ARCHIVE_TABLE)
	  $results[$i][$questionNumberIndex]->params="domainId=$domainId&versionDate=$versionDate";
	//$results[$i][$questionNumberIndex]->target='_blank';
      }
      if ($parentNumberIndex >= 0)
	if ($results[$i][$parentNumberIndex] == 0)
	  $results[$i][$parentNumberIndex] = '';
      $ref2 = new TplCaption('View');
      $ref2->href='question_view.php';
      if ($archived) {
	//$ref3->href='archive_list.php';
	$ref1 = new TplCaption('Restore');
	$ref1->href='archive_list.php';
	$ref1->params="restore=1&domainId=$domainId&versionDate=".urlencode($versionDate);
	$ref2->params="get_archived=1&domainId=$domainId&versionDate=".urlencode($versionDate);
	//$ref3->params="domainId=$domainId&versionDate=".urlencode($versionDate);
	array_push($results[$i], array($ref1));
      } else {
	if ($categoryId == 118) { // dedicated AB1825 category
	  $ref1 = new TplCaption('Preview');
	  $ref1->href="javascript:launchPopup('Popup','../presentation/q_type.php?questionId=$questionId&review=0&preview=1&domainId=$domainId&languageId=$languageId','no',850,1000);";
	  array_push($results[$i], array($ref1));
	} else {
	  $ref1 = new TplCaption('Edit');
	  $ref1->href='question_edit.php';
	  if (((int)$userId != 1 && $questionId < 100000) || (int)$userId != (int)$ownerUserId)
	    $ref1->onClick="return AskEditQuestion('$questionId');";
	  $ref3 = new TplCaption('Archive');
	  $ref3->href='question_list.php';
	  $ref3->onClick="return AskDeleteQuestion('$questionId');";
	  array_push($results[$i], array($ref1, $ref3));
	}
      }
      array_push($qIds, $questionId);
    }

    return array($qIds, $results);
  }




  // This function returns the data to be displayed for each row of the list of questions.
  function GetSummaryList($columnIds, $searchColumnId, $searchFor, $sortColumnId,
			  $sortOrder, &$pageNumber, $rowsPerPage, &$summaryTracks) {

    // Sanity check (if only viewing summary from tracks then if no tracks, then no summary)
    if ($summaryTracks !== FALSE && count($summaryTracks) == 0)
      return array();

    if (empty($columnIds)) {
      if (DEBUG & DEBUG_CLASS_FUNCTIONS)
	error_log("No columns selected for track list so immediately returning with empty result sets");
      return array();
    }
    
    if ($pageNumber <= 0) {
      error_log("Question.GetQuestionList(): Invalid Page Number($pageNumber) passed, adjusting to 1");
      $pageNumber = 1;
    }
    
    if ($rowsPerPage <= 0) {
      error_log("Question.GetQuestionList(): Invalid Rows Per Page($rowsPerPage) passed, adjusting to 1");
      $rowsPerPage = 1;
    }
    
    $summaryListSqlColumns = unserialize(SUMMARY_LIST_SQL_COLUMNS);
    $summaryListAsSqlColumns = unserialize(SUMMARY_LIST_AS_SQL_COLUMNS);
    $summaryListToAssignmentMap = unserialize(SUMMARY_LIST_COLUMNS_MAP);
    $assignmentSqlColumns = unserialize(ASSIGNMENT_SQL_COLUMNS);
    $departmentSqlColumns = unserialize(DEPARTMENT_SQL_COLUMNS);
    $trackSqlColumns = unserialize(TRACK_SQL_COLUMNS);
    $userSqlColumns = unserialize(USER_SQL_COLUMNS);

    $summaryListColumns = array();
    $assignmentListColumns = array();
    $sortBy = '';
    $whereTracks = '';
    $assignmentSearchColumnId = $summaryListToAssignmentMap[$searchColumnId];
    $joinsArray = array();
    $joinsAssignmentArray = array();
    $qIds = array();
    $answerIdx = (-1);
    $resultIdx = (-1);
    $exitIdx = (-1);
    $idIdx = (-1);
    $userNameIdx = (-1);

    // Setup sort name
    if ($sortColumnId)
      $sortBy = $summaryListAsSqlColumns[$sortColumnId];

    // Now only select the columns currently selected in the interface for display
    for ($i = 0; $i < count($summaryListSqlColumns); $i++) {
      if (1 << $i & $columnIds) {
	switch(1 << $i) {
	case SUMMARY_LIST_COLUMN_DOMAIN_ID:
	  array_push($summaryListColumns, DOMAIN_TABLE.'.'.$this->domainSqlColumns[DOMAIN_COLUMN_NAME].
		     ' AS '.$summaryListAsSqlColumns[SUMMARY_LIST_COLUMN_DOMAIN_ID]);
	  $joinArray[DOMAIN_TABLE] = array(QUESTION_DATA_TABLE.'.'.$summaryListSqlColumns[SUMMARY_LIST_COLUMN_DOMAIN_ID],
					   '=', DOMAIN_TABLE.'.'.$this->domainSqlColumns[DOMAIN_COLUMN_ID]);
	  array_push($assignmentListColumns, DOMAIN_TABLE.'.'.$this->domainSqlColumns[DOMAIN_COLUMN_NAME].
		     ' AS '.$summaryListAsSqlColumns[SUMMARY_LIST_COLUMN_DOMAIN_ID]);
	  $joinAssignmentArray[DOMAIN_TABLE] = array(ASSIGNMENT_TABLE.'.'.$assignmentSqlColumns[ASSIGNMENT_COLUMN_DOMAIN_ID],
						     '=', DOMAIN_TABLE.'.'.$this->domainSqlColumns[DOMAIN_COLUMN_ID]);
	  break;
	case SUMMARY_LIST_COLUMN_USER_ID:
	  array_push($summaryListColumns, '0 AS '.$summaryListAsSqlColumns[SUMMARY_LIST_COLUMN_USER_ID]);
	  array_push($assignmentListColumns, USER_TABLE.'.'.$userSqlColumns[USER_COLUMN_FULL_NAME].
		     ' AS '.$summaryListAsSqlColumns[SUMMARY_LIST_COLUMN_USER_ID]);
	  $joinAssignmentArray[USER_TABLE] = array(ASSIGNMENT_TABLE.'.'.$assignmentSqlColumns[ASSIGNMENT_COLUMN_USER_ID],
						     '=', USER_TABLE.'.'.$userSqlColumns[USER_COLUMN_ID]);
	  $userNameIdx = $i;
	  break;
	case SUMMARY_LIST_COLUMN_DEPARTMENT_ID:
	  array_push($summaryListColumns, DEPARTMENT_TABLE.'.'.$departmentSqlColumns[DEPARTMENT_COLUMN_NAME].
		     ' AS '.$summaryListAsSqlColumns[SUMMARY_LIST_COLUMN_DEPARTMENT_ID]);
	  $joinArray[DEPARTMENT_TABLE] = array(QUESTION_DATA_TABLE.'.'
					       .$summaryListSqlColumns[SUMMARY_LIST_COLUMN_DEPARTMENT_ID],
					       '=', DEPARTMENT_TABLE.'.'.$departmentSqlColumns[DEPARTMENT_COLUMN_ID]);
	  array_push($assignmentListColumns, DEPARTMENT_TABLE.'.'.$departmentSqlColumns[DEPARTMENT_COLUMN_NAME].
		     ' AS '.$summaryListAsSqlColumns[SUMMARY_LIST_COLUMN_DEPARTMENT_ID]);
	  $joinAssignmentArray[DEPARTMENT_TABLE] = array(ASSIGNMENT_TABLE.'.'
							 .$assignmentSqlColumns[ASSIGNMENT_COLUMN_DEPARTMENT_ID],
							 '=', DEPARTMENT_TABLE.'.'.$departmentSqlColumns[DEPARTMENT_COLUMN_ID]);
	  break;
	case SUMMARY_LIST_COLUMN_TRACK_ID:
	  array_push($summaryListColumns, TRACK_TABLE.'.'.$trackSqlColumns[TRACK_COLUMN_NAME].
		     ' AS '.$summaryListAsSqlColumns[SUMMARY_LIST_COLUMN_TRACK_ID]);
	  $joinArray[TRACK_TABLE] = array(QUESTION_DATA_TABLE.'.'
					       .$summaryListSqlColumns[SUMMARY_LIST_COLUMN_TRACK_ID],
					       '=', TRACK_TABLE.'.'.$trackSqlColumns[TRACK_COLUMN_ID]);
	  array_push($assignmentListColumns, TRACK_TABLE.'.'.$trackSqlColumns[TRACK_COLUMN_NAME].
		     ' AS '.$summaryListAsSqlColumns[SUMMARY_LIST_COLUMN_TRACK_ID]);
	  $joinAssignmentArray[TRACK_TABLE] = array(ASSIGNMENT_TABLE.'.'
					       .$assignmentSqlColumns[ASSIGNMENT_COLUMN_TRACK_ID],
					       '=', TRACK_TABLE.'.'.$trackSqlColumns[TRACK_COLUMN_ID]);
	  break;
	case SUMMARY_LIST_COLUMN_ANSWER_SELECTED:
	  array_push($summaryListColumns, QUESTION_DATA_TABLE.'.'.$summaryListSqlColumns[1 << $i].
		     ' AS '.$summaryListAsSqlColumns[SUMMARY_LIST_COLUMN_ANSWER_SELECTED]);
	  array_push($assignmentListColumns, ASSIGNMENT_TABLE.'.'.$assignmentSqlColumns[ASSIGNMENT_COLUMN_FIRST_ANSWER].
		     ' AS '.$summaryListAsSqlColumns[SUMMARY_LIST_COLUMN_ANSWER_SELECTED]);
	  $answerIdx = $i;
	  break;
	case SUMMARY_LIST_COLUMN_RESULT:
	  array_push($summaryListColumns, QUESTION_DATA_TABLE.'.'.$summaryListSqlColumns[1 << $i].
		     ' AS '.$summaryListAsSqlColumns[SUMMARY_LIST_COLUMN_RESULT]);
	  array_push($assignmentListColumns, ASSIGNMENT_TABLE.'.'.$assignmentSqlColumns[ASSIGNMENT_COLUMN_ANSWERED_CORRECTLY].
		     ' AS '.$summaryListAsSqlColumns[SUMMARY_LIST_COLUMN_RESULT]);
	  $resultIdx = $i;
	  break;
	case SUMMARY_LIST_COLUMN_EXIT_INFO:
	  array_push($summaryListColumns, QUESTION_DATA_TABLE.'.'.$summaryListSqlColumns[1 << $i].
		     ' AS '.$summaryListAsSqlColumns[SUMMARY_LIST_COLUMN_EXIT_INFO]);
	  array_push($assignmentListColumns, ASSIGNMENT_TABLE.'.'.$assignmentSqlColumns[ASSIGNMENT_COLUMN_EXIT_INFO].
		     ' AS '.$summaryListAsSqlColumns[SUMMARY_LIST_COLUMN_EXIT_INFO]);
	  $exitIdx = $i;
	  break;
	case SUMMARY_LIST_COLUMN_QUESTION_ID:
	  array_push($summaryListColumns, QUESTION_DATA_TABLE.'.'.$summaryListSqlColumns[1 << $i].
		     ' AS '.$summaryListAsSqlColumns[SUMMARY_LIST_COLUMN_QUESTION_ID]);
	  array_push($assignmentListColumns, ASSIGNMENT_TABLE.'.'.$assignmentSqlColumns[ASSIGNMENT_COLUMN_QUESTION_NUMBER].
		     ' AS '.$summaryListAsSqlColumns[SUMMARY_LIST_COLUMN_QUESTION_ID]);
	  $idIdx = $i;
	  break;
	case SUMMARY_LIST_COLUMN_VERSION_DATE:
	  array_push($summaryListColumns, 'DATE('.QUESTION_DATA_TABLE.'.'.$summaryListSqlColumns[1 << $i].')'.
		     ' AS '.$summaryListAsSqlColumns[SUMMARY_LIST_COLUMN_VERSION_DATE]);
	  array_push($assignmentListColumns, 'DATE('.ASSIGNMENT_TABLE.'.'.
		     $assignmentSqlColumns[ASSIGNMENT_COLUMN_VERSION_DATE].')'.
		     ' AS '.$summaryListAsSqlColumns[SUMMARY_LIST_COLUMN_VERSION_DATE]);
	  break;
	case SUMMARY_LIST_COLUMN_COMPLETION_DATE:
	  array_push($summaryListColumns, 'DATE('.QUESTION_DATA_TABLE.'.'.$summaryListSqlColumns[1 << $i].')'.
		     ' AS '.$summaryListAsSqlColumns[SUMMARY_LIST_COLUMN_COMPLETION_DATE]);
	  array_push($assignmentListColumns, 'DATE('.ASSIGNMENT_TABLE.'.'.
		     $assignmentSqlColumns[ASSIGNMENT_COLUMN_COMPLETION_DATE].')'.
		     ' AS '.$summaryListAsSqlColumns[SUMMARY_LIST_COLUMN_COMPLETION_DATE]);
	  break;
	default:
	  //array_push($summaryListColumns, QUESTION_DATA_TABLE.'.'.$summaryListSqlColumns[1 << $i]);
	  error_log("Question::GetSummaryList(): Uknown assignmentList column (".(1<<$i)."): ".$summaryListSqlColumns[1 << $i]);
	  break;
	}
      }
    }

    // Always select the necessary fields to build the links.  These just get popped off of the result
    // set so that they are not sent to the template.
    array_push($summaryListColumns, QUESTION_DATA_TABLE.'.'.$summaryListSqlColumns[SUMMARY_LIST_COLUMN_QUESTION_ID]);
    array_push($assignmentListColumns, ASSIGNMENT_TABLE.'.'.$assignmentSqlColumns[ASSIGNMENT_COLUMN_QUESTION_NUMBER]);

    // Begin building query
    $this->Select($summaryListColumns, QUESTION_DATA_TABLE);
    if (!empty($joinArray))
      $this->LeftJoinUsing($joinArray);
    $this->GetSummaryListSetupSearch($searchColumnId, $searchFor, QUESTION_DATA_TABLE);
    $this->Where($summaryListSqlColumns[SUMMARY_LIST_COLUMN_COMPLETION_DATE]."<='".MIGRATION_DATE_V2_1."'");

    // Limit tracks
    if ($summaryTracks !== FALSE) {
      $whereTracks = '(';
      for ($i = 0; $i < count($summaryTracks); $i++) {
	if ($i == 0)
	  $whereTracks .= QUESTION_DATA_TABLE.'.'.
	    $summaryListSqlColumns[SUMMARY_LIST_COLUMN_TRACK_ID]."=$summaryTracks[$i]";
	else
	  $whereTracks .= ' OR '.QUESTION_DATA_TABLE.'.'.
	    $summaryListSqlColumns[SUMMARY_LIST_COLUMN_TRACK_ID]."=$summaryTracks[$i]";
      }
      $whereTracks .= ')';
      $this->Where($whereTracks);
    }
    

    // Add union part of query
    $this->Union($assignmentListColumns, ASSIGNMENT_TABLE);
    if (!empty($joinAssignmentArray))
      $this->LeftJoinUsing($joinAssignmentArray);
    $this->GetSummaryListSetupSearch($assignmentSearchColumnId, $searchFor, ASSIGNMENT_TABLE);
    if ($whereTracks) {
      $whereTracks = str_replace(QUESTION_DATA_TABLE, ASSIGNMENT_TABLE, $whereTracks);
      $this->Where($whereTracks);
    }
    $this->UnionSortBy($sortBy, $sortOrder);
    $this->SetPage($pageNumber, $rowsPerPage);
    $pageCount = $this->GetPageCount($rowsPerPage);
    if ($pageNumber > $pageCount && $pageCount > 0) {
      error_log("Question.GetQuestionList(): Page Number ($pageNumber) is greater than actual pages ($pageCount)");
      $pageNumber = $pageCount;
      $this->SetPage($pageNumber, $rowsPerPage);
    }

    $this->Where($assignmentSqlColumns[ASSIGNMENT_COLUMN_DELIVERY_DATE].">'".MIGRATION_DATE_V2_1."'");

    $results = $this->Query(MANY_ROWS, VALUES_FORMAT);

    // Add actions for this listing
    for($i = 0; $i < count($results); $i++) {
      array_push($qIds, array_pop($results[$i]));
      if ($answerIdx >= 0)
	$results[$i][$answerIdx] = $this->AnswerNumberToLetter((int)($results[$i][$answerIdx]));
      if ($resultIdx >= 0) {
	if ($results[$i][$resultIdx] == 1)
	  $results[$i][$resultIdx] = 'Correct';
	elseif ($results[$i][$resultIdx] == 2)
	  $results[$i][$resultIdx] = 'Incorrect';
	else
	  $results[$i][$resultIdx] = 'Not Finished';
      }	  
      if ($exitIdx >= 0) {
	if ($results[$i][$exitIdx] == 1)
	  $results[$i][$exitIdx] = 'Understood';
	elseif ($results[$i][$exitIdx] == 2)
	  $results[$i][$exitIdx] = 'Organization Contact';
	else
	  $results[$i][$exitIdx] = 'Not Finished';
      }
      if ($userNameIdx >= 0)
	$results[$i][$userNameIdx] = implode(' ', array_reverse(explode(', ', $results[$i][$userNameIdx])));
      if ($idIdx >= 0) {
	$ref = new TplCaption(sprintf('%06s', $results[$i][$idIdx]));
	$ref->href='question_view.php';
	$ref->params = 'return=summary';
	$results[$i][$idIdx] = $ref;
      }
    }

    return array($qIds, $results);
  }


  // Helper function for GetSummaryList to setup the searchby params
  function GetSummaryListSetupSearch($searchColumnId, $searchFor, $table) {

    if (DEBUG & DEBUG_CLASS_FUNCTIONS)
      error_log("Question::GetSummaryListSetupSearch(searchColumnId=$searchColumnId,searchFor=$searchFor,table=$table");

    // Local vars
    $summaryListSqlColumns = unserialize(SUMMARY_LIST_SQL_COLUMNS);
    $domainSqlColumns = unserialize(DOMAIN_SQL_COLUMNS);
    $trackSqlColumns = unserialize(TRACK_SQL_COLUMNS);
    $languageSqlColumns = unserialize(LANGUAGE_SQL_COLUMNS);
    $departmentSqlColumns = unserialize(DEPARTMENT_SQL_COLUMNS);
    $assignmentSqlColumns = unserialize(ASSIGNMENT_SQL_COLUMNS);
    $userSqlColumns = unserialize(USER_SQL_COLUMNS);
    $searchExact = FALSE;

    if ($searchFor) {
      if ($table == QUESTION_DATA_TABLE) {
	switch($searchColumnId) {
	case SUMMARY_LIST_COLUMN_QUESTION_ID:
	  $searchExact = TRUE;
	  $this->SearchBy($summaryListSqlColumns[SUMMARY_LIST_COLUMN_QUESTION_ID], $searchFor, $searchExact);
	  break;
	case SUMMARY_LIST_COLUMN_LANGUAGE_ID:
	  $this->SearchBy(LANGUAGE_TABLE.'.'.$this->languageSqlColumns[LANGUAGE_COLUMN_NAME], $searchFor, $searchExact);
	  break;
	case SUMMARY_LIST_COLUMN_USER_ID: // Question Data table has no user id column, so eliminate all question data search
	  $this->SearchBy($summaryListSqlColumns[SUMMARY_LIST_COLUMN_QUESTION_ID], 0, TRUE);
	  break;
	case SUMMARY_LIST_COLUMN_DOMAIN_ID:
	  $this->SearchBy(DOMAIN_TABLE.'.'.$this->domainSqlColumns[DOMAIN_COLUMN_NAME], $searchFor, $searchExact);
	  break;
	case SUMMARY_LIST_COLUMN_DEPARTMENT_ID:
	  $this->SearchBy(DEPARTMENT_TABLE.'.'.$departmentSqlColumns[DEPARTMENT_COLUMN_NAME], $searchFor, $searchExact);
	  break;
	case SUMMARY_LIST_COLUMN_TRACK_ID:
	  $this->SearchBy(TRACK_TABLE.'.'.$trackSqlColumns[TRACK_COLUMN_NAME], $searchFor, $searchExact);
	  break;
	case SUMMARY_LIST_COLUMN_EXIT_INFO:
	  $searchExact = TRUE;
	  if (strstr('understood', strtolower($searchFor)) !== FALSE)
	    $searchFor = EXIT_INFO_UNDERSTOOD;
	  else if (strstr('organization contact', strtolower($searchFor)) !== FALSE)
	    $searchFor = EXIT_INFO_CONTACT;
	  else if (strstr('not finished', strtolower($searchFor)) !== FALSE)
	    $searchFor = EXIT_INFO_INCOMPLETE;
	  else
	    $searchFor = (-1);
	  $this->SearchBy($summaryListSqlColumns[SUMMARY_LIST_COLUMN_EXIT_INFO], $searchFor, $searchExact);
	  break;
	case SUMMARY_LIST_COLUMN_RESULT:
	  $searchExact = TRUE;
	  if (strstr('correct', strtolower($searchFor)) !== FALSE)
	    $searchFor = RESULT_CORRECT;
	  else if (strstr('incorrect', strtolower($searchFor)) !== FALSE)
	    $searchFor = RESULT_INCORRECT;
	  else
	    $searchFor = (-1);
	  $this->SearchBy($summaryListSqlColumns[SUMMARY_LIST_COLUMN_RESULT], $searchFor, $searchExact);
	  break;
	case SUMMARY_LIST_COLUMN_ANSWER_SELECTED:
	  // This will convert e.g. the letter A to 1, B to 2, etc...
	  // Its a whole heck of alot better than a giant case statement
	  if (strlen($searchFor) == 1 &&
	      ((ord($searchFor) >= 65 && ord($searchFor) <= 72) ||
	       (ord($searchFor) >= 97 && ord($searchFor) <= 104))) {
	    $searchFor = strtoupper($searchFor);
	    $searchFor = chr((ord($searchFor) - 16));
	    $searchExact = TRUE;
	  }
	  $this->SearchBy($summaryListSqlColumns[SUMMARY_LIST_COLUMN_ANSWER_SELECTED], $searchFor, $searchExact);
	  break;
	case SUMMARY_LIST_COLUMN_VERSION_DATE:
	  $this->SearchBy($summaryListSqlColumns[SUMMARY_LIST_COLUMN_VERSION_DATE], $searchFor, $searchExact);
	  break;
	case SUMMARY_LIST_COLUMN_COMPLETION_DATE:
	  $this->SearchBy($summaryListSqlColumns[SUMMARY_LIST_COLUMN_COMPLETION_DATE], $searchFor, $searchExact);
	  break;
	default:
	  error_log("Question::GetSummaryListSetupSearch(searchColumnId=$searchColumnId,searchFor=$searchFor,".
		    'summaryListSqlColumns=()): Invalid column id, value unknown, empty or not defined');
	  break;
	}

	// Always make sure we are limiting the results to only those questions that have been answered
	$this->SearchBy($summaryListSqlColumns[SUMMARY_LIST_COLUMN_COMPLETION_DATE], 0, TRUE, '!=');

      } else {

	switch($searchColumnId) {
	case ASSIGNMENT_COLUMN_QUESTION_NUMBER:
	  $searchExact = TRUE;
	  $this->SearchBy($assignmentSqlColumns[ASSIGNMENT_COLUMN_QUESTION_NUMBER], $searchFor, $searchExact);
	  break;
	case ASSIGNMENT_COLUMN_LANGUAGE_ID:
	  $this->SearchBy(LANGUAGE_TABLE.'.'.$languageSqlColumns[LANGUAGE_COLUMN_NAME], $searchFor, $searchExact);
	  break;
	case ASSIGNMENT_COLUMN_USER_ID:
	  $this->SearchBy(USER_TABLE.'.'.$userSqlColumns[USER_COLUMN_FULL_NAME], $searchFor, $searchExact);
	  break;
	case ASSIGNMENT_COLUMN_DOMAIN_ID:
	  $this->SearchBy(DOMAIN_TABLE.'.'.$domainSqlColumns[DOMAIN_COLUMN_NAME], $searchFor, $searchExact);
	  break;
	case ASSIGNMENT_COLUMN_DEPARTMENT_ID:
	  $this->SearchBy(DEPARTMENT_TABLE.'.'.$departmentSqlColumns[DEPARTMENT_COLUMN_NAME], $searchFor, $searchExact);
	  break;
	case ASSIGNMENT_COLUMN_TRACK_ID:
	  $this->SearchBy(TRACK_TABLE.'.'.$trackSqlColumns[TRACK_COLUMN_NAME], $searchFor, $searchExact);
	  break;
	case ASSIGNMENT_COLUMN_EXIT_INFO:
	  $searchExact = TRUE;
	  if (strstr('understood', strtolower($searchFor)) !== FALSE)
	    $searchFor = EXIT_INFO_UNDERSTOOD;
	  else if (strstr('organization contact', strtolower($searchFor)) !== FALSE)
	    $searchFor = EXIT_INFO_CONTACT;
	  else if (strstr('not finished', strtolower($searchFor)) !== FALSE)
	    $searchFor = EXIT_INFO_INCOMPLETE;
	  else
	    $searchFor = (-1);
	  $this->SearchBy($assignmentSqlColumns[ASSIGNMENT_COLUMN_EXIT_INFO], $searchFor, $searchExact);
	  break;
	case ASSIGNMENT_COLUMN_ANSWERED_CORRECTLY:
	  $searchExact = TRUE;
	  if (strstr('correct', strtolower($searchFor)) !== FALSE)
	    $searchFor = RESULT_CORRECT;
	  else if (strstr('incorrect', strtolower($searchFor)) !== FALSE)
	    $searchFor = RESULT_INCORRECT;
	  else
	    $searchFor = (-1);
	  $this->SearchBy($assignmentSqlColumns[ASSIGNMENT_COLUMN_ANSWERED_CORRECTLY], $searchFor, $searchExact);
	  break;
	case ASSIGNMENT_COLUMN_FIRST_ANSWER:
	  // This will convert e.g. the letter A to 1, B to 2, etc...
	  // Its a whole heck of alot better than a giant case statement
	  if (strlen($searchFor) == 1 &&
	      ((ord($searchFor) >= 65 && ord($searchFor) <= 72) ||
	       (ord($searchFor) >= 97 && ord($searchFor) <= 104))) {
	    $searchFor = strtoupper($searchFor);
	    $searchFor = chr((ord($searchFor) - 16));
	    $searchExact = TRUE;
	  }
	  $this->SearchBy($assignmentSqlColumns[ASSIGNMENT_COLUMN_FIRST_ANSWER], $searchFor, $searchExact);
	  break;
	case ASSIGNMENT_COLUMN_VERSION_DATE:
	  $this->SearchBy($assignmentSqlColumns[ASSIGNMENT_COLUMN_VERSION_DATE], $searchFor, $searchExact);
	  break;
	case ASSIGNMENT_COLUMN_COMPLETION_DATE:
	  $this->SearchBy($assignmentSqlColumns[ASSIGNMENT_COLUMN_COMPLETION_DATE], $searchFor, $searchExact);
	  break;
	default:
	  error_log("Question::GetSummaryListSetupSearch(searchColumnId=$searchColumnId,searchFor=$searchFor,".
		    'summaryListAsSqlColumns=()): Invalid column id, value unknown, empty or not defined');
	  break;
	}

	// Always make sure we are limiting the results to only those questions that have been answered
	$this->SearchBy($assignmentSqlColumns[ASSIGNMENT_COLUMN_COMPLETION_DATE], 0, TRUE, '!=');
      }
    }    
    return TRUE;
  }


  // Filters content coming from the web page to ensure that it does not mess up
  // the database queries nor the javascript code (lame implementation that this is)
  // Javascript in the database, especially for multiple data elements? Blech!!!!
  function QuestionContentFilter($value) {

    //$value = str_replace("\\", "\\\\", $value);
    return str_replace("\n", "<BR>", str_replace("\r", "", $value));
    //return str_replace('"', '\"', str_replace("\n", "<BR>", str_replace("\r", "", $value)));
    // This regular expression replaces everything not already preceeded by a quote but it does not work
    // for double escaped quotes within the text.  I am leaving it here for reference purposes only.
    //return preg_replace('/([^\\\])"/', '${1}\"', str_replace("\n", "<BR>", str_replace("\r", "", $value)));
  }


  // Separate Insert and Update functions in this class make it easier to deal
  // with all the special logic that surrounds updating one of these stupid things
  function InsertQuestion() {
    $rc = $this->Insert(array($this->questionSqlColumns[QUESTION_COLUMN_ID] => $this->questionId,
			      $this->questionSqlColumns[QUESTION_COLUMN_PARENT_ID] => $this->parentQuestionId,
			      $this->questionSqlColumns[QUESTION_COLUMN_LANGUAGE_ID] => $this->languageId,
			      $this->questionSqlColumns[QUESTION_COLUMN_DOMAIN_ID] => $this->domainId,
			      $this->questionSqlColumns[QUESTION_COLUMN_CATEGORY_ID] => $this->categoryId,
			      $this->questionSqlColumns[QUESTION_COLUMN_FRAMETYPE_ID] => $this->frameTypeId,
			      $this->questionSqlColumns[QUESTION_COLUMN_VERSION_DATE] => $this->versionDate,
			      $this->questionSqlColumns[QUESTION_COLUMN_IS_MANAGEMENT] => $this->isManagement,
			      $this->questionSqlColumns[QUESTION_COLUMN_AUTHOR] => $this->author,
			      $this->questionSqlColumns[QUESTION_COLUMN_NOTES] => $this->notes,
			      $this->questionSqlColumns[QUESTION_COLUMN_TITLE] => $this->title,
			      $this->questionSqlColumns[QUESTION_COLUMN_SUPPORT_INFO] => $this->supportInfo,
			      $this->questionSqlColumns[QUESTION_COLUMN_QUESTION] => $this->question,
			      $this->questionSqlColumns[QUESTION_COLUMN_MULTIPLE_CHOICE1] => $this->multipleChoice1,
			      $this->questionSqlColumns[QUESTION_COLUMN_MULTIPLE_CHOICE2] => $this->multipleChoice2,
			      $this->questionSqlColumns[QUESTION_COLUMN_MULTIPLE_CHOICE3] => $this->multipleChoice3,
			      $this->questionSqlColumns[QUESTION_COLUMN_MULTIPLE_CHOICE4] => $this->multipleChoice4,
			      $this->questionSqlColumns[QUESTION_COLUMN_MULTIPLE_CHOICE5] => $this->multipleChoice5,
			      $this->questionSqlColumns[QUESTION_COLUMN_FEEDBACK_CHOICE1] => $this->feedbackChoice1,
			      $this->questionSqlColumns[QUESTION_COLUMN_FEEDBACK_CHOICE2] => $this->feedbackChoice2,
			      $this->questionSqlColumns[QUESTION_COLUMN_FEEDBACK_CHOICE3] => $this->feedbackChoice3,
			      $this->questionSqlColumns[QUESTION_COLUMN_FEEDBACK_CHOICE4] => $this->feedbackChoice4,
			      $this->questionSqlColumns[QUESTION_COLUMN_FEEDBACK_CHOICE5] => $this->feedbackChoice5,
			      $this->questionSqlColumns[QUESTION_COLUMN_CORRECT_ANSWER] => $this->correctAnswer,
			      $this->questionSqlColumns[QUESTION_COLUMN_PURPOSE] => $this->purpose,
			      $this->questionSqlColumns[QUESTION_COLUMN_LEARNING_POINTS] => $this->learningPoints,
			      $this->questionSqlColumns[QUESTION_COLUMN_MEDIA_FILE_PATH] => $this->mediaFilePath,
			      $this->questionSqlColumns[QUESTION_COLUMN_MEDIA_DURATION_SECONDS] => $this->mediaDurationSeconds,
			      $this->questionSqlColumns[QUESTION_COLUMN_CAP_CONFIRM_THANKS] => $this->capConfirmThanks,
			      $this->questionSqlColumns[QUESTION_COLUMN_REQUIRE_CORRECT_ANSWER] => $this->requireCorrectAnswer,
			      $this->questionSqlColumns[QUESTION_COLUMN_DISPLAY_CHOICES] => $this->displayChoices,
			      $this->questionSqlColumns[QUESTION_COLUMN_OWNER_USER_ID] => $this->ownerUserId),
			QUESTION_TABLE);
			      //$this->questionSqlColumns[QUESTION_COLUMN_CONTENT] => $this->content,

    if (!$rc) {
      error_log('Question.InsertQuestion(): Failed inserting question into db for questionId='.$this->questionId);
      return RC_QUERY_FAILED;
    }

    return RC_OK;    
  }


  // This function moves a question from the table containing all the question to an archive table
  function ArchiveQuestion($deleteOnArchive = FALSE, $optQuestionId = 0) {

    // Reconnect as user with permissions to delete
    //$db = new DatabaseObject($this->dbName, ORG_ADMIN_USER, ORG_ADMIN_PASS, DB_HOST);

    if ($optQuestionId)
      $qId = $optQuestionId;
    else
      $qId = $this->questionId;

    $sql = "REPLACE INTO ".QUESTION_ARCHIVE_TABLE." SELECT * FROM ".QUESTION_TABLE
      ." WHERE ".$this->questionSqlColumns[QUESTION_COLUMN_ID].'='.$qId;
    //      ." AND ".$this->questionSqlColumns[QUESTION_COLUMN_VERSION_DATE]."='".$this->versionDate."'"
    //      ." AND ".$this->questionSqlColumns[QUESTION_COLUMN_DOMAIN_ID].'='.$this->domaindId;
    $rc = $this->Execute($sql);
    if (!$rc) {
      error_log("Question.ArchiveQuestion(deleteOnArchive=$deleteOnArchive): ".RcToText($rc));
      return RC_QUERY_FAILED;
    }

    if ($deleteOnArchive && $this->DeleteQuestion() != RC_OK) {
      error_log("Question.ArchiveQuestion(deleteOnArchive=$deleteOnArchive): ".
		'Inserted into archive but unable to delete'.$this->GetErrorMessage());
      return RC_QUERY_FAILED;
    }

    return RC_OK;
  }


  // This function moves a question from the table containing all the question to an archive table
  function UnArchiveQuestion($domainId, $versionDate) {

    //    if (DEBUG & DEBUG_CLASS_FUNCTIONS)
    //      error_log("FUNCTION CALL: Question.UnArchiveQuestion(domainId=$domainId, versionDate=$versionDate)");

    if (!is_numeric($domainId) || !preg_match("/\d{4}-\d{2}-\d{2}( (\d{2}:){2}\d{2})?/", $versionDate)) {
      error_log("Question.UnArchiveQuestion($domainId,$versionDate): Invalid argument");
      return RC_INVALID_ARG;
    }

    // If there is a question in the question table currently with the same id
    // then it must be archived first.
    $this->ArchiveQuestion();

    // Reconnect as user with permissions to delete
    $db = new DatabaseObject($this->dbName, ORG_ADMIN_USER, ORG_ADMIN_PASS, DB_HOST);

    // this is necessary because those losers who wrote this app the first time messed up the db pretty badly
    $db->Where($this->questionSqlColumns[QUESTION_COLUMN_ID].'='.$this->questionId);
    $db->Delete(QUESTION_TABLE);

    $sql = "REPLACE INTO ".QUESTION_TABLE." SELECT * FROM ".QUESTION_ARCHIVE_TABLE
      ." WHERE ".$this->questionSqlColumns[QUESTION_COLUMN_ID].' = '.$this->questionId
      .' AND '.$this->questionSqlColumns[QUESTION_COLUMN_DOMAIN_ID].' = '.$domainId
      .' AND '.$this->questionSqlColumns[QUESTION_COLUMN_VERSION_DATE]." = '".$versionDate."'";
    $rc = $db->Execute($sql);
    if (!$rc) {
      error_log("Question.UnArchiveQuestion(): ".$this->GetErrorMessage());
      return RC_QUERY_FAILED;
    }

    if ($this->DeleteArchivedQuestion($domainId, $versionDate) != RC_OK) {
      error_log('Question.ArchiveQuestion(): Inserted into archive but unable to delete'.$this->GetErrorMessage());
      return RC_QUERY_FAILED;
    }

    return RC_OK;
  }


  // This function removes a question from all queues and current questions
  function DeleteQuestion() {

    $db = NULL;
    $track = NULL;
    $trackId = 0;
    $trackList = array();

    if (!$this->questionId) {
      error_log("Question.DeleteQuestion(): Invalid question ID, did you forget to set it when creating object?");
      return RC_INVALID_ARG;
    }

    $track = new Track($this->orgId);

    // Get a list of tracks with this question in it
    $trackList = $track->GetTrackIdsWithAssignedQuestion($this->questionId);
    if ($trackList < 0) { // error occured
      error_log("Question.DeleteQuestion(): Unable to obtain list of tracks containing this question, aborting...");
      return RC_QUERY_FAILED;
    }

    // Remove this question from all Queues
    foreach ($trackList as $delTrackId) {
      // Skip loading of all track info since it is not necessary
      // The only member this method requires is $trackId
      $track->trackId = $delTrackId;
      $track->UnAssignQuestion($this->questionId);
    }

    // Rather than delete the question John just wants it archived and then removed from question table
    $this->ArchiveQuestion();

    // Reconnect as user with permissions to delete
    $db = new DatabaseObject($this->dbName, ORG_ADMIN_USER, ORG_ADMIN_PASS, DB_HOST);
    $db->Where($this->questionSqlColumns[QUESTION_COLUMN_ID].'='.$this->questionId);
    //    $db->Where($this->questionSqlColumns[QUESTION_COLUMN_VERSION_DATE]."='".$this->versionDate."'");
    //    $db->Where($this->questionSqlColumns[QUESTION_COLUMN_DOMAIN_ID].'='.$this->domainId);
    if (!$db->Delete(QUESTION_TABLE)) {
      error_log("Question.DeleteQuestion(): Unable to remove questionId ".$this->questionId." from question table");
      return RC_QUERY_FAILED;
    }
    
    return RC_OK;
  }


  // This function removes a question from all queues and current questions
  function DeleteArchivedQuestion($domainId=0, $versionDate='') {

    if (!$this->questionId) {
      error_log("Question.DeleteArchivedQuestion(): Invalid question ID, did you forget to set it when creating object?");
      return RC_INVALID_ARG;
    }

    if (!$domainId)
      $domainId = $this->domainId;
    if ($versionDate == '')
      $versionDate = $this->versionDate;

    // Reconnect as user with permissions to delete
    $db = new DatabaseObject($_SESSION['dbName'], ORG_ADMIN_USER, ORG_ADMIN_PASS, DB_HOST);

    $db->Where($this->questionSqlColumns[QUESTION_COLUMN_ID].'='.$this->questionId);
    $db->Where($this->questionSqlColumns[QUESTION_COLUMN_VERSION_DATE]."='".$versionDate."'");
    $db->Where($this->questionSqlColumns[QUESTION_COLUMN_DOMAIN_ID].'='.$domainId);
    if (!$db->Delete(QUESTION_ARCHIVE_TABLE)) {
      error_log("Question.DeleteArchivedQuestion(): Unable to remove questionId ".$this->questionId." from question archive table");
      return RC_QUERY_FAILED;
    }
    
    return RC_OK;
  }


  // If This function receives a new question id for the current object
  // Then it will examine the the current questionId to see if that was
  // to be an insert or an update and then use the new ID if possible.
  function UpdateQuestion($newQuestionId = 0) {

    $this->Where($this->questionSqlColumns[QUESTION_COLUMN_ID].'='.$this->questionId);
    $rc = $this->Update(array($this->questionSqlColumns[QUESTION_COLUMN_PARENT_ID] => $this->parentQuestionId,
			      $this->questionSqlColumns[QUESTION_COLUMN_NOTES] => $this->notes,
			      $this->questionSqlColumns[QUESTION_COLUMN_SUPPORT_INFO] => $this->supportInfo,
			      $this->questionSqlColumns[QUESTION_COLUMN_CAP_CONFIRM_THANKS] => $this->capConfirmThanks,
			      $this->questionSqlColumns[QUESTION_COLUMN_REQUIRE_CORRECT_ANSWER] => $this->requireCorrectAnswer,
			      $this->questionSqlColumns[QUESTION_COLUMN_LANGUAGE_ID] => $this->languageId,
			      $this->questionSqlColumns[QUESTION_COLUMN_DOMAIN_ID] => $this->domainId,
			      $this->questionSqlColumns[QUESTION_COLUMN_CATEGORY_ID] => $this->categoryId,
			      $this->questionSqlColumns[QUESTION_COLUMN_FRAMETYPE_ID] => $this->frameTypeId,
			      $this->questionSqlColumns[QUESTION_COLUMN_VERSION_DATE] => $this->versionDate,
			      $this->questionSqlColumns[QUESTION_COLUMN_IS_MANAGEMENT] => $this->isManagement,
			      $this->questionSqlColumns[QUESTION_COLUMN_AUTHOR] => $this->author,
			      $this->questionSqlColumns[QUESTION_COLUMN_TITLE] => $this->title,
			      $this->questionSqlColumns[QUESTION_COLUMN_QUESTION] => $this->question,
			      $this->questionSqlColumns[QUESTION_COLUMN_MULTIPLE_CHOICE1] => $this->multipleChoice1,
			      $this->questionSqlColumns[QUESTION_COLUMN_MULTIPLE_CHOICE2] => $this->multipleChoice2,
			      $this->questionSqlColumns[QUESTION_COLUMN_MULTIPLE_CHOICE3] => $this->multipleChoice3,
			      $this->questionSqlColumns[QUESTION_COLUMN_MULTIPLE_CHOICE4] => $this->multipleChoice4,
			      $this->questionSqlColumns[QUESTION_COLUMN_MULTIPLE_CHOICE5] => $this->multipleChoice5,
			      $this->questionSqlColumns[QUESTION_COLUMN_FEEDBACK_CHOICE1] => $this->feedbackChoice1,
			      $this->questionSqlColumns[QUESTION_COLUMN_FEEDBACK_CHOICE2] => $this->feedbackChoice2,
			      $this->questionSqlColumns[QUESTION_COLUMN_FEEDBACK_CHOICE3] => $this->feedbackChoice3,
			      $this->questionSqlColumns[QUESTION_COLUMN_FEEDBACK_CHOICE4] => $this->feedbackChoice4,
			      $this->questionSqlColumns[QUESTION_COLUMN_FEEDBACK_CHOICE5] => $this->feedbackChoice5,
			      $this->questionSqlColumns[QUESTION_COLUMN_CORRECT_ANSWER] => $this->correctAnswer,
			      $this->questionSqlColumns[QUESTION_COLUMN_PURPOSE] => $this->purpose,
			      $this->questionSqlColumns[QUESTION_COLUMN_LEARNING_POINTS] => $this->learningPoints,
			      $this->questionSqlColumns[QUESTION_COLUMN_MEDIA_FILE_PATH] => $this->mediaFilePath,
			      $this->questionSqlColumns[QUESTION_COLUMN_MEDIA_DURATION_SECONDS] => $this->mediaDurationSeconds,
			      $this->questionSqlColumns[QUESTION_COLUMN_DISPLAY_CHOICES] => $this->displayChoices,
			      $this->questionSqlColumns[QUESTION_COLUMN_OWNER_USER_ID] => $this->ownerUserId),
			QUESTION_TABLE);
    //$this->questionSqlColumns[QUESTION_COLUMN_CONTENT] => $this->content,
    if ($rc != RC_OK)
      error_log('Question.UpdateQuestion(): Failed updating question data for questionId='.$this->questionId);
     
    return $rc;
  }
  
  
  function UpdateAllQuestionsConfirm($userId) {

    if (!is_numeric($userId) || $userId <= 0) {
      error_log("Question.UpdateAllQuestionsConfirm(userId=$userId): Invalid argument for userId, must be ".
		"numeric and have a value > 0");
      return RC_INVALID_ARG;
    }

    // Force the update to all question
    $this->Where($this->questionSqlcolumns[QUESTION_COLUMN_OWNER_USER_ID].'='.$userId);
    $rc = $this->Update(array($this->questionSqlColumns[QUESTION_COLUMN_CAP_CONFIRM_THANKS] => $this->capConfirmThanks),
			QUESTION_TABLE, TRUE);
    if ($rc != RC_OK)
      error_log('Question.UpdateQuestion(): Failed updating question data for questionId='.$this->questionId);
     
    return $rc;
  }
  
  
  // This will update only the question support info without having to
  // worry about the ids
  function UpdateQuestionSupport() {
    
    $this->Where($this->questionSqlColumns[QUESTION_COLUMN_ID].'='.$this->questionId);
    $rc = $this->Update(array($this->questionSqlColumns[QUESTION_COLUMN_NOTES] => $this->notes,
			      $this->questionSqlColumns[QUESTION_COLUMN_SUPPORT_INFO] => $this->supportInfo,
			      $this->questionSqlColumns[QUESTION_COLUMN_CAP_CONFIRM_THANKS] => $this->capConfirmThanks,
			      $this->questionSqlColumns[QUESTION_COLUMN_REQUIRE_CORRECT_ANSWER] => $this->requireCorrectAnswer),
			QUESTION_TABLE);
    if ($rc != RC_OK)
      error_log('Question.UpdateQuestionSupport(): Failed updating support info for questionId='.$this->questionId);
     
    return $rc;
  }


  function ConvertSmartQuotes($string)   { 
    $search = array(chr(145), chr(146), chr(130), chr(180), chr(96), chr(132), chr(147), chr(148), chr(150), chr(151), chr(160)); 
    $replace = array("'", "'", "'", "'", "'", '"', '"', '"', '-', '-', ' '); 
 
    return str_replace($search, $replace, $string); 
  } 


  function PrepForDisplay() {
    /*
    $this->question = preg_replace("/\/*\/g", '\"', $this->question);
    $this->multipleChoice1 = preg_replace("/\/*\/g", '\"', $this->multipleChoice1);
    $this->multipleChoice2 = preg_replace("/\/*\/g", '\"', $this->multipleChoice2);
    $this->multipleChoice3 = preg_replace("/\/*\/g", '\"', $this->multipleChoice3);
    $this->multipleChoice4 = preg_replace("/\/*\/g", '\"', $this->multipleChoice4);
    $this->multipleChoice5 = preg_replace("/\/*\/g", '\"', $this->multipleChoice5);
    $this->feedbackChoice1 = preg_replace("/\/*\/g", '\"', $this->feedbackChoice1);
    $this->feedbackChoice2 = preg_replace("/\/*\/g", '\"', $this->feedbackChoice2);
    $this->feedbackChoice3 = preg_replace("/\/*\/g", '\"', $this->feedbackChoice3);
    $this->feedbackChoice4 = preg_replace("/\/*\/g", '\"', $this->feedbackChoice4);
    $this->feedbackChoice5 = preg_replace("/\/*\/g", '\"', $this->feedbackChoice5);
    $this->purpose = preg_replace("/\/*\/g", '\"', $this->purpose);
    $this->learningPoints = preg_replace("/\/*\/g", '\"', $this->learningPoints);
    $this->mediaFilePath = preg_replace("/\/*\/g", '\"', $this->mediaFilePath);
    $this->capConfirmThanks = preg_replace("/\/*\/g", '\"', $this->capConfirmThanks);
    $this->supportInfo = preg_replace("/\/*\/g", '\"', $this->supportInfo);
    $this->notes = preg_replace("/\/*\/g", '\"', $this->notes);
    $this->title = preg_replace("/\/*\/g", '\"', $this->title);
    $this->author = preg_replace("/\/*\/g", '\"', $this->author);
    */
    $this->question = str_replace('"', '\"', $this->question);
    $this->multipleChoice1 = str_replace('"', '\"', $this->multipleChoice1);
    $this->multipleChoice2 = str_replace('"', '\"', $this->multipleChoice2);
    $this->multipleChoice3 = str_replace('"', '\"', $this->multipleChoice3);
    $this->multipleChoice4 = str_replace('"', '\"', $this->multipleChoice4);
    $this->multipleChoice5 = str_replace('"', '\"', $this->multipleChoice5);
    $this->feedbackChoice1 = str_replace('"', '\"', $this->feedbackChoice1);
    $this->feedbackChoice2 = str_replace('"', '\"', $this->feedbackChoice2);
    $this->feedbackChoice3 = str_replace('"', '\"', $this->feedbackChoice3);
    $this->feedbackChoice4 = str_replace('"', '\"', $this->feedbackChoice4);
    $this->feedbackChoice5 = str_replace('"', '\"', $this->feedbackChoice5);
    $this->purpose = str_replace('"', '\"', $this->purpose);
    $this->learningPoints = str_replace('"', '\"', $this->learningPoints);
    $this->mediaFilePath = str_replace('"', '\"', $this->mediaFilePath);
    $this->capConfirmThanks = str_replace('"', '\"', $this->capConfirmThanks);
    $this->supportInfo = str_replace('"', '\"', $this->supportInfo);
    $this->notes = str_replace('"', '\"', $this->notes);
    $this->title = str_replace('"', '\"', $this->title);
    $this->author = str_replace('"', '\"', $this->author);
    
  }


  function ImportFile($fileName, &$msg, $userId, $linkToDir = '', $linkFromDir = '') {

    // Local vars
    $csv = NULL;
    $todaysDate = date("Y-m-d");
    $queueSqlColumns = unserialize(QUEUE_SQL_COLUMNS);
    $columnHeaders = array();
    $questions = array();
    $questionData = array();
    $lineNumber = 0;
    $lineNumberStart = 0;
    $fh = NULL;
    $i = 0;
    $msg = '';
    $tmpMsg = '';
    //$questionContent = '';
    $learningPoints = '';
    $parentReferences = array();
    $parentQuestions = array_keys($this->GetParentQuestions());
    $languages = $this->GetLanguages();
    $domains = $this->GetDomains();
    $domainAbbreviations = Organization::GetDomainAbbreviations($this->orgId);
    $categories = $this->GetCategories();
    $frameTypes = $this->GetFrameTypes();
    error_log('FRAME TYPES: '.print_r($frameTypes, TRUE));
    $choices = unserialize(MULTIPLE_CHOICE_OPTIONS);

    
    //    if (DEBUG & DEBUG_CLASS_FUNCTIONS) {
    error_log("Question.ImportFile executed with args (fileName=$fileName, msg=$msg, ".
	      "userId=$userId, linkTodir=$linkToDir, linkFromDir=$linkFromDir)");
    
    //    }
    
    if (!is_numeric($userId) || $userId <= 0) {
      error_log("Question.ImportFile(fileName=$fileName,msg=$msg,userId=$userId ".
		"invalid userid passed to function, userid must be numeric and > 0");
      return RC_INVALID_ARG;
    }

    // Verify file exists and is readable
    if (!is_readable($fileName)) {
      error_log("Question.ImportFile(Unable to read uploaded file, please have sysadmin admin check this)");
      return RC_OPEN_FILE_FAILED;
    }

    // Open the CSV file
    //$fh = fopen($fileName, 'r');
    $csv = new CustomCsv($fileName, 'r');
    if ($csv === FALSE) {
      error_log("Question.ImportFile(fileName=$fileName,msg=$msg,userId=$userId): Failed to open file");
      return RC_OPEN_FILE_FAILED;
    }

    // Read everything from CSV
    $questions = $csv->ReadCsv(CSV_EXPECTED_COLUMN_COUNT);
    if ($questions === FALSE) {
      error_log("Question.ImportFile(fileName=$fileName,msg=$msg,userId=$userId): failed to read csv header info");
      return RC_CSV_READ_FAILED;
    }

    // Empty or nonexistent file on client was uploaded
    if (empty($questions)) {
      error_log("Question.ImportFile(fileName=$fileName,msg=$msg,userId=$userId): empty or nonexistant file was uploaded by org(".
		$this->orgId.")");
      $msg .= "The file uploaded either does not exist or contains no data<BR>\n";
      return RC_OPEN_FILE_FAILED;
    }

    // Toss the columns headers
    if ($questions[0][0] == 'Question_Number' || !is_numeric($questions[0][0])) {
      $columnHeaders = array_shift($questions);
      $lineNumberStart++;
      $lineNumber++;
    } else
      $columnHeaders = array('Question_Number', 'Parent_Question_Number', 'Language', 'Domain', 'Version_Date',
			     'Category', 'Frametype', 'Is_Management', 'Author', 'Notes', 'Title', 'Support_Info',
			     'Question', 'Answer_A', 'Answer_B', 'Answer_C', 'Answer_D', 'Answer_E', 'Correct_Answer',
			     'Purpose', 'Feedback_A', 'Feedback_B', 'Feedback_C', 'Feedback_D', 'Feedback_E',
			     'Learning_Point1', 'Learning_Point2', 'Learning_Point3', 'Learning_Point4', 'Learning_Point5',
			     'Learning_Point6', 'Learning_Point7', 'Learning_Point8', 'Learning_Point9', 'Learning_Point10',
			     'Media', 'Media_Duration', 'Cap_Confirm_Thanks');

    // Reconnect as the auth user
    $this->Close();
    if (!$this->Open($this->dbName, ORG_ADMIN_USER, ORG_ADMIN_PASS, DB_HOST)) {
      error_log("Question.ImportFile($fileName, $msg): Failed to connect as the org admin user for question import");
      $msg .= "Database connection for authorized user failed";
      return RC_ORG_DB_CONNECT_FAILED;
    }

    // Initial data validation routine.  This must be performed prior to any actual replacement.
    // Additional validation checks are performed later but because of the parent child relationships
    // all question numbers and parent references must be known prior to any actual importing since
    // a child could reference a parent question that appears later in the import and is also not yet
    // in the database.
    foreach ($questions as $questionData) {
      $lineNumber++;
      if (!empty($questionData[0]) && !empty($questionData[1]) && $questionData[1] == $questionData[0]) {
	error_log("Question.ImportFile($fileName, $msg) line($lineNumber): Question can not be a child of itself: ".
		  "'$questionData[0]' QUESTION_DATA: ".print_r($questionData, TRUE));
	$tmpMsg .= "Questions cannot have themselves as parents, question and parent numbers must be different in $columnHeaders[1] (2nd column) on line $lineNumber<BR>\n";
      }
      if (!empty($questionData[1]))
	array_push($parentReferences, $questionData[1]);
      elseif (!in_array((int)$questionData[0], $parentQuestions))
	array_push($parentQuestions, $questionData[0]);
    }
    if (!empty($tmpMsg)) {
      $msg .= $tmpMsg;
      return RC_INVALID_DATA_FORMAT;
    }
    $lineNumber = $lineNumberStart;

    // Final checks of all referenced parents against a consolidated list of all parent questions
    // in the database as well as those that will be imported (assuming all future validations pass)
    foreach($parentReferences as $parentReference) {
      if (!in_array($parentReference, $parentQuestions)) {
	error_log("Question.ImportFile($fileName, $msg): Non-existant parent question $parentReference referenced in import");
	$tmpMsg .= "Parent question $parentReference does not exist in the system, please double check parent question number.<BR>\n";
      }
    }
    if (!empty($tmpMsg)) {
      $msg .= $tmpMsg;
      return RC_PARENT_QUESTION_NOEXIST;
    }

    // Read question data from csv until empty
    foreach ($questions as &$questionData) {

      $lineNumber++;

      $fieldConcat = '';
      for($j = 0; $j < 10; $j++)
	$fieldConcat .= $questionData[$j];
      if (empty($fieldConcat)) {
	error_log("Question.ImportFile($fileName): WARN: Ignoring line at $lineNumber because the first 10 values were all empty");
	continue;
      }

      //Ensure question number is above 100000 if not a superuser
      //user is a superuser if userid=1
      if ($userId != 1 && (int)$questionData[0] < 100000 && (int)$questionData[0] > 0) {
	error_log("Question.ImportFile($fileName, $msg, $userId): Non-super user attempted import < 100000");
	$msg .= "Only the super user may import question numbers less than 100000<BR>\n"
	  ."Please renumber your questions to start at or above 100000.  Note that<BR>\n"
	  ."any existing questions in the system with the same question number<BR>\n"
	  ."being imported will be overwritten and the original question will be lost.";
	return RC_CSV_INVALID_QUESTION_NUMBER;
      }

      $questionData[10] = $this->ConvertSmartQuotes($questionData[10]);
      for ($i = 9; $i <= 37; $i++) {
	$questionData[$i] = trim($this->ConvertSmartQuotes($questionData[$i]));
	//$questionData[$i] = str_replace(array('"1"', '"2"', '"3"', '"4"', '"5"'),
	//				array('"A"', '"B"', '"C"', '"D"', '"E"'),
	//				$questionData[$i]);
	//$questionData[$i] = str_replace('/"', '\"', $questionData[$i]);
	//$questionData[$i] = preg_replace("/(\\\\)+\"/", "\\\"", $questionData[$i]);
	// Escape quote marks for javascript;
	//$questionData[$i] = str_replace('"', '\"', $questionData[$i]);
      }

      // Make sure question number and parent are both numeric
      if (!is_numeric($questionData[0])) {
	error_log("Question.ImportFile($fileName, $msg) line($lineNumber): Question ID must be a numeric value: ".
		  "'$questionData[0]' QUESTION_DATA: ".print_r($questionData, TRUE));
	$msg .= "Question numbers must be numeric in column 1($columnHeaders[0]) on line $lineNumber, value was: $questionData[0]<BR>\n";
	return RC_INVALID_DATA_FORMAT;
      }

      if (!empty($questionData[1]) && !is_numeric($questionData[1])) {
	error_log("Question.ImportFile($fileName, $msg) line($lineNumber): Parent Question ID must be a numeric value: ".
		  "'$questionData[0]' QUESTION_DATA: ".print_r($questionData, TRUE));
	$msg .= "Parent question numbers must be numeric in column2($columnHeaders[1]) on line $lineNumber, value was: $questionData[0]<BR>\n";
	return RC_INVALID_DATA_FORMAT;
      }
      
      // If language was left blank then auto assign english
      if (empty($questionData[2]))
	$questionData[2] = 1;
      // Convert language name to an ID if it isnt already and validate
      if (!is_numeric($questionData[2])) {
	$tmpArr = array_change_key_case(array_flip($languages));
	if (array_key_exists(strtolower($questionData[2]), $tmpArr))
	  $questionData[2] = $tmpArr[strtolower($questionData[2])];
	else {
	  error_log("Question.ImportFile(filename=$filename, msg_ref, userId=$userId), line $lineNumber: ".
		    "unrecognized text language ($questionData[2]) specified in language column 2 of import.");
	  $msg .= "Language specified, $questionData[2], in column 3($columnHeaders[2]) on line $lineNumber is not supported or unrecognized.<BR>\n";
	}
      } else if (!in_array((int)$questionData[2], array_keys($languages))) {
	error_log("Question.ImportFile(filename=$filename, msg_ref, userId=$userId), line $lineNumber: ".
		  "Invalid or unrecognized data for column 3($questionData[2]) specified.");
	$msg .= "Language specified in column 3($columnHeaders[2]) on line $lineNumber is not supported or unrecognized.<BR>\n";
      }

      // Use default domain of Federal or 1 when it was not specified
      if (empty($questionData[3]))
	$questionData[3] = 1;
      // Convert domain to an ID if it isnt already and validate
      if (!is_numeric($questionData[3])) {
	$tmpArr = array_change_key_case(array_flip($domains));
	$tmpArr1 = array_change_key_case(array_flip($domainAbbreviations));
	if (array_key_exists(strtolower($questionData[3]), $tmpArr))
	  $questionData[3] = $tmpArr[strtoLower($questionData[3])];
	else if (array_key_exists(strtolower($questionData[3]), $tmpArr1)) {
	  $questionData[3] = $tmpArr1[strtolower($questionData[3])];
	} else {
	  error_log("Question.ImportFile(filename=$filename, msg_ref, userId=$userId), line $lineNumber: ".
		    "unrecognized text language ($questionData[3]) specified in language column 4 of import.");
	  $msg .= "Language specified, $questionData[3], in column 4($columnHeaders[3]) on line $lineNumber is not supported or unrecognized.<BR>\n";
	}
      } else if (!in_array((int)$questionData[3], array_keys($domains))) {
	error_log("Question.ImportFile(filename=$filename, msg_ref, userId=$userId), line $lineNumber: ".
		  "Invalid value $questionData[3] in column3($columnHeaders[2]) on line $lineNumber is not supported or unrecognized.");
	$msg .= "Domain specified in column 4($columnHeaders[3]) on line $lineNumber is not supported or unrecognized.<BR>\n";
      }

      // If version date was left blank then auto assign the version date
      if (empty($questionData[4]))
	$questionData[4] = date("Y-m-d");
      if (preg_match("/\d{1,2}\/\d{1,2}\/\d{4}/", $questionData[4])) {
	//error_log("Question.ImportFile(fileName=$fileName,msgref): Date Format mm/dd/yyyy detected, converting to YYYY-MM-DD");
	$dateParts = explode('/', $questionData[4]);
	$questionData[4] = sprintf("%d-%02d-%02d", $dateParts[2], $dateParts[0], $dateParts[1]);
      }
      if (!preg_match("/\d{4}-\d{2}-\d{2}/", $questionData[4])) {
	error_log("Question.ImportFile(fileName=$fileName, msgref): Invalid version date specified ($questionData[4])");
	$msg .= "Invalid date specified in column 5($columnHeaders[4]), format should be YYYY-MM-DD or MM/DD/YYYY in record $lineNumber<BR>\n";
	return RC_INVALID_DATA_FORMAT;
      }

      // Default a value for the category
      if (empty($questionData[5]))
	$questionData[5] = 10000;

      // Frametype is a required field
      if (empty($questionData[6])) {
	error_log("Question.ImportFile(fileName=$fileName, msgref): The Frametype is a required field");
	$msg .= "The value in column 7($columnHeaders[6]) is a required field and must be specified on line $lineNumber<BR>\n";
	//return RC_INVALID_DATA_FORMAT;
      } else if (!is_numeric($questionData[6])) {
	$tmpArr = array_change_key_case(array_flip($frameTypes));
	if (array_key_exists(strtolower($questionData[6]), $tmpArr))
	  $questionData[6] = $tmpArr[strtolower($questionData[6])];
	else {
	  error_log("Question.ImportFile(filename=$filename, msg_ref, userId=$userId), line $lineNumber: ".
		    "unrecognized frame type ($questionData[6]) specified in frame type column 7 of import.");
	  $msg .= "The frame type name specified, $questionData[6], in column 7($columnHeaders[6]) on line $lineNumber is not supported or unrecognized.<BR>\n";
	}
      } else if (!in_array((int)$questionData[6], array_keys($frameTypes))) {
	error_log("Question.ImportFile(filename=$fileName, msg_ref, userId=$userId), line $lineNumber: ".
		  "unrecognized frame type ($questionData[6]) specified in frame type column 7 of import.");
	$msg .= "The frame type ID specified, $questionData[6], in column 7($columnHeaders[6]) on line $lineNumber is not supported or unrecognized.<BR>\n";
      } else if ($questionData[6] >=2 && empty($questionData[1]) && empty($questionData[35])) {
	error_log("Question.ImportFile(filename=$fileName, msg_ref, userId=$userId), line $lineNumber: ".
		  "FrameType specified requires the use of a file but none was specified.");
	$msg .= "The frame type specified, $questionData[6], in column 7($columnHeaders[6]) on line $lineNumber requires a media file in column 36.<BR>\n";
      }

      // Default and data validation for Is_Management
      if (empty($questionData[7]))
	$questionData[7] = 0;
      if (!is_numeric($questionData[7])) {
	if (strtolower(trim($questionData[7])) == 'yes')
	  $questionData[7] = 1;
	else if (strtolower(trim($questionData[7])) == 'no')
	  $questionData[7] = 0;
	else {
	  error_log("Question.ImportFile(fileName=$fileName, msgref): Invalid value for column 8($columnHeaders[7])");
	  $msg .= "The value in column 8($columnHeaders[7]) is invalid, please specify, yes/no, or 1/0<BR>\n";
	}
      }
      if (!is_numeric($questionData[7]) || $questionData[7] < 0 || $questionData[7] > 1) {
	error_log("Question.ImportFile(fileName=$fileName, msgref): Invalid value for column 8($columnHeaders[7])");
	$msg .= "The value in column 8($columnHeaders[7]) is invalid, please specify, yes/no, or 1/0<BR>\n";
      }

      // field 8 (0 based) is the author and anything is allowed there as well as empty

      // field 9 (0 based) are the notes and anything is allowed there as well as empty

      // Question title is required
      if (empty($questionData[10])) {
	error_log("Question.ImportFile(fileName=$fileName, msgref): Question title is a required field");
	$msg .= "Question title is a required field and must be specified on line $lineNumber<BR>\n";
      }
     
      // field 11 (0 based) is the support_info and anything is allowed there as well as empty

      // This is the question content, field 12 (0 based) is required but only when
      // the frame types are not multiple choice flash (slide show, value 3) and are
      // not any of the AB1825 question types (also slide show based but use parent
      // and child question numbers
      if (empty($questionData[12]) && $questionData[6] != 3 && 
	  $questionData[6] != 5 && $questionData[6] != 6) {
	error_log("Question.ImportFile(fileName=$fileName, msgref): The question field on ".
		  "line $lineNumber is required whenever frametype is not slide show (3)");
	$msg .= "The field $columnHeaders[12] is a required field and must be specified on line $lineNumber<BR>\n";
      }

      // This is the first answer which is a required field
      if (empty($questionData[13])) {
	error_log("Question.ImportFile(fileName=$fileName, msg=$msg): The first answer is required");
	$msg .= "The field $columnHeaders[13] is a required field and must be specified on line $lineNumber<BR>\n";
      }

      // fields 14 through 17 (0 based) are answers B through E which are not required

      // The correct answer field 18 (0 based)
      if (empty($questionData[18])) {
	error_log("Question.ImportFile(fileName=$fileName, msgref) line($lineNumber): correct answer field 18 on line $lineNumber required");
	$msg .= "The field $columnHeaders[18] is required but was empty on line $lineNumber";
      } else if (!is_numeric($questionData[18]) && strlen($questionData[18]) == 1) {
	$tmpArr = array_change_key_case(array_flip($choices));
	if (array_key_exists(strtolower($questionData[18]), $tmpArr))
	  $questionData[18] = $tmpArr[strtolower($questionData[18])];
	else {
	  error_log("Question.ImportFile(fileName=$fileName, msgref) line($lineNumber): failed to read csv data, expected numeric value: ".
		    "'$questionData[18]' QUESTION_DATA: ".print_r($questionData, TRUE));
	  $msg .= "Invalid non-numeric value for $columnHeaders[18] (19th column) on line $lineNumber, value was: $questionData[18]<BR>\n";
	  //return RC_INVALID_DATA_FORMAT;
	}
      } else {
	error_log("Question.ImportFile(fileName=$fileName, msgref) line($lineNumber): failed to read csv data, expected numeric value: ".
		  "'$questionData[18]' QUESTION_DATA: ".print_r($questionData, TRUE));
	$msg .= "Invalid non-numeric value for $columnHeaders[18] (19th column) on line $lineNumber, value was: $questionData[18]<BR>\n";
	//return RC_INVALID_DATA_FORMAT;
      }

      // For consistencies sake make sure that if media duration was specified media file was also specified
      if (!empty($questionData[36])) {
	if (empty($questionData[35])) {
	  error_log("Question.ImportFile(fileName=$fileName, msgref): Media duration was specified but no media file was specified on line $lineNumber<BR>\n");
	  $msg .= "A media file duration was specified on line $lineNumber but there was no media file specified<BR>\n";
	  //return RC_INVALID_DATA_FORMAT;
	}

	if (preg_match("/(\d+):(\d{1,2})/", $questionData[36], $matches)) {
	  $questionData[36] = ((int)$matches[1] * 60) + (int)$matches[2];
	} elseif (!preg_match("/^\d+$/", trim($questionData[36]))) {
	  error_log("Question.ImportFile(fileName=$fileName, msgref): Media duration must be numeric or formatted by Minutes:Seconds<BR>\n");
	  $msg .= "Invalid media duration format on line $lineNumber. Acceptable formats include number of seconds (nnn) or Minutes:Seconds<BR>\n";
	  //return RC_INVALID_DATA_FORMAT;
	}
      }

      // Force specification of media file and media file duration in seconds for AB1825 parent questions only
      if (empty($questionData[1]) &&
	  ((int)$questionData[6] == 5 || (int)$questionData[6] == 6) &&
	  (empty($questionData[36]) || empty($questionData[35]))) {
	error_log("Question.ImportFile(fileName=$fileName, msgref): Missing media or Media duration.  These fields required for AB1825 parent questions.");
	$msg .= "Media and Media Duration are required when importing AB1825 parent questions.  One of these fields was omitted at line $lineNumber<BR>\n";
	//return RC_INVALID_DATA_FORMAT;
      }
	  
      // We need to make sure the media file exists.
      // If we are doing an import across all organizations and there is a single source
      // for the media file then $linkToDir will not be empty.  If $linkToDir is empty
      // then $linkFromDir will be the location of the media file for this organization.
      if (!empty($questionData[35])) {
	$rc = LinkToFile($linkFromDir, $linkToDir, $questionData[35]);
	if ($rc != RC_OK) {
	  error_log("Question.ImportFile(fileName=$fileName, msgref): ".
		    "LinkToFile failed with rc=$rc in org=$this->orgId, please see previous log messages for reason");
	  $msg .= "There was a problem with the media file '$questionData[35]' specified at line $lineNumber.  It may not exist or permissions may be incorrect.<BR>\n";
	  return $rc;
	}
      }

      // Assemble learning points
      $learningPoints = '';
      if (!empty($questionData[25])) {
	$learningPoints = '<ul>';
	for ($i = 25; $i <= 34; $i++) {
	  if (!empty($questionData[$i]))
	    $learningPoints .= '<li>'.trim($questionData[$i]).'</li>';
	}
	$learningPoints .= '</ul>';
	$questionData[25] = $learningPoints;
      }

      // Retain customized policy and timing information from pre-existing import
      if ($questionData[0] == 50900) {
	//			    $this->questionSqlColumns[QUESTION_COLUMN_MEDIA_DURATION_SECONDS]),
	$this->Select($this->questionSqlColumns[QUESTION_COLUMN_FEEDBACK_CHOICE1],
		      QUESTION_TABLE);
	$this->Where($this->questionSqlColumns[QUESTION_COLUMN_ID].'='.$questionData[0]);
	$val = $this->Query(SINGLE_VALUE, VALUES_FORMAT);
	if ($val) {
	  $questionData[20] = $val;
	} else
	  error_log("Question::ImportFile(): No existing question data found so not saving company policy data (feedback choice1)");
      }
      
      //      $questionData = array();
    }

    // If there were any errors then bail
    if (!empty($msg))
      return false;


    // Read question data from csv until empty
    foreach ($questions as $questionData) {

      $this->SetQuestionId($questionData[0]);
      // If the version dates are different then archive the question.
      // If not then overwrite the old question with the new one.
      if (array_shift(explode(' ', $this->versionDate)) != $questionData[4])
	$this->ArchiveQuestion(FALSE, $questionData[0]);

      $rc = $this->Replace(array($this->questionSqlColumns[QUESTION_COLUMN_ID] => $questionData[0],
				 $this->questionSqlColumns[QUESTION_COLUMN_PARENT_ID] => $questionData[1],
				 $this->questionSqlColumns[QUESTION_COLUMN_LANGUAGE_ID] => $questionData[2],
				 $this->questionSqlColumns[QUESTION_COLUMN_DOMAIN_ID] => $questionData[3],
				 $this->questionSqlColumns[QUESTION_COLUMN_CATEGORY_ID] => $questionData[5],
				 $this->questionSqlColumns[QUESTION_COLUMN_FRAMETYPE_ID] => $questionData[6],
				 $this->questionSqlColumns[QUESTION_COLUMN_VERSION_DATE] => $questionData[4],
				 $this->questionSqlColumns[QUESTION_COLUMN_IS_MANAGEMENT] => $questionData[7],
				 $this->questionSqlColumns[QUESTION_COLUMN_AUTHOR] => $questionData[8],
				 $this->questionSqlColumns[QUESTION_COLUMN_NOTES] => $questionData[9],
				 $this->questionSqlColumns[QUESTION_COLUMN_TITLE] => $questionData[10],
				 $this->questionSqlColumns[QUESTION_COLUMN_SUPPORT_INFO] => $questionData[11],
				 $this->questionSqlColumns[QUESTION_COLUMN_QUESTION] => $questionData[12],
				 $this->questionSqlColumns[QUESTION_COLUMN_MULTIPLE_CHOICE1] => $questionData[13],
				 $this->questionSqlColumns[QUESTION_COLUMN_MULTIPLE_CHOICE2] => $questionData[14],
				 $this->questionSqlColumns[QUESTION_COLUMN_MULTIPLE_CHOICE3] => $questionData[15],
				 $this->questionSqlColumns[QUESTION_COLUMN_MULTIPLE_CHOICE4] => $questionData[16],
				 $this->questionSqlColumns[QUESTION_COLUMN_MULTIPLE_CHOICE5] => $questionData[17],
				 $this->questionSqlColumns[QUESTION_COLUMN_CORRECT_ANSWER] => $questionData[18],
				 $this->questionSqlColumns[QUESTION_COLUMN_PURPOSE] => $questionData[19],
				 $this->questionSqlColumns[QUESTION_COLUMN_FEEDBACK_CHOICE1] => $questionData[20],
				 $this->questionSqlColumns[QUESTION_COLUMN_FEEDBACK_CHOICE2] => $questionData[21],
				 $this->questionSqlColumns[QUESTION_COLUMN_FEEDBACK_CHOICE3] => $questionData[22],
				 $this->questionSqlColumns[QUESTION_COLUMN_FEEDBACK_CHOICE4] => $questionData[23],
				 $this->questionSqlColumns[QUESTION_COLUMN_FEEDBACK_CHOICE5] => $questionData[24],
				 $this->questionSqlColumns[QUESTION_COLUMN_LEARNING_POINTS] => $questionData[25],
				 $this->questionSqlColumns[QUESTION_COLUMN_MEDIA_FILE_PATH] => escapeshellcmd($questionData[35]),
				 $this->questionSqlColumns[QUESTION_COLUMN_MEDIA_DURATION_SECONDS] => $questionData[36],
				 $this->questionSqlColumns[QUESTION_COLUMN_CAP_CONFIRM_THANKS] => $questionData[37],
				 $this->questionSqlColumns[QUESTION_COLUMN_OWNER_USER_ID] => $userId),
			   QUESTION_TABLE);
      if ($rc === FALSE) {
	error_log("Question.ImportFile(fileName=$fileName, msgref): Failed to replace question number($questionData[0]), skipping...");
	$msg .= "Query failed while updating question on record $lineNumber<BR>\n";
      }

    }

    // Reopen with standard authorized user
    $this->Close();
    if (!$this->Open($this->dbName)) {
      error_log("Question.ImportFile($fileName, $msg): Failed to reconnect as the standard auth user for question import");
      $msg .= "Database connection for authorized user failed<BR>";
      return RC_ORG_DB_CONNECT_FAILED;
    }

    $msg .= "Questions imported successfully.<BR>\n";
    return RC_OK;
  }


  function GetQuestionSetSize() {
    if ($this->parentQuestionId) {
      $this->Select('COUNT('.$this->questionSqlColumns[QUESTION_COLUMN_ID].')', QUESTION_TABLE);
      $this->Where($this->questionSqlColumns[QUESTION_COLUMN_PARENT_ID].'='.$this->parentQuestionId);
      $this->Where($this->questionSqlColumns[QUESTION_COLUMN_ID].'='.$this->parentQuestionId, ' OR ');
    } else {
      $this->Select('COUNT('.$this->questionSqlColumns[QUESTION_COLUMN_ID].')', QUESTION_TABLE);
      $this->Where($this->questionSqlColumns[QUESTION_COLUMN_PARENT_ID].'='.$this->questionId);
      $this->Where($this->questionSqlColumns[QUESTION_COLUMN_ID].'='.$this->questionId, ' OR ');
    }
    $cnt = $this->Query(SINGLE_VALUE, VALUES_FORMAT);
    if ($cnt === FALSE) {
      error_log('*** ERROR Question::GetQuestionSetSize(): Failed to execute query on question('.$this->questionId.')');
      return RC_QUERY_FAILED;
    }
    return $cnt;
  }


  function GetNextChildQuestionId() {

    $this->Select($this->questionSqlColumns[QUESTION_COLUMN_ID], QUESTION_TABLE);
    if ($this->parentQuestionId)
      $this->Where($this->questionSqlColumns[QUESTION_COLUMN_PARENT_ID].'='.$this->parentQuestionId);
    else
      $this->Where($this->questionSqlColumns[QUESTION_COLUMN_PARENT_ID].'='.$this->questionId);
    $this->Where($this->questionSqlColumns[QUESTION_COLUMN_ID].'>'.$this->questionId);
    $res = $this->Query(SINGLE_VALUE, VALUES_FORMAT);
    if ($res === FALSE) {
      error_log('Question::GetNextChildQuestionId(): failed to execute for question number('.$this->questionId.')');
      return RC_QUERY_FAILED;
    }

    return $res;
  }

  
  function AnswerNumberToLetter($num = 0) {

    $letters = array('-','A', 'B', 'C', 'D', 'E', 'F', 'G', 'H');
    
    if ($num)
      return $letters[(int)$num];
    else
      return $letters[(int)$this->correctAnswer];
  }


  function AnswerLetterToNumber($letter = '-') {

    $numbers = array('-' => 0, 'A' => 1, 'B' => 2, 'C' => 3, 'D' => 4, 'E' => 5, 'F' => 6, 'G' => 7, 'H' => 8);    
    return $numbers[$letter];
  }

}
