<?php
/////////////////////////////////////////////////////////////////////////////
//	organization_list.php
//		Page used for administration of organizations stored on the current server
//


// Include libs which starts session and creates smarty objects
require_once ('../includes/common.php');
require_once ('../includes/User.php');

// Ensure user has logged in, otherwise exit now
if (!isset($_SESSION['userId'])) {
  header("Location: /desktop/login/expired.php");
  exit(0);
}

// Ensure user is superuser or an org admin
if (!(isset($_SESSION['superUser']) && $_SESSION['superUser']) &&
    !(isset($_SESSION['role']) && $_SESSION['role'] != ROLE_END_USER &&
      $_SESSION['role'] != ROLE_CLASS_CONTACT_SUPERVISOR)) {
  header("Location: /desktop/login/perm_denied.php");
  exit(0);
}

// Specify this as the current tab
$_SESSION['currentTab'] = TAB_USER;
$_SESSION['currentSubTab'] = SUBTAB_USER_LIST;

if (isset($_REQUEST['searchBy'])) {
  $searchBy = $_REQUEST['searchBy'];
} else {
  $searchBy = USER_LIST_COLUMN_LAST_NAME;
}

if (isset($_REQUEST['searchFor'])) {
  $searchFor = $_REQUEST['searchFor'];
  $_SESSION['userPageNumber'] = 1;
} else {
  $searchFor = '';
}

// Delete all users if that was selected
if (isset($_SESSION['superUser']) && $_SESSION['superUser'] &&
    isset($_REQUEST['deleteAll']) && $_REQUEST['deleteAll']) {
  User::DeleteAll($_SESSION['orgId']);
}


// create new employee list
$errMsg = '';
$rc = 0;
$firstRow = 0;
$lastRow = 0;
$numRows = 0;
$numPages = 0;
$userIds = array();
$users = array();
$userList = new User($_SESSION['orgId']);


// Delete a user if previously selected in the interface
if (isset($_REQUEST['userId']) && $_REQUEST['userId'] &&
    is_numeric($_REQUEST['userId']) &&
    isset($_REQUEST['delete']) && $_REQUEST['delete']) {
  $userList->DeleteUser($_REQUEST['userId']);
}

// Now that user has been removed, get the new list for the interface
$rc = $userList->GetUserList($_SESSION['userShowColumns'], $searchBy, $searchFor,
			     $_SESSION['userSortColumn'], $_SESSION['userSortDescending'],
			     $_SESSION['userPageNumber'], $_SESSION['rowsPerPage'], $_SESSION['userId']);


if ($rc === FALSE) {
  echo "Failed to get employee list\n";
  exit(1);
}

if (DEBUG & DEBUG_FORM) {
  $smarty->assign('GetUserList', $rc);
}


if (!empty($rc)) {
  list($userIds, $users) = $rc;
  $userList->GetRowCounts($firstRow, $lastRow, $numRows);
  $numPages = $userList->GetPageCount($_SESSION['rowsPerPage']);
}

// Create link to be shown at top of page
$topLinkList = array(new TplCaption('Create new user'));
$topLinkList[0]->href='user_edit.php';
if (isset($_SESSION['superUser']) && $_SESSION['superUser']) {
  array_push($topLinkList, new TplCaption('Delete all users'));
  $topLinkList[1]->href='user_list.php';
  $topLinkList[1]->params='deleteAll=1';
}


#$createLink = new TplCaption('Create new user');
#$createLink->href='user_edit.php';



// Assign links to appear at top of page
$smarty->assign('topLinkList', $topLinkList);

// This alone is enough information to identify a search column
$smarty->assign('columnLabels', $userList->GetColumnLabels());
$smarty->assign('showColumns', $_SESSION['userShowColumns']);

// Keep the selected search by field
$smarty->assign('searchBy', $searchBy);

// Set the one to be selected
$smarty->assign('sortColumn', $_SESSION['userSortColumn']);
$smarty->assign('sortDescending', $_SESSION['userSortDescending']);


$smarty->assign('firstRow', $firstRow);
$smarty->assign('lastRow', $lastRow);
$smarty->assign('numRows', $numRows);
$smarty->assign('pageNumber', $_SESSION['userPageNumber']);
$smarty->assign('numPages', $numPages);
$smarty->assign_by_ref('recordList', $users);
$smarty->assign_by_ref('recordIds', $userIds);

if (DEBUG) {
  $smarty->assign('ErrMsg', $userList->GetErrorMessage());
}

$userList->Close();

$smarty->assign('currentTab', $_SESSION['currentTab']);
$smarty->assign('currentSubTab', $_SESSION['currentSubTab']);
$smarty->assign('toggleParamName', 'toggleUserColumn');
$smarty->assign('sortParamName', 'userSortColumn');
#$smarty->assign('createLinkLabel', 'user');
#$smarty->assign('createLink', 'user_edit.php');
$smarty->assign('editLink', 'user_edit.php');
$smarty->assign('deleteLink', 'user_delete.php');
$smarty->assign('idParamName', 'userId');
$smarty->assign('pageNumberParamName', 'userPageNumber');
$smarty->assign('orderParamName', 'userSortDescending');
$smarty->assign('showSelectedColumn', FALSE);
$smarty->assign('rowsPerPage', $_SESSION['rowsPerPage']);
$smarty->assign('editLinkLabel', 'Edit');
$smarty->assign('deleteLinkLabel', 'Delete');
$smarty->assign('localHeader', 'user_header');
$smarty->assign('uiTheme', $_SESSION['uiTheme']);
$smarty->display('admin/generic_list.tpl');


?>
