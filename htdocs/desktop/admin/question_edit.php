<?php

// Libs
require_once('../includes/common.php');
require_once('../includes/Config.php');
require_once('../includes/Question.php');


// Ensure user has logged in, otherwise exit now
if (!isset($_SESSION['userId'])) {
  header("Location: /desktop/login/expired.php");
  exit(0);
}

// Ensure user is superuser or an org admin
if (!(isset($_SESSION['superUser']) && $_SESSION['superUser']) &&
    !(isset($_SESSION['role']) && $_SESSION['role'] != ROLE_END_USER &&
      $_SESSION['role'] != ROLE_CLASS_CONTACT_SUPERVISOR)) {
  header("Location: /desktop/login/perm_denied.php");
  exit(0);
}


// Set the page and values to be able to return to this page
$_SESSION['currentSubTab'] = SUBTAB_QUESTION_EDIT;


$errMsg = '';
$config = new Config($_SESSION['orgId']);
$q = new Question($_SESSION['orgId']);
$retLink = new TplCaption('Return to Question List');
$retLink->href='question_list.php';
$archiveQuestion = FALSE;
$parentQuestions = array();

//error_log("+_+_+_+_+_+_+_+_+_+: QUESTION ID: ".$_REQUEST['questionId']);

// Handle form submission
if(isset($_REQUEST['questionId'])) {

  $_SESSION['editQuestionId'] = $_REQUEST['questionId'];
  //  if (DEBUG & DEBUG_FORM)
  //error_log("question_edit.php: FORM: questionId set to ".$_REQUEST['questionId']);

  if ($_REQUEST['questionId'] != 'To Be Assigned') {

    // Validate some form data to make sure form content was not altered
    if (!is_numeric($_REQUEST['questionId'])) {
      header("Location: /desktop/login/perm_denied.php");
      exit;
    }

    $q->SetQuestionId($_REQUEST['questionId']);
  }

  // $_REQUEST['author'] is just an arbitrary field that we use to know the
  // form was submitted but we could just as easily use any of the others.
  if (isset($_REQUEST['author'])) {
    error_log("+_+_+_+_+_+_+_+_+: Creating new question since author specified and To Be Assigned was qid");
    if ($q->languageId != $_REQUEST['languageId'] || (isset($_REQUEST['domainId']) && $q->domainId != $_REQUEST['domainId']) ||
	$q->categoryId != $_REQUEST['categoryId'] || $q->frameTypeId != $_REQUEST['frameTypeId'] ||
	$q->versionDate != $_REQUEST['versionDate'] || $q->author != $_REQUEST['author'] ||
	$q->notes != $_REQUEST['notes'] || $q->title != $_REQUEST['title'] ||
	$q->supportInfo != $_REQUEST['supportInfo'] || $q->capConfirmThanks != $_REQUEST['capConfirmThanks'] ||
	(isset($_REQUEST['isManagement']) && $_REQUEST['isManagement'] &&
	 (($q->isManagement && !$_REQUEST['isManagement']) || (!$q->isManagement && $_REQUEST['isManagement']))) ||
	$q->question != $q->QuestionContentFilter($_REQUEST['question']) ||
	$q->multipleChoice1 != $q->QuestionContentFilter($_REQUEST['answerA']) ||
	$q->multipleChoice2 != $q->QuestionContentFilter($_REQUEST['answerB']) ||
	$q->multipleChoice3 != $q->QuestionContentFilter($_REQUEST['answerC']) ||
	$q->multipleChoice4 != $q->QuestionContentFilter($_REQUEST['answerD']) ||
	$q->multipleChoice5 != $q->QuestionContentFilter($_REQUEST['answerE']) ||
	$q->correctAnswer != $q->QuestionContentFilter($_REQUEST['correctAnswer']) ||
	$q->purpose != $q->QuestionContentFilter($_REQUEST['purpose']) ||
	$q->feedbackChoice1 != $q->QuestionContentFilter($_REQUEST['responseA']) ||
	$q->feedbackChoice2 != $q->QuestionContentFilter($_REQUEST['responseB']) ||
	$q->feedbackChoice3 != $q->QuestionContentFilter($_REQUEST['responseC']) ||
	$q->feedbackChoice4 != $q->QuestionContentFilter($_REQUEST['responseD']) ||
	$q->feedbackChoice5 != $q->QuestionContentFilter($_REQUEST['responseE']) ||
	$q->learningPoints != $q->QuestionContentFilter($_REQUEST['keyLearningPoints']) ||
	$q->supportInfo != $q->QuestionContentFilter($_REQUEST['supportInfo']) ||
	$q->notes != $q->QuestionContentFilter($_REQUEST['notes']) ||
	(isset($_REQUEST['media']) && $q->mediaFilePath != $q->QuestionContentFilter($_REQUEST['media'])))
      $archiveQuestion = TRUE;

    $q->languageId = $_REQUEST['languageId'];
    // This one could be disabled
    if (isset($_REQUEST['domainId']))
      $q->domainId = $_REQUEST['domainId'];
    $q->categoryId = $_REQUEST['categoryId'];
    $q->frameTypeId = $_REQUEST['frameTypeId'];
    $q->versionDate = $_REQUEST['versionDate'];
    $q->author = $_REQUEST['author'];
    $q->notes = $_REQUEST['notes'];
    $q->title = $_REQUEST['title'];
    $q->supportInfo = $_REQUEST['supportInfo'];
    $q->capConfirmThanks = $_REQUEST['capConfirmThanks'];
    if (isset($_REQUEST['displayChoices']))
      $q->displayChoices = $_REQUEST['displayChoices'];

    // Deal with checkboxes
    if (isset($_REQUEST['isManagement']) && $_REQUEST['isManagement'])
      $q->isManagement = TRUE;
    else
      $q->isManagement = FALSE;

    if (isset($_REQUEST['requireCorrectAnswer']) && $_REQUEST['requireCorrectAnswer'])
      $q->requireCorrectAnswer = TRUE;
    else
      $q->requireCorrectAnswer = FALSE;

    // Deal with properties used in creating questionContent
    $q->question = $q->QuestionContentFilter($_REQUEST['question']);
    $q->multipleChoice1 = $q->QuestionContentFilter($_REQUEST['answerA']);
    $q->multipleChoice2 = $q->QuestionContentFilter($_REQUEST['answerB']);
    $q->multipleChoice3 = $q->QuestionContentFilter($_REQUEST['answerC']);
    $q->multipleChoice4 = $q->QuestionContentFilter($_REQUEST['answerD']);
    $q->multipleChoice5 = $q->QuestionContentFilter($_REQUEST['answerE']);
    if (isset($_REQUEST['correctAnswer']))
      $q->correctAnswer = $q->QuestionContentFilter($_REQUEST['correctAnswer']);
    else
      $q->correctAnswer = 0;
    $q->purpose = $q->QuestionContentFilter($_REQUEST['purpose']);
    $q->feedbackChoice1 = $q->QuestionContentFilter($_REQUEST['responseA']);
    $q->feedbackChoice2 = $q->QuestionContentFilter($_REQUEST['responseB']);
    $q->feedbackChoice3 = $q->QuestionContentFilter($_REQUEST['responseC']);
    $q->feedbackChoice4 = $q->QuestionContentFilter($_REQUEST['responseD']);
    $q->feedbackChoice5 = $q->QuestionContentFilter($_REQUEST['responseE']);
    $q->learningPoints = $q->QuestionContentFilter($_REQUEST['keyLearningPoints']);
    $q->supportInfo = $q->QuestionContentFilter($_REQUEST['supportInfo']);
    $q->notes = $q->QuestionContentFilter($_REQUEST['notes']);
    // This could also be disabled
    if (isset($_REQUEST['media']))
      $q->mediaFilePath = $q->QuestionContentFilter($_REQUEST['media']);

    $rc = RC_OK;

    // If a new record or modification of predefined question, go get a new questionID
    if ($q->questionId < 100000 || $_REQUEST['questionId'] == 'To Be Assigned') {

      //if ($_SESSION['superUser']) {
      //$q->questionId = $q->GetNextQuestionId(1, 100000);
      //} else {
      $q->questionId = $q->GetNextQuestionId(100000, 1000000);
      $q->versionDate = date('Y-m-d')." 00:00:00";
      $q->domainId = 100000;
      //}

      $q->ownerUserId = $_SESSION['userId'];
      if ($q->questionId > 0) {
	if ($rc = $q->InsertQuestion() !== RC_OK)
	  $q->questionId = 0;
	else {
	  // TODO: Update all tracks using this question with the new question number ??
	  // Do we want this feature?
	}
      }
    } else { // Update

      $qOriginal = new Question($_SESSION['orgId'], $q->questionId);
      if ($q->UpdateRequired($qOriginal)) {
	//	if (!$q->FullUpdateRequired($qOriginal)) {
	//	  // Do a micro update on just the support stuff
	//	  $q->UpdateQuestionSupport();
	//	} else { // Update the entire question but archive it first if not superUser
	//	if ($_SESSION['superUser']) {
	//	  $rc = $q->UpdateQuestion();
	//	} else {
	// If only updated the requireCorrectAnswerFlag then do not bother update question
	if ($archiveQuestion) {
	  $rc = $q->ArchiveQuestion();
	  $q->versionDate = date('Y-m-d')." 00:00:00";
	  $q->domainId = 100000;
	}
	$q->ownerUserId = $_SESSION['userId'];
	$rc = $q->UpdateQuestion();
	  //	} // end if/else superUser updating question
	//	} // end if/else FullUpdateRequired
      } // end if UpdateRequired($qOriginal)

      if (isset($_REQUEST['CapReplicate']) && $_REQUEST['CapReplicate'])
      	$q->UpdateAllQuestionsConfirm($_SESSION['userId']);
    }
  }
} else { // Form not being submitted, initialize author to current user
  
  // Set the question author for new questions
  $u = new User($_SESSION['orgId'], $_SESSION['userId']);
  $q->author = $u->firstName.' '.$u->lastName;
  
}

// If this question is an existing question then remove this question number
// from the list of available parent question numbers to ensure the user does
// not setup of a parent question of itself.
$parentQuestions = $q->GetParentQuestions();
if ($q->questionId > 0) {
  foreach($parentQuestions as $key => $val) {
    $parentQuestions[$key] = "$key - $val";
  }
}
$smarty->assign_by_ref('parentQuestions', $parentQuestions);


// Setup the list of options for correct response selection dropdown
$smarty->assign('displayChoices', $q->displayChoices);
$smarty->assign('useQuestionSetting', $config->GetValue(CONFIG_DISPLAY_CHOICES_USE_QUESTION_SETTING));
$smarty->assign('errMsg', $errMsg);
$smarty->assign('retLink', $retLink);
$smarty->assign('currentSubTab', $_SESSION['currentSubTab']);
$smarty->assign('correctOpts', array("1" => "A", "2" => "B", "3" => "C",  "4" => "D", "5" => "E"));
$smarty->assign('superUser', $_SESSION['superUser']);
$smarty->assign_by_ref('q', $q);

$smarty->assign('uiTheme', $_SESSION['uiTheme']);
$smarty->display('admin/question_edit.tpl');

$q->Close();


?>