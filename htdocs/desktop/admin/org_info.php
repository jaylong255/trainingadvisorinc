<?php
//include 'create_db.php';

require_once("../includes/common.php");
require_once("../includes/Config.php");
require_once("../includes/Organization.php");
require_once("../includes/Question.php");

// Ensure user has logged in, otherwise exit now
if (!isset($_SESSION['userId'])) {
  header('Location: /desktop/login/expired.php');
  exit(0);
}

// Ensure user is at least an admin
if (!(isset($_SESSION['superUser']) && $_SESSION['superUser']) &&
    !(isset($_SESSION['role']) && $_SESSION['role'] == ROLE_ORG_ADMIN)) {
  error_log("SECURITY: $_SESSION[userName] ($_SESSION[userId]), role=$_SESSION[role]: "
	    ."attempted to access org_info.php without permission");
  header('Location: /desktop/login/perm_denied.php');
  exit(0);
}

// Local script variables
$errMsg = '';
$orgName = '';
$orgDirectory = '';
$orgPolicies = '';

$rc = 0;
$org = new Organization;
$config = new Config($_SESSION['orgId']);

// Specify this as the current tab
$_SESSION['currentTab'] = TAB_ORG;
$_SESSION['currentSubTab'] = SUBTAB_ORG_INFO;


// Create a new organization
if (isset($_REQUEST['create']) && $_REQUEST['create']) {

  $smarty->assign('create', TRUE);
  // Create form being submitted
  if (isset($_REQUEST['saveMe']) && $_REQUEST['saveMe']) {
    $rc = $org->CreateOrg($_REQUEST['orgName'], trim($_REQUEST['orgDirectory']));
    if ($rc == RC_OK) {
      //      ChangeOrg($org->GetId());
      if (isset($_REQUEST['orgPolicies'])) {
	$q = new Question($_SESSION['orgId'], AB1825_POLICY_QUESTION);
	$q->feedbackChoice1 = $q->QuestionContentFilter(trim($_REQUEST['orgPolicies']));
	$q->UpdateQuestion();
      }
      header("Location: /desktop/login/change_prefs.php?orgId=".$org->GetId()."&returnUrl=../admin/org_info.php");
      //header("Location: org_list.php");
      exit;
      //      $smarty->assign('create', FALSE);
    } else {
      $errMsg = "Unable to create organization: ".RcToText($rc);
    }
    $org->Close();
    $orgName = $_REQUEST['orgName'];
    $orgDirectory = trim($_REQUEST['orgDirectory']);
    $orgPolicies = trim($_REQUEST['orgPolicies']);
  }

} else {

  // Validation check on the orgnaization
  if (!is_numeric($_SESSION['orgId'])) {
    header("Location: /desktop/login/security.php");
    exit(0);
  }

  // Go get the configuration data setup for for this organization
  $config = new Config($_SESSION['orgId']);
  $q = new Question($_SESSION['orgId'], AB1825_POLICY_QUESTION);

  // Check for org header and motif changes
  if (isset($_REQUEST['changeOrg']) && $_REQUEST['changeOrg']) {
    $smarty->assign('changeOrg', TRUE);
    SetupMotifDisplay($smarty, $config);
  }

  if (isset($_REQUEST['saveMe']) && $_REQUEST['saveMe']) {
    if (isset($_REQUEST['orgPolicies'])) {
      $q->feedbackChoice1 = $q->QuestionContentFilter(trim($_REQUEST['orgPolicies']));
      $q->UpdateQuestion();
    }
    if ($config->SetValue(CONFIG_ORG_NAME, trim($_REQUEST['orgName'])) === FALSE)
      $errMsg .= "Unable to update organization name";
    $org->SetOrgId($_SESSION['orgId'], trim($_REQUEST['orgName']));    
  } else
    $org->SetOrgId($_SESSION['orgId']);

  // This function automatically updates associated session vars
  ChangeOrg($_SESSION['orgId']);
  $smarty->assign('create', FALSE);

  $orgName = $config->GetValue(CONFIG_ORG_NAME);
  $orgDirectory = $config->GetValue(CONFIG_ORG_DIRECTORY);
  $orgPolicies = str_replace("<BR>", "\n", $q->feedbackChoice1);

}


if ($config->GetValue(CONFIG_LICENSED_AB1825_CONTENT)) {
  $smarty->assign('licenseAb1825', TRUE);
} else {
  $smarty->assign('licenseAb1825', FALSE);
}
     

$smarty->assign('orgPolicies', $orgPolicies);
$smarty->assign('currentTab', $_SESSION['currentTab']);
$smarty->assign('currentSubTab', $_SESSION['currentSubTab']);
$smarty->assign('errMsg', $errMsg);
$smarty->assign('superUser', $_SESSION['superUser']);
//$smarty->assign('role', $_SESSION['role']);
$smarty->assign('orgId', $_SESSION['orgId']);
$smarty->assign('orgName', $orgName);
$smarty->assign('orgDirectory', $orgDirectory);
$smarty->assign('uiTheme', $_SESSION['uiTheme']);
$smarty->display('admin/org_info.tpl');

?>
