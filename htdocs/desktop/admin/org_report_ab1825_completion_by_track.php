<?php

require_once ('../includes/common.php');
//require_once ('../includes/Organization.php');
require_once ('../includes/ReportAb1825CompletionByTrack.php');


// Ensure user has logged in, otherwise exit now
if (!isset($_SESSION['userId'])) {
  header("Location: /desktop/login/expired.php");
  exit(0);
}

// Ensure user is an authorized user
if (!(isset($_SESSION['superUser']) && $_SESSION['superUser']) &&
    !(isset($_SESSION['role']) && $_SESSION['role'] != ROLE_END_USER)) {
  header("Location: /desktop/login/perm_denied.php");
  exit(0);
}

// Make sure we have a current org id and if not, send to security
if (!isset($_SESSION['orgId'])) {
  error_log("*** No session ID found");
  header("Location: /desktop/login/security.php");
  exit(0);
}

// TODO: make sure if contact, this user only gets the tracks for which they
// are responsible


// Create session and local/(script global) variables
$rep = NULL;
$trackId = '';
//$resultFormat = 0;
$trackIdsSelected = array();
$showIncomplete = FALSE;

// Specify this as the current tab
$_SESSION['currentTab'] = TAB_ORG;
$_SESSION['currentSubTab'] = SUBTAB_ORG_REPORTS;


if (isset($_REQUEST['trackIdsSelected']))
  $trackIdsSelected = $_REQUEST['trackIdsSelected'];

//if (isset($_REQUEST['questionId']))
//  $questionId = $_REQUEST['questionId'];

//if (!isset($_REQUEST['resultFormatNumbers']) && !isset($_REQUEST['resultFormatPercentages'])) {
//  $resultFormat = RESULT_FORMAT_NUMBERS;
//} else {
//  if (isset($_REQUEST['resultFormatNumbers']) && $_REQUEST['resultFormatNumbers'] == 1)
//    $resultFormat |= RESULT_FORMAT_NUMBERS;
//  if (isset($_REQUEST['resultFormatPercentages']) && $_REQUEST['resultFormatPercentages'] == 1)
//    $resultFormat |= RESULT_FORMAT_PERCENTAGES;
//}



if (!isset($_REQUEST['outputFormat'])) {
  $outputFormat = OUTPUT_FORMAT_HTML;
} else {
  switch($_REQUEST['outputFormat']) {
  case 'html':
    $outputFormat = OUTPUT_FORMAT_HTML;
    break;
  case 'csv':
    $outputFormat = OUTPUT_FORMAT_CSV;
    break;
  case 'pdf':
    $outputFormat = OUTPUT_FORMAT_PDF;
    break;
  default:
    error_log("org_report_accuracy_by_track.php: invalid output format passed to form: ".$_REQUEST['outputFormat']);
    $outputFormat = OUTPUT_FORMAT_HTML;
    break;
  }
}


// Specify this as the current tab
$_SESSION['currentTab'] = TAB_ORG;
$_SESSION['currentSubTab'] = SUBTAB_ORG_REPORTS;


$smarty->assign('superUser', $_SESSION['superUser']);
$smarty->assign('currentTab', $_SESSION['currentTab']);
$smarty->assign('currentSubTab', $_SESSION['currentSubTab']);
//$smarty->display('admin/org_reports.tpl');


// Validate tracks, questions, and dates
if (isset($trackIdsSelected) && $trackIdsSelected) {
  foreach ($trackIdsSelected as $localTrackId) {
    if (!is_numeric($localTrackId)) {
      error_log("ort_report_accuracy_by_track.php: ERROR(Form Validation): non numeric value submitted for trackId($localTrackId)");
      header("Location: /desktop/login/perm_denied.php");
      exit(0);
    }
  }
}


// Check for param to report on only incomplete users
if (isset($_REQUEST['showOnlyIncomplete']) && $_REQUEST['showOnlyIncomplete']) {
  $showIncomplete = TRUE;
}     

// Check to see if form is being populated or submitted.  This first block
// of the if statement is the form being submitted.
if ((isset($_REQUEST['execute']) && $_REQUEST['execute'] == '1') ||
    (isset($_REQUEST['execute_x']) && isset($_REQUEST['execute_y']))) {

  // Create the report that has functions to 
  $rep = new ReportAb1825CompletionByTrack($_SESSION['orgId'], $trackIdsSelected, $showIncomplete);

  $results = $rep->GetReportData();
  switch($outputFormat) {
  case OUTPUT_FORMAT_HTML:
    $smarty->assign('trackIdsSelected', implode('&trackIdsSelected[]=', $trackIdsSelected));
    $smarty->assign('trackList', $results);
    $smarty->assign('showOnlyIncomplete', $showIncomplete);
    $smarty->assign('uiTheme', $_SESSION['uiTheme']);
    $smarty->display('admin/org_report_ab1825_completion_by_track_results.tpl');
    break;
  case OUTPUT_FORMAT_PDF:
    $output = $rep->CreatePdf($results);
    break;
  case OUTPUT_FORMAT_CSV:
    $rep->CreateCsv($results);
    break;
  }

} else {

  // Only need access to the functions that get distinct elements so just a Report object works
  $rep = new ReportAb1825CompletionByTrack($_SESSION['orgId'], array());
  $tracks = $rep->GetDistinctAb1825Tracks();
  if (empty($tracks))
    $smarty->assign('reportsErrMsg', 'There are no tracks defined.  This report can be run after some tracks have been setup.');
  else {
    $trackIds = array_keys($tracks);
    $trackNames = array_values($tracks);

    $smarty->assign_by_ref('trackIds', $trackIds);
    $smarty->assign_by_ref('trackNames', $trackNames);
    $smarty->assign('trackIdsSelected', $trackIdsSelected);
    if (isset($_REQUEST['outputFormat']))
      $smarty->assign('outputFormat', $_REQUEST['outputFormat']);
  }

  $smarty->assign('superUser', $_SESSION['superUser']);
  $smarty->assign('currentTab', $_SESSION['currentTab']);
  $smarty->assign('currentSubTab', $_SESSION['currentSubTab']);
  $smarty->assign('showOnlyIncomplete', $showIncomplete);

  $smarty->assign('uiTheme', $_SESSION['uiTheme']);
  $smarty->display('admin/org_report_ab1825_completion_by_track.tpl');
}

exit(0);
?>
