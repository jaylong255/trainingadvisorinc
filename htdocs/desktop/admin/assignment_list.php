<?php
/////////////////////////////////////////////////////////////////////////////
//	assignment_list.php
//		Page used for administration of organizations stored on the current server
//


// Include libs which starts session and creates smarty objects
require_once ('../includes/common.php');
require_once ('../includes/Track.php');


// Ensure user has logged in, otherwise exit now
if (!isset($_SESSION['userId'])) {
  header("Location: /desktop/login/expired.php");
  exit;
}


// Ensure user is superuser or an org admin
if (!(isset($_SESSION['superUser']) && $_SESSION['superUser']) &&
    !(isset($_SESSION['role']) && $_SESSION['role'] != ROLE_END_USER)) {
  header("Location: /desktop/login/perm_denied.php");
}


// Make sure if this is only a track admin this is the track they are super for
if (!$_SESSION['superUser'] && $_SESSION['role'] == ROLE_CLASS_CONTACT_SUPERVISOR) {
  $track = new Track($_SESSION['orgId'], $_SESSION['trackId']);
  if ($_SESSION['userId'] != $track->supervisorId) {
    error_log("Security error, redirecting to track denied for trackId(".$track->trackId);
    header("Location: /desktop/login/perm_denied.php?trackId=".$track->trackId);
    exit;
  }
}


// Specify this as the current tab
$_SESSION['currentTab'] = TAB_TRACK;
$_SESSION['currentSubTab'] = SUBTAB_TRACK_ASSIGNMENTS;

// Sets the Search interface drop down default value
if (isset($_REQUEST['searchBy'])) {
  $searchBy = $_REQUEST['searchBy'];
} else {
  $searchBy = QUEUE_LIST_COLUMN_QUESTION_TITLE;
}

// Sets up what we are searching for
if (isset($_REQUEST['searchFor'])) {
  $searchFor = $_REQUEST['searchFor'];
  $_SESSION['assignmentPageNumber'] = 1;
} else {
  $searchFor = '';
}

// create new employee list
$errMsg = '';
$rc = RC_OK;
$firstRow = 0;
$lastRow = 0;
$numRows = 0;
$numPages = 0;
$pageNumber = 1;
$questionIds = array();
$questions = array();
$questionList = new Track($_SESSION['orgId'], $_SESSION['trackId']);
$assignmentId = 0;
$i = 0;
$linkArr = array();


// Handle assignment or unassignment of a question
if (isset($_REQUEST['questionId']) && $_REQUEST['questionId'] &&
    is_numeric($_REQUEST['questionId'])) {
/*
  if (isset($_REQUEST['action'])) {
    switch($_REQUEST['action']) {
    case 'add':
      $questionList->AssignQuestion($_REQUEST['questionId']);
      break;
    case 'remove':
      $questionList->UnAssignQuestion($_REQUEST['questionId']);
      break;
    default:
      error_log('assignment_list.php: Unknown action('.$_REQUEST['action'].')');
      break;
    }
  }
*/

  // Valid values are only numeric
  if (isset($_REQUEST['sequenceId']))
    $rc = $questionList->SetQueueSequenceId($_REQUEST['questionId'], $_REQUEST['sequenceId']);

  // Valid values are 'New', 'Unassign', or <numeric>
  if (isset($_REQUEST['groupId']))
    $rc = $questionList->SetQueueGroupId($_REQUEST['questionId'], $_REQUEST['groupId']);

  // Check return code from eithe of the two previous operations (they are mutually exclusive)
  if ($rc != RC_OK) {
    $errMsg .= RcToText($rc)."<BR>";
  }
}



// Check for remove all and add all actions
if (isset($_REQUEST['removeAll']) && $_REQUEST['removeAll'] == 1) {
  error_log("Removing all questions from class: ".$_SESSION['trackId']);
  $questionList->RemoveAllFromTrack();

}


if (!$questionList->isAb1825) {
  // The conventional case
  // Setup quick links in the page
  if ($_SESSION['assignmentShowAll']) {
    $showLink = new TplCaption('Show only questions assigned to this class');
    $showLink->href = '/desktop/login/change_prefs.php';
    $showLink->params = 'returnUrl=/desktop/admin/assignment_list.php&assignmentShowAll=0';
    array_push($linkArr, $showLink);
    $showLink = new TplCaption('Add all to class');
    $showLink->href = 'javascript:if (WarnAddAllQuestionsToTrack()) { window.location=\'/desktop/admin/assignment_list.php?addAll=1\'; }';
    //$showLink->params = 'addAll=1';
    //$showLink->onClick = 'WarnAddAllQuestionsToTrack()';
    array_push($linkArr, $showLink);
  } else {
    $showLink = new TplCaption('Show all questions');
    $showLink->href = '/desktop/login/change_prefs.php';
    $showLink->params = 'returnUrl=/desktop/admin/assignment_list.php&assignmentShowAll=1';
    // These two lines are what allows us to only get the assignments for the working track
    $searchBy = array(QUEUE_LIST_COLUMN_TRACK_ID, $searchBy);
    $searchFor = array($_SESSION['trackId'], $searchFor);
    array_push($linkArr, $showLink);
    $showLink = new TplCaption('Remove all from class');
    $showLink->href = 'javascript:if (WarnRemoveAllQuestionsFromTrack()) { window.location=\'/desktop/admin/assignment_list.php?removeAll=1\'; }';
    //$showLink->params = 'removeAll=1';
    //$showLink->onClick = 'WarnRemoveAllQuestionsFromTrack()';
    array_push($linkArr, $showLink);
  }
} else {
  // AB1825 tracks may only ever list the AB1825 assignments/questions since
  // content assignment is automated, the user may never choose the assignments
  $searchBy = array(QUEUE_LIST_COLUMN_TRACK_ID, $searchBy);
  $searchFor = array($_SESSION['trackId'], $searchFor);
}


// Handle the case where all questions are assigned to track
if (isset($_REQUEST['addAll']) && $_REQUEST['addAll'] == 1) {

  //error_log('Adding all questions as assignments to class '.$_SESSION['trackId']);
  
  $rc = $questionList->GetQueueList($_SESSION['assignmentShowColumns'], $searchBy, $searchFor,
				    QUEUE_LIST_COLUMN_QUESTION_ID, FALSE, $pageNumber, 99999);

  if ($rc === FALSE) {
    error_log("Failed to get queue list to add all questions to track\n");
    echo "Failed to get question list\n";
    exit(1);
  }
  
  if (DEBUG & DEBUG_FORM) {
    $smarty->assign('GetAssignmentList', $rc);
  }
  
  if (!empty($rc[0])) {
    // Loop through assignment ids and if 
    foreach($rc[0] as $i => $assignmentId) {
      // [1][$i][1] references the sequence number
      // If there is no sequence number then the question must be added to the track
      if (!$rc[1][$i][1])
	$questionList->AssignQuestion($assignmentId);
    }
  } else {
    error_log("The list of questions is empty, skipping assignment of all to track: ".$_SESSION['trackId']);
  }
}



$rc = $questionList->GetQueueList($_SESSION['assignmentShowColumns'], $searchBy, $searchFor,
				  $_SESSION['assignmentSortColumn'], $_SESSION['assignmentSortDescending'],
				  $_SESSION['assignmentPageNumber'], $_SESSION['rowsPerPage']);

if ($rc === FALSE) {
  echo "Failed to get employee list\n";
  exit(1);
}

if (DEBUG & DEBUG_FORM) {
  $smarty->assign('GetAssignmentList', $rc);
}


if (!empty($rc)) {
  list($questionIds, $questions) = $rc;
  $questionList->GetRowCounts($firstRow, $lastRow, $numRows);
  $numPages = $questionList->GetPageCount($_SESSION['rowsPerPage']);

  // Get the selected question number (next question to be delivered)
  $selected = $questionList->GetNextQuestionIdInSequence();
}



// Deal with the inclusion of sequence and group div menus when either of these
// columns is selected
if ($_SESSION['assignmentShowColumns'] & QUEUE_LIST_COLUMN_GROUP_ID ||
    $_SESSION['assignmentShowColumns'] & QUEUE_LIST_COLUMN_SEQUENCE) {

  // This is necessary for the javascript div id selection menus to function properly
  $maxGroupId    = $questionList->GetMaxGroupId();
  $maxSequenceId = $questionList->GetMaxSequenceId();
  
  if ($maxGroupId >= 0 && $maxSequenceId >= 0) {
    $smarty->assign('maxGroupId', $maxGroupId);
    $smarty->assign('maxSequenceId', $maxSequenceId);
    $smarty->assign('includeSequenceMenu', TRUE);
    $smarty->assign('includeGroupMenu', TRUE);
  }
}


// This alone is enough information to identify a search column
// Strip Track column since we are only looking at assignments of a given track
// The argument to GetColumnLabels here is an exclude list of columns we do not want
$smarty->assign('columnLabels', $questionList->GetQueueColumnLabels());
$smarty->assign('showColumns', $_SESSION['assignmentShowColumns']);

// Keep the selected search by field
$smarty->assign('searchBy', $searchBy);

// Set the one to be selected
$smarty->assign('sortColumn', $_SESSION['assignmentSortColumn']);
$smarty->assign('sortDescending', $_SESSION['assignmentSortDescending']);


$smarty->assign('firstRow', $firstRow);
$smarty->assign('lastRow', $lastRow);
$smarty->assign('numRows', $numRows);
$smarty->assign('pageNumber', $_SESSION['assignmentPageNumber']);
$smarty->assign('numPages', $numPages);
$smarty->assign('recordList', $questions);
$smarty->assign('recordIds', $questionIds);
if ($questionList->isBinding) {
  $smarty->assign('recordId', $questionList->GetNextQuestionIdInSequence());
}

$smarty->assign('errMsg', $errMsg);

$questionList->Close();



$smarty->assign('currentTab', $_SESSION['currentTab']);
$smarty->assign('currentSubTab', $_SESSION['currentSubTab']);
$smarty->assign('toggleParamName', 'toggleAssignmentColumn');
$smarty->assign('sortParamName', 'assignmentSortColumn');
$smarty->assign('deleteLink', 'assignment_delete.php');
$smarty->assign('deleteLinkLabel', 'Remove');
$smarty->assign('idParamName', 'questionId');
$smarty->assign('pageNumberParamName', 'assignmentPageNumber');
$smarty->assign('orderParamName', 'assignmentSortDescending');
$smarty->assign('showSelectedColumn', TRUE);
$smarty->assign('rowsPerPage', $_SESSION['rowsPerPage']);
$smarty->assign('localHeader', 'track_header');
$smarty->assign_by_ref('topLinkList', $linkArr);
$smarty->assign('uiTheme', $_SESSION['uiTheme']);
$smarty->display('admin/generic_list.tpl');


?>
