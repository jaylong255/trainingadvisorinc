<?php

include ("../global.php");

$Link;
$where = '';
/*if ($DEBUG) {
  print_r($_REQUEST);
  echo "<BR>\n";
}*/


//////////////////////////////////////////////
//Validate the Form
//////////////////////////////////////////////
function Validate_Form_Gen_Edit($Link) {
  
  global $ErrMsg;


  if ($_REQUEST['FormAction'] == "New") {

    $Query = "SELECT * FROM ".esql($_REQUEST['TableName'])." WHERE Name='".esql($_REQUEST['F_Value'])."'";

    $Result = mysql_query($Query, $Link);
    $Row = mysql_fetch_array($Result);
    if(!$Row)	{
      $Query = "INSERT INTO ".esql($_REQUEST['TableName'])." (Name) VALUES ('".esql($_REQUEST['F_Value'])."')";
      $Result = mysql_query($Query, $Link);
      if(!$Result) {
	$ErrMsg .= "Could not write the ".$_REQUEST['TableName']." to the Database.<BR>\n";
      } else {
	$_REQUEST['F_Item'] = '';
	return TRUE;
      }
    } else {
      print("A ".$_REQUEST['TableName']." name already exists with this value.");
    }
  } else if ($_REQUEST['FormAction'] == "Delete") {
    
    if($_REQUEST['F_Item']) {
      
      $Query = "SELECT * FROM ".esql($_REQUEST['TableName'])." WHERE ".esql($_REQUEST['ColumnName2'])
	." = '".esql($_REQUEST['F_Item'])."'";
      //      echo "QUERY: $Query<BR>\n";
      $Result = mysql_query($Query, $Link);
      if (!$Result) {
	echo "Unable to execute query: $Query: ".mysql_error()."<BR>\n";
	exit;
      }
      $Row = mysql_fetch_array($Result);
      //      echo "ROW: ";
      //      print_r($Row);
      if($Row) {
	if($_REQUEST['TableName'] == "Employment_Status" && $_REQUEST['F_Item'] <= 3) {
	  $ErrMsg .= "That is a Reserved System Value and cannot be deleted<BR>\n";
	} else {
	  $Query = "DELETE FROM ".esql($_REQUEST['TableName'])." WHERE ".esql($_REQUEST['ColumnName2'])
	    ." = '".esql($_REQUEST['F_Item'])."'";
	  if(mysql_query($Query, $Link)) {
	    $_REQUEST['F_Item'] = "";
	    $_REQUEST['F_Value'] = "";
	    $_REQUEST['F_ID'] = "";
	    
	    return TRUE;
	  } else {
	     echo "Could not delete ".$_REQUEST['F_Value']." from ".$_REQUEST['TableName'];
	  }
	}
      }
    }
  } else if ($_REQUEST['FormAction'] == "Update") {
    
    if($_REQUEST['TableName'] == "Employment_Status" && $_REQUEST['F_Item'] <= 3) {
      $ErrMsg .= "That is a Reserved System Value and cannot be changed<BR>\n";
    } else {
      $Query = "UPDATE ".esql($_REQUEST['TableName'])." SET name='".esql($_REQUEST['F_Value'])
	."' WHERE ".esql($_REQUEST['ColumnName2'])." = '".esql($_REQUEST['F_Item'])."'";
      if(mysql_query($Query, $Link)) {
	$_REQUEST['F_Item'] = '';
	return TRUE;
      } else {
	print("Unable to update ".$_REQUEST['F_Value']." in ".$_REQUEST['TableName']);
      }
    }  
  }
  
  return FALSE;
}


//////////////////////////////////////////////
//Start/Restart PHP Page
//////////////////////////////////////////////
OpenDatabase();


if($_REQUEST['Insert'] == 'TRUE') {

  Validate_Form_Gen_Edit($Link);

} // end if Insert value submitted from form


// If this is employment status, we need to make sure we exclude the system reserved employment
// status' in the table.  So we will add a where clause that states it.
if ($_REQUEST['TableName'] == 'Employment_Status') {
  $where = "WHERE Employment_Status_ID > 3";
}


//Retreive_Template_Items("$TableName","$ColumnName","$ColumnName2");
Retrieve_Template_Items($Link, esql($_REQUEST['TableName']), $_REQUEST['ColumnName2'], "F_Item",
			esql($_REQUEST['ColumnName']), esql($_REQUEST['F_Item']), $where); 

CloseDatabase();



$smarty->assign('TableName', $_REQUEST['TableName']);
$smarty->assign('ColumnName', $_REQUEST['ColumnName']);
$smarty->assign('ColumnName2', $_REQUEST['ColumnName2']);
$smarty->assign('ErrMsg', $ErrMsg);
$smarty->assign('uiTheme', $_SESSION['uiTheme']);

$smarty->display('admin/gen_edit.tpl');


?>