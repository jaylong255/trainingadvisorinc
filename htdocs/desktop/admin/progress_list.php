<?php
/////////////////////////////////////////////////////////////////////////////
//	progress_list.php
//		Page used for reviewing the progress of a class
//


// Include libs which starts session and creates smarty objects
require_once ('../includes/common.php');
require_once ('../includes/Track.php');

// Ensure user has logged in, otherwise exit now
if (!isset($_SESSION['userId'])) {
  header("Location: /desktop/login/expired.php");
  exit(0);
}

// Ensure user is superuser or an org admin
if (!(isset($_SESSION['superUser']) && $_SESSION['superUser']) &&
    !(isset($_SESSION['role']) && $_SESSION['role'] != ROLE_END_USER)) {
  header("Location: /desktop/login/perm_denied.php");
  exit(0);
}

// Make sure if this is only a track admin this is the track they are super for
if ($_SESSION['role'] == ROLE_CLASS_CONTACT_SUPERVISOR) {
  $track = new Track($_SESSION['orgId'], $_SESSION['trackId']);
  if ($_SESSION['userId'] != $track->supervisorId) {
    error_log("Redirecting to track denied for trackId(".$track->trackId);
    header("Location: /desktop/login/perm_denied.php?trackId=".$track->trackId);
    exit(0);
  }
}


// Specify this as the current tab
$_SESSION['currentTab'] = TAB_TRACK;
$_SESSION['currentSubTab'] = SUBTAB_TRACK_PROGRESS;



if (isset($_REQUEST['searchBy'])) {
  $searchBy = $_REQUEST['searchBy'];
} else {
  $searchBy = ASSIGNMENT_COLUMN_COMPLETION_DATE;
}

if (isset($_REQUEST['searchFor'])) {
  $searchFor = $_REQUEST['searchFor'];
  $_SESSION['progressPageNumber'] = 1;
} else {
  $searchFor = '';
}


// create progress list
$errMsg = '';
$rc = 0;
$firstRow = 0;
$lastRow = 0;
$numRows = 0;
$numPages = 0;
$trackIds = array();
$tracks = array();
$progressList = new Track($_SESSION['orgId'], $_SESSION['trackId']);

$rc = $progressList->GetProgressList($_SESSION['progressShowColumns'], $searchBy, $searchFor,
				     $_SESSION['progressSortColumn'], $_SESSION['progressSortDescending'],
				     $_SESSION['progressPageNumber'], $_SESSION['rowsPerPage']);
if ($rc === FALSE) {
  echo "Failed to get track list\n";
  exit(1);
}

if (DEBUG & DEBUG_FORM) {
  $smarty->assign('GetTrackList', $rc);
}

if (!empty($rc)) {
  //list($trackIds, $tracks) = $rc;
  $progressList->GetRowCounts($firstRow, $lastRow, $numRows);
  $numPages = $progressList->GetPageCount($_SESSION['rowsPerPage']);
}

// This alone is enough information to identify a search column
$smarty->assign('columnLabels', $progressList->GetAssignmentColumnLabels($_SESSION['progressShowColumns']));
$smarty->assign('showColumns', $_SESSION['progressShowColumns']);

// Keep the selected search by field
$smarty->assign('searchBy', $searchBy);

// Set the one to be selected
$smarty->assign('sortColumn', $_SESSION['progressSortColumn']);
$smarty->assign('sortDescending', $_SESSION['progressSortDescending']);


$smarty->assign('firstRow', $firstRow);
$smarty->assign('lastRow', $lastRow);
$smarty->assign('numRows', $numRows);
$smarty->assign('pageNumber', $_SESSION['progressPageNumber']);
$smarty->assign('numPages', $numPages);
$smarty->assign('recordList', $rc);
//$smarty->assign('recordIds', $trackIds);

if (DEBUG) {
  $smarty->assign('ErrMsg', $progressList->GetErrorMessage());
}

$progressList->Close();

$smarty->assign('currentTab', $_SESSION['currentTab']);
$smarty->assign('currentSubTab', $_SESSION['currentSubTab']);
$smarty->assign('toggleParamName', 'toggleProgressColumn');
$smarty->assign('sortParamName', 'progressSortColumn');
//We can't use this abreviated mechanism this time since the record ids are not the same
//$smarty->assign('idParamName', 'questionId');
$smarty->assign('hideActionHeader', TRUE);
$smarty->assign('pageNumberParamName', 'progressPageNumber');
$smarty->assign('orderParamName', 'progressSortDescending');
$smarty->assign('showSelectedColumn', FALSE);
$smarty->assign('rowsPerPage', $_SESSION['rowsPerPage']);
$smarty->assign('localHeader', 'track_header');
$smarty->assign('uiTheme', $_SESSION['uiTheme']);
$smarty->display('admin/generic_list.tpl');

exit(0);
?>
