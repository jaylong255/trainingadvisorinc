<?php

require_once('../includes/common.php');
require_once('../includes/Question.php');

// Ensure user has logged in, otherwise exit now
if (!isset($_SESSION['userId'])) {
  //    echo "Required session login information not present.";
  header("Location: /desktop/login/expired.php");
  exit(0);
}

// Ensure user is superuser or an org admin
if (!(isset($_SESSION['superUser']) && $_SESSION['superUser']) &&
    !(isset($_SESSION['role']) && $_SESSION['role'] != ROLE_END_USER)) {
  header("Location: /desktop/login/perm_denied.php");
  exit(0);
}

// Make sure if this is only a track admin this is the track they are super for
if ($_SESSION['role'] == ROLE_CLASS_CONTACT_SUPERVISOR) {
  $track = new Track($_SESSION['orgId'], $_SESSION['trackId']);
  if ($_SESSION['userId'] != $track->supervisorId || !isset($_REQUEST['return'])) {
    error_log("Redirecting to track denied for trackId(".$track->trackId);
    header("Location: /desktop/login/perm_denied.php?trackId=".$track->trackId);
    exit;
  }
}


// Set the page and values to be able to return to this page
$_SESSION['currentSubTab'] = SUBTAB_QUESTION_VIEW_CURRENT;
$_SESSION['editQuestionId'] = $_REQUEST['questionId'];


$errMsg = '';
$retLink = NULL;


if (!isset($_REQUEST['questionId'])) {
  $errMsg = 'Not all required parameters were passed to this page (missing questionId)';
  error_log("question_view.php: $errMsg");
} else {

  if (isset($_REQUEST['domainId']) && isset($_REQUEST['versionDate'])) {
    // So Ctrl-F5 still works in the browser we set these
    $_SESSION['editQuestionDomainId'] = $_REQUEST['domainId'];
    $_SESSION['editQuestionVersionDate'] = $_REQUEST['versionDate'];
    $_SESSION['currentSubTab'] = SUBTAB_QUESTION_VIEW_ARCHIVED;
    $retLink = new TplCaption('Return to Archive List');
    $retLink->href='archive_list.php';
    $q = new Question($_SESSION['orgId'], $_REQUEST['questionId'], $_REQUEST['domainId'], $_REQUEST['versionDate']);
    if (isset($_REQUEST['return']) && $_REQUEST['return'] == 'progress') {
      $retLink = new TplCaption('Return to Progress List');
      $retLink->href='progress_list.php';
    }
  } else {
    if (isset($_REQUEST['return'])) {
      if ($_REQUEST['return'] == 'assignment') {
	$retLink = new TplCaption('Return to Assignment List');
	$retLink->href='assignment_list.php';
      } elseif ($_REQUEST['return'] == 'summary') {
	$retLink = new TplCaption('Return to Summary List');
	$retLink->href='question_summary.php';
      }
    } else {
      $retLink = new TplCaption('Return to Question List');
      $retLink->href='question_list.php';
    }
    $q = new Question($_SESSION['orgId'], $_REQUEST['questionId']);
  }
}

// Prepare the question for display
//$q->PrepForDisplay();
$q->correctAnswer = $q->AnswerNumberToLetter();
$q->parentQuestionId = (int)$q->parentQuestionId;

if (isset($_REQUEST['contentOnly']) && $_REQUEST['contentOnly'])
  $smarty->assign('contentOnly', TRUE);
else
  $smarty->assign('contentOnly', FALSE);

$smarty->assign_by_ref('retLink', $retLink);
$smarty->assign('errMsg', $errMsg);
$smarty->assign('currentSubTab', $_SESSION['currentSubTab']);
$smarty->assign('q', $q);
//$smarty->assign('correctOpts', array("1" => "A", "2" => "B", "3" => "C",  "4" => "D", "5" => "E"));
$smarty->assign('hideAction', TRUE);
$smarty->assign('uiTheme', $_SESSION['uiTheme']);
$smarty->display('admin/question_view.tpl');

?>
