<?php

require_once('../includes/common.php');
require_once('../includes/RecurrenceObject.php');


// Ensure user has logged in, otherwise exit now
if (!isset($_SESSION['userId'])) {
  //    echo "Required session login information not present.";
  header("Location: /desktop/login/expired.php");
  exit(0);
}

// Ensure user is superuser or an org admin
if (!(isset($_SESSION['superUser']) && $_SESSION['superUser']) &&
    !(isset($_SESSION['role']) && $_SESSION['role'] != ROLE_END_USER)) {
  header("Location: /desktop/login/perm_denied.php");
  exit(0);
}

// Make sure if this is only a track admin this is the track they are super for
if ($_SESSION['role'] == ROLE_CLASS_CONTACT_SUPERVISOR) {
  $track = new Track($_SESSION['orgId'], $_SESSION['trackId']);
  if ($_SESSION['userId'] != $track->supervisorId) {
    error_log("Security error, contact attempted to access recurrence.php for track they "
	      ."do not own, redirecting to track denied for trackId(".$track->trackId);
    header("Location: /desktop/login/perm_denied.php?trackId=".$track->trackId);
    exit(0);
  }
}



// Form getting submitted so create new recurrence string and
// set create = TRUE so that parent form will get updated
if (isset($_REQUEST['periodicity'])) {
  
  $create = TRUE;
  // Add code to call SetRecurrence and get recurrence string
  $recObj = SetRecurrence();

} else { // Form being displayed for the first time and passed recurrence param

  $create = FALSE;
  $recObj = new RecurrenceObject($_REQUEST['recurrence']);

}


$smarty->assign('create', $create);
$smarty->assign('recObj', $recObj);
$smarty->assign('uiTheme', $_SESSION['uiTheme']);
$smarty->display('admin/recurrence.tpl');


?>



