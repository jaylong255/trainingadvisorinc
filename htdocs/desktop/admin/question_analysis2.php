<?php

$Link;
include '../global.php';


$Answer_A;
$Answer_B;
$Answer_C;
$Answer_D;
$Answer_E;


//////////////////////////////////////////////
//Open Database
//////////////////////////////////////////////
/*
function OpenDatabase() {

  global $session_Host;
  global $session_User;
  global $session_Password;
  global $Link;
  
  $Link = mysql_connect($session_Host,$session_User,$session_Password);
}

//////////////////////////////////////////////
//Close Database
//////////////////////////////////////////////
function CloseDatabase() {

  global $Link;

  mysql_close($Link);
}
*/


//////////////////////////////////////////////
//Build Table Item List
//////////////////////////////////////////////
// Function: Retrieve_Items
// Purpose:  Collect a set of data for display in an html template.  This function
//  uses the global $smary variable to assign the collected data to an html_options
//  smarty tag in template.  It also assigns a var for the selected value
// Pre:      $Link is a valid DB link
// Post:     smarty variables have been assigned
function Retrieve_Items($Link, $tplSelectedName, $ColumnID, $ComparisonValue, $Table = '') {

  global $smarty;
  $Options = array();

  if(strlen($Table) > 0) {

    $Query = "SELECT DISTINCT $Table.Name, $Table.$ColumnID from Question_Data, $Table "
      ."WHERE Question_Data.$ColumnID=$Table.$ColumnID";

    $Result = mysql_query($Query);

    while($Row = mysql_fetch_array($Result)) {
      $Name=$Row['Name'];
      if(!strcasecmp($Row[$ColumnID], $ComparisonValue))
	$smarty->assign($tplSelectedName, $Row[$ColumnID]);
      $Options = array_merge($Options, array($Row[$ColumnID] => $Name));
	//<option value='$Row[$ColumnID]'>$Name</option>\n");
    }
    $smarty->assign($tplSelectedName."_Opts", $Options);

  } else {

    $Query = "SELECT Distinct $ColumnID from Question_Data ORDER BY $ColumnID";
    $Result = mysql_query($Query);
    while($Row = mysql_fetch_array($Result)) {
      
      $Name=$Row[$ColumnID];
      if(!strcasecmp($ColumnID,"Result")) {
	if($Name==1) $Name="Correct";
	else $Name="Incorrect";
	
      } elseif (!strcasecmp($ColumnID,"Exit_Info")) {
	
	if($Name==1) $Name="Understood";
	else $Name="HR Contact";
      } elseif (!strcasecmp($ColumnID,"Foil_Selected")) {
	
	if($Name=="01") $Name="A";
	else if($Name=="02") $Name="B";
	else if($Name=="03") $Name="C";
	else if($Name=="04") $Name="D";
	else if($Name=="05") $Name="E";
      }
      
      if(!strcasecmp($Row[$ColumnID],$ComparisonValue))
	//	print("<option value='$Row[$ColumnID]' selected>$Name</option>\n");
	$smarty->assign($tplSelectedName, $Row[$ColumnID]);
      $Options = array_merge($Options, array($Row[$ColumnID] => $Name));
    }
    
    $smarty->assign($tplSelectedName."_Opts", $Options);
  }

} // end function Retrieve_Items()



//////////////////////////////////////////////
//Get Count
//////////////////////////////////////////////
function GetCount($column, $value, $where) {
  
  if(strlen($where) <= 0)
    $Where="WHERE";
  else
    $Where= $where." AND";
  
  $Query = "Select Count('$column') AS Number from Question_Data ".$Where." $column='$value'";
  //print("<br>----->$column = $Query $Where");
  if($Result = mysql_query($Query)) {

    if($Row = mysql_fetch_array($Result)) {
      $Count=$Row['Number'];
      return $Count;
    }

  }

  return 0;

} // end function GetCount()



//////////////////////////////////////////////
//Validate the Form
//////////////////////////////////////////////
function Validate_Form_Question_Analysis2() {

  /*  global $F_Question_Number;
  global $F_Domain_ID;
  global $F_Version_Date1;
  global $F_Version_Date2;
  global $F_Completion_Date1;
  global $F_Completion_Date2;
  global $F_Result;
  global $F_Foil_Selected;
  global $F_Exit_Info;
  global $F_Track_ID;
  global $F_Department_ID;
  */
  global $Rows_Returned;
  global $Correct;
  global $Incorrect;
  global $Answer_A;
  global $Answer_B;
  global $Answer_C;
  global $Answer_D;
  global $Answer_E;

  global $Understood;
  global $HR_Contact;
  global $ErrMsg;


  
  $Action = "SELECT COUNT(*) AS Number FROM Question_Data";
  if(strlen($_REQUEST['F_Question_Number']) > 0)
    $Where = " Question_Number='$F_Question_Number' AND";
  if(strlen($_REQUEST['F_Domain_ID']) > 0)
    $Where .= " Domain_ID='$F_Domain_ID' AND";
  if(strlen($_REQUEST['F_Version_Date1']) > 0)
    $Where .= " Version_Date>='$F_Version_Date1' AND";
  if(strlen($_REQUEST['F_Version_Date2']) > 0)
    $Where .= " Version_Date<='$F_Version_Date2' AND";
  if(strlen($_REQUEST['F_Completion_Date1']) > 0)
    $Where .= " Completion_Date>='$F_Completion_Date1' AND";
  if(strlen($_REQUEST['F_Completion_Date2']) > 0)
    $Where .= " Completion_Date<='$F_Completion_Date2' AND";
  if(strlen($_REQUEST['F_Result']) > 0)
    $Where .= " Result='$F_Result' AND";
  if(strlen($_REQUEST['F_Foil_Selected']) > 0)
    $Where .= " Foil_Selected='$F_Foil_Selected' AND";
  if(strlen($_REQUEST['F_Exit_Info']) > 0)
    $Where .= " Exit_Info='$F_Exit_Info' AND";
  if(strlen($_REQUEST['F_Track_ID']) > 0)
    $Where .= " Track_ID='$F_Track_ID' AND";
  if(strlen($_REQUEST['F_Department_ID']) > 0)
    $Where .= " Department_ID='$F_Department_ID' AND";

  if(isset($Where))
    $Where=" WHERE".substr($Where,0,strlen($Where)-4);
  
  $Query = $Action.$Where;
  
  if ($DEBUG)
    print("<br><br><br><br><br><br><br><br><br><br><br><br><br><br>Q=$Query");
  

  if($Result = mysql_query($Query)) {
    
    if($Row = mysql_fetch_array($Result)) {
      $Rows_Returned = $Row['Number'];
      
      if(strlen($F_Foil_Selected)<=0) {
	$Answer_A = GetCount('Foil_Selected','01',$Where);
	$Answer_B = GetCount('Foil_Selected','02',$Where);
	$Answer_C = GetCount('Foil_Selected','03',$Where);
	$Answer_D = GetCount('Foil_Selected','04',$Where);
	$Answer_E = GetCount('Foil_Selected','05',$Where);
      } else {
	$Answer_A="--";
	$Answer_B="--";
	$Answer_C="--";
	$Answer_D="--";
	$Answer_E="--";
	if(!strcasecmp($F_Foil_Selected,"01")) $Answer_A="X";
	if(!strcasecmp($F_Foil_Selected,"02")) $Answer_B="X";
	if(!strcasecmp($F_Foil_Selected,"03")) $Answer_C="X";
	if(!strcasecmp($F_Foil_Selected,"04")) $Answer_D="X";
	if(!strcasecmp($F_Foil_Selected,"05")) $Answer_E="X";
      }
      
      if(strlen($F_Result)<=0) {
	$Correct=GetCount('Result','1',$Where);
	$Incorrect=GetCount('Result','0',$Where);
      } else {
	if($F_Result==0) {
	  $Correct="--";
	  $Incorrect="X";
	} else {
	  $Correct="X";
	  $Incorrect="--";
	}
      }
      
      
      if(strlen($F_Exit_Info)<=0) {
	$Understood=GetCount('Exit_Info','1',$Where);
	$HR_Contact=GetCount('Exit_Info','0',$Where);
      } else {
	if(!strcasecmp($F_Exit_Info,"1")) {
	  $Understood="X";
	  $HR_Contact="--";
	} else {
	  $Understood="--";
	  $HR_Contact="X";
	}
	
      }
    }
  }
  return FALSE;
}

/////////////////////////////
//Start
////////////////////////////

// First open db for all other functions that use the database
OpenDatabase();
 
Validate_Form_Question_Analysis2();


// Param1 (e.g. $Link) = DB link
// Param2 (e.g. F_Question_Number) = Template substitution name
// Param3 (e.g. Question_Number) = DB Column Name
// Param4 (e.g. $F_Question_Number) = Comparison Value for determing selected value
// Param5 (e.g. "" or Domain) = Table name to Query
Retrieve_Items($Link, "F_Question_Number", "Question_Number", $F_Question_Number);
Retrieve_Items($Link, "F_Version_Date1", "Version_Date", $F_Version_Date1);
Retrieve_Items($Link, "F_Version_Date2", "Version_Date", $F_Version_Date2);
Retrieve_Items($Link, "F_Completion_Date1", "Completion_Date", $F_Completion_Date1);
Retrieve_Items($Link, "F_Completion_Date2", "Completion_Date", $F_Completion_Date2);
Retrieve_Items($Link, "F_Domain_ID", "Domain_ID", $F_Domain_ID, "Domain");
Retrieve_Items($Link, "F_TracK_ID", "Track_ID", $F_Track_ID, "Track");
Retrieve_Items($Link, "F_Departmnet_ID", "Department_ID", $F_Department_ID, "Department");
Retrieve_Items($Link, "F_Result", "Result", $F_Result);
Retrieve_Items($Link, "F_Foll_Selected", "Foil_Selected",$F_Foil_Selected);
Retrieve_Items($Link, "F_Exit_Info", "Exit_Info", $F_Exit_Info);

CloseDatabase();



//Pass Parameters
$options="get_user=0";
if ($get_return_name && $get_return_URL) {
  $decode_return_url = DecodeReturnURL($get_return_URL);
  $options=$options."&get_return_URL=".$get_return_URL."&get_return_name=".$get_return_name;
  //print("<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>$options");
}
$return_url = EncodeReturnURL("$PHP_SELF?$options");


$smarty->assign('Rows_Returned', $Rows_Returned);
$smarty->assign('Correct', $Correct);
$smarty->assign('Incorrect', $Incorrect);
$smarty->assign('Answer_A', $Answer_A);
$smarty->assign('Answer_B', $Answer_B);
$smarty->assign('Answer_C', $Answer_C);
$smarty->assign('Answer_D', $Answer_D);
$smarty->assign('Answer_E', $Answer_E);
$smarty->assign('Understood', $Understood);
$smarty->assign('HR_Contact', $HR_Contact);

// Display the template with all assign values
$smarty->assign('uiTheme', $_SESSION['uiTheme']);
$smarty->display('admin/question_analysis2.tpl');


?>
