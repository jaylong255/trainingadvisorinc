<?php

// File Manager

// Include libs which starts session and creates smarty objects
require_once ('../includes/common.php');
require_once ('../includes/Config.php');
require_once ('../includes/FileManager.php');

// Ensure user has logged in, otherwise exit now
if (!isset($_SESSION['userId'])) {
  header("Location: /desktop/login/expired.php");
  exit(0);
}

// Ensure user is superuser or an org admin
if (!(isset($_SESSION['superUser']) && $_SESSION['superUser']) &&
    !(isset($_SESSION['role']) && $_SESSION['role'] != ROLE_END_USER &&
      $_SESSION['role'] != ROLE_CLASS_CONTACT_SUPERVISOR)) {
  header("Location: /desktop/login/perm_denied.php");
  exit(0);
}

$config = new Config($_SESSION['orgId']);
$fm = new FileManager(APPLICATION_ROOT.'/organizations/'.$config->GetValue(CONFIG_ORG_DIRECTORY));
$nodes = $fm->BuildTree();


$smarty->assign('currentTab', $_SESSION['currentTab']);
$smarty->assign('currentSubTab', $_SESSION['currentSubTab']);
$smarty->assign('orgName', $config->GetValue(CONFIG_ORG_NAME));

$smarty->assign('nodes', $nodes);
$smarty->assign('uiTheme', $_SESSION['uiTheme']);
$smarty->display('admin/file_man_nav.tpl');

?>