<?php
/////////////////////////////////////////////////////////////////////////////
//  question_list.php
//	Page used for administration of questions for the current company.
//
//  Get parameters:



/////////////////////////////////////////////////////////////////////////////
//	question_list.php
//		Page used for administration of questions stored on the current server
//


// Include libs which starts session and creates smarty objects
require_once ('../includes/common.php');
require_once ('../includes/Question.php');
require_once ('../includes/Assignment.php');

// Ensure user has logged in, otherwise exit now
if (!isset($_SESSION['userId'])) {
  header("Location: /desktop/login/expired.php");
  exit(0);
}

// Ensure user is superuser
if (!(isset($_SESSION['superUser']) && $_SESSION['superUser']) &&
    !(isset($_SESSION['role']) && $_SESSION['role'] != ROLE_END_USER &&
      $_SESSION['role'] != ROLE_CLASS_CONTACT_SUPERVISOR)) {
  header("Location: /desktop/login/perm_denied.php");
  exit(0);
}


// Specify this as the current tab
$_SESSION['currentTab'] = TAB_QUESTION;
$_SESSION['currentSubTab'] = SUBTAB_QUESTION_CURRENT;


if (isset($_REQUEST['searchBy'])) {
  $searchBy = $_REQUEST['searchBy'];
} else {
  $searchBy = QUESTION_LIST_COLUMN_TITLE;
}

if (isset($_REQUEST['searchFor'])) {
  $searchFor = $_REQUEST['searchFor'];
  $_SESSION['questionPageNumber'] = 1;
} else {
  $searchFor = '';
}

$createLink = '';
if (isset($_REQUEST['get_archived'])) {
  $archived = $_REQUEST['get_archived'];
  $_SESSION['currentSubTab'] = SUBTAB_QUESTION_ARCHIVE;
} else {
  $archived = FALSE;
  $createLink = new TplCaption('Create New Question');
  $createLink->href='question_edit.php';
}


// If the questionId is set, it means we are deleting that question
if (isset($_REQUEST['questionId']) && $_REQUEST['questionId'] &&
    is_numeric($_REQUEST['questionId'])) {

  $questionList = new Question($_SESSION['orgId'], $_REQUEST['questionId']);

  if (isset($_REQUEST['get_archived']) && $_REQUEST['get_archived']) {
    if (isset($_REQUEST['versionDate']))
      $questionList->versionDate = $_REQUEST['versionDate'];
    if (isset($_REQUEST['domainId']))
      $questionList->domainId = $_REQUEST['domainId'];
    if (isset($_REQUEST['restore']) && $_REQUEST['restore'])
      $questionList->UnArchiveQuestion();
    else {
      $questionList->DeleteArchivedQuestion();
    }
  } else {
    $deleteOnArchive = TRUE;
    $questionList->ArchiveQuestion($deleteOnArchive);
    $assignments = new Assignment($_SESSION['orgId']);
    $assignments->DeleteUnansweredAssignments($_REQUEST['questionId']);
    $tracks = new Track($_SESSION['orgId']);
    $trackIds = $tracks->GetTrackIdsWithAssignedQuestion($_REQUEST['questionId']);
    foreach($trackIds as $trackId) {
      $tracks->SetTrackId($trackId);
      $tracks->UnassignQuestion($_REQUEST['questionId']);
    }
  }
}


// create new employee list
$errMsg = '';
$rc = 0;
$firstRow = 0;
$lastRow = 0;
$numRows = 0;
$numPages = 0;
$questionIds = array();
$questions = array();
$questionList = new Question($_SESSION['orgId']);
$rc = $questionList->GetQuestionList($_SESSION['questionShowColumns'], $searchBy, $searchFor,
				     $_SESSION['questionSortColumn'], $_SESSION['questionSortDescending'],
				     $_SESSION['questionPageNumber'], $_SESSION['rowsPerPage'],
				     $_SESSION['userId'], $archived);


if ($rc === FALSE) {
  echo "Failed to get employee list\n";
  exit(1);
}


if (DEBUG & DEBUG_FORM) {
  $smarty->assign('GetQuestionList', $rc);
}


if (!empty($rc)) {
  list($questionIds, $questions) = $rc;
  $questionList->GetRowCounts($firstRow, $lastRow, $numRows);
  $numPages = $questionList->GetPageCount($_SESSION['rowsPerPage']);
}


// setup links across the top of the page
$smarty->assign('topLinkList', $createLink);

$smarty->assign('currentTab', $_SESSION['currentTab']);
$smarty->assign('currentSubTab', $_SESSION['currentSubTab']);
$smarty->assign('columnLabels', $questionList->GetColumnLabels());
$smarty->assign('showColumns', $_SESSION['questionShowColumns']);

// Set Search params
$smarty->assign('searchBy', $searchBy);
$smarty->assign('searchFor', $searchFor);

// Set the one to be selected
$smarty->assign('sortColumn', $_SESSION['questionSortColumn']);
$smarty->assign('sortDescending', $_SESSION['questionSortDescending']);

$smarty->assign('firstRow', $firstRow);
$smarty->assign('lastRow', $lastRow);
$smarty->assign('numRows', $numRows);
$smarty->assign('pageNumber', $_SESSION['questionPageNumber']);
$smarty->assign('numPages', $numPages);
$smarty->assign('recordList', $questions);
$smarty->assign('recordIds', $questionIds);

if (DEBUG) {
  $smarty->assign('ErrMsg', $questionList->GetErrorMessage());
}

$questionList->Close();

$smarty->assign('toggleParamName', 'toggleQuestionColumn');
$smarty->assign('sortParamName', 'questionSortColumn');
$smarty->assign('idParamName', 'questionId');
$smarty->assign('pageNumberParamName', 'questionPageNumber');
$smarty->assign('orderParamName', 'questionSortDescending');
$smarty->assign('showSelectedColumn', FALSE);
$smarty->assign('rowsPerPage', $_SESSION['rowsPerPage']);
$smarty->assign('localHeader', 'question_header');
$smarty->assign('uiTheme', $_SESSION['uiTheme']);
$smarty->display('admin/generic_list.tpl');


exit(0);

?>
