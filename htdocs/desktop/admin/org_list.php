<?php
/////////////////////////////////////////////////////////////////////////////
//	organization_list.php
//		Page used for administration of organizations stored on the current server
//


// Include libs which starts session and creates smarty objects
require_once ('../includes/common.php');
require_once ('../includes/Config.php');
require_once ('../includes/Organization.php');
require_once ('../includes/TplClasses.php');

// Ensure user has logged in, otherwise exit now
if (!isset($_SESSION['userId'])) {
  //    echo "Required session login information not present.";
  header("Location: /desktop/login/expired.php");
  exit;
}

// Ensure user is superuser
if (!isset($_SESSION['superUser']) || !$_SESSION['superUser']) {
  header("Location: /desktop/login/perm_denied.php");
}


// create new organization list
$errMsg = '';
$rc = 0;
$firstRow = 0;
$lastRow = 0;
$numRows = 0;
$numPages = 0;
$orgIds = array();
$orgs = array();
$config = NULL;
$orgList = new Organization();
$changeOrg = FALSE;
$rc = 0;

// Specify this as the current tab
$_SESSION['currentTab'] = TAB_ORG;
$_SESSION['currentSubTab'] = SUBTAB_NONE;

if (isset($_REQUEST['searchBy'])) {
  $searchBy = $_REQUEST['searchBy'];
} else {
  $searchBy = ORG_LIST_COLUMN_NAME;
}

if (isset($_REQUEST['searchFor'])) {
  $searchFor = $_REQUEST['searchFor'];
  $_SESSION['orgPageNumber'] = 1;
} else {
  $searchFor = '';
}

if (isset($_REQUEST['changeOrg']) && $_REQUEST['changeOrg']) {
  $changeOrg = TRUE;
}

// If an orgId was passed into us then delete button was clicked so delete it
if (isset($_REQUEST['orgId'])) {

  // Validation check on the orgnaization
  if (!is_numeric($_REQUEST['orgId'])) {
    header("Location: /desktop/login/security.php");
    exit(0);
  }

  $config = new Config($_REQUEST['orgId']);
  $org = new Organization($_REQUEST['orgId']);
  if (!strlen($config->GetValue(CONFIG_ORG_DIRECTORY))) {
    $smarty->assign('superUser', $_SESSION['superUser']);
    $smarty->assign('role', $_SESSION['role']);
    $smarty->assign('trackAdmin', $_SESSION['trackAdmin']);
    $smarty->assign('errMsg', 'Unable to get org directory for org('.$_SESSION['orgId'].') from config, see error log');
    $smarty->assign('create', FALSE);
    $smarty->assign('currentTab', $_SESSION['currentTab']);
    $smarty->assign('currentSubTab', SUBTAB_ORG_DELETE);
    $smarty->assign('uiTheme', $_SESSION['uiTheme']);
    $smarty->display('admin/org_delete.tpl');
    exit(1);
  }

  $rc = $org->DeleteOrg($config->GetValue(CONFIG_ORG_DIRECTORY));
  if ($rc != RC_OK) {
    $smarty->assign('superUser', $_SESSION['superUser']);
    $smarty->assign('role', $_SESSION['role']);
    $smarty->assign('trackAdmin', $_SESSION['trackAdmin']);
    $smarty->assign('errMsg', $org->RcToText($rc));
    $smarty->assign('create', FALSE);
    $smarty->assign('currentTab', $_SESSION['currentTab']);
    $smarty->assign('currentSubTab', SUBTAB_ORG_DELETE);
    $smarty->assign('uiTheme', $_SESSION['uiTheme']);
    $smarty->display('admin/org_delete.tpl');
    exit(1);
  }

  // After organization successfully deleted, default back to the prototype database
  header("Location: /desktop/login/change_prefs.php?orgId=8&returnUrl=../admin/org_list.php");
  exit(0);
}

$rc = $orgList->GetOrganizationList($_SESSION['orgShowColumns'], $searchBy, $searchFor,
				    $_SESSION['orgSortColumn'], $_SESSION['orgSortDescending'],
				    $_SESSION['orgPageNumber'], $_SESSION['rowsPerPage']);
$config = new Config($_SESSION['orgId']);

if ($rc === FALSE) {
  echo "Failed to get Organization List\n";
  exit(1);
}

if (DEBUG & DEBUG_SMARTY) {
  $smarty->assign('debug_numPages', $numPages);
}

if (!empty($rc)) {
  list($orgIds, $orgs) = $rc;
  $orgList->GetRowCounts($firstRow, $lastRow, $numRows);
  $numPages = $orgList->GetPageCount($_SESSION['rowsPerPage']);
}


$createLink = new TplCaption('Create new organization');
$createLink->href='org_info.php';
$createLink->params='create=1';


// setup links across the top of the page
$smarty->assign('topLinkList', $createLink);

// This alone is enough information to identify a search column
$smarty->assign('columnLabels', $orgList->GetColumnLabels($_SESSION['orgShowColumns']));
$smarty->assign('showColumns', $_SESSION['orgShowColumns']);

// Keep the selected search by field
$smarty->assign('searchBy', $searchBy);

// Set the one to be selected
$smarty->assign('sortColumn', $_SESSION['orgSortColumn']);
$smarty->assign('sortDescending', $_SESSION['orgSortDescending']);


// TODO: make this dynamic by adding an organization column to the Organization table
// and a widget in the admin to control display of logo
$smarty->assign('displayLogo', TRUE);

$smarty->assign('firstRow', $firstRow);
$smarty->assign('lastRow', $lastRow);
$smarty->assign('numRows', $numRows);
$smarty->assign('pageNumber', $_SESSION['orgPageNumber']);
$smarty->assign('numPages', $numPages);
$smarty->assign_by_ref('recordList', $orgs);
$smarty->assign_by_ref('recordIds', $orgIds);
$smarty->assign('recordId', $_SESSION['orgId']);
$smarty->assign('orgName', $config->GetValue(CONFIG_ORG_NAME));

$orgList->SetOrgId($_SESSION['orgId']);

SetupMotifDisplay($smarty, $config);

if (DEBUG) {
  $smarty->assign('ErrMsg', $orgList->GetErrorMessage());
}

$orgList->Close();

$smarty->assign('changeOrg', $changeOrg);

$smarty->assign('currentTab', $_SESSION['currentTab']);
$smarty->assign('currentSubTab', $_SESSION['currentSubTab']);
$smarty->assign('toggleParamName', 'toggleOrgColumn');
$smarty->assign('sortParamName', 'orgSortColumn');
$smarty->assign('createLinkLabel', 'organization');
$smarty->assign('createLink', 'org_info.php');
$smarty->assign('editLink', 'org_info.php');
$smarty->assign('deleteLink', 'org_delete.php');
$smarty->assign('idParamName', 'orgId');
$smarty->assign('pageNumberParamName', 'orgPageNumber');
$smarty->assign('orderParamName', 'orgSortDescending');
$smarty->assign('showSelectedColumn', TRUE);
$smarty->assign('rowsPerPage', $_SESSION['rowsPerPage']);
$smarty->assign('editLinkLabel', 'Edit');
$smarty->assign('deleteLinkLabel', 'Delete');
$smarty->assign('localHeader', 'org_header');
$smarty->assign('uiTheme', $_SESSION['uiTheme']);
$smarty->display('admin/generic_list.tpl');

?>
