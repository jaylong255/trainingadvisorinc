<?php
	session_start();
?>

<html>
<head>
<title>Employee Form</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<?php
	$Host = "localhost";
	$User = "HR_User";
	$Password = "HR_Tools";
	$DBName = "HR_Tools_Authentication";
	$Link;
	$logon_ErrMsg;

	function OpenDatabase()
	{
		global $Host;
		global $User;
		global $Password;
		global $Link;

		$Link = mysql_connect($Host,$User,$Password);
	}

	function CloseDatabase()
	{
		global $Link;
		mysql_close($Link);
	}

	function Retreive_Items($TableName,$ColumnName,$Item)
	{
		OpenDatabase();

		global $Link;
		global $DBName;

		$Query = "SELECT * from $TableName";
		$Result = mysql_db_query($DBName,$Query,$Link);
		while($Row = mysql_fetch_array($Result))
		{
			if(!strcasecmp($Row["id"],$Item)) print("<option selected>$Row[$ColumnName]</option>\n");
			else print("<option>$Row[$ColumnName]</option>\n");
		}

		CloseDatabase();
	}
	

	function Validate_Form()
	{
		OpenDatabase();

		global $DBName;
		global $Link;
		global $ErrMsg;
		
		global $F_User_ID;
		global $F_First_Name;
		global $F_Last_Name;
		global $F_SSN;
		global $F_Password;
		global $F_Email;
		global $F_Phone;
		global $F_Employment_Status_ID;
		global $F_Language_ID;
		global $F_Track_ID;
		global $F_Contact_ID;
		global $F_State_ID;
		global $F_Use_Supervisor;
		global $F_Is_HR_Contact;
		global $F_Is_Administrator;
		global $F_Department_ID;
		global $F_Division_ID;
		global $F_Custom1_ID;
		global $F_Custom2_ID;
		global $F_Custom3_ID;

		if (strcmp($FormAction,"New")==0)
		{
			$Query = "SELECT * FROM Student_Info WHERE ssn='123456789'";
			$Result = mysql_db_query($DBName,$Query,$Link);
			$Row = mysql_fetch_array($Result);
			if(!$Row)
			{
				$Query = "INSERT into Student_Info (ssn,fname,lname) VALUES ('123456789','','')";
				$Result = mysql_db_query($DBName,$Query,$Link);
				if(!$Result)
				{
					$Err_Msg="Could not write the student information to the database.";
				}
				else
				{
					$User_ID = mysql_insert_id();
					$IDNumber = "123456789";
					$F_First_Name = "";
					$F_Last_Name = "";
					
					$Err_Msg="A new student was successfully added. Please modify the form values above and click the \"Update\" button to commit your changes.";
					return TRUE;
				}
			}
			else
			{
				$User_ID = $Row["id"];
				$UserItem=$User_ID;
				$IDNumber = $Row["ssn"];
				$FirstName = $Row["fname"];
				$LastName = $Row["lname"];
				$Class_ID = $Row["class_id"];
				$Level_ID = $Row["level_id"];
				$State_ID = $Row["student_status_id"];

				$Query = "SELECT * FROM Level WHERE id='$Level_ID'";
				$Result = mysql_db_query($DBName,$Query,$Link);
				$Row = mysql_fetch_array($Result);
				$Level = $Row['entry'];

				$Query = "SELECT * FROM Class WHERE id='$Class_ID'";
				$Result = mysql_db_query($DBName,$Query,$Link);
				$Row = mysql_fetch_array($Result);
				$Class = $Row['name'];

				$Query = "SELECT * FROM Level WHERE entry='Supervisor'";
				$Result = mysql_db_query($DBName,$Query,$Link);
				$Row = mysql_fetch_array($Result);
				$Supervisor_ID = $Row['id'];

				$Query = "SELECT * FROM Student_Info WHERE class_id='$Class_ID' AND level_id='$Supervisor_ID'";
				$Result = mysql_db_query($DBName,$Query,$Link);
				$Row = mysql_fetch_array($Result);

				if($Row['id']>0)  $Supervisor_Name = $Row['fname'] ." ". $Row['lname'];
				else $Supervisor_Name="";

				$Err_Msg="Please modify the form values above and click the \"Update\" button to commit your changes.";
				return TRUE;
			}
		}
		else if (strcmp($FormAction,"Delete")==0)
		{
			if($User_ID)
			{
				$Query = "SELECT * FROM User WHERE User_ID='$F_User_ID'";
				$Result = mysql_db_query($DBName,$Query,$Link);
				$Row = mysql_fetch_array($Result);
				if(!$Row)
				{
					$Query = "Delete FROM User WHERE id='$F_User_ID'";
					if(mysql_db_query($DBName,$Query,$Link))
					{
						$Err_Msg="Student was succesfully deleted.  Select a Student by SSN, or click the \"New\" button.";
						$User_ID = "";
						$UserItem = "";
						$IDNumber = "";
						$FirstName = "";
						$LastName = "";
						$Class = "";
						$Class_ID = "";
						$State = "";
						$State_ID = "";
						$Level = "";
						$Level_ID = "";
						return TRUE;
					}
					else $Err_Msg="Could not delete this Student from the Database.";
				}
				else $Err_Msg="There is course information for this student in the database.  Deleting this student profile will remove data saved for this student in the databaes.";
			}
		}
		else if (strcmp($FormAction,"Update")==0)
		{
			$Query = "SELECT * FROM Student_Status WHERE entry='$State'";
			$Result = mysql_db_query($DBName,$Query,$Link);
			$Row = mysql_fetch_array($Result);
			$State_ID = $Row['id'];

			$Query = "SELECT * FROM Level WHERE entry='$Level'";
			$Result = mysql_db_query($DBName,$Query,$Link);
			$Row = mysql_fetch_array($Result);
			$Level_ID = $Row['id'];

			$Query = "SELECT * FROM Class WHERE name='$Class'";
			$Result = mysql_db_query($DBName,$Query,$Link);
			$Row = mysql_fetch_array($Result);
			$Class_ID = $Row['id'];

			$Query = "SELECT * FROM Level WHERE entry='Supervisor'";
			$Result = mysql_db_query($DBName,$Query,$Link);
			$Row = mysql_fetch_array($Result);
			$Supervisor_ID = $Row['id'];

			$Query = "SELECT * FROM Student_Info WHERE class_id='$Class_ID' AND level_id='$Supervisor_ID'";
			$Result = mysql_db_query($DBName,$Query,$Link);
			$Row = mysql_fetch_array($Result);
			$ClassSupervisor_ID = $Row['id'];

			if($ClassSupervisor_ID>0)  $Supervisor_Name = $Row['fname'] ." ". $Row['lname'];
			else{$ClassSupervisor_ID=0; $Supervisor_Name="";}

			print("$ClassSupervisor_ID with $User_ID");
			if(strcasecmp($Level,"Supervisor")==0)
			{
				if($ClassSupervisor_ID<>0)
				{
					if(strcasecmp($User_ID."",$ClassSupervisor_ID."")<>0)
					{
						$Err_Msg = "Could not update student information. This Training Group already has a Supervisor assigned to it. Please change the Training Group or Level for this student.";
						return FALSE;
					}
				}
			}


			$Query = "UPDATE Student_Info SET ssn='$IDNumber',fname='$FirstName', lname='$LastName', class_id='$Class_ID', student_status_id='$State_ID', level_id='$Level_ID' WHERE id='$User_ID'";
			if(mysql_db_query($DBName,$Query,$Link))
			{
				$Query = "SELECT * FROM Student_Info WHERE class_id='$Class_ID' AND level_id='$Supervisor_ID'";
				$Result = mysql_db_query($DBName,$Query,$Link);
				$Row = mysql_fetch_array($Result);
				if($Row['id']>0)  $Supervisor_Name = $Row['fname'] ." ". $Row['lname'];
				else $Supervisor_Name="";

				$Err_Msg="Student information was successfully updated.";
				return TRUE;
			}
			else $Err_Msg = "Unable to update Student information.";

		}
		else if (strcmp($FormAction,"Change")==0)
		{

				$Query = "SELECT * FROM Student_Info WHERE ssn='$UserItem'";
				$Result = mysql_db_query($DBName,$Query,$Link);
				$Row = mysql_fetch_array($Result);

				$User_ID = $Row["id"];
				$IDNumber = $Row["ssn"];
				$UserItem = $IDNumber;
				$FirstName = $Row["fname"];
				$LastName = $Row["lname"];
				$Class_ID = $Row["class_id"];
				$Level_ID = $Row["level_id"];
				$State_ID = $Row["student_status_id"];

				$Query = "SELECT * FROM Level WHERE id='$Level_ID'";
				$Result = mysql_db_query($DBName,$Query,$Link);
				$Row = mysql_fetch_array($Result);
				$Level = $Row['entry'];

				$Query = "SELECT * FROM Class WHERE id='$Class_ID'";
				$Result = mysql_db_query($DBName,$Query,$Link);
				$Row = mysql_fetch_array($Result);
				$Class = $Row['name'];

				$Query = "SELECT * FROM Level WHERE entry='Supervisor'";
				$Result = mysql_db_query($DBName,$Query,$Link);
				$Row = mysql_fetch_array($Result);
				$Supervisor_ID = $Row['id'];

				$Query = "SELECT * FROM Student_Info WHERE class_id='$Class_ID' AND level_id='$Supervisor_ID'";
				$Result = mysql_db_query($DBName,$Query,$Link);
				$Row = mysql_fetch_array($Result);
				if($Row['id']>0)  $Supervisor_Name = $Row['fname'] ." ". $Row['lname'];
				else $Supervisor_Name="";


				//print("Values=$User_ID,$IDNumber,$FirstName,$Class_ID,$Level_ID,$State_ID");
				return TRUE;
		}
		else $Err_Msg = "Illegal Form Action.";

		return FALSE;
	}

	//////////////////////////////////////////////
	//Start/Restart PHP Page
	//////////////////////////////////////////////
	if($Insert==TRUE)
	{
		if(Validate_Form())
		{
			 $ErrMsg = "Form Action";
	 	     print('<script type="text/javascript">');
	 		 //print("alert('$FormAction');");
			 print('</script>');
		}
		CloseDatabase();
	}

?>


<style type="text/css">
.FormText {  font-family: Arial, Helvetica, sans-serif; font-size: 10pt; color: #0000CC}
</style>
</head>

<script language="JavaScript">
function MM_findObj(n, d) { //v3.0
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
}

function New()
{
  	FormObj = MM_findObj("AdminForm");
 	FormObj.action="user.php?FormAction=New";
  	FormObj.submit();
}

function Delete()
{
	  if(confirm("Are you sure that you want to delete this record?"))
	  {
		  FormObj = MM_findObj("Form");
		  FormObj.action="user.php?FormAction=Delete";
		  FormObj.submit();
	  }
}

function Update()
{

	  IDobj = MM_findObj("IDNumber");
	  if(IDobj.value){
	  	  if(isNumberString (IDobj.value))
	  	  {
	  	  	FormObj = MM_findObj("AdminForm");
		  	FormObj.action="user.php?FormAction=Update";
		  	FormObj.submit();
		  }
	   	  else alert("Please enter a nummeric value in the ID Number for this User.");
	  }
	  else alert("There is no ID Number .");
}

function ChangeItem()
{
	  FormObj = MM_findObj("AdminForm");
	  FormObj.action="user.php?FormAction=Change";
	  FormObj.submit();
}

function isNumberString (InString)
{
	if(InString.length==0) return (false);
	var RefString="1234567890";
	for (Count=0; Count < InString.length; Count++)
	{
		TempChar= InString.substring (Count, Count+1);
		if (RefString.indexOf (TempChar, 0)==-1)
		return (false);
	}
	return (true);
}
</script>

<body bgcolor="#FFFCED">
<form name="form1" method="post" action="">
  <div id="Layer1" style="position:absolute; left:35px; top:23px; width:302px; height:31px; z-index:1"> 
    <table border="0">
    <tr>
      <td class="FormText">User ID: &nbsp;&nbsp;</td>
      <td> <input type="text" name="F_UserId" onClick="alert('This field is automatically generated.');" disabled ></td>
	</tr>
    <tr>
        <td class="FormText" height="29">First Name: &nbsp;&nbsp;</td>
        <td height="29"> 
          <input type="text" name="F_FName" maxlength="45" size="30">
        </td>
	</tr>
	<tr>
      <td class="FormText">Last Name: &nbsp;&nbsp;</td>
      <td> 
          <input type="text" name="F_LName" maxlength="45" size="30">
        </td>
    </tr>
	<tr>
        <td class="FormText">
          Last 6 digits<br>of SS#: &nbsp;&nbsp;
          </td>
      <td> 
          <input type="text" name="F_SSN" maxlength="6" size="10">
        </td>
    </tr>
    <tr>
        <td class="FormText" height="29">Password: &nbsp;&nbsp;</td>
        <td height="29"> 
          <input type="password" name="F_Password" size="30" maxlength="45">
        </td>
	</tr>
	<tr>
        <td class="FormText">E-Mail:</td>
      <td> 
          <input type="text" name="F_Email" maxlength="30" size="30">
        </td>
    </tr>
	<tr>
        <td class="FormText">Phone #:</td>
      <td> 
          <input type="text" name="F_Phone" size="30" maxlength="30">
        </td>
    </tr>

  </table>
</div>
  <div id="Layer2" style="position:absolute; left:373px; top:23px; width:331px; height:178px; z-index:2"> 
    <table border="0">
	<tr>
        <td class="FormText">Employment Status: &nbsp;&nbsp;</td>
      <td>
          <select name="F_EmpStatus">
          </select>
      </td>
    </tr>
    <tr>
        <td class="FormText">Language: &nbsp;&nbsp;</td>
      <td>
          <select name="F_Language">
          </select>
      </td>
	</tr>
    <tr>
        <td class="FormText">Track: &nbsp;&nbsp;</td>
      <td>
          <select name="F_Track">
          </select>
      </td>
	</tr>
	<tr>
        <td class="FormText">HR Contact: &nbsp;&nbsp;</td>
      <td>
          <select name="F_Contact" size="1">
          </select>
      </td>
    </tr>
	<tr>
        <td class="FormText">State: &nbsp;&nbsp;</td>
      <td>
          <select name="F_State">
          </select>
      </td>
    </tr>
  </table>
</div>

<div id="Layer3" style="position:absolute; left:370px; top:166px; width:338px; height:31px; z-index:3"> 
  <table border="0">
	<tr>      
      <td class="FormText">&nbsp;</td>
      <td class="FormText">Yes</td>
    </tr>
	<tr>      
      <td class="FormText">Use the Supervisor as the HR Contact?: &nbsp;&nbsp;</td>
      <td><input type="checkbox" name="F_bSupervisor" value="checkbox">
      </td>
    </tr>
	<tr>       
      <td class="FormText">Is this user an HR Contact?: &nbsp;&nbsp;</td>
      <td><input type="checkbox" name="F_bHRContact" value="checkbox">
      </td>
    </tr>
	<tr>       
      <td class="FormText">Is this user a System Administrator?: &nbsp;&nbsp;</td>
      <td><input type="checkbox" name="F_bAdministrator" value="checkbox">
      </td>
    </tr>
 </table>
</div>

</form>
</body>
</html>
