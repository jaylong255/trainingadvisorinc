<?php

// Libs
require_once("../includes/common.php");
require_once("../includes/RecurrenceObject.php");
require_once('../includes/Track.php');

// Ensure user has logged in, otherwise exit now
if (!isset($_SESSION['userId'])) {
  header("Location: /desktop/login/expired.php");
  exit;
}

// Ensure user is superuser or an org admin or track supervisor
if (!(isset($_SESSION['superUser']) && $_SESSION['superUser']) &&
    !(isset($_SESSION['role']) && $_SESSION['role'] != ROLE_END_USER)) {
  header('Location: /desktop/login/perm_denied.php');
  exit;
}

// Set tab navigation options
$_SESSION['currentSubTab'] = SUBTAB_TRACK_INFO;

// Global variables
$errMsg = '';
$config = new Config($_SESSION['orgId']);
$track = new Track($_SESSION['orgId']);


// Track form is getting submitted or specified track edited
if (isset($_REQUEST['trackId'])) {

  // Load existing track data
  if ($_REQUEST['trackId'] != 'To Be Assigned') {

    // Validate existing user id to make sure form content was not altered
    if (!is_numeric($_REQUEST['trackId'])) {
      error_log("Redirecting to permission deined due ot non-numeric trackId");
      header("Location: /desktop/login/perm_denied.php");
      exit;
    }

    $track->SetTrackId($_REQUEST['trackId']);

    // Make sure if this is only a track admin this is the track they are super for
    if ($_SESSION['role'] == ROLE_CLASS_CONTACT_SUPERVISOR && $_SESSION['userId'] != $track->supervisorId) {
      error_log("Redirecting to track denied for trackId(".$track->trackId);
      header("Location: /desktop/login/perm_denied.php?trackId=".$track->trackId);
      exit;
    }

  }

  if (isset($_REQUEST['isAb1825']) && $_REQUEST['isAb1825'] == 1)
    $track->isAb1825 = TRUE;
  else
    $track->isAb1825 = FALSE;

  // Process form data
  if (isset($_REQUEST['trackName'])) {
    
    if (DEBUG & DEBUG_FORM)
      error_log("Processing submitted track form for trackId=".$track->trackId);
      
    $track->trackName = $_REQUEST['trackName'];
    $track->supervisorId = $_REQUEST['supervisorId'];
    $track->emailReplyTo = $_REQUEST['emailReplyTo'];
    $track->startDate = $_REQUEST['startDate'];
    $track->recurrence = $_REQUEST['recurrence'];
    //$track->trackNewsUrl = $_REQUEST['trackNewsUrl'];
    $track->delinquencyNotification = $_REQUEST['delinquencyNotification'];
    if (isset($_REQUEST['isManagement']) && $_REQUEST['isManagement'] == 1)
      $track->isManagement = TRUE;
    else
      $track->isManagement = FALSE;
    
    // track binding is now only set when track created
    if (!is_numeric($_REQUEST['trackId'])) {
      if ((isset($_REQUEST['isBinding']) && $_REQUEST['isBinding'] == 1) ||
	  (!isset($_REQUEST['isBinding']) && $track->isAb1825))
	$track->isBinding = TRUE;
      else
	$track->isBinding = FALSE;
    }

    if (isset($_REQUEST['isLooping']) && $_REQUEST['isLooping'] == 1)
      $track->isLooping = TRUE;
    else
      $track->isLooping = FALSE;
    
    if ((isset($_REQUEST['groupAssignments']) && is_numeric($_REQUEST['groupAssignments']) &&
	 $_REQUEST['groupAssignments'] == 1) ||
	(!isset($_REQUEST['groupAssignments']) && $track->isAb1825))
      $rc = $track->UpdateInfo(1);
    else
      $rc = $track->UpdateInfo();

    if ($rc != RC_OK) {
	error_log("Failed to update track with trackId ".$track->trackId);
	$errMsg .= RcToText($rc);
    }


  } else {
    
    // Load existing track data for editing
    $track->SetTrackId($_REQUEST['trackId']);
    //    error_log("ab1825 value for track: ".$_REQUEST['trackId']." ".$track->isAb1825);

  }

  $_SESSION['trackId'] = $track->trackId;

} else {

  // This will cover us when a user clicks on the Info tab
  // from one of the other tabs, the trackId is not available
  // except from the session variable
  // If it was in fact passed then it will get set above.
  if (!isset($_REQUEST['create']) && isset($_SESSION['trackId']))
    $track->SetTrackId($_SESSION['trackId']);

}

if ($config->GetValue(CONFIG_LICENSED_AB1825_CONTENT)) {
  $smarty->assign('licenseAb1825', TRUE);
} else {
  $smarty->assign('licenseAb1825', FALSE);
}
     

$smarty->assign('track', $track);
$smarty->assign('currentTab', $_SESSION['currentTab']);
$smarty->assign('currentSubTab', $_SESSION['currentSubTab']);
$smarty->assign('uiTheme', $_SESSION['uiTheme']);
$smarty->display('admin/track_edit.tpl');


?>