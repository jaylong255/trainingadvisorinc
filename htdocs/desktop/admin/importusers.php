<?php
	session_start();
/////////////////////////////////////////////////////////////////////////////
//	test_data_file.php
//		Test the DataFileObject class

	function makesalt() {
	  srand((double)microtime()*1000000);
	  $saltseeds =
	  array('.','/','0','1','2','3','4','5','6','7','8','9',
	   'a','b','c','d','e','f','g','h','i','j','k','l','m',
	   'n','o','p','q','r','s','t','u','v','w','x','y','z',
	   'A','B','C','D','E','F','G','H','I','J','K','L','M',
	   'N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
	  return $saltseeds[rand(0,63)] . $saltseeds[rand(0,63)];
	}

	/////////////////////////////////////////////////////////////////////////////
	//	output 			//Write the following string to the document and logfile.
	////////////////////////////////////////////////////////////////////////////
	function output($string,$dir)
	{
		$strFileName = "import_users_log.htm";

		$strLogFile = "../organizations/$dir/".$strFileName;

		if(!file_exists($strLogFile))
		{
													// See if the log directory exists, create if needed
				if (!is_dir("../organizations/$dir"))
				{
					$ErrMsg="<br>Directory Does Not Exist";

					if (!is_writable("../organizations/$dir")) {
						$ErrMsg="NOTICE: Unable to create log directory, no log file will be saved.<br>";
					}
					$oldumask = umask(0);
					if (!mkdir("../organizations/$dir", 0777)) {
						$ErrMsg="NOTICE: Unable to create log directory, no log file will be saved.<br>";
					}
					umask($oldumask);
				}
													// Ensure log directory is writable
				if (!is_writable("../organizations/$dir")) {
					$ErrMsg="NOTICE: File access to log folder not allowed on server, no log file will be saved.<br>";
				}

													// Create a new empty log file
				$fp = fopen($strLogFile, "w");
				if ($fp != FALSE)
					fclose($fp);
		}

		if (file_exists($strLogFile))
		{

			if (!is_writable("../organizations/$dir/".$strFileName))
			{
				$ErrMsg = "NOTICE: File access to report folder not allowed on server, no log report will be created.";
			}

					// Attempt to append to the log file
			$fp = fopen($strLogFile, "a");
			if ($fp != FALSE)
			{
				fwrite($fp, $string);
				fclose($fp);
			}
			else $ErrMsg = "<br>NOTICE: File could not be appended.";
		}
		//print("<br><br><br><br><br><br><br><br><br><br><br><br><br>Error=$ErrMsg");

	}

	$Link;
	function OutputString( $textClass, $String )
	{
		global $session_Organization_Directory;

		echo "<span class='".$textClass."'>".$String."</span>";
		output($String,"$session_Organization_Directory/reports");
	}

	OpenDatabase();

	echo "<html>";
	echo "<head>";
	echo "<title>Import Users</title>";
	echo "<style type='text/css'>";
	echo "	.body{ scrollbar-base-color: #EAD8B2 }";
	echo "	.error {  font-family: Arial, Helvetica, sans-serif; font-size: 9pt; color: #FE3000; font-weight: bold; text-decoration: none}";
	echo "	.success {  font-family: Arial, Helvetica, sans-serif; font-size: 9pt; color: #097B27; font-weight: bold; text-decoration: none}";
	echo "</style>";
	echo "</head>";
	echo "<body bgcolor='#FFFCED'>";

	OutputString("success", "<br>-----------------------------------------------------<br>Run Date & Time: ".date("Y/m/d H:i:s")."<br>-----------------------------------------------------<br>");

    include 'DataFileObject.php';
	$objData = new DataFileObject("../organizations/".$session_Organization_Directory."/reports/import_users.txt");
	$objData->SpecifyColumns(array("Password", "SSN","FName","LName","Email","UseSuperv","IsHRContact","LangID","EmployStatusID","StateAbbrv","TrackID","Phone","IsAdmin"));

	$LineNumber = 0;
	while ($objData->GetNextRecord())
	{
		$LineNumber ++;
		$StateQuery = "SELECT Domain_ID FROM Domain WHERE Abbreviation='".$objData->GetColumn("StateAbbrv")."'";
		$StateResult = mysql_db_query($session_DBName,$StateQuery,$Link);
		$State = mysql_fetch_array($StateResult);
		if($State)
		{
			if(($SSNlen = strlen($objData->GetColumn("SSN"))) >= 6)
			{
				$LastSix = substr(($objData->GetColumn("SSN")), (SSNlen-6), 6);
				$NewLogin_ID = strtolower(substr(($objData->GetColumn("FName")), 0, 1).substr(($objData->GetColumn("LName")), 0, 1)).$LastSix;
				$NewLogin_Query = "SELECT User_ID, Password FROM User WHERE Login_ID='$NewLogin_ID'";
				$NewLogin_Result = mysql_db_query('HR_Tools_Authentication',$NewLogin_Query,$Link);
				$ValidNewLogin = TRUE;

				while($Row = mysql_fetch_array($NewLogin_Result))
				{
					if (crypt($objData->GetColumn("Password"), $Row["Password"]) == $Row["Password"])
					{
						OutputString("error", "Error on line ".$LineNumber.": Unable to insert User ".$objData->GetColumn("FName")." ".$objData->GetColumn("LName")." into the Authentication table. Please use a different User_ID.<br>");
						$ValidNewLogin = FALSE;
						break;
					}
				}
				if($ValidNewLogin)
				{
					$newPass = crypt($objData->GetColumn("Password"), makesalt());
					$AuthQuery="INSERT INTO User (Login_ID, Password, Organization_ID) VALUES('".$NewLogin_ID."', '".$newPass."', ".$session_Organization_ID.")";
					$AuthResult = mysql_db_query("HR_Tools_Authentication",$AuthQuery,$Link);
					if($AuthResult)
					{
						$NewUser_ID = mysql_insert_id();
						$OrgQuery="INSERT INTO User (User_ID, Login_ID, First_Name, Last_Name, Full_Name, Email, Use_Supervisor, Is_HR_Contact, Language_ID, Employment_Status_ID, State_ID, Track_ID) VALUES(".$NewUser_ID.",'".$NewLogin_ID."', '".$objData->GetColumn("FName")."', '".$objData->GetColumn("LName")."', '".$objData->GetColumn("LName").", ".$objData->GetColumn("FName")."', '".$objData->GetColumn("Email")."', ".$objData->GetColumn("UseSuperv").", ".$objData->GetColumn("IsHRContact").", ".$objData->GetColumn("LangID").", ".$objData->GetColumn("EmployStatusID").", ".$State["Domain_ID"].", ".$objData->GetColumn("TrackID").")";
						$OrgResult = mysql_db_query($session_DBName,$OrgQuery,$Link);
						if($OrgResult)
						{
							$HRQuery="INSERT INTO HR_Contact_Info (User_ID, Phone, Is_Admin) VALUES(".$NewUser_ID.",'".$objData->GetColumn("Phone")."',".$objData->GetColumn("IsAdmin").")";
							$HRResult = mysql_db_query($session_DBName,$HRQuery,$Link);
							if($HRResult)
							{
								OutputString("success", "Added User ".$objData->GetColumn("FName")." ".$objData->GetColumn("LName").".<br>");
							}
							else
							{
								OutputString("error", "Error on line ".$LineNumber.": Unable to insert User ".$objData->GetColumn("FName")." ".$objData->GetColumn("LName")." into the HR Contact Information table.<br>");
							}
						}
						else
						{
							OutputString("error", "Error on line ".$LineNumber.": Unable to insert User ".$objData->GetColumn("FName")." ".$objData->GetColumn("LName")." into the Organization table.<br>");
						}
					}
					else
					{
						OutputString("error", "Error on line ".$LineNumber.": Unable to insert User ".$objData->GetColumn("FName")." ".$objData->GetColumn("LName")." into the Authentication table.<br>");
					}
				}
			}
			else
			{
				OutputString("error", "Error on line ".$LineNumber.": User SSN data is incomplete. You must provide at least the last six digits.<br>");
			}
		}
		else
		{
			OutputString("error", "Error parsing data on line ".$LineNumber.".");
		}

	}
	CloseDatabase();
	echo "</body>";
	echo "</html>";
?>