<?php

// File Manager

// Include libs which starts session and creates smarty objects
require_once ('../includes/common.php');
require_once ('../includes/Config.php');
require_once ('../includes/FileManager.php');
require_once ('../includes/agent/agent.php');

// Ensure user has logged in, otherwise exit now
if (!isset($_SESSION['userId'])) {
  header("Location: /desktop/login/expired.php");
  exit(0);
}

// Ensure user is superuser or an org admin
if (!(isset($_SESSION['superUser']) && $_SESSION['superUser']) &&
    !(isset($_SESSION['role']) && $_SESSION['role'] != ROLE_END_USER &&
      $_SESSION['role'] != ROLE_CLASS_CONTACT_SUPERVISOR)) {
  header("Location: /desktop/login/perm_denied.php");
  exit(0);
}


// Global vars
$reloadNavFrame = FALSE;
$isDir = TRUE;
$isEditable = FALSE;
$isEmail = FALSE;
$isImmutable = TRUE;
$isRoot = TRUE;
$showEdit = FALSE;
$showNewsOptions = FALSE;
$publishSelected = FALSE;
$toAddr = '';
$ccAddr = '';
$hdr = '';
$subj = '';
$fileExt = '';
$fileContents = '';
$refreshMotif = FALSE;
$config = new Config($_SESSION['orgId']);
$dirRoot = APPLICATION_ROOT.'/organizations/'.$config->GetValue(CONFIG_ORG_DIRECTORY);
$fm = new FileManager($dirRoot);
$path = '';
//$maxFileSize = ini_get('upload_max_filesize');
$maxFileSize = 20971520;
$errMsg = '';

// Assume root if none specified
if (isset($_REQUEST['path'])) {
  
  if (!empty($_REQUEST['path']) && $_REQUEST['path'] != '/')
    $isRoot = FALSE;

  $path = $_REQUEST['path'];
  if (empty($path))
    $path = '/';

  // See if we should turn on the news options display allowing
  // user to configure the organization news url setting
  if (substr($_REQUEST['path'], 0, 6) == '/news/') {
    $config = new Config($_SESSION['orgId']);
    if (!$config) {
      header("Location: /desktop/login/error.php");
      exit(0);
    }
    $showNewsOptions = TRUE;

    if (isset($_REQUEST['submitNewsFile'])) {
      if (isset($_REQUEST['makeNewsFile']) && $_REQUEST['makeNewsFile'] == 1) {
	$config->SetValue(CONFIG_ORG_NEWS_URL, $fm->FilterPath($_REQUEST['path']));
	//$org->UpdateCustomData();
      } else {
	$org->SetValue(CONFIG_ORG_NEWS_URL, '');
	//$org->UpdateCustomData();
      }
    }

    if ($config->GetValue(CONFIG_ORG_NEWS_URL) == $fm->FilterPath($_REQUEST['path'])) {
      $publishSelected = TRUE;
    }
  }

  if (isset($_REQUEST['sendSubmit'])) {
    // Make sure size was not exceeded
    if (isset($_FILES['sendFile']['size']) &&
	$_FILES['sendFile']['size'] > $maxFileSize) {
      error_log("file_man_edit.php: ".$_FILES['sendFile']['size']." > max allowed of $maxFileSize");
      $errMsg = "The file you attempted to upload exceeds the allowed maximum of $maxFileSize";
    }
    // Check for existance and validation of file upload
    if (isset($_FILES['sendFile']['tmp_name'])) {
      $reloadNavFrame = TRUE;
      if (!$fm->MoveUpload($_FILES['sendFile']['tmp_name'],
			   $dirRoot.$_REQUEST['path'].'/'.$_FILES['sendFile']['name']))
	error_log('file_man_edit.php: File upload of "'.$_FILES['sendFile']['tmp_name'].'" failed');
    } else {
      error_log("file_man_edit.php: No file found for upload");
    }
  } elseif (isset($_REQUEST['renameSubmit'])) {
    // replace everything after last slash with rename
    rename($dirRoot.$fm->FilterPath($_REQUEST['path']),
	   $dirRoot.substr_replace($_REQUEST['path'], $_REQUEST['renameFile'],
				   strrpos($_REQUEST['path'], '/')+1));
    $reloadNavFrame = TRUE;
  } elseif (isset($_REQUEST['createFolderSubmit'])) {
    mkdir($dirRoot.$_REQUEST['path'].'/'.$_REQUEST['createFolder']);
    $reloadNavFrame = TRUE;
  } elseif (isset($_REQUEST['copyMoveSubmit'])) {
    if ($_REQUEST['copyMove'] == 'copy') {
      $cmd = "cp -pR \"".$dirRoot.$fm->FilterPath($_REQUEST['path'])
	."\" \"".$dirRoot.$fm->FilterPath($_REQUEST['copyMoveDest'])."\"";
      //error_log("CMD: $cmd");
      system($cmd);
    } elseif ($_REQUEST['copyMove'] == 'move') {
      $cmd = "mv \"".$dirRoot.$fm->FilterPath($_REQUEST['path'])
	."\" \"".$dirRoot.$fm->FilterPath($_REQUEST['copyMoveDest'])."\"";
      //error_log("CMD: $cmd");
      $path = $_REQUEST['copyMoveDest'].'/'.basename($_REQUEST['path']);
      system($cmd);
    }
    $path = str_replace('//', '/', $path);
    $reloadNavFrame = TRUE;
  } elseif (isset($_REQUEST['deleteSubmit'])) {
    $fm->Unlink($dirRoot.$_REQUEST['path']);
    $pathElms = explode('/', $_REQUEST['path']);
    array_pop($pathElms);
    $path = implode('/', $pathElms);
    $reloadNavFrame = TRUE;
  } elseif (isset($_REQUEST['testEmailSubmit'])) {
    if (isset($_REQUEST['testEmailUser']) && is_numeric($_REQUEST['testEmailUser'])) {
      $user = new User($_SESSION['orgId'], $_REQUEST['testEmailUser']);
      if (preg_match("/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i", $user->email) > 0)
	$toAddr = $user->email;
      else
	$errMsg .= "The contact $user->firstName $user->lastName has an invalid email address "
	  ."($user->email).<BR>Please try again after updating this contact's email address.<BR>\n";      
    }
    if (isset($_REQUEST['testEmailAddr']) && !empty($_REQUEST['testEmailAddr'])) {
      if (preg_match("/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i", $_REQUEST['testEmailAddr']) > 0) {
	if (empty($toAddr))
	  $toAddr = $_REQUEST['testEmailAddr'];
	else
	  $ccAddr = $_REQUEST['testEmailAddr'];
      } else {
	$errMsg .= "Invalid email address entered, please check the email address entered and try again<BR>\n";
      }
    }
    if (empty($toAddr))
      $errMsg .= "No valid email addresses or contacts with valid email addresses were supplied,<BR>"
	." please select a contact with a valid email address or enter one to use for the test.<BR>\n";
    else {
      if (!empty($ccAddr))
	$hdr = "Cc: $ccAddr";
      $subjFileName = str_replace(".html", ".txt", str_replace('Message', 'Subject', $_REQUEST['path']));
      if (file_exists($dirRoot.$subjFileName))
	$subj = file_get_contents($dirRoot.$subjFileName);
      else
	$subj = file_get_contents(APPLICATION_ROOT.'/organizations/ORG2'.$subjFileName);
      $msg = file_get_contents($dirRoot.$_REQUEST['path']);
      $msg = str_replace('__USER_NAME__', 'jdoe', $msg);
      $msg = str_replace('__FIRST_NAME__', 'John', $msg);
      $msg = str_replace('__LAST_NAME__', 'Doe', $msg);
      $msg = str_replace('__TRACK_NAME__', 'Underwater Basket Weaving', $msg);
      $msg = str_replace('__SUPERVISOR__', 'Jane Doe', $msg);
      $msg = str_replace('__NUMBER_UNANSWERED__', '1', $msg);

      if (strtolower(ENV) == 'test')
	$msg = str_replace('__BASE_URL__', 'http://test.trainingadvisorinc.com', $msg);
      else
	$msg = str_replace('__BASE_URL__', 'http://www.trainingadvisorinc.com', $msg);

      if ((int)($_SESSION['orgId']) == 404 || (int)($_SESSION['orgId']) == 405)
	$msg = str_replace('__LOGIN_PATH__', '/desktop/login/phi_login.php', $msg);
      else
	$msg = str_replace('__LOGIN_PATH__', '/desktop/login/login.php', $msg);
      
      $msg = str_replace('__QUERY_PARAMS__', '?User_ID='.$_SESSION['userName'].'&Org_ID='.$_SESSION['orgId'].'&fromEmail=1', $msg);

      if (strlen($config->GetValue(CONFIG_TOP_LEFT_FRONT)) &&
	  file_exists($dirRoot.$config->GetValue(CONFIG_TOP_LEFT_FRONT))) {
	$motif_path = FQ_URL_ROOT.'/organizations/'.$config->GetValue(CONFIG_ORG_DIRECTORY);
	$msg = str_replace('__HEADER_LOGO_IMAGE_URL__', $motif_path.$config->GetValue(CONFIG_TOP_LEFT_FRONT), $msg);

	if ($config->GetValue(CONFIG_TOP_LEFT_BACK))
	  $msg = str_replace('__HEADER_LEFT_BACK_IMAGE_URL__', $motif_path.$config->GetValue(CONFIG_TOP_LEFT_BACK), $msg);
	else
	  $msg = str_replace('__HEADER_LEFT_BACK_IMAGE_URL__', '', $msg);

	if ($config->GetValue(CONFIG_TOP_MIDDLE_BACK))
	  $msg = str_replace('__HEADER_MIDDLE_BACK_IMAGE_URL__', $motif_path.$config->GetValue(CONFIG_TOP_MIDDLE_BACK), $msg);
	else
	  $msg = str_replace('__HEADER_MIDDLE_BACK_IMAGE_URL__', '', $msg);

	if ($config->GetValue(CONFIG_TOP_MIDDLE_FRONT))
	  $msg = str_replace('__HEADER_MIDDLE_FRONT_IMAGE_URL__', $motif_path.$config->GetValue(CONFIG_TOP_MIDDLE_FRONT), $msg);
	else
	  $msg = str_replace('__HEADER_MIDDLE_FRONT_IMAGE_URL__', '', $msg);

	if ($config->GetValue(CONFIG_TOP_RIGHT_BACK))
	  $msg = str_replace('__HEADER_RIGHT_BACK_IMAGE_URL__', $motif_path.$config->GetValue(CONFIG_TOP_RIGHT_BACK), $msg);
	else
	  $msg = str_replace('__HEADER_RIGHT_BACK_IMAGE_URL__', '', $msg);

	if ($config->GetValue(CONFIG_TOP_RIGHT_FRONT))
	  $msg = str_replace('__HEADER_RIGHT_FRONT_IMAGE_URL__', $motif_path.$config->GetValue(CONFIG_TOP_RIGHT_FRONT), $msg);
	else
	  $msg = str_replace('__HEADER_RIGHT_FRONT_IMAGE_URL__', '', $msg);
      } else {
	$motif_path = FQ_URL_ROOT.'/themes/Default/presentation';
	$msg = str_replace('__HEADER_LOGO_IMAGE_URL__', $motif_path.'/wtp_logo.gif', $msg);
	$msg = str_replace('__HEADER_LEFT_BACK_IMAGE_URL__', $motif_path.'/header_middle.gif', $msg);
	$msg = str_replace('__HEADER_MIDDLE_BACK_IMAGE_URL__', $motif_path.'/header_middle.gif', $msg);
	$msg = str_replace('__HEADER_MIDDLE_FRONT_IMAGE_URL__', $motif_path.'/transparent.gif', $msg);
	$msg = str_replace('__HEADER_RIGHT_BACK_IMAGE_URL__', $motif_path.'/header_middle.gif', $msg);
	$msg = str_replace('__HEADER_RIGHT_FRONT_IMAGE_URL__', $motif_path.'/hrtools_logo.gif', $msg);
      }
      $header = 'From: '.$config->GetValue(CONFIG_ORG_NAME)." <nobody@trainingadvisorinc.com>\n";
      SendEmail($toAddr, '', $subj, $msg, $header);
    }
  } elseif (isset($_REQUEST['editSubmit'])) {
    //error_log("Saving file contents to: ".$dirRoot.$fm->FilterPath($_REQUEST['path']));
    $fm->WriteFile($dirRoot.$fm->FilterPath($_REQUEST['path']), $_REQUEST['fileContents']);
    //file_put_contents($dirRoot.$fm->FilterPath($_REQUEST['path']), $_REQUEST['fileContents']);
  }

  // Manage changes to the enable/disable default motif button on the /motif folder page
  if (isset($_REQUEST['publishMotif'])) {
    if ($_REQUEST['publishMotif'] == 'Disable Default Motif') {
      $config->SetValue(CONFIG_USE_DEFAULT_MOTIF, 0);
      $refreshMotif = TRUE;
    } elseif ($_REQUEST['publishMotif'] == 'Publish Default Motif') {
      $config->SetValue(CONFIG_USE_DEFAULT_MOTIF, 1);
      $refreshMotif = TRUE;
    } elseif ($_REQUEST['publishMotif'] == 'Publish Changes') {
    
      // Manage the use of the default motif
      CheckCustomizeMotif($config, $fm, 'useDefaultMotif', CONFIG_USE_DEFAULT_MOTIF, $refreshMotif, TRUE);

      // Manage other custom image specifications for top left area
      CheckCustomizeMotif($config, $fm, 'topLeftBack', CONFIG_TOP_LEFT_BACK, $refreshMotif);
      CheckCustomizeMotif($config, $fm, 'topLeftFront', CONFIG_TOP_LEFT_FRONT, $refreshMotif);
      CheckCustomizeMotif($config, $fm, 'topLeftBackRepeat', CONFIG_TOP_LEFT_BACK_REPEAT, $refreshMotif, TRUE);
      
      // Manage other custom image specifications for top middle area
      CheckCustomizeMotif($config, $fm, 'topMiddleBack', CONFIG_TOP_MIDDLE_BACK, $refreshMotif);
      CheckCustomizeMotif($config, $fm, 'topMiddleFront', CONFIG_TOP_MIDDLE_FRONT, $refreshMotif);
      CheckCustomizeMotif($config, $fm, 'topMiddleBackRepeat', CONFIG_TOP_MIDDLE_BACK_REPEAT, $refreshMotif, TRUE);
      
      // Manage other custom image specifications for top right area
      CheckCustomizeMotif($config, $fm, 'topRightBack', CONFIG_TOP_RIGHT_BACK, $refreshMotif);
      CheckCustomizeMotif($config, $fm, 'topRightFront', CONFIG_TOP_RIGHT_FRONT, $refreshMotif);
      CheckCustomizeMotif($config, $fm, 'topRightBackRepeat', CONFIG_TOP_RIGHT_BACK_REPEAT, $refreshMotif, TRUE);
      
    } else {
      error_log('file_man_edit.php: unrecognized value ('.$_REQUEST[publishMotif].
		') in REQUEST[publishMotif]');
    }
  }

  // This only needed when refreshing the motif
  $smarty->assign('refreshMotif', $refreshMotif);
  
  // Even it not refreshing motif, background repeat values come out of here
  SetupMotifDisplay($smarty, $config);

  // Get file contents if an editable file
  $fileExt = end(explode('.', $fm->FilterPath($path)));  
  if (strcasecmp($fileExt, 'txt') == 0 || strcasecmp($fileExt, 'text') == 0 ||
      strcasecmp($fileExt, 'htm') == 0 || strcasecmp($fileExt, 'html') == 0 ||
      strcasecmp($fileExt, 'eml') == 0 || strcasecmp($fileExt, 'email') == 0 ||
      strcasecmp($fileExt, 'rtf') == 0) {
    $showEdit = TRUE;
    if (substr($_REQUEST['path'], 0, 7) == '/email/' && strpos($_REQUEST['path'], 'Message') !== FALSE)
      $isEmail = TRUE;
    $fileContents = file_get_contents($dirRoot.$fm->FilterPath($_REQUEST['path']));
  } elseif ($fileExt == 'gif' || $fileExt == 'jpg' || $fileExt == 'jpeg' || $fileExt == 'png') {
    $imageUrlPath = URL_ROOT.'/organizations/'.$config->GetValue(CONFIG_ORG_DIRECTORY).$path;
    $smarty->assign('imageUrlPath', $imageUrlPath);

    //    if (!isset($org)) {
    //      $org = new Organization($_SESSION['orgId']);
    //    }
    // Assign smarty variables for current values so we know which checkboxes to check on display
    $smarty->assign('configTopLeftBack', $config->GetValue(CONFIG_TOP_LEFT_BACK));
    $smarty->assign('configTopLeftFront', $config->GetValue(CONFIG_TOP_LEFT_FRONT));
    $smarty->assign('configTopMiddleBack', $config->GetValue(CONFIG_TOP_MIDDLE_BACK));
    $smarty->assign('configTopMiddleFront', $config->GetValue(CONFIG_TOP_MIDDLE_FRONT));
    $smarty->assign('configTopRightBack', $config->GetValue(CONFIG_TOP_RIGHT_BACK));
    $smarty->assign('configTopRightFront', $config->GetValue(CONFIG_TOP_RIGHT_FRONT));
    $smarty->assign('useDefaultMotif', $config->GetValue(CONFIG_USE_DEFAULT_MOTIF));
  } elseif ($_REQUEST['path'] == '/motif') {
    $config = new Config($_SESSION['orgId']);
    $smarty->assign('motifFolderSelected', TRUE);
    $smarty->assign('useDefaultMotif', $config->GetValue(CONFIG_USE_DEFAULT_MOTIF));
  }
}


$smarty->assign('path', $path);
$smarty->assign('basepath', basename($path));
$smarty->assign('isRoot', $isRoot);
$smarty->assign('isDir', $fm->IsDirectory($path));
$smarty->assign('isImmutable', $fm->IsImmutable($path));
$smarty->assign('isEditable', $fm->IsEditable($path));
$smarty->assign('isEmail', $isEmail);
$smarty->assign('contactList', Organization::GetContacts($_SESSION['orgId']));
$smarty->assign('reloadFrame', $reloadNavFrame);
$smarty->assign('showEdit', $showEdit);
$smarty->assign('showNewsOptions', $showNewsOptions);
$smarty->assign('publishSelected', $publishSelected);
$smarty->assign('fileContents', $fileContents);
$smarty->assign('currentTab', $_SESSION['currentTab']);
$smarty->assign('orgName', $config->GetValue(CONFIG_ORG_NAME));
$smarty->assign('nodes', $fm->BuildTree());
$smarty->assign('uiTheme', $_SESSION['uiTheme']);
$smarty->assign('errMsg', $errMsg);
$smarty->display('admin/file_man_edit.tpl');

$config->Close();

?>
