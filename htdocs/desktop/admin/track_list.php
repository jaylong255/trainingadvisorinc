<?php
/////////////////////////////////////////////////////////////////////////////
//	organization_list.php
//		Page used for administration of organizations stored on the current server
//


// Include libs which starts session and creates smarty objects
include_once ('../includes/common.php');
include_once ('../includes/Track.php');

// Ensure user has logged in, otherwise exit now
if (!isset($_SESSION['userId'])) {
  header("Location: /desktop/login/expired.php");
  exit(0);
}

// Ensure user is superuser or an org admin
if (!(isset($_SESSION['superUser']) && $_SESSION['superUser']) &&
    !(isset($_SESSION['role']) && $_SESSION['role'] != ROLE_END_USER)) {
  header('Location: /desktop/login/perm_denied.php');
  exit(0);
}

// Specify this as the current tab
$_SESSION['currentTab'] = TAB_TRACK;
$_SESSION['currentSubTab'] = SUBTAB_NONE;

if (isset($_REQUEST['searchBy'])) {
  $searchBy = $_REQUEST['searchBy'];
} else {
  $searchBy = TRACK_LIST_COLUMN_NAME;
}

if (isset($_REQUEST['searchFor'])) {
  $searchFor = $_REQUEST['searchFor'];
  $_SESSION['trackPageNumber'] = 1;
} else {
  $searchFor = '';
}

// create new employee list
$errMsg = '';
$rc = 0;
$firstRow = 0;
$lastRow = 0;
$numRows = 0;
$numPages = 0;
$trackIds = array();
$tracks = array();
$trackList = new Track($_SESSION['orgId']);

if (isset($_REQUEST['trackId'])) {
  $trackList->SetTrackId($_REQUEST['trackId']);
  $trackList->DeleteTrack();
}

$rc = $trackList->GetTrackList($_SESSION['trackShowColumns'], $searchBy, $searchFor,
			       $_SESSION['trackSortColumn'], $_SESSION['trackSortDescending'],
			       $_SESSION['trackPageNumber'], $_SESSION['rowsPerPage'], $_SESSION['userId']);
if ($rc === FALSE) {
  echo "Failed to get track list\n";
  exit(1);
}

if (DEBUG & DEBUG_FORM) {
  $smarty->assign('GetTrackList', $rc);
}

if (!empty($rc)) {
  list($trackIds, $tracks) = $rc;
  $trackList->GetRowCounts($firstRow, $lastRow, $numRows);
  $numPages = $trackList->GetPageCount($_SESSION['rowsPerPage']);
}


$createLink = new TplCaption('Create New Class');
$createLink->href='track_edit.php';
$createLink->params='create=1';


// setup links across the top of the page
$smarty->assign('topLinkList', $createLink);

// This alone is enough information to identify a search column
$smarty->assign('columnLabels', $trackList->GetColumnLabels($_SESSION['trackShowColumns']));
$smarty->assign('showColumns', $_SESSION['trackShowColumns']);

// Keep the selected search by field
$smarty->assign('searchBy', $searchBy);

// Set the one to be selected
$smarty->assign('sortColumn', $_SESSION['trackSortColumn']);
$smarty->assign('sortDescending', $_SESSION['trackSortDescending']);


// TODO: make this dynamic by adding an organization column to the Organization table
// and a widget in the admin to control display of logo
$smarty->assign('displayLogo', TRUE);


$smarty->assign('firstRow', $firstRow);
$smarty->assign('lastRow', $lastRow);
$smarty->assign('numRows', $numRows);
$smarty->assign('pageNumber', $_SESSION['trackPageNumber']);
$smarty->assign('numPages', $numPages);
$smarty->assign('recordList', $tracks);
$smarty->assign('recordIds', $trackIds);

if (DEBUG) {
  $smarty->assign('ErrMsg', $trackList->GetErrorMessage());
}

$trackList->Close();

$smarty->assign('currentTab', $_SESSION['currentTab']);
$smarty->assign('currentSubTab', $_SESSION['currentSubTab']);
$smarty->assign('toggleParamName', 'toggleTrackColumn');
$smarty->assign('sortParamName', 'trackSortColumn');
$smarty->assign('idParamName', 'trackId');
$smarty->assign('pageNumberParamName', 'trackPageNumber');
$smarty->assign('orderParamName', 'trackSortDescending');
$smarty->assign('showSelectedColumn', FALSE);
$smarty->assign('rowsPerPage', $_SESSION['rowsPerPage']);
$smarty->assign('localHeader', 'track_header');
$smarty->assign('uiTheme', $_SESSION['uiTheme']);
$smarty->display('admin/generic_list.tpl');


?>
