<?php

require_once("../includes/common.php");
require_once("../includes/Organization.php");

// Ensure user has logged in, otherwise exit now
if (!isset($_SESSION['userId'])) {
  header('Location: /desktop/login/expired.php');
  exit(0);
}

// Ensure user is at least an admin
if (!(isset($_SESSION['superUser']) && $_SESSION['superUser']) &&
    !(isset($_SESSION['role']) && $_SESSION['role'] == ROLE_ORG_ADMIN)) {
  header('Location: /desktop/login/perm_denied.php');
  exit(0);
}


$filename = 'question_import_sample_v2_6.csv';

if (!file_exists($filename)) {
  echo '<html><body><script language="javascript">alert("Unable to find the sample user import file");</script></body></html>';
} else {
  header("Content-disposition: attachment; filename=$filename");
  readfile($filename);
}

?>
