<?php

// Include libs which starts session and creates smarty objects
require_once ('../includes/common.php');
require_once ('../includes/User.php');
require_once ('../includes/Organization.php');


// Ensure user has logged in, otherwise exit now
if (!isset($_SESSION['userId'])) {
  header("Location: /desktop/login/expired.php");
  exit(0);
}

// Ensure user is superuser or an org admin
if (!(isset($_SESSION['superUser']) && $_SESSION['superUser']) &&
    !(isset($_SESSION['role']) && $_SESSION['role'] != ROLE_END_USER &&
      $_SESSION['role'] != ROLE_CLASS_CONTACT_SUPERVISOR)) {
  header("Location: /desktop/login/perm_denied.php");
  exit(0);
}


// Set the page and values to be able to return to this page
$_SESSION['currentSubTab'] = SUBTAB_USER_EDIT;


$errMsg = '';
$trackMemberList = array();
$roles = unserialize(ROLE_SELECT_LABELS);
$user = new User($_SESSION['orgId']);


// Cleanup the roles list (div admin should not be able to setup org admins)
if ($user->role == ROLE_DIVISION_ADMIN || $user->role == ROLE_DEPARTMENT_ADMIN)
  unset($roles[ROLE_ORG_ADMIN]);


if (isset($_REQUEST['userId'])) {

  $_SESSION['editUserId'] = $_REQUEST['userId'];

  if ($_REQUEST['userId'] != 'To Be Assigned') {

    // Validate existing user id to make sure form content was not altered
    if (!is_numeric($_REQUEST['userId'])) {
      header("Location: /desktop/login/perm_denied.php");
      exit;
    }

    $user->SetUserId($_REQUEST['userId']);
  }

  // Form was submitted so we attempt to update user
  if (isset($_REQUEST['firstName'])) {

    // This method checks for duplicates
    $user->firstName = esql($_REQUEST['firstName']);
    $user->lastName = esql($_REQUEST['lastName']);
    $user->password = esql($_REQUEST['password']);
    $user->email = esql($_REQUEST['email']);
    $user->phone = esql($_REQUEST['phone']);
    $user->languageId = esql($_REQUEST['language']);
    if (isset($_REQUEST['contactId']))
      $user->contactId = esql($_REQUEST['contactId']);
    $user->domainId = esql($_REQUEST['domainId']);
    $user->departmentId = esql($_REQUEST['departmentId']);
    $user->divisionId = esql($_REQUEST['divisionId']);
    $user->employmentStatusId = esql($_REQUEST['employmentStatusId']);
    if (isset($_REQUEST['useSupervisor']) && $_REQUEST['useSupervisor'])
      $user->useSupervisor = TRUE;
    else
      $user->useSupervisor = FALSE;
    if (isset($_REQUEST['isContact']) && $_REQUEST['isContact'])
      $user->isContact = TRUE;
    else
      $user->isContact = FALSE;
    $user->role = $_REQUEST['role'];

    $rc = $user->GetUserIdByLogin(esql($_REQUEST['login']));
    if ($rc == RC_USER_UNKNOWN || (int)$rc == (int)$user->userId) {
      $user->login = esql($_REQUEST['login']);
      $rc = $user->UpdateUser();
      if ($rc != RC_OK) {
	error_log("Failed to update userId ".$user->userId);
	$errMsg .= RcToText($rc);
      }
      // Update session variabes if the user being updated is the current user
      if ($_SESSION['userId'] == $user->userId) {
	$_SESSION['role'] = $user->role;
	$_SESSION['departmentId'] = $user->departmentId;
	$_SESSION['divisionId'] = $user->divisionId;
	$_SESSION['contactId'] = $user->contactId;
	$_SESSION['stateId'] = $user->domainId;
	$_SESSION['statusId'] = $user->employmentStatusId;
	$_SESSION['languageId'] = $user->languageId;
      }
    } else if ($rc > 0 && $rc != $user->userId) {
      $errMsg .= RcToText(RC_DUPLICATE_LOGIN);
    } else {
      $errMsg .= RcToText($rc);
    }    

    if (isset($_REQUEST['keepTrackIds']) && !empty($_REQUEST['keepTrackIds'])) {
      if (!is_numeric($_REQUEST['userId'])) {
	header("Location: /desktop/login/perm_denied.php");
	exit;
      }
      $trackMemberList = array_keys($user->GetParticipantTracks());
      $removeList = array_diff($trackMemberList, $_REQUEST['keepTrackIds']);
      foreach($removeList as $trackId)
	$user->RemoveFromTrack($trackId);
    }
  } else {

    // Load user existing information for first time form edit
    $user->SetUserId($_REQUEST['userId']);

  }

  // Load user track membership list
  $trackMemberList = $user->GetParticipantTracks();

  //if (DEBUG & DEBUG_CLASS_FUNCTIONS) {
  //$user->DumpAll();
  //}
} elseif (isset($_REQUEST['userIdAsTemplate']) && is_numeric($_REQUEST['userIdAsTemplate']) &&
	  $_REQUEST['userIdAsTemplate'] > 0) {

  $user->SetUserId($_REQUEST['userIdAsTemplate']);
  $user->userId = 0;
  $user->firstName = '';
  $user->lastName = '';
  $user->login = '';
  $user->password = '';
  $user->email = '';
  $user->phone = '';
  $user->role = 0;
  $trackMemberList = array();
}


$smarty->assign_by_ref('user', $user);

$smarty->assign('currentTab', $_SESSION['currentTab']);
$smarty->assign('currentSubTab', $_SESSION['currentSubTab']);

// These are for populating drop down boxes items
$smarty->assign('employmentStatusValues', Organization::GetEmploymentStatusValues($_SESSION['orgId']));
//$smarty->assign('customLabels', Organization::GetCustomLabels($_SESSION['orgId']));
$smarty->assign('divisions', Organization::GetDivisions($_SESSION['orgId']));
$smarty->assign('domains', Organization::GetDomains($_SESSION['orgId']));
$smarty->assign('languages', Organization::GetLanguages($_SESSION['orgId']));
$smarty->assign('contacts', Organization::GetContacts($_SESSION['orgId']));
$smarty->assign('departments', Organization::GetDepartments($_SESSION['orgId']));
$smarty->assign('trackMemberIdList', array_keys($trackMemberList));
$smarty->assign('trackMemberNameList', array_values($trackMemberList));
$smarty->assign('roles', $roles);

$smarty->assign('errMsg', $errMsg);
$smarty->assign('uiTheme', $_SESSION['uiTheme']);
$smarty->display('admin/user_edit.tpl');
exit(0);

?>




