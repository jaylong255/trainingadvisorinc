-- ALTER TABLE Question CHANGE Content Old_Content text;
DELETE FROM User WHERE User_ID=1;
DELETE FROM HR_Contact_Info WHERE User_ID=1;
UPDATE Question SET Title='Non-Employee Sexual Harassment' WHERE Question_Number=402;
UPDATE Question SET Title='False Claim Sexual Harassment' WHERE Question_Number=403;
DELETE FROM Config WHERE Name='Show_User_That_Answered';
INSERT INTO Config VALUES ('', 'Show_User_That_Answered', '0', '', '0', '', NOW(), NOW(), 0);
DELETE FROM Config WHERE Name='Track_Admin_Summary_Default';
INSERT INTO Config VALUES ('', 'Track_Admin_Summary_Default', '0', '', '0', '', NOW(), NOW(), 0);
