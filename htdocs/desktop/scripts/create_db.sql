-- Table structure for table 'Category'
CREATE TABLE Category (
  Category_ID int(6) unsigned zerofill NOT NULL auto_increment,
  Name varchar(50) default NULL,
  PRIMARY KEY  (Category_ID)
) ENGINE=MyISAM;

INSERT INTO Category VALUES (000001,'Preventing Sexual Harassment (by co-employees)');
INSERT INTO Category VALUES (000002,'Preventing Sexual Harassment (by managers)');
INSERT INTO Category VALUES (000003,'Reducing Unlawful Harassment');
INSERT INTO Category VALUES (000004,'Reducing Workplace Violence');
INSERT INTO Category VALUES (000005,'Effective Hiring');
INSERT INTO Category VALUES (000006,'Protecting Employee Privacy');
INSERT INTO Category VALUES (000007,'Improving Performance Evaluations');
INSERT INTO Category VALUES (000008,'Effective Investigations and Disciplines');
INSERT INTO Category VALUES (000009,'Unlawful Terminations');
INSERT INTO Category VALUES (000010,'Addressing Substance Abuse Prevention');
INSERT INTO Category VALUES (000011,'Proactive Labor Relations');
INSERT INTO Category VALUES (000012,'The Law of Employment Discrimination');
INSERT INTO Category VALUES (000013,'Enhancing Workplace Safety');
INSERT INTO Category VALUES (000014,'Accomodating Americans With Disabilities');
INSERT INTO Category VALUES (000015,'Preventive Employment Relations');
INSERT INTO Category VALUES (000016,'Policies & Procedures');
INSERT INTO Category VALUES (000022,'Videos');
INSERT INTO Category VALUES (000100,'Preventing Sexual Harassment');
INSERT INTO Category VALUES (000101,'Preventing Unlawful Harassment (non-sexual)');
INSERT INTO Category VALUES (000102,'Preventing Unlawful Discrimination');
INSERT INTO Category VALUES (000103,'Preventing Retaliation');
INSERT INTO Category VALUES (000104,'Best Practices: Hiring and Termination');
INSERT INTO Category VALUES (000105,'Best Practices: Performance Appraisals');
INSERT INTO Category VALUES (000106,'Employee Leave Laws');
INSERT INTO Category VALUES (000107,'Understanding the American with Disabilities Act');
INSERT INTO Category VALUES (000108,'Requirements of the Fair Labor Standards Act');
INSERT INTO Category VALUES (000109,'Investigating and Addressing Employee Misconduct');
INSERT INTO Category VALUES (000110,'Computer and Internet Use in the Workplace');
INSERT INTO Category VALUES (000111,'Issues for Public Employers');
INSERT INTO Category VALUES (000113,'Recordkeeping');
INSERT INTO Category VALUES (000117,'Slide Show');
INSERT INTO Category VALUES (000118,'AB1825');
INSERT INTO Category VALUES (000119,'OSHA');
INSERT INTO Category VALUES (010000,'Misc.');



-- Table structure for table 'Config'
CREATE TABLE Config (
  Config_ID int unsigned NOT NULL auto_increment,
  Name varchar(50) NOT NULL,
  Value varchar(255) NOT NULL,
  Type varchar(12) NOT NULL,
  Default_Value varchar(255) NOT NULL,
  Default_Type varchar(12) NOT NULL,
  Date_Created datetime NOT NULL default 0,
  Date_Modified timestamp NOT NULL,
  Obsolete tinyint(1) NOT NULL default 0,
  PRIMARY KEY (Config_ID),
  UNIQUE KEY Config_Name_Idx (Name)
) ENGINE=MyISAM;


-- *************************************************************************************
-- To load the Config table with initial set of values when creating a new organization,
-- modify the CreateOrg function in the Organization class in includes/Organization.php
-- DO NOT PUT NEW CONFIGURATION VALUES NEEDED FOR NEW ORGANIZATIONS INTO THIS FILE
-- *************************************************************************************


-- Table structure for table 'Custom1'
CREATE TABLE Custom1 (
  Custom1_ID int(6) unsigned zerofill NOT NULL auto_increment,
  Name varchar(50) default NULL,
  PRIMARY KEY  (Custom1_ID)
) ENGINE=MyISAM;



-- Table structure for table 'Custom2'
CREATE TABLE Custom2 (
  Custom2_ID int(6) unsigned zerofill NOT NULL auto_increment,
  Name varchar(50) default NULL,
  PRIMARY KEY  (Custom2_ID)
) ENGINE=MyISAM;



-- Table structure for table 'Custom3'
CREATE TABLE Custom3 (
  Custom3_ID int(6) unsigned zerofill NOT NULL auto_increment,
  Name varchar(50) default NULL,
  PRIMARY KEY  (Custom3_ID)
) ENGINE=MyISAM;



-- Table structure for table 'Custom_Label_Lookup'
CREATE TABLE Custom_Label_Lookup (
  Custom1 varchar(30) default NULL,
  Custom2 varchar(30) default NULL,
  Custom3 varchar(30) default NULL
) ENGINE=MyISAM;



-- Table structure for table 'Department'
CREATE TABLE Department (
  Department_ID int(6) unsigned zerofill NOT NULL auto_increment,
  Name varchar(50) default NULL,
  PRIMARY KEY  (Department_ID)
) ENGINE=MyISAM;



-- Table structure for table 'Division'
CREATE TABLE Division (
  Division_ID int(6) unsigned zerofill NOT NULL auto_increment,
  Name varchar(50) default NULL,
  PRIMARY KEY  (Division_ID)
) ENGINE=MyISAM;



-- Table structure for table 'Domain'
CREATE TABLE Domain (
  Domain_ID int(6) unsigned zerofill NOT NULL default '000000',
  Name varchar(50) default NULL,
  Abbreviation char(2) default NULL,
  PRIMARY KEY  (Domain_ID)
) ENGINE=MyISAM;



-- Data for table 'Domain'
INSERT INTO Domain VALUES (000001,'Federal','FD');
INSERT INTO Domain VALUES (000002,'Alabama','AL');
INSERT INTO Domain VALUES (000003,'Alaska','AK');
INSERT INTO Domain VALUES (000004,'Arizona','AZ');
INSERT INTO Domain VALUES (000005,'Arkansas','AR');
INSERT INTO Domain VALUES (000006,'California','CA');
INSERT INTO Domain VALUES (000007,'Colorado','CO');
INSERT INTO Domain VALUES (000008,'Connecticut','CT');
INSERT INTO Domain VALUES (000009,'Delaware','DE');
INSERT INTO Domain VALUES (000010,'District of Columbia','DC');
INSERT INTO Domain VALUES (000011,'Florida','FL');
INSERT INTO Domain VALUES (000012,'Georgia','GA');
INSERT INTO Domain VALUES (000013,'Hawaii','HI');
INSERT INTO Domain VALUES (000014,'Iowa','IA');
INSERT INTO Domain VALUES (000015,'Idaho','ID');
INSERT INTO Domain VALUES (000016,'Illinois','IL');
INSERT INTO Domain VALUES (000017,'Indiana','IN');
INSERT INTO Domain VALUES (000018,'Kansas','KS');
INSERT INTO Domain VALUES (000019,'Kentucky','KY');
INSERT INTO Domain VALUES (000020,'Louisiana','LA');
INSERT INTO Domain VALUES (000021,'Maine','ME');
INSERT INTO Domain VALUES (000022,'Maryland','MD');
INSERT INTO Domain VALUES (000023,'Massachusetts','MA');
INSERT INTO Domain VALUES (000024,'Michigan','MI');
INSERT INTO Domain VALUES (000025,'Minnesota','MN');
INSERT INTO Domain VALUES (000026,'Mississippi','MS');
INSERT INTO Domain VALUES (000027,'Missouri','MO');
INSERT INTO Domain VALUES (000028,'Montana','MT');
INSERT INTO Domain VALUES (000029,'Nebraska','NE');
INSERT INTO Domain VALUES (000030,'Nevada','NV');
INSERT INTO Domain VALUES (000031,'New Hampshire','NH');
INSERT INTO Domain VALUES (000032,'New Jersey','NJ');
INSERT INTO Domain VALUES (000033,'New Mexico','NM');
INSERT INTO Domain VALUES (000034,'New York','NY');
INSERT INTO Domain VALUES (000035,'North Carolina','NC');
INSERT INTO Domain VALUES (000036,'North Dakota','ND');
INSERT INTO Domain VALUES (000037,'Ohio','OH');
INSERT INTO Domain VALUES (000038,'Oklahoma','OK');
INSERT INTO Domain VALUES (000039,'Oregon','OR');
INSERT INTO Domain VALUES (000040,'Pennsylvania','PA');
INSERT INTO Domain VALUES (000041,'Rhode Island','RI');
INSERT INTO Domain VALUES (000042,'South Carolina','SC');
INSERT INTO Domain VALUES (000043,'South Dakota','SD');
INSERT INTO Domain VALUES (000044,'Tennessee','TN');
INSERT INTO Domain VALUES (000045,'Texas','TX');
INSERT INTO Domain VALUES (000046,'Utah','UT');
INSERT INTO Domain VALUES (000047,'Vermont','VT');
INSERT INTO Domain VALUES (000048,'Virginia','VA');
INSERT INTO Domain VALUES (000049,'Washington','WA');
INSERT INTO Domain VALUES (000050,'West Virginia','WV');
INSERT INTO Domain VALUES (000051,'Wisconsin','WI');
INSERT INTO Domain VALUES (000052,'Wyoming','WY');
INSERT INTO Domain VALUES (100000,'Organization','OG');


-- Table structure for table 'Employment_Status'

CREATE TABLE Employment_Status (
  Employment_Status_ID int(6) unsigned zerofill NOT NULL auto_increment,
  Name varchar(50) default NULL,
  PRIMARY KEY  (Employment_Status_ID)
) ENGINE=MyISAM;



-- Data for table 'Employment_Status'

INSERT INTO Employment_Status VALUES (000002,'On Leave');
INSERT INTO Employment_Status VALUES (000001,'Active');
INSERT INTO Employment_Status VALUES (000003,'Terminated');



-- Table structure for table 'Faq'

CREATE TABLE Faq (
  Track_ID int(6) unsigned zerofill NOT NULL default '000000',
  Question text,
  Answer text,
  Faq_ID int(6) unsigned zerofill NOT NULL auto_increment,
  PRIMARY KEY  (Faq_ID)
) ENGINE=MyISAM;



-- Table structure for table 'Frametype'

CREATE TABLE Frametype (
  Frametype_ID int(6) unsigned zerofill NOT NULL auto_increment,
  Name varchar(50) default NULL,
  ItemName varchar(200) default NULL,
  PRIMARY KEY  (Frametype_ID)
) ENGINE=MyISAM;



-- Data for table 'Frametype'

INSERT INTO Frametype VALUES (000001,'q_type1.php',"Multiple Choice Text");
INSERT INTO Frametype VALUES (000002,'q_type2.php',"Multiple Choice Graphic");
INSERT INTO Frametype VALUES (000003,'q_type3.php',"Multiple Choice Flash");
INSERT INTO Frametype VALUES (000004,'q_type4.php',"Multiple Choice Video");
INSERT INTO Frametype VALUES (000005,'q_type5.php',"AB1825 Required");
INSERT INTO Frametype VALUES (000006,'q_type5.php',"AB1825 Optional");



-- Table structure for table 'Language'

CREATE TABLE Language (
  Language_ID int(6) unsigned zerofill NOT NULL auto_increment,
  Name varchar(50) default NULL,
  PRIMARY KEY  (Language_ID)
) ENGINE=MyISAM;


-- Data for table 'Language'

INSERT INTO Language VALUES (000001,'English');



-- Table structure for table 'Question'


CREATE TABLE Question (
  Question_Number int(6) unsigned zerofill NOT NULL default '000000',
  Parent_Question_Number int(6) unsigned zerofill NOT NULL default '000000',
  Language_ID int(6) unsigned zerofill NOT NULL default '000000',
  Domain_ID int(6) unsigned zerofill NOT NULL default '000000',
  Category_ID int(6) unsigned zerofill NOT NULL default '000000',
  Frametype_ID int(6) unsigned zerofill NOT NULL default '000000',
  Version_Date datetime default NULL,
  Is_Management tinyint(1) default '0',
  Author varchar(80) default NULL,
  Notes text,
  Title varchar(100) default NULL,
  Question text NOT NULL DEFAULT '',
  Multiple_Choice1 TEXT NOT NULL DEFAULT '',
  Multiple_Choice2 TEXT NOT NULL DEFAULT '',
  Multiple_Choice3 TEXT NOT NULL DEFAULT '',
  Multiple_Choice4 TEXT NOT NULL DEFAULT '',
  Multiple_Choice5 TEXT NOT NULL DEFAULT '',
  Feedback_Choice1 TEXT NOT NULL DEFAULT '',
  Feedback_Choice2 TEXT NOT NULL DEFAULT '',
  Feedback_Choice3 TEXT NOT NULL DEFAULT '',
  Feedback_Choice4 TEXT NOT NULL DEFAULT '',
  Feedback_Choice5 TEXT NOT NULL DEFAULT '',
  Correct_Answer SMALLINT NOT NULL DEFAULT 0,
  Purpose TEXT NOT NULL DEFAULT '',
  Learning_Points TEXT NOT NULL DEFAULT '',
  Media_File_Path VARCHAR(255) NOT NULL DEFAULT '',
  Media_Duration_Seconds int(6) NOT NULL DEFAULT 0,
  Support_Info TEXT NOT NULL DEFAULT '',
  CapConfirmThanks VARCHAR(255) NOT NULL DEFAULT '',
  RequireCorrectAnswer TINYINT(1) NOT NULL DEFAULT 1,
  Display_Choices VARCHAR(32) NOT NULL DEFAULT 'default',
  Owner_User_ID INT(11) UNSIGNED ZEROFILL NOT NULL DEFAULT '00000000000',
  PRIMARY KEY  (Question_Number,Language_ID,Domain_ID)
) ENGINE=MyISAM;



-- Table structure for table 'Question_Archive'

CREATE TABLE Question_Archive (
  Question_Number int(6) unsigned zerofill NOT NULL default '000000',
  Parent_Question_Number int(6) unsigned zerofill NOT NULL default '000000',
  Language_ID int(6) unsigned zerofill NOT NULL default '000000',
  Domain_ID int(6) unsigned zerofill NOT NULL default '000000',
  Category_ID int(6) unsigned zerofill NOT NULL default '000000',
  Frametype_ID int(6) unsigned zerofill NOT NULL default '000000',
  Version_Date datetime default NULL,
  Is_Management tinyint(1) default '0',
  Author varchar(80) default NULL,
  Notes text,
  Title varchar(100) default NULL,
  Question text NOT NULL DEFAULT '',
  Multiple_Choice1 TEXT NOT NULL DEFAULT '',
  Multiple_Choice2 TEXT NOT NULL DEFAULT '',
  Multiple_Choice3 TEXT NOT NULL DEFAULT '',
  Multiple_Choice4 TEXT NOT NULL DEFAULT '',
  Multiple_Choice5 TEXT NOT NULL DEFAULT '',
  Feedback_Choice1 TEXT NOT NULL DEFAULT '',
  Feedback_Choice2 TEXT NOT NULL DEFAULT '',
  Feedback_Choice3 TEXT NOT NULL DEFAULT '',
  Feedback_Choice4 TEXT NOT NULL DEFAULT '',
  Feedback_Choice5 TEXT NOT NULL DEFAULT '',
  Correct_Answer SMALLINT NOT NULL DEFAULT 0,
  Purpose TEXT NOT NULL DEFAULT '',
  Learning_Points TEXT NOT NULL DEFAULT '',
  Media_File_Path VARCHAR(255) NOT NULL DEFAULT '',
  Media_Duration_Seconds int(6) NOT NULL DEFAULT 0,
  Support_Info TEXT NOT NULL DEFAULT '',
  CapConfirmThanks VARCHAR(255) NOT NULL DEFAULT '',
  RequireCorrectAnswer TINYINT(1) NOT NULL DEFAULT 1,
  Display_Choices VARCHAR(32) NOT NULL DEFAULT 'default',
  Owner_User_ID INT(11) UNSIGNED ZEROFILL NOT NULL DEFAULT '00000000000',
  PRIMARY KEY  (Question_Number,Language_ID,Version_Date,Domain_ID)
) ENGINE=MyISAM;



-- Table structure for table 'Question_Data'

CREATE TABLE Question_Data (
  Question_Number int(6) unsigned zerofill NOT NULL default '000000',
  Version_Date datetime NOT NULL default '0000-00-00 00:00:00',
  Domain_ID int(6) unsigned zerofill NOT NULL default '000000',
  Completion_Date date default NULL,
  Result tinyint(1) default '0',
  Foil_Selected int(2) unsigned zerofill NOT NULL default '00',
  Exit_Info tinyint(1) default '0',
  Track_ID int(6) unsigned zerofill NOT NULL default '000000',
  Department_ID int(6) unsigned zerofill NOT NULL default '000000',
  Language_ID int(6) unsigned zerofill NOT NULL default '000000'
) ENGINE=MyISAM;



-- Table structure for table 'Queue'

CREATE TABLE Queue (
  Track_ID int(6) unsigned zerofill NOT NULL default '000000',
  Question_Number int(6) unsigned zerofill NOT NULL default '000000',
  Sequence int(6) unsigned zerofill NOT NULL default '000000',
  GroupID int(6) unsigned zerofill NOT NULL default '000000',
  PRIMARY KEY  (Track_ID,Question_Number)
) ENGINE=MyISAM;



-- Table structure for table 'Track'

CREATE TABLE Track (
  Track_ID int(6) unsigned zerofill NOT NULL auto_increment,
  Name varchar(30) default NULL,
  Supervisor_ID int(11) unsigned zerofill default NULL,
  Is_Management tinyint(1) not null default 0,
  Is_Binding tinyint(1) not null default 1,
  Is_Looping tinyint(1) not null default 0,
  Is_Ab1825 tinyint(1) not null default 0,
  Email_Reply_To varchar(80) default NULL,
  Recurrence varchar(30) default NULL,
  Start_Date datetime default NULL,
  Delinquency_Notification varchar(6) default '0',
  Last_Question_Number int(6) default '0',
  Last_Question_Date datetime default NULL,
  Track_News_URL varchar(255) default '',
  PRIMARY KEY  (Track_ID)
) ENGINE=MyISAM;



-- Table structure for table 'User'

CREATE TABLE User (
  User_ID int(11) unsigned zerofill NOT NULL default '00000000000',
  Login_ID varchar(45) NOT NULL default '',
  First_Name varchar(30) default NULL,
  Last_Name varchar(30) default NULL,
  Full_Name varchar(62) default NULL,
  Phone varchar(15) not null default '',
  Email varchar(80) default NULL,
  Use_Supervisor tinyint(1) default '0',
  Language_ID int(6) unsigned zerofill default NULL,
  Contact_User_ID int(11) unsigned zerofill default NULL,
  Department_ID int(6) unsigned zerofill default 0,
  Division_ID int(6) unsigned zerofill default 0,
  Employment_Status_ID int(6) unsigned zerofill default NULL,
  State_ID int(6) unsigned zerofill default NULL,
  Custom1_ID int(6) unsigned zerofill default NULL,
  Custom2_ID int(6) unsigned zerofill default NULL,
  Custom3_ID int(6) unsigned zerofill default NULL,
  Employment_Status_Date date default NULL,
  Role int unsigned not null default 0,
  PRIMARY KEY  (User_ID)
) ENGINE=MyISAM;


--
-- Table structure for table `Participant`
--

DROP TABLE IF EXISTS `Participant`;
CREATE TABLE `Participant` (
  `Participant_ID` int(12) unsigned NOT NULL auto_increment,
  `User_ID` int(11) unsigned zerofill NOT NULL default '00000000000',
  `Track_ID` int(6) unsigned zerofill NOT NULL default '000000',
  `Last_Question_Number` int(6) unsigned NOT NULL default '0',
  `Placed_Date` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`Participant_ID`),
  UNIQUE KEY `User_Track_ID_Idx` (`User_ID`,`Track_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Table structure for table `Assignment`
--

DROP TABLE IF EXISTS `Assignment`;
CREATE TABLE `Assignment` (
  `Assignment_ID` int(10) unsigned NOT NULL auto_increment,
  `Assignment_Parent_ID` int(10) unsigned NOT NULL default 0,
  `User_ID` int(11) unsigned zerofill NOT NULL default '00000000000',
  `Track_ID` int(6) unsigned zerofill NOT NULL default '000000',
  `Question_Number` int(6) unsigned zerofill NOT NULL default '000000',
  `Version_Date` datetime NOT NULL default '0000-00-00 00:00:00',
  `Domain_ID` int(6) unsigned zerofill NOT NULL default '000000',
  `Language_ID` int(6) unsigned zerofill NOT NULL default '000000',
  `Delivery_Date` datetime NOT NULL default '0000-00-00 00:00:00',
  `Due_Date` datetime NOT NULL default '0000-00-00 00:00:00',
  `Completion_Date` datetime NOT NULL default '0000-00-00 00:00:00',
  `First_Answer` int(2) unsigned NOT NULL default '0',
  `Final_Answer` int(2) unsigned NOT NULL default '0',
  `Answered_Correctly` tinyint(1) unsigned NOT NULL default '0',
  `Answered_Late` tinyint(1) unsigned NOT NULL default '0',
  `Exit_Info` tinyint(1) unsigned NOT NULL default '0',
  `Department_ID` int(6) unsigned zerofill NOT NULL default '000000',
  `Division_ID` int(6) unsigned zerofill NOT NULL default '000000',
  `Completed_Seconds` int(6) unsigned NOT NULL default 0,
  PRIMARY KEY  (`Assignment_ID`),
  KEY `Assignment_Parent_ID_Idx` (`Assignment_Parent_ID`),
  KEY `Question_Version_Domain_Idx` (`Question_Number`,`Version_Date`,`Domain_ID`),
  KEY `Question_Version_Idx` (`Question_Number`,`Version_Date`),
  KEY `Question_Idx` (`Question_Number`),
  KEY `Completion_Idx` (`Completion_Date`),
  KEY `Delivery_Idx` (`Delivery_Date`),
  KEY `User_Idx` (`User_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


-- Table structure for table 'User_Assignment'

CREATE TABLE User_Assignment (
  Question_Number int(6) unsigned zerofill NOT NULL default '000000',
  User_ID int(11) unsigned zerofill NOT NULL default '00000000000',
  Track_ID int(6) unsigned zerofill default NULL,
  Delivery_Date datetime NOT NULL default '0000-00-00 00:00:00',
  Completion_Date date default NULL,
  Exit_Info tinyint(1) default '0',
  Version_Date datetime NOT NULL default '0000-00-00 00:00:00',
  Domain_ID int(6) unsigned zerofill NOT NULL default '000000',
  Language_ID int(6) unsigned zerofill NOT NULL default '000000',
  PRIMARY KEY  (Question_Number,User_ID,Delivery_Date)
) ENGINE=MyISAM;

-- GRANT PRIVILEGES
-- These statements have been commented out since the HR_Track_Super role has been
-- removed because the code is now secure enough to support one org admin role and
-- because the wildcard permissions for database access have been successfully proven
-- using ORG% in the db table for each of the reamining administrative accounts
-- GRANT INSERT, SELECT, UPDATE ON * TO HR_Auth_User@localhost IDENTIFIED BY 'Author1zed4USe';
-- GRANT DELETE, INSERT, SELECT, UPDATE ON * TO HR_Track_Super@localhost IDENTIFIED BY 'sOUp4Sup';
-- GRANT DELETE, INSERT, SELECT, UPDATE ON * TO HR_Org_Admin@localhost IDENTIFIED BY 'orG8Admin';

