
DROP TABLE IF EXISTS Assignments;
DROP TABLE IF EXISTS Participants;
DROP TABLE IF EXISTS Assignment;
DROP TABLE IF EXISTS Participant;

-- Table structure for table 'Assignments'
CREATE TABLE Assignment (
  Assignment_ID int unsigned NOT NULL auto_increment,
  User_ID int(11) unsigned zerofill NOT NULL default 0,
  Track_ID int(6) unsigned zerofill NOT NULL default 0,
  Question_Number int(6) unsigned zerofill NOT NULL default 0,
  Version_Date datetime NOT NULL default '0000-00-00 00:00:00',
  Domain_ID int(6) unsigned zerofill NOT NULL default 0,
  Language_ID int(6) unsigned zerofill NOT NULL default 0,
  Delivery_Date datetime NOT NULL default '0000-00-00 00:00:00',
  Due_Date datetime NOT NULL default '0000-00-00 00:00:00',
  Completion_Date datetime NOT NULL default '0000-00-00 00:00:00',
  First_Answer int(2) unsigned NOT NULL default 0,
  Final_Answer int(2) unsigned NOT NULL default 0,
  Answered_Correctly tinyint(1) unsigned NOT NULL default 0,
  Answered_Late tinyint(1) unsigned NOT NULL default 0,
  Exit_Info tinyint(1) unsigned NOT NULL default 0,
  Department_ID int(6) unsigned zerofill NOT NULL default 0,
  PRIMARY KEY (Assignment_ID),
  INDEX Question_Version_Domain_Idx (Question_Number, Version_Date, Domain_ID),
  INDEX Question_Version_Idx (Question_Number, Version_Date),
  INDEX Question_Idx (Question_Number),
  INDEX Completion_Idx (Completion_Date),
  INDEX Delivery_Idx (Delivery_Date)
) TYPE=MyISAM;


-- Table structure for table 'Participant'
CREATE TABLE Participant (
  Participant_ID int(12) unsigned NOT NULL auto_increment,
  User_ID int(11) unsigned zerofill NOT NULL default 0,
  Track_ID int(6) unsigned zerofill NOT NULL default 0,
  Last_Question_Number int(6) unsigned NOT NULL default 0,
  Placed_Date datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY (Participant_ID),
  UNIQUE KEY User_Track_ID_Idx (User_ID, Track_ID)
) TYPE=MyISAM;

UPDATE Question_Data SET Exit_Info=2 WHERE Exit_Info=0;
UPDATE Question_Data SET Result=2 WHERE Result IS NULL OR Result=0;
UPDATE User_Assignment SET Exit_Info=2 WHERE (Exit_Info IS NULL OR Exit_Info=0) AND Completion_Date!=0;
