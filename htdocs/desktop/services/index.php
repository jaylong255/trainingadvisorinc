<?php

// Include files for the JSON/RPC server
if(!class_exists('DateTime'))
  require_once('../includes/DateTime.class.php');
require_once('../includes/RPCServer.class.php');

// Include files supporting sim communications
require_once('../includes/common.php');
require_once('../includes/XmlUtility.php');



// Instantiate the server
$server = RPCServer::getInstance(); //note that the RPCServer class is a singleton


// This is an internally used function to validate session information used
// in those methods requiring a logged in user
function validateSession() {
  // Ensure user has logged in, otherwise exit now
  if (!isset($_SESSION['userId'])) {
    //    echo "Required session login information not present.";
    error_log("ERROR: unauthorized attempt to access web service by unauthenticated user");
    trigger_error("Permission denied. You do not have rights to use this web service.");
    exit;
  }
  return true;
}




// Get list of organizations based on part of a username
function getUserOrgs($username) {

  // Ensure user is superuser
  //  if (!isset($_SESSION['superUser']) || !$_SESSION['superUser']) {
  //    error_log("CRIT(service): nonsuper user attempted to delete org");
  //    trigger_error("Permission denied.  You do not have rights to delete a user.");
  //    return $error;
  //  }

  require_once('../includes/User.php');
  //  // Remove the specified organization number
  //  //$usr = new User();

  $orgs = User::GetUserOrgs($username);

  if ($orgs === FALSE) {
    trigger_error("Failed to retrieve list of valid organizations: ".RcToText($orgs));
    return;
  }

  return $orgs;
}
// Register method with service
$server->addMethod("getUserOrgs");




// Authenticate a user via web services.  See lib.php which has the UserLogin routine
function userAuth($uname, $upass, $orgId) {
  if ($orgId == 0) {
    trigger_error("Failed to provide orgId");
    return;
  }

  $rc = UserLogin($uname, $upass, $orgId);
  if ($rc == RC_OK)
    return 1;
  return 0;
}
$server->addMethod("userAuth");



// Method to obtain a list of available exercises
function deleteOrg($orgNum)
{
  validateSession();

  // Ensure user is superuser
  if (!isset($_SESSION['superUser']) || !$_SESSION['superUser']) {
    error_log("CRIT(service): nonsuper user attempted to delete org");
    trigger_error("Permission denied.  You do not have rights to delete a user.");
    //return $error;
    return;
  }

  require_once('../includes/Organization.php');
  // Remove the specified organization number
  $org = new Organization($orgNum);
  $rc = $org->DeleteOrg($orgNum);
  if ($rc != RC_OK) {
    trigger_error("Failed to delete organization: ".RcToText($rc));
    return;
  }
  return 1;
}
// Add this method to the RPC server
$server->addMethod("deleteOrg");




// Method to obtain a list of available exercises
function deleteUser($userNum)
{
  validateSession();

  //error_log("deleteUser($userNum): service function called using session id: ".session_id());

  // Check rights to ensure that the user is at least an admin
  if (!(isset($_SESSION['superUser']) && $_SESSION['superUser']) &&
      !(isset($_SESSION['role']) && $_SESSION['role'] != ROLE_END_USER &&
  	$_SESSION['role'] != ROLE_CLASS_CONTACT_SUPERVISOR)) {
    error_log("CRIT(service): ".$_SESSION['userName']." who does not have rights attempted to delete user $userNum");
    trigger_error("Permission denied.  You do not have rights to delete a user.");
    return;
  }
  
  // Remove the specified user
  require_once('../includes/User.php');
  $usr = new User($_SESSION['orgId']);
  if ($usr->DeleteUser($userNum) == FALSE) {
    error_log("CRIT(service): Failed to remove user number $userNum from system.");
    trigger_error("Failed to delete user[$userNum]");
    return;
  }
  return 1;
}
// Register method with service
$server->addMethod("deleteUser");




// Add a user to a class
function toggleClassParticipant($userId, $trackId) {

  validateSession();

  // Check rights to ensure that the user is at least an admin
  if (!(isset($_SESSION['superUser']) && $_SESSION['superUser']) &&
      !(isset($_SESSION['role']) && $_SESSION['role'] != ROLE_END_USER &&
  	$_SESSION['role'] != ROLE_CLASS_CONTACT_SUPERVISOR)) {
    error_log("CRIT(service): ".$_SESSION['userName']." who does not have rights attempted to add a user to class $trackId");
    trigger_error("Permission denied.  You do not have rights to add a participant to a class.");
    return;
  }
  
  // Remove the specified user
  require_once('../includes/User.php');
  $usr = new User($_SESSION['orgId'], $userId);
  $rc = $usr->MemberOfTrack($trackId);
  if ($rc === TRUE) {
    if ($usr->RemoveFromTrack($trackId) !== RC_OK) {
      error_log("CRIT(service): Failed to remove user [$userId] from class [$trackId]");
      trigger_error("Failed to assign user to class [$trackId].");
      return;
    }
  } elseif ($rc === FALSE) {
    if ($usr->AssignToTrack($trackId) !== RC_OK) {
      error_log("CRIT(service): Failed to assign user [$userId] to class [$trackId]");
      trigger_error("Failed to assign user to class [$trackId].");
      return;
    }
  } else {
    error_log("CRIT(service): Failed to determine track membership for user [$userId] in track [$trackId]");
    trigger_error("Unable to determine track membership for user [$userId] in track [$trackId]");
    return;
  }

  return 1;
}
// Register method with service
$server->addMethod("toggleClassParticipant");





// Add a user to a class
function toggleClassAssignment($trackId, $questionId) {
  validateSession();

  // Check rights to ensure that the user is at least an admin
  if (!(isset($_SESSION['superUser']) && $_SESSION['superUser']) &&
      !(isset($_SESSION['role']) && $_SESSION['role'] != ROLE_END_USER &&
  	$_SESSION['role'] != ROLE_CLASS_CONTACT_SUPERVISOR)) {
    error_log("CRIT(service): ".$_SESSION['userName']." who does not have rights attempted to add a user to class $trackId");
    trigger_error("Permission denied.  You do not have rights to add a participant to a class.");
    return;
  }
  
  if (!is_numeric($trackId) || !is_numeric($questionId) || $trackId < 0 || $questionId < 0) {
    error_log("CRIT(service): Invalid argument provided to toggleClassAssignment(trackId=$trackId, questionId=$questionId)");
    trigger_error("Invalid argument: non-numerical argument passed to function.");
    return;
  }

  // Get track ids that use this question
  require_once('../includes/Track.php');
  $cls = new Track($_SESSION['orgId'], $trackId);
  $tIds = $cls->GetTrackIdsWithAssignedQuestion($questionId);

  // Handle case when question is already assigned and vice versa
  if (array_search($trackId, $tIds) !== FALSE) {
    $cls->UnAssignQuestion($questionId);
  } else {
    $seq = $cls->AssignQuestion($questionId);
    if ($seq === FALSE) {
      error_log("CRIT(service): Failed to assign question to track [$trackId]");
      trigger_error("Unable to assign question to track [$trackId]");
      return;
    }
    return $seq;
  }

  return 0;
}
// Register method with service
$server->addMethod("toggleClassAssignment");




// Update sequence numbers in a class
function updateClassSequence($trackId, $questionId, $newSequenceNum) {
  validateSession();

  // Check rights to ensure that the user is at least an admin
  if (!(isset($_SESSION['superUser']) && $_SESSION['superUser']) &&
      !(isset($_SESSION['role']) && $_SESSION['role'] != ROLE_END_USER &&
  	$_SESSION['role'] != ROLE_CLASS_CONTACT_SUPERVISOR)) {
    error_log("CRIT(service): ".$_SESSION['userName']." who does not have rights attempted to change class[$trackId] question sequence");
    trigger_error("Permission denied.  You do not have rights to change question sequence in a class.");
    return;
  }
  
  if (!is_numeric($trackId) || !is_numeric($questionId) || !is_numeric($newSequenceNum) || $trackId <= 0 || $questionId <= 0 || $newSequenceNum <= 0) {
    error_log("CRIT(service): Invalid argument provided to updateClassSequence(trackId=$trackId, questionId=$questionId, newSequenceNum=$newSequenceNum)");
    trigger_error("Invalid argument: non-numerical argument or non-positive value passed to function.");
    return;
  }

  // Get track ids that use this question
  require_once('../includes/Track.php');
  $cls = new Track($_SESSION['orgId'], $trackId);
  $tIds = $cls->GetTrackIdsWithAssignedQuestion($questionId);

  // Handle case when question is already assigned and vice versa
  if (array_search($trackId, $tIds) !== FALSE) {
    $cls->UnAssignQuestion($questionId);
  } else {
    $seq = $cls->AssignQuestion($questionId);
    if ($seq === FALSE) {
      error_log("CRIT(service): Failed to assign question to track [$trackId]");
      trigger_error("Unable to assign question to track [$trackId]");
      return;
    }
    return $seq;
  }

  return 0;
}
// Register method with service
$server->addMethod("updateClassSequence");



$server->processRequest(); //required in PHP4, otherwise called automatically by the destructor in PHP 5

?>
