<?php

include("../global.php");

$Content = '';
$bodyAttributes = '';

if($R != "review" && $R != "tracking" && $R != "admin") {
  $R=$_SESSION['session_ReviewMode'];
  $Q=$_SESSION['session_Question_Number'];
  $D=$_SESSION['session_Question_Domain_ID'];
  $L=$_SESSION['session_Question_Language_ID'];
  $T=$_SESSION['session_Question_Track_ID'];
} else {
  $_SESSION['session_ReviewMode'] = $R;
}

OpenDatabase();

$Query = "SELECT Notes, Version_Date, Content, Support_Info, Domain_ID, Language_ID, RequireCorrectAnswer FROM "
."Question WHERE Question_Number=$Q AND Language_ID=$L AND Domain_ID=$D";

$Result = mysql_db_query($session_DBName,$Query,$Link);

if($Result) {

  if($Row = mysql_fetch_array($Result)) {

      $Content .= $Row['Content'].'var strSupport_Info="'.$Row['Support_Info'].'"; var strNotes="'.$Row['Notes'].'";';
      $_SESSION['session_Question_Number'] = $Q;
      $_SESSION['session_Version_Date'] = $Row['Version_Date'];
      $_SESSION['session_Question_Domain_ID'] = $Row['Domain_ID'];
      $_SESSION['session_Question_Language_ID'] = $Row['Language_ID'];
      $_SESSION['session_Question_Track_ID'] = $T;
      $RequireCorrectAnswer = $Row['RequireCorrectAnswer'];

  } else {
    print("// Error retrieving question. Error Processing Results.");
  }
} else {
  print("// Error retrieving question. No Results in Query.");
}
CloseDatabase();

if($_SESSION['session_bInQuestion'] == TRUE) {
  $bodyAttributes .= "javascript:goMove(0);";
} elseif ($R != 'admin') {
  $_SESSION['session_bInQuestion'] = TRUE;
}

// Setup reasonable defaults
if (!isset($_SESSION['session_iCurrentPage'])) {
  $_SESSION['session_iCurrentPage'] = 0;
}


$smarty->assign('nResult', $_SESSION['session_nResult']);
$smarty->assign('iFirstChoice', $_SESSION['session_iFirstChoice']);
$smarty->assign('iStudentAnswer', $_SESSION['session_iSelected']);
$smarty->assign('iCurrentPage', $_SESSION['session_iCurrentPage']);
$smarty->assign('Org_Logo', $_SESSION['session_Organization_Logo']);
$smarty->assign('Org_Dir', $_SESSION['session_Organization_Directory']);
$smarty->assign('qType', $_REQUEST['qType']);
$smarty->assign('Content', $Content);
$smarty->assign('RequireCorrectAnswer', $RequireCorrectAnswer);
$smarty->assign('strMode', $R);

$smarty->assign('uiTheme', $_SESSION['uiTheme']);
$smarty->display('presentation/q_type.tpl');




/*
{if ($strMode == 'review' || $strMode == 'admin')}

  <!--div id="btnNextCover" style="position:absolute; left:697px; top:106px; width:8px; height:19px; z-index:5; visibility:hidden;">
  <img name="Cover" id="Cover" src="graphics/btn_FAQ_next_dim.gif" alt="" border="0" /></div-->


  <div id="btnBackCover" style="position:absolute; left:622px; top:106px; width:8px; height:19px; z-index:7; visibility:visible;">
  <img name="Cover" id="Cover" src="graphics/btn_FAQ_back_dim.gif" alt="" border="0"></div>

{else if ($strMode == 'tracking')}

  <!--div id="btnNext" style="position:absolute; left:697px; top:106px; width:8px; height:19px; z-index:4; visibility:hidden;"-->
  <a href="javascript: goMove(1);" onmouseout="document.images.btnNextImage.src=\'graphics/btn_FAQ_next_up.gif\' "
     onmouseover="document.images.btnNextImage.src=\'graphics/btn_FAQ_next_down.gif\' ">
  <img name="btnNextImage" id="btnNextImage" src="graphics/btn_FAQ_next_up.gif" alt="" border="0" /></a><!--/div-->

  <!--div id="btnNextCover" style="position:absolute; left:697px; top:106px; width:8px; height:19px; z-index:5; visibility:visible;"-->
  <img name="Cover" id="Cover" src="graphics/btn_FAQ_next_dim.gif" alt="" border="0" /><!--/div-->

  <!--div id="btnBack" style="position:absolute; left:622px; top:106px; width:8px; height:19px; z-index:6; visibility:hidden;"-->
  <a href="javascript: goMove(-1);" onmouseout="document.images.btnBackImage.src=\'graphics/btn_FAQ_back_up.gif\' "
     onmouseover="document.images.btnBackImage.src=\'graphics/btn_FAQ_back_down.gif\' ">
  <img name="btnBackImage" id="btnBackImage" src="graphics/btn_FAQ_back_up.gif" alt="" border="0" /></a><!--/div-->

  <div id="btnBackCover" style="position:absolute; left:622px; top:106px; width:8px; height:19px; z-index:7; visibility:visible;">
  <img name="Cover" id="Cover" src="graphics/btn_FAQ_back_dim.gif" alt="" border="0" /></div>
{/if}

  // Show Back to Menu Button


    </td>
  </tr>
</TABLE>
*/

?>


