<?php
	session_start();
?>
<html>
<head>
<style type="text/css">
	.Red {  font-family: Arial, Helvetica, sans-serif; font-size:10pt; color:#A31A2A;  }
	.Green {  font-family: Arial, Helvetica, sans-serif; font-size:10pt; color:#239E06;  }
	.Blue {  font-family: Arial, Helvetica, sans-serif; font-size:12pt; color:#2A5DC9; font-weight:bold; }
	.Title {  font-family: Arial, Helvetica, sans-serif; font-size:13pt; color:#FFFFFF; font-weight:bold; }
</style>
<script language="JavaScript">
	function MM_findObj(n, d) { //v3.0
	  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
	    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
	  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
	  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document); return x;
	}
</script>
</head>
<body bgcolor="#FFFFFF">
<?php
	$Action = "hrmail.php?Name=".$Name."&Email=".$Email;
	function Validate_Form()
	{
		global $Topic;
		global $Comments;
		global $Message;

		if(strlen($Topic)==0)
		{
			$Message = "<span class='red'>Please type a topic for this message</span>";
			return FALSE;
		}
		else if(strlen($Comments)==0)
		{
			$Message = "<span class='red'>Please type your questions or comments below.  Then click Send Message.</span>";
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	function SendHRMail()
	{
		global $Name;
		global $Email;
		global $Topic;
		global $Comments;
		global $session_User_Email;
		global $session_HR_Contact_Email;
		global $session_HR_Contact_Full_Name;
		global $Message;

		if(mail($Email, $Topic, $Comments, "From: $session_User_Email\r\n"."Reply-To: $session_User_Email\r\n"))
		{
			$Message = "<span class='Green'>Your message has been delivered to our server.</span>";
		}
		else
		{
			$Message = "<span class='red'>The server was unable to send your message.</span>";
		}
	}

	if($Insert==TRUE)
	{
		if(Validate_Form())
		{
			SendHRMail();
			print("<script language='JavaScript'>");
			print("parent.window.close();");
			print("</script>");
		}
	}
?>

<form name="SendHRMail" method="POST" action="<?php echo $Action; ?>">
  <div id="OldPassword" style="position:absolute; left:49px; top:15px; width:411px; height:43px; z-index:1">
    <span class="Blue">
    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To: <?php echo $Name ?><br>
    	From: <?php echo $session_Last_Name ?>, <?php echo $session_First_Name ?>
    </span>
  </div>
  <div id="TopicEntry" style="position:absolute; left:49px; top:80px; width:412px; height:43px; z-index:1">
    <span class="Blue">Topic:<br></span><textarea rows="2" cols="45" name="Topic"></textarea>
  </div>
  <div id="CommentsEntry" style="position:absolute; left:49px; top:152px; width:412px; height:43px; z-index:1">
    <span class="Blue">Comments:<br></span><textarea rows="6" cols="45" name="Comments"></textarea>
  </div>
  <input type="hidden" name="Insert" value="TRUE">
</form>
<div style="position:absolute; left:49px; top:0px; width:331px; height:43px; z-index:1">
	<?php echo $Message ?>
</div>
<div id="btnSubmit" style="position:absolute; left:140px; top:300px; width:98px; height:19px; z-index:4;"><a href="javascript:Formobj=MM_findObj('SendHRMail'); Formobj.submit();" onmouseout="document.images.btnSubmitImage.src='graphics/send_hr_up.gif' " onmouseover="document.images.btnSubmitImage.src='graphics/send_hr_down.gif' "><img name="btnSubmitImage" id="btnSubmitImage" src="graphics/send_hr_up.gif" alt="" border="0" /></a></div>
</body>
</html>