<?php

require_once('../includes/common.php');

// Set the current page for doing browser page relods
$_SESSION['iCurrentPage'] = PAGE_QUESTION;

if(!$_SESSION['reviewMode'] /* != 'admin' */) {

  if (isset($_REQUEST['nResult'])) {
    $_SESSION['nResult'] = $_REQUEST['nResult'];
  }

  $_SESSION['iFirstChoice'] = $_REQUEST['iFirstChoice'];
  
  if (isset($_REQUEST['iStudentAnswer'])) {
    $_SESSION['iSelected'] = $_REQUEST['iStudentAnswer'];
  }
  
}

$smarty->assign('qType', $_REQUEST['qType']);
$smarty->assign('Org_Dir', $_SESSION['orgDirectory']);
$smarty->assign('iCurrentPage', $_SESSION['iCurrentPage']);

$smarty->assign('uiTheme', $_SESSION['uiTheme']);
$smarty->display('presentation/q.tpl');

?>




