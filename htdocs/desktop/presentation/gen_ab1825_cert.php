<?php

require_once('../includes/common.php');
require_once('../includes/Assignment.php');


//error_log("Entering thanks.php with request vars: ".print_r($_REQUEST, TRUE));

// Ensure user has logged in, otherwise exit now
if (!isset($_SESSION['userId'])) {
  header("Location: /desktop/login/expired.php");
  exit;
}


// This block is used to support the case when this page is referred to
// from the reporting system by a logged in administrator rather than the
// user completing the training.
if (isset($_REQUEST['luid']) && is_numeric($_REQUEST['luid']))
  $uID = $_REQUEST['luid'];
else
  $uID = $_SESSION['userId'];

$u = new User($_SESSION['orgId'], $uID);
$a = new Assignment($_SESSION['orgId'], 0);

// This technically isn't really necessary but it does get
// passed as an additional validation routine.
if (isset($_REQUEST['assignmentId']) && is_numeric($_REQUEST['assignmentId'])) {
  if ($a->SetAssignmentId($_REQUEST['assignmentId']) != RC_OK)
    error_log("*** ERROR: Problem setting assignment ID to ".$_REQUEST['assignmentId']." in gen_ab1825_cert.php");
} else {
  if (isset($_REQUEST['trackId']) && is_numeric($_REQUEST['trackId'])) {
    if (!$a->GetAb1825LastCompleted($_REQUEST['trackId']))
      error_log("*** ERROR: gen_ab1825_cert.php called without a completed final question!");
  } else
    error_log("*** ERROR: gen_ab1825_cert.php called to create post produced certificate but no trackID specified");
}



if (isset($_REQUEST['luid']) ||
    (isset($_REQUEST['assignmentId']) &&
     ($a->questionId == 50900 && $a->CheckAb1825IncompleteCount() <= 1))) {
  $a->GenerateAb1825Certificate($u);
}

exit(0);

?>