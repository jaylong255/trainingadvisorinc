<?php

require_once('../includes/common.php');
require_once('../includes/Config.php');
require_once('
include("../includes/db.php");


if (isset($_REQUEST['displayHeader']) && $_REQUEST['displayHeader'] == 1) {
  $smarty->assign('displayHeader', TRUE);
}

OpenDatabase();

$Result = mysql_db_query($_SESSION['dbName'], "SELECT HR_Contact_ID FROM Organization", $Link);

if ($Result === FALSE) {
  error_log("contacts.php: SQL(".mysql_errno($Link)."): ".mysql_error());
}


if($Result) {

  $R = mysql_fetch_array($Result);
				
  $Query = "SELECT First_Name, Last_Name, Email, Phone "
    ."FROM User, HR_Contact_Info "
    ."WHERE HR_Contact_Info.User_ID=".$R['HR_Contact_ID']." AND User.User_ID=".$R['HR_Contact_ID'];
  if (DEBUG & DEBUG_SQL) {
    error_log("SQL: $Query");
  }
  $Result = mysql_query($Query);

  if ($Result === FALSE) {
    error_log("contacts.php: SQL(".mysql_errno($Link)."): ".mysql_error());
  }

  if($Result) {
    $Row = mysql_fetch_array($Result);
    if ($Row['Email'] != '') {
      $Location="mailform.php?Name=".urlencode($Row["First_Name"].' '.$Row['Last_Name'])."&Email=".urlencode($Row["Email"]);
      $smarty->assign('Location', $Location);
    }
    $smarty->assign('Full_Name', $Row['First_Name'].' '.$Row['Last_Name']);
    $smarty->assign('Phone', $Row['Phone']);
    $smarty->assign('Email', $Row['Email']);
  } else {
    $smarty->assign('noContact', TRUE);
  }
  
  if (isset($_SESSION['contactId']) && $_SESSION['contactId']) {
    $Query = "SELECT First_Name, Last_Name, Email, Phone "
      ."FROM User, HR_Contact_Info "
      ."WHERE HR_Contact_Info.User_ID=".$_SESSION['contactId']
      ." AND User.User_ID=HR_Contact_Info.User_ID";
    if (DEBUG & DEBUG_SQL) {
      error_log("SQL: $Query");
    }
    
    $Result = mysql_query($Query);
    
    if ($Result === FALSE) {
      error_log("contacts.php: SQL(".mysql_errno($Link)."): ".mysql_error());
    }
    
    if($Result && mysql_num_rows($Result) > 0) {
      $Row = mysql_fetch_array($Result);
      if (($Row['First_Name'].$Row['Last_Name']) != '' || !$Row['Phone'] != '' || !$Row['Email'] != '') {
	$Location="mailform.php?Name=".urlencode($Row["First_Name"].' '.$Row["Last_Name"])."&Email=".$Row["Email"];
	$smarty->assign('Org_Location', $Location);
	$smarty->assign('Org_Full_Name', $Row['First_Name'].' '.$Row['Last_Name']);
	$smarty->assign('Org_Phone', $Row['Phone']);
	$smarty->assign('Org_Email', $Row['Email']);
      }
    }
  }
}

CloseDatabase($Link);


//if (strlen($session_HR_Contact_Email) != 0) {
  //print('<a href="mailform.php" onmouseout="document.images.btnContactHRImage.src=\'graphics/contact_hr_up.gif\' " onmouseover="document.images.btnContactHRImage.src=\'graphics/contact_hr_down.gif\' "><img name="btnContactHRImage" id="btnContactHRImage" src="graphics/contact_hr_up.gif" alt="" border="0" /></a>');
//}

//$smarty->assign("session_Organization_Logo", $_SESSION['orgLogo']);


$smarty->assign('uiTheme', $_SESSION['uiTheme']);
$smarty->display('presentation/contacts.tpl');

?>
