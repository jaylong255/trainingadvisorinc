<?php

require_once('../includes/common.php');
require_once('../includes/Config.php');
require_once('../includes/Assignment.php');


// Ensure user has logged in, otherwise exit now
if (!isset($_SESSION['userId'])) {
  header("Location: /desktop/login/expired.php");
  exit(0);
}


$questionCaption = '';
$curDate = date('Y-m-d');

$config = new Config($_SESSION['orgId']);
$user = new User($_SESSION['orgId'], $_SESSION['userId']);
$assignment = new Assignment($_SESSION['orgId']);
$assignments = array();
$divAssignments = array();
$deptAssignments = array();


// Sets up the customized image layout atop the screen (if any)
SetupMotifDisplay($smarty, $config);
AssignContactInfo($smarty, $config->GetValue(CONFIG_ORG_CONTACT_ID), $user);

$smarty->assign('superUser', $_SESSION['superUser']);
$smarty->assign('role', $_SESSION['role']);

$smarty->assign('currentTab', $_SESSION['currentTab']);

$smarty->assign('firstName', $user->firstName);
$smarty->assign('lastName', $user->lastName);
$smarty->assign('preview', FALSE); // preview mode is used in the admin interface only for previewing editable questions


if (substr($config->GetValue(CONFIG_ORG_NEWS_URL), 0, 7) == 'http://') {
  $smarty->assign('newsUrl', $config->GetValue(CONFIG_ORG_NEWS_URL));
} elseif (!$config->GetValue(CONFIG_ORG_NEWS_URL)) {
  $smarty->assign('newsUrl', '');
} elseif (!file_exists(APPLICATION_ROOT.'/organizations/'.$config->GetValue(CONFIG_ORG_DIRECTORY).$config->GetValue(CONFIG_ORG_NEWS_URL))) {
  error_log("Specified file does not exist: ".APPLICATION_ROOT.'/organizations/'.$config->GetValue(CONFIG_ORG_DIRECTORY).$config->GetValue(CONFIG_ORG_NEWS_URL));
  $config->SetValue(CONFIG_ORG_NEWS_URL, '');
  $smarty->assign('newsUrl', '');
} else {
  $smarty->assign('newsUrl', URL_ROOT.'/organizations/'.$config->GetValue(CONFIG_ORG_DIRECTORY).$config->GetValue(CONFIG_ORG_NEWS_URL));
}


$assignments = $assignment->GetIncompleteAssignments($_SESSION['userId'], 0, 0);
if (is_numeric($assignments)) {
  error_log("portal.php: user->GetIncompleteAssignments() failed ($assignments): ".RcToText($assignments));
  $errMsg = "Failed to obtain your assignments, the database may be undergoing maintenance.  Please try later.";
}

if ($user->divisionId > 0) {
  $divAssignments = $assignment->GetIncompleteAssignments($_SESSION['userId'], $user->divisionId, 0);
  if (is_numeric($assignments)) {
    error_log("portal.php: user->GetIncompleteAssignments() failed ($assignments): ".RcToText($assignments));
    $errMsg = "Failed to obtain your assignments, the database may be undergoing maintenance.  Please try later.";
  }
}

if ($user->departmentId > 0) {
  $deptAssignments = $assignment->GetIncompleteAssignments($_SESSION['userId'], 0, $user->departmentId);
  if (is_numeric($assignments)) {
    error_log("portal.php: user->GetIncompleteAssignments() failed ($assignments): ".RcToText($assignments));
    $errMsg = "Failed to obtain your assignments, the database may be undergoing maintenance.  Please try later.";
  }
}

$completedAssignments = $assignment->GetCompletedAssignments($_SESSION['userId']);
if (is_numeric($completedAssignments)) {
  error_log("portal.php: user->GetCompletedAssignments() failed ($assignments): ".RcToText($completedAssignments));
  $errMsg = "Failed to obtain your assignments, the database may be undergoing maintenance.  Please try later.";
}

if (isset($_REQUEST['ab1825CompletedAssignmentId']) &&
    is_numeric($_REQUEST['ab1825CompletedAssignmentId'])) {
  $smarty->assign('ab1825CompletedAssignmentId', $_REQUEST['ab1825CompletedAssignmentId']);
} else {
  $smarty->assign('ab1825CompletedAssignmentId', 0);
}

if ($_SESSION['uiTheme'] == 'Blue') {
  $smarty->assign('showPoweredBy', FALSE);
} else {
  $smarty->assign('showPoweredBy', TRUE);
}

$smarty->assign('assignments', $assignments);
$smarty->assign('divAssignments', $divAssignments);
$smarty->assign('deptAssignments', $deptAssignments);
$smarty->assign('completedAssignments', $completedAssignments);

$smarty->assign('uiTheme', $_SESSION['uiTheme']);
$smarty->display('presentation/portal.tpl');

$config->Close();
// Apparently these are reusing the same database connections
// because they generate errors on close
//$user->Close();
//$assignment->Close();

?>
