<?php
	session_start();
?>
<html>
<head>
<style type="text/css">
	.QuestionText {  font-family: Arial, Helvetica, sans-serif; font-size:12pt; color:#000000; }
	.FoilText {  font-family: Arial, Helvetica, sans-serif; font-size:12pt; color:#000000; }
</style>
</head>
<body>
<?php
	if($session_ReviewMode!='admin')
	{
		$session_nResult=$nResult;
		$session_iFirstChoice=$iFirstChoice;
		//$session_iSelected=$iSelected;
		$session_iCurrentPage=$iCurrentPage;

		session_register("session_nResult");
		session_register("session_iFirstChoice");
		//session_register("session_iSelected");
		session_register("session_iCurrentPage");
	}
?>
<div id="theQuestion" style="position:absolute; left:24px; top:5px; width:325px; height:100px; z-index:2">
  <script language="JavaScript">
		var strObjectToWrite;
  	var strMediaPath='../organizations/<?php echo $session_Organization_Directory ?>/motif/'+parent.strMedia;
		if (window.parent)	{
			document.writeln(window.parent.name + "\n");
		}
		if (strMediaPath.indexOf(".mov") > -1)	{
			strObjectToWrite = '<OBJECT \n';
		  strObjectToWrite += 'CLASSID="clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B" WIDTH="320" HEIGHT="240" CODEBASE="http://www.apple.com/qtactivex/qtplugin.cab">';
			strObjectToWrite += '<PARAM name="SRC" VALUE="'+strMediaPath+'">\n';
			strObjectToWrite += '<PARAM name="AUTOPLAY" VALUE="true">\n';
			strObjectToWrite += '<PARAM name="CONTROLLER" VALUE="false">\n';
			strObjectToWrite += '<EMBED SRC="'+strMediaPath+'" WIDTH="320" HEIGHT="240" AUTOPLAY="true" CONTROLLER="false" PLUGINSPAGE="http://www.apple.com/quicktime/download/" qtsrcdontusebrowser qtsrcchokespeed=56000 qtsrc="'+strMediaPath+'"></EMBED>\n';
			strObjectToWrite += '</OBJECT>\n';
		}	else {
			if ( document.all )
			{
				strObjectToWrite = '<OBJECT\n';
				strObjectToWrite += 'ID="mediaPlayer"\n';
				strObjectToWrite += 'CLASSID="CLSID:22d6f312-b0f6-11d0-94ab-0080c74c7e95"\n';
				strObjectToWrite += 'CODEBASE="http://activex.microsoft.com/activex/controls/mplayer/en/nsmp2inf.cab#Version=5,1,52,701"\n';
				strObjectToWrite += 'STANDBY="Loading Microsoft Windows Media Player components..."\n';
				strObjectToWrite += 'TYPE="application/x-oleobject" width=320, height=240>\n';
				strObjectToWrite += '<PARAM NAME="fileName" VALUE="'+strMediaPath+'">\n';
				strObjectToWrite += '<PARAM NAME="animationatStart" VALUE="true">\n';
				strObjectToWrite += '<PARAM NAME="transparentatStart" VALUE="true">\n';
				strObjectToWrite += '<PARAM NAME="autoStart" VALUE="true">\n';
				strObjectToWrite += '<PARAM NAME="showControls" VALUE="true">\n';
				strObjectToWrite += '</OBJECT>\n';
			}
			else
			{
				strObjectToWrite = '<embed name="mediaPlayer" src="'+strMediaPath+'" showControls="true" animationatStart="true" transparentatStart="true" autoStart="true" pluginspage="http://microsoft.com/windows/mediaplayer/download/" type="application/x-mplayer2" width="320px" height="240px" bgcolor="#FFFFFF"></embed>\n';
			}
		}
		document.writeln(strObjectToWrite+"<BR>");
		document.writeln('<span class="QuestionText">' + parent.strStem + '</span>');
  </script>
</div>

<div id="theFoils" style="position:absolute; left:375px; top:5px; width:375px; height:100px; z-index:2">
  <span class="FoilText">
  <form id="foils">
  <table border="0" cellpadding="3" cellspacing="5">
  		<script language="JavaScript">
  			if (parent.strFoil1.length > 0){
  				document.writeln('<tr valign="top" align="left"><td width="12%"><input type="radio" id="foil1" name="foil" onclick="javascript:parent.itemSelected(1);"><label for="foil1"><b>A.</b></label></td>');
  				document.writeln('<td><label for="foil1">' + parent.strFoil1 + '</label></td></tr>');
  			}
  			if (parent.strFoil2.length > 0){
				document.writeln('<tr valign="top" align="left"><td><input type="radio" id="foil2" name="foil" onclick="javascript:parent.itemSelected(2);"><label for="foil2"><b>B.</b></label></td>');
  				document.writeln('<td><label for="foil2">' + parent.strFoil2 + '</label></td></tr>');
  			}
  			if (parent.strFoil3.length > 0){
				document.writeln('<tr valign="top" align="left"><td><input type="radio" id="foil3" name="foil" onclick="javascript:parent.itemSelected(3);"><label for="foil3"><b>C.</b></label></td>');
  				document.writeln('<td><label for="foil3">' + parent.strFoil3 + '</label></td></tr>');
  			}
  			if (parent.strFoil4.length > 0){
				document.writeln('<tr valign="top" align="left"><td><input type="radio" id="foil4" name="foil" onclick="javascript:parent.itemSelected(4);"><label for="foil4"><b>D.</b></label></td>');
  				document.writeln('<td><label for="foil4">' + parent.strFoil4 + '</label></td></tr>');
  			}
  			if (parent.strFoil5.length > 0){
				document.writeln('<tr valign="top" align="left"><td><input type="radio" id="foil5" name="foil" onclick="javascript:parent.itemSelected(5);"><label for="foil5"><b>E.</b></label></td>');
  				document.writeln('<td><label for="foil5">' + parent.strFoil5 + '</label></td></tr>');
  			}
  		</script>
  </table>
  </form>
  </span>
</div>

</body>
</html>