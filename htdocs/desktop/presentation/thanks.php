<?php

require_once('../includes/common.php');
require_once('../includes/Assignment.php');

//error_log("Entering thanks.php with request vars: ".print_r($_REQUEST, TRUE));

// Ensure user has logged in, otherwise exit now
if (!isset($_SESSION['userId'])) {
  header("Location: /desktop/login/expired.php");
  exit;
}


// If this is the demo organization then do not update record status
if ($_SESSION['orgId'] == 2 || $_SESSION['orgId'] == 404) {
  error_log('thanks.php: returning to the portal because this is a demo organization');
  header('Location: portal.php');
  exit(0);
}


//$errMsg = '';
$nextChild = 0;
$startTime = 0;
$endTime = time();
$exitInfo = 0;
$u = new User($_SESSION['orgId'], $_SESSION['userId']);
$a = NULL;


// Reset navigation if tabs are messed up...just to be sure
if ($_SESSION['currentTab'] < 100) {
  $_SESSION['currentTab'] = TAB_ASSIGNED;
}


//error_log("thanks.php: assignmentID=".$_REQUEST['assignmentId'].", nextChild=".$_REQUEST['nextChild'].", exitInfo=".$_REQUEST['exitInfo'].", firstAnswer=".$_REQUEST['firstAnswer'].", finalAnswer=".$_REQUEST['finalAnswer'].", correctAnswer=".$_REQUEST['correctAnswer']);

// Setup next child if submitted
if (isset($_REQUEST['nextChild']) && $_REQUEST['nextChild'] &&
    is_numeric($_REQUEST['nextChild'])) {
  $nextChild = (int)$_REQUEST['nextChild'];
}


// Get the start time so we can calc time delta and how long it took to answer question
if (isset($_REQUEST['startTime']) && $_REQUEST['startTime'] &&
    is_numeric($_REQUEST['startTime'])) {
  $startTime = $_REQUEST['startTime'];
} else {
  error_log("thanks.php: problem with request start time var(".$_REQUEST['startTime'].")");
}


if (!$nextChild) {

  // Check for understanding of questions
  // Technically a user could fudge this value, but not when a message is actually sent
  if (isset($_REQUEST['exitInfo']) && is_numeric($_REQUEST['exitInfo'])) {
    if ($_REQUEST['exitInfo'] == 1)
      $exitInfo = 1;
    elseif ($_REQUEST['exitInfo'] == 2)
      $exitInfo = 2;
    else
      $exitInfo = 0;
  }


  // If we need to send a message do so now and set exitInfo = Contact
  if ((!isset($_REQUEST['preview']) || !$_REQUEST['preview']) &&
      isset($_REQUEST['msgToAddr']) && $_REQUEST['msgToAddr'] &&
      isset($_REQUEST['msgSubject']) && $_REQUEST['msgSubject'] &&
      isset($_REQUEST['msgBody']) && $_REQUEST['msgBody']) {
    
    //    error_log("Preparing to send mail to $_REQUEST[msgToAddr] from -".$u->firstName.' '.$u->lastName.' '.$u->email.'-');
    
    // Set exit info level indicating contact was chosen instead of understood
    $exitInfo = 2;

    // When the user has an empty email address then contact should still be made
    // but an additional warning message must be included
    $u->firstName = trim($u->firstName);
    $u->lastName = trim($u->lastName);
    $u->email = trim($u->email);

    // The first name and last name are required records when editing or importing
    // users but if for some reason they wind up empty we will the login as last resort.
    if (empty($u->firstName) &&
	empty($u->lastName))
      $u->firstName = $u->login;

    // Same for email
    if (empty($u->email)) {
      $u->email='no-reply@trainingadvisorinc.com';
      $_REQUEST['msgBody'] = "Do not reply to this message.  A trainee has requested that you be contacted, however our system currently does not have an email address on record for the user requesting contact so you will need to find an alternate way of responding to the sender of the following message:\n\n".$_REQUEST['msgBody'];
    }
    
    // Send the message to the intended recipient
    if (preg_match("/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i", $_REQUEST['msgToAddr']) &&
	preg_match("/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i", $u->email) &&
	preg_match("/^[\w\s']+$/i", $u->firstName.' '.$u->lastName)) {
      //      !preg_match("/[\x00\x08\x0B\x0C\x0E-\x1F]/", $_REQUEST['msgSubject']))
      str_replace("\r\n", "", $_REQUEST['msgSubject']);
      str_replace("\r", "", $_REQUEST['msgSubject']);
      str_replace("\n", "", $_REQUEST['msgSubject']);
      $addHeaders = 'From: "'.$u->firstName.' '.$u->lastName.'" <'.$u->email.'>';
      //      error_log("Sending mail to $_REQUEST[msgToAddr]");
      if (!mail($_REQUEST['msgToAddr'], $_REQUEST['msgSubject'], $_REQUEST['msgBody'], $addHeaders)) {
	error_log("Failed to call mail($_REQUEST[msgToAddr], $_REQUEST[msgSubject], $_REQUEST[msgBody], $addHeaders) successfully");
      }
    }
  }
}


// This handles the case where it is a review of a child questions
if ($nextChild && isset($_REQUEST['review']) && $_REQUEST['review']) {
  header("Location: q_type.php?assignmentId=$nextChild&review=".$_REQUEST['review']);
  exit(0);
}


// This handles the case where it is a review of a child question
if ($nextChild && isset($_REQUEST['preview']) && $_REQUEST['preview']) {
  //error_log("****===> Redirecting to before: q_type.php?questionId=$nextChild&review=0&preview=1&languageId=".$a->languageId);
  $a = new Question($_SESSION['orgId'], $nextChild);
  //error_log("****===> Redirecting to after: q_type.php?questionId=$nextChild&review=0&preview=1&languageId=".$a->languageId);
  header("Location: q_type.php?questionId=$nextChild&review=0&preview=1&languageId=".$a->languageId);
  exit(0);
}


// If exitInfo is 0 (question not answered) for any reason, simply return to portal
if ($exitInfo == 0 && !$nextChild) {
  // This should not happen because code in the templates prevents this
  // from getting called and rightly so since the window can not be closed
  // with javascript in the location header
  error_log("*** ERROR: Data integrity problem, exitInfo=$exitInfo and nextChild=$nextChild, check Assignment ID: ".$a->assignmentId);
  header('Location: portal.php');
  exit(0);
}


// Also if the user is simply reviewing the question then return as well of course
// any message should be sent first.
if ((isset($_REQUEST['review']) && is_numeric($_REQUEST['review']) && $_REQUEST['review']) ||
    (isset($_REQUEST['preview']) && is_numeric($_REQUEST['preview']) && $_REQUEST['preview'])) {
  header('Location: portal.php');
  exit(0);
}


$a = new Assignment($_SESSION['orgId'], $_REQUEST['assignmentId']);


// If data is good, then u and a should come back fine.  If not, likely
// either been tampered with or db offline
// TODO: Clean this up so it is handled more gracefully
if (!$a || !$u) {
  error_log('*** ERROR: assignment or user objects are null, fatal error');
  header('Location: portal.php');
  exit(0);
}


// Now we can update the assignment
$a->Completed($_REQUEST['firstAnswer'], $_REQUEST['finalAnswer'],
	      $_REQUEST['correctAnswer'], $exitInfo, (int)($endTime - $startTime));


if ($nextChild) {
  //  error_log("+_+_+_+_+ relocating browser to next question: $nextChild vi q_type.php?assignemntId=$nextChild");
  header("Location: q_type.php?assignmentId=$nextChild");
} elseif ($a->parentAssignmentId) { // Could be the last child
  // If this section gets executed then the assignment is the last
  // in a set of parent child questions and it is now time to update
  // exit info (understood/contact) for all questions in the set
  $a->UpdateAssignmentSetExitInfo($exitInfo);
  header('Location: portal.php');
} else {
  header('Location: portal.php');
}

exit(0);

?>