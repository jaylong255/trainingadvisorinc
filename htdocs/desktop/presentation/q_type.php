<?php

require_once('../includes/common.php');
require_once('../includes/Config.php');
require_once('../includes/Assignment.php');


// Ensure user has logged in, otherwise exit now
if (!isset($_SESSION['userId'])) {
  header("Location: /desktop/login/expired.php");
  exit(0);
}


$a = NULL;
$user = new User($_SESSION['orgId'], $_SESSION['userId']);
$content = '';
$progress = 0;
$assignmentTrackId = 0;
$questionSetSize = 0;
$incorrectInSet = 0;

if (isset($_REQUEST['assignmentId'])) {
  $assignmentId = $_REQUEST['assignmentId'];
} else {
  $assignmentId = NULL;
}
     


unset($_SESSION['iCurrentPage']);

if (isset($_REQUEST['assignmentId'])) {
  // Added to support the parent child question relationships
  do {
    $a = new Assignment($_SESSION['orgId'], $assignmentId);
    if ($a->GetIncompleteInSet() == 0)
      break;
    $assignmentId = $a->nextAssignmentId;
  } while (substr($a->completionDate, 0, 10) != '0000-00-00' && $assignmentId);
  $assignmentTrackId = $a->trackId;
  if ($a->questionId == 50900) {
    if ($a->CheckAb1825IncompleteCount() > 1) {
      $smarty->assign('uiTheme', $_SESSION['uiTheme']);
      $smarty->display('presentation/ab1825_incomplete.tpl');
      exit(0);
    } else
      $smarty->assign('genAb1825Cert', TRUE);
  }
  $incorrectInSet = $a->GetIncorrectInSet();
} elseif (isset($_REQUEST['questionId'])) {
  $a = new Question($_SESSION['orgId'], $_REQUEST['questionId']);
}

if (!$a) {
  $errMsg .= "q_type.php: Failed to create new assignment object<BR>\n";
}

$config = new Config($_SESSION['orgId']);
SetupMotifDisplay($smarty, $config);
AssignContactInfo($smarty, $config->GetValue(CONFIG_ORG_CONTACT_ID), $user, $assignmentTrackId);

//$content .= $a->content."\n".'var strSupport_Info="'.$a->supportInfo.'"; '."\n".'var strNotes="'.$a->notes.'";'."\n";

// One case is an assignment that has already been completed and is being reviewed
if (isset($_REQUEST['assignmentId'])) {
  if (preg_match("/20\d{2}(-\d{2}){2}/", substr($a->completionDate, 0, 10))) {
    $smarty->assign('review', 1);
    //$smarty->assign('assignmentId', $a->assignmentId);
  } else { // assignment is being completed
    $smarty->assign('review', 0);
    //$smarty->assign('assignmentId', $a->assignmentId);
  }
  if ($a->questionSetSize == 0)
    $smarty->assign('progStep', 0);
  else
    $smarty->assign('progStep', (int)(100.0 / $a->questionSetSize / 5));
  $smarty->assign('preview', 0);
} elseif (isset($_REQUEST['questionId']) && $_REQUEST['questionId']) {
  $smarty->assign('review', 0);
  $smarty->assign('preview', 1);
  //$smarty->assign('assignmentId', 0);
} else {
  error_log("q_type.php: called with invalid parameters, unable to determine if assignment, review, or preview");
}


if ($config->GetValue(CONFIG_DISPLAY_CHOICES_USE_QUESTION_SETTING) == 0 ||
    !$a->displayChoices || $a->displayChoices == 'default') {
  $smarty->assign('displayChoices', $config->GetValue(CONFIG_DISPLAY_CHOICES));
} else {
  $smarty->assign('displayChoices', $a->displayChoices);
}

if ($a->frameTypeId >= 2 && $a->frameTypeId <= 6 &&
    !file_exists('../organizations/'.$config->GetValue(CONFIG_ORG_DIRECTORY).'/motif/'.$a->mediaFilePath)) {
  $smarty->assign('contentErrorMessage', 'Media file not found, please go back and click on the Contact link for further assistance.');
}

// Escape quotes on individual data fields.
$a->PrepForDisplay();
$smarty->assign('orgDirectory', $config->GetValue(CONFIG_ORG_DIRECTORY));
$smarty->assign('startTime', time());
$smarty->assign('incorrectInSet', $incorrectInSet);
$smarty->assign('a', $a);
//$smarty->assign('content', $content);
//$smarty->assign('requireCorrectAnswer', $a->requireCorrectAnswer);
//$smarty->assign('capConfirmThanks', $a->capConfirmThanks);
$smarty->assign('uiTheme', $_SESSION['uiTheme']);
$smarty->display('presentation/q_type.tpl');

?>
