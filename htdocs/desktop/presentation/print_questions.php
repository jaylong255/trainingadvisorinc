<?php
require_once('../includes/common.php');
require_once('../includes/Config.php');
require_once('../includes/Question.php');
require_once('../includes/Assignment.php');


// Ensure user has logged in, otherwise exit now
if (!isset($_SESSION['userId'])) {
  header("Location: /desktop/login/expired.php");
  exit;
}

//$_SESSION['currentTab'] = TAB_COMPLETED;

$questionList = array();
$questionIds = array();
$config = new Config($_SESSION['orgId']);
$assignment = NULL;

if (!$config->GetValue(CONFIG_LICENSED_PRINT_QUESTIONS)) {
  header("Location: /desktop/login/perm_denied.php");
  exit;
}

if (isset($_REQUEST['aId']) && !empty($_REQUEST['aId'])) {
  foreach($_REQUEST['aId'] as $assignmentId) {
    if (!$assignment)
      $assignment = new Assignment($_SESSION['orgId'], $assignmentId);
    else
      $assignment->SetAssignmentId($assignmentId);
    
    if (!array_search($assignment->questionId, $questionIds)) {
      $questionIds[] = $assignment->questionId;
      $q = new Question($_SESSION['orgId'], $assignment->questionId);
      $q->correctAnswer = $q->AnswerNumberToLetter();
      $questionList[] = $q;
    }
  }
} else {
  error_log('WARNING: No questions were selected for printing');
}

$smarty->assign('questions', $questionList);
$smarty->assign('uiTheme', $_SESSION['uiTheme']);
$smarty->display('presentation/print_questions.tpl');

exit(0);

?>
