<?php


require_once('../includes/common.php');

if (isset($_REQUEST['trackId']) && is_numeric($_REQUEST['trackId']) &&
    $_SESSION['trackAdmin'] && $_REQUEST['trackId'] != $_SESSION['userId']) {
  $smarty->assign('wrongTrack', TRUE);
}

$smarty->assign('uiTheme', 'Default');
$smarty->display('login/perm_denied.tpl');

?>
