<?php


/////////////////////////////////////////////////////////////////////////////
//	change_prefs.php
//	  Used to change preference values throughout the application
//


// Include libs which starts session and creates smarty objects
include_once ('../includes/common.php');
include_once ('../includes/Organization.php');


// Ensure user has logged in, otherwise exit now
if (!isset($_SESSION['userId'])) {
  //    echo "Required session login information not present.";
  header("Location: /desktop/login/expired.php");
  exit;
}


// Get the return URL.  If one was passed in, we redirect there
// otherwise, send the user back to where they came from

if (!isset($_REQUEST['returnUrl'])) {
  header("Location: /desktop/warning.php");
  exit;
}

$returnUrl = $_REQUEST['returnUrl'];

// The set of variables in this block are only accessible by the super user
if ($_SESSION['superUser']) {

  // If user is not a super admin then only show their organization
  if (isset($_REQUEST['orgSortDescending']) && $_SESSION['superUser']) {
    $_SESSION['orgSortDescending'] = $_REQUEST['orgSortDescending'];
  }
  
  if (isset($_REQUEST['orgId']) && $_SESSION['superUser'] &&
      is_numeric($_REQUEST['orgId']) && $_REQUEST['orgId'] > 0) {
    //echo "CALLING CHANGEORG()<BR>\n";
    ChangeOrg($_REQUEST['orgId']);
    // Add param back to the org list to let it know that the org must be changed
    $returnUrl .= "?changeOrg=1";
  }

  if (isset($_REQUEST['orgSortColumn'])) {
    //Nice new feature that if the current column is already the sort column
    //then flip the ascending descending property to emulate windows with which
    //everyone and their dog is already familiar
    if ($_REQUEST['orgSortColumn'] == $_SESSION['orgSortColumn']) {
      $_SESSION['orgSortDescending'] ^= 1;
    } else {
      $_SESSION['orgSortColumn'] = $_REQUEST['orgSortColumn'];
    }
  }
  
  if (isset($_REQUEST['toggleOrgColumn'])) {
    $_SESSION['orgShowColumns'] ^= $_REQUEST['toggleOrgColumn'];
    // If they deselected the one that was getting sorted on, pick another
    if (!($_SESSION['orgShowColumns'] & $_REQUEST['toggleOrgColumn']))
      for ($i = 0; $i < 32; $i++)
	if ($_SESSION['orgShowColumns'] & 1 << $i)
	  $_SESSION['orgSortColumn'] = 1 << $i;
  }
  
  if (isset($_REQUEST['orgPageNumber']) && is_numeric($_REQUEST['orgPageNumber']) &&
      $_REQUEST['orgPageNumber'] > 0 && $_REQUEST['orgPageNumber'] <= MAX_PAGES) {
    $_SESSION['orgPageNumber'] = $_REQUEST['orgPageNumber'];
  }

}



if (isset($_REQUEST['showRows']) && is_numeric($_REQUEST['showRows']) &&
    $_REQUEST['showRows'] > 0) {
  $_SESSION['rowsPerPage'] = $_REQUEST['showRows'];
}



// User display preferences
// Toggling columns in the user list
if (isset($_REQUEST['toggleUserColumn'])) {
  $_SESSION['userShowColumns'] ^= $_REQUEST['toggleUserColumn'];
  // If they deselected the one that was getting sorted on, pick another
  if (!($_SESSION['userShowColumns'] & $_REQUEST['toggleUserColumn']))
    for ($i = 0; $i < 32; $i++)
      if ($_SESSION['userShowColumns'] & 1 << $i)
	$_SESSION['userSortColumn'] = 1 << $i;
}

if (isset($_REQUEST['userSortColumn'])) {
  //Nice new feature that if the current column is already the sort column
  //then flip the ascending descending property to emulate windows with which
  //everyone and their dog is already familiar
  if ($_REQUEST['userSortColumn'] == $_SESSION['userSortColumn']) {
    $_SESSION['userSortDescending'] ^= 1;
  } else {
    $_SESSION['userSortColumn'] = $_REQUEST['userSortColumn'];
  }
}
  
// If user is not an admin then only show their organization
if (isset($_REQUEST['userSortDescending'])) {
  $_SESSION['userSortDescending'] = $_REQUEST['userSortDescending'];
}
  
if (isset($_REQUEST['userPageNumber']) && is_numeric($_REQUEST['userPageNumber']) &&
    $_REQUEST['userPageNumber'] > 0 && $_REQUEST['userPageNumber'] <= MAX_PAGES) {
  $_SESSION['userPageNumber'] = $_REQUEST['userPageNumber'];
}




// Participants display preferences
// Toggling columns in the user list
if (isset($_REQUEST['toggleParticipantColumn'])) {
  $_SESSION['participantShowColumns'] ^= $_REQUEST['toggleParticipantColumn'];
  // If they deselected the one that was getting sorted on, pick another
  if (!($_SESSION['participantShowColumns'] & $_REQUEST['toggleParticipantColumn']))
    for ($i = 0; $i < 32; $i++)
      if ($_SESSION['participantShowColumns'] & 1 << $i)
	$_SESSION['participantSortColumn'] = 1 << $i;
}

if (isset($_REQUEST['participantSortColumn'])) {
  //Nice new feature that if the current column is already the sort column
  //then flip the ascending descending property to emulate windows with which
  //everyone and their dog is already familiar
  if ($_REQUEST['participantSortColumn'] == $_SESSION['participantSortColumn']) {
    $_SESSION['participantSortDescending'] ^= 1;
  } else {
    $_SESSION['participantSortColumn'] = $_REQUEST['participantSortColumn'];
  }
}
  
// If participant is not an admin then only show their organization
if (isset($_REQUEST['participantSortDescending'])) {
  $_SESSION['participantSortDescending'] = $_REQUEST['participantSortDescending'];
}
  
if (isset($_REQUEST['participantPageNumber']) && is_numeric($_REQUEST['participantPageNumber']) &&
    $_REQUEST['participantPageNumber'] > 0 && $_REQUEST['participantPageNumber'] <= MAX_PAGES) {
  $_SESSION['participantPageNumber'] = $_REQUEST['participantPageNumber'];
}

if (isset($_REQUEST['participantShowAll']) && is_numeric($_REQUEST['participantShowAll'])) {
  $_SESSION['participantShowAll'] = $_REQUEST['participantShowAll'];
}




// Assignments display preferences
// Toggling columns in the user list
if (isset($_REQUEST['toggleAssignmentColumn'])) {
  $_SESSION['assignmentShowColumns'] ^= $_REQUEST['toggleAssignmentColumn'];
  // If they deselected the one that was getting sorted on, pick another
  if (!($_SESSION['assignmentShowColumns'] & $_REQUEST['toggleAssignmentColumn']))
    for ($i = 0; $i < 32; $i++)
      if ($_SESSION['assignmentShowColumns'] & 1 << $i)
	$_SESSION['assignmentSortColumn'] = 1 << $i;
}

if (isset($_REQUEST['assignmentSortColumn'])) {
  //Nice new feature that if the current column is already the sort column
  //then flip the ascending descending property to emulate windows with which
  //everyone and their dog is already familiar
  if ($_REQUEST['assignmentSortColumn'] == $_SESSION['assignmentSortColumn']) {
    $_SESSION['assignmentSortDescending'] ^= 1;
  } else {
    $_SESSION['assignmentSortColumn'] = $_REQUEST['assignmentSortColumn'];
  }
}
  
// If assignment is not an admin then only show their organization
if (isset($_REQUEST['assignmentSortDescending'])) {
  $_SESSION['assignmentSortDescending'] = $_REQUEST['assignmentSortDescending'];
}
  
if (isset($_REQUEST['assignmentPageNumber']) && is_numeric($_REQUEST['assignmentPageNumber']) &&
    $_REQUEST['assignmentPageNumber'] > 0 && $_REQUEST['assignmentPageNumber'] <= MAX_PAGES) {
  $_SESSION['assignmentPageNumber'] = $_REQUEST['assignmentPageNumber'];
}

if (isset($_REQUEST['assignmentShowAll']) && is_numeric($_REQUEST['assignmentShowAll'])) {
  $_SESSION['assignmentShowAll'] = $_REQUEST['assignmentShowAll'];
}




// Track display preferences
// Toggling columns in the track list
if (isset($_REQUEST['toggleTrackColumn'])) {
  $_SESSION['trackShowColumns'] ^= $_REQUEST['toggleTrackColumn'];
  // If they deselected the one that was getting sorted on, pick another
  if (!($_SESSION['trackShowColumns'] & $_REQUEST['toggleTrackColumn']))
    for ($i = 0; $i < 32; $i++)
      if ($_SESSION['trackShowColumns'] & 1 << $i)
	$_SESSION['trackSortColumn'] = 1 << $i;
}

if (isset($_REQUEST['trackSortColumn'])) {
  //Nice new feature that if the current column is already the sort column
  //then flip the ascending descending property to emulate windows with which
  //everyone and their dog is already familiar
  if ($_REQUEST['trackSortColumn'] == $_SESSION['trackSortColumn']) {
    $_SESSION['trackSortDescending'] ^= 1;
  } else {
    $_SESSION['trackSortColumn'] = $_REQUEST['trackSortColumn'];
  }
}
  
// If track is not an admin then only show their organization
if (isset($_REQUEST['trackSortDescending'])) {
  $_SESSION['trackSortDescending'] = $_REQUEST['trackSortDescending'];
}
  
if (isset($_REQUEST['trackPageNumber']) && is_numeric($_REQUEST['trackPageNumber']) &&
    $_REQUEST['trackPageNumber'] > 0 && $_REQUEST['trackPageNumber'] <= MAX_PAGES) {
  $_SESSION['trackPageNumber'] = $_REQUEST['trackPageNumber'];
}



// Progresss display preferences
// Toggling columns in the user list
if (isset($_REQUEST['toggleProgressColumn'])) {
  $_SESSION['progressShowColumns'] ^= $_REQUEST['toggleProgressColumn'];
  // If they deselected the one that was getting sorted on, pick another
  if (!($_SESSION['progressShowColumns'] & $_REQUEST['toggleProgressColumn']))
    for ($i = 0; $i < 32; $i++)
      if ($_SESSION['progressShowColumns'] & 1 << $i)
	$_SESSION['progressSortColumn'] = 1 << $i;
}

if (isset($_REQUEST['progressSortColumn'])) {
  //Nice new feature that if the current column is already the sort column
  //then flip the ascending descending property to emulate windows with which
  //everyone and their dog is already familiar
  if ($_REQUEST['progressSortColumn'] == $_SESSION['progressSortColumn']) {
    $_SESSION['progressSortDescending'] ^= 1;
  } else {
    $_SESSION['progressSortColumn'] = $_REQUEST['progressSortColumn'];
  }
}
  
// If progress is not an admin then only show their organization
if (isset($_REQUEST['progressSortDescending'])) {
  $_SESSION['progressSortDescending'] = $_REQUEST['progressSortDescending'];
}
  
if (isset($_REQUEST['progressPageNumber']) && is_numeric($_REQUEST['progressPageNumber']) &&
    $_REQUEST['progressPageNumber'] > 0 && $_REQUEST['progressPageNumber'] <= MAX_PAGES) {
  $_SESSION['progressPageNumber'] = $_REQUEST['progressPageNumber'];
}

if (isset($_REQUEST['progressShowAll']) && is_numeric($_REQUEST['progressShowAll'])) {
  $_SESSION['progressShowAll'] = $_REQUEST['progressShowAll'];
}




// Faq display preferences
// Toggling columns in the faq list
if (isset($_REQUEST['toggleFaqColumn'])) {
  $_SESSION['faqShowColumns'] ^= $_REQUEST['toggleFaqColumn'];
  // If they deselected the one that was getting sorted on, pick another
  if (!($_SESSION['faqShowColumns'] & $_REQUEST['toggleFaqColumn']))
    for ($i = 0; $i < 32; $i++)
      if ($_SESSION['faqShowColumns'] & 1 << $i)
	$_SESSION['faqSortColumn'] = 1 << $i;
}

if (isset($_REQUEST['faqSortColumn'])) {
  //Nice new feature that if the current column is already the sort column
  //then flip the ascending descending property to emulate windows with which
  //everyone and their dog is already familiar
  if ($_REQUEST['faqSortColumn'] == $_SESSION['faqSortColumn']) {
    $_SESSION['faqSortDescending'] ^= 1;
  } else {
    $_SESSION['faqSortColumn'] = $_REQUEST['faqSortColumn'];
  }
}
  
// If faq is not an admin then only show their organization
if (isset($_REQUEST['faqSortDescending'])) {
  $_SESSION['faqSortDescending'] = $_REQUEST['faqSortDescending'];
}
  
if (isset($_REQUEST['faqPageNumber']) && is_numeric($_REQUEST['faqPageNumber']) &&
    $_REQUEST['faqPageNumber'] > 0 && $_REQUEST['faqPageNumber'] <= MAX_PAGES) {
  $_SESSION['faqPageNumber'] = $_REQUEST['faqPageNumber'];
}



// Question display preferences
// Toggling columns in the question list
if (isset($_REQUEST['toggleQuestionColumn'])) {
  $_SESSION['questionShowColumns'] ^= $_REQUEST['toggleQuestionColumn'];
  // If they deselected the one that was getting sorted on, pick another
  if (!($_SESSION['questionShowColumns'] & $_REQUEST['toggleQuestionColumn']))
    for ($i = 0; $i < 32; $i++)
      if ($_SESSION['questionShowColumns'] & 1 << $i)
	$_SESSION['questionSortColumn'] = 1 << $i;
}

if (isset($_REQUEST['questionSortColumn'])) {
  //Nice new feature that if the current column is already the sort column
  //then flip the ascending descending property to emulate windows with which
  //everyone and their dog is already familiar
  if ($_REQUEST['questionSortColumn'] == $_SESSION['questionSortColumn']) {
    $_SESSION['questionSortDescending'] ^= 1;
  } else {
    $_SESSION['questionSortColumn'] = $_REQUEST['questionSortColumn'];
  }
}
  
// If question is not an admin then only show their organization
if (isset($_REQUEST['questionSortDescending'])) {
  $_SESSION['questionSortDescending'] = $_REQUEST['questionSortDescending'];
}
  
if (isset($_REQUEST['questionPageNumber']) && is_numeric($_REQUEST['questionPageNumber']) &&
    $_REQUEST['questionPageNumber'] > 0 && $_REQUEST['questionPageNumber'] <= MAX_PAGES) {
  $_SESSION['questionPageNumber'] = $_REQUEST['questionPageNumber'];
}



// Question display preferences
// Toggling columns in the question list
if (isset($_REQUEST['toggleArchiveColumn'])) {
  $_SESSION['archiveShowColumns'] ^= $_REQUEST['toggleArchiveColumn'];
  // If they deselected the one that was getting sorted on, pick another
  if (!($_SESSION['archiveShowColumns'] & $_REQUEST['toggleArchiveColumn']))
    for ($i = 0; $i < 32; $i++)
      if ($_SESSION['archiveShowColumns'] & 1 << $i)
	$_SESSION['archiveSortColumn'] = 1 << $i;
}

if (isset($_REQUEST['archiveSortColumn'])) {
  //Nice new feature that if the current column is already the sort column
  //then flip the ascending descending property to emulate windows with which
  //everyone and their dog is already familiar
  if ($_REQUEST['archiveSortColumn'] == $_SESSION['archiveSortColumn']) {
    $_SESSION['archiveSortDescending'] ^= 1;
  } else {
    $_SESSION['archiveSortColumn'] = $_REQUEST['archiveSortColumn'];
  }
}
  
// If question is not an admin then only show their organization
if (isset($_REQUEST['archiveSortDescending'])) {
  $_SESSION['archiveSortDescending'] = $_REQUEST['archiveSortDescending'];
}
  
if (isset($_REQUEST['archivePageNumber']) && is_numeric($_REQUEST['archivePageNumber']) &&
    $_REQUEST['archivePageNumber'] > 0 && $_REQUEST['archivePageNumber'] <= MAX_PAGES) {
  $_SESSION['archivePageNumber'] = $_REQUEST['archivePageNumber'];
}



// Summary list display preferences
// Toggling columns in the question list
if (isset($_REQUEST['toggleSummaryColumn'])) {
  $_SESSION['summaryShowColumns'] ^= $_REQUEST['toggleSummaryColumn'];
  // If they deselected the one that was getting sorted on, pick another
  if (!($_SESSION['summaryShowColumns'] & $_REQUEST['toggleSummaryColumn']))
    for ($i = 0; $i < 32; $i++)
      if ($_SESSION['summaryShowColumns'] & 1 << $i)
	$_SESSION['summarySortColumn'] = 1 << $i;
}

if (isset($_REQUEST['summarySortColumn'])) {
  //Nice new feature that if the current column is already the sort column
  //then flip the ascending descending property to emulate windows with which
  //everyone and their dog is already familiar
  if ($_REQUEST['summarySortColumn'] == $_SESSION['summarySortColumn']) {
    $_SESSION['summarySortDescending'] ^= 1;
  } else {
    $_SESSION['summarySortColumn'] = $_REQUEST['summarySortColumn'];
  }
}
  
// If question is not an admin then only show their organization
if (isset($_REQUEST['summarySortDescending'])) {
  $_SESSION['summarySortDescending'] = $_REQUEST['summarySortDescending'];
}
  
if (isset($_REQUEST['summaryPageNumber']) && is_numeric($_REQUEST['summaryPageNumber']) &&
    $_REQUEST['summaryPageNumber'] > 0 && $_REQUEST['summaryPageNumber'] <= MAX_PAGES) {
  $_SESSION['summaryPageNumber'] = $_REQUEST['summaryPageNumber'];
}


// This is used when someone clicks the 'To Desktop' link in the admin interface
if (isset($_REQUEST['currentTab']) && is_numeric($_REQUEST['currentTab']) &&
    $_REQUEST['currentTab'] >= 0 && $_REQUEST['currentTab'] < MAX_TABS) {
  $_SESSION['currentTab'] = $_REQUEST['currentTab'];
}


// If assignment is not an admin then only show their organization
if (isset($_REQUEST['trackAdminSummary'])) {
  $_SESSION['trackAdminSummary'] = $_REQUEST['trackAdminSummary'];
}
  

header("Location: $returnUrl");


?>
