<?php

require_once("../includes/common.php");

$isValid = FALSE;
$errMsg  = '';

if (isset($_REQUEST['User_ID']))  {

  $rc = UserLogin($_REQUEST['User_ID'], $_REQUEST['UPassword']);
  if($rc == RC_OK) {
    if($_SESSION['superUser']) {
      header('Location: ../admin/main.php');
    } else {
      print('<script type="text/JavaScript">
		var ScreenWidth=screen.width;
		var ScreenHeight=screen.height;
	        var zeroX=(ScreenWidth/2-400);
		var zeroY=(ScreenHeight/2-300);
		var options = "toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=792,height=545,left="+zeroX+",top="+zeroY;
		var strPageToOpen = (window.location.search != null) ? "../presentation/portal.php"+window.location.search : "../presentation/portal.php";
		window.location=strPageToOpen;
	</script> ');
    }
    exit;
  }
  
  $errMsg = RcToText($rc);
  $smarty->assign('User_ID', $_REQUEST['User_ID']);

} else {
  $smarty->assign('User_ID',  '');
  session_destroy();
}

$smarty->assign('uiTheme', 'Blue');
$smarty->assign('showTaLogo', FALSE);
$smarty->assign('showPoweredBy', FALSE);
$smarty->assign('ErrMsg', $errMsg);
$smarty->assign('copyrightYear', 2010);

$smarty->display('login/login.tpl');

?>
