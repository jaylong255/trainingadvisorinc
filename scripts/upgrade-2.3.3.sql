ALTER TABLE User ADD Role int unsigned not null default 0;
ALTER TABLE User ADD Phone varchar(15) not null default '' AFTER Full_Name;

UPDATE User, HR_Contact_Info SET User.Role=1 WHERE User.User_ID=HR_Contact_Info.User_ID AND HR_Contact_Info.Is_Admin=1;
UPDATE User SET Role=16 WHERE Is_HR_Contact=1;
UPDATE User, HR_Contact_Info SET User.Phone = HR_Contact_Info.Phone WHERE User.User_ID=HR_Contact_Info.User_ID;

ALTER TABLE User CHANGE HR_Contact_ID Contact_User_ID int(11) unsigned zerofill default NULL;
ALTER TABLE User DROP Is_HR_Contact;

DROP TABLE HR_Contact_Info;

-- This is no longer needed now that we have parsed the question data out into multiple fields
-- as should have been done in the first place
ALTER TABLE Question DROP Content;
ALTER TABLE Question DROP Litler_Licensed;
ALTER TABLE Question ADD AB1825 tinyint(1) not null default 0;
ALTER TABLE Question ADD Owner_User_ID int(11) unsigned zerofill not null default '00000000000';
-- Set the owner for all standardized content to be the super admin account
UPDATE Question SET Owner_User_ID=1 WHERE Question_Number < 100000;
UPDATE Question, User SET Question.Owner_User_ID=User.User_ID WHERE Question.Question_Number >= 100000 AND User.Role = 1 AND User.User_ID != 1;

ALTER TABLE Question_Archive DROP Content;
ALTER TABLE Question_Archive DROP Litler_Licensed;
ALTER TABLE Question_Archive ADD AB1825 tinyint(1) not null default 0;
ALTER TABLE Question_Archive ADD Owner_User_ID int(11) unsigned zerofill not null default '00000000000';
-- Set the owner for all standardized content to be the super admin account
UPDATE Question_Archive SET Owner_User_ID=1 WHERE Question_Number < 100000;
UPDATE Question_Archive, User SET Question_Archive.Owner_User_ID=User.User_ID WHERE Question_Archive.Question_Number >= 100000 AND User.Role = 1 AND User.User_ID != 1;


-- Add a column to the assignment table to better facilitate department and division groups
-- and to shield users from changes in track, division, or department admins
ALTER TABLE Assignment ADD Division_ID int(6) unsigned zerofill not null default '000000';

