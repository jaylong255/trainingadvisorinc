DROP TABLE IF EXISTS Config;

-- Table structure for table 'Config'
CREATE TABLE Config (
  Config_ID int unsigned NOT NULL auto_increment,
  Name varchar(50) NOT NULL,
  Value varchar(255) NOT NULL,
  Type varchar(12) NOT NULL,
  Default_Value varchar(255) NOT NULL,
  Default_Type varchar(12) NOT NULL,
  Date_Created datetime NOT NULL default 0,
  Date_Modified timestamp NOT NULL,
  Obsolete tinyint(1) NOT NULL default 0,
  PRIMARY KEY (Config_ID),
  UNIQUE KEY Config_Name_Idx (Name)
) TYPE=MyISAM;

