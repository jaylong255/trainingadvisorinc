ALTER TABLE Question DROP Correct_Feedback;
ALTER TABLE Question DROP AB1825;
ALTER TABLE Question ADD Parent_Question_Number int(6) unsigned zerofill NOT NULL default '000000';
ALTER TABLE Question ADD Media_Duration_Seconds int(6) not null default 0 after Media_File_Path;

ALTER TABLE Question_Archive DROP Correct_Feedback;
ALTER TABLE Question_Archive DROP AB1825;
ALTER TABLE Question_Archive ADD Parent_Question_Number int(6) unsigned zerofill NOT NULL default '000000';
ALTER TABLE Question_Archive ADD Media_Duration_Seconds int(6) not null default 0 after Media_File_Path;

ALTER TABLE Assignment ADD Completed_Seconds int(6) not null default 0;
