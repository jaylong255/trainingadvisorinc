<?php /* Smarty version 2.6.26, created on 2012-12-19 15:58:28
         compiled from admin/recurrence.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'admin/admin_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>


<BODY class="ContentFrameBody" onLoad="javascript:SetRecurrence('<?php echo $this->_tpl_vars['recObj']->m_recurrence; ?>
', '<?php echo $this->_tpl_vars['recObj']->m_strRecurrence; ?>
', <?php if ($this->_tpl_vars['create']): ?>1<?php else: ?>0<?php endif; ?>, '<?php echo $this->_tpl_vars['recObj']->m_flagFrequency; ?>
', <?php echo $this->_tpl_vars['recObj']->m_countDays; ?>
, <?php echo $this->_tpl_vars['recObj']->m_countWeeks; ?>
, <?php echo $this->_tpl_vars['recObj']->m_weekDays; ?>
, <?php echo $this->_tpl_vars['recObj']->m_flagWeek; ?>
, <?php echo $this->_tpl_vars['recObj']->m_countMonths; ?>
);">
  <div id="Title" style="position:absolute; left:2px; top:5px; width:522px; height:204px; z-index:1" class="RecurrenceFormText">
    <div align="center">Define Recurrence Pattern
    <hr>
  </div>
</div>


<div id="UpdateDiv" style="position:absolute; left:430px; top:175px; width:100px; height:25px; z-index:10">
	<a class="RecurrenceFormText1" href="javascript:UpdateRecurrence();">Accept</a>
</div>


<form name="AdminForm" id="AdminForm" method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>
">
  <div id="Nav" style="position:absolute; left:13px; top:76px; width:88px; height:82px; z-index:2">
    <table border="0" class="RadioText">
    <tr>
     <td>
          <input id="Interval1" type="radio" name="periodicity" value="Daily"
		 onclick="javascript:Range(1);" checked>
          <label for="Interval1">
        Daily</label>
     </td>
    </tr>
    <tr>
     <td><input id="Interval2" type="radio" name="periodicity" value="Weekly"
		onclick="javascript:Range(2);"><label for="Interval2"> Weekly</label>
     </td>
    </tr>
    <tr>
     <td><input id="Interval3" type="radio" name="periodicity" value="Monthly"
		onclick="javascript:Range(3);"><label for="Interval3"> Monthly</label>
     </td>
   </tr>
  </table>
</div>
  <div id="Table1" style="position:absolute; left:188px; top:82px; width:162px; height:64px; z-index:2; visibility: visible">
    <table border="0">
    <tr>
      <td><input type="radio" name="Day" id="Day1" value="Number" onclick="document.getElementById('DApart').disabled=false">
        Every
      <td><input type="text" name="DApart" id="DApart" size="4" maxlength="3">day(s)</td>
    </tr>
    <tr>
      <td colspan=2>
          <input type="radio" name="Day" id="Day2" value="Weekday" onclick="document.getElementById('DApart').disabled=true">
              Every weekday.
	  </td>
    </tr>
  </table>
</div>
  <div id="Table2" style="position:absolute; left:106px; top:61px; width:414px; height:116px; z-index:2; visibility: hidden">
    <table border="0">
<tr><td>
  <table border="0">
    <tr>
      <td>Recur
      <td>
                <input type="text" name="WApart" id="WApart" size="4" maxlength="3" value="<?php echo $this->_tpl_vars['recObj']->m_countWeeks; ?>
">
        week(s) on</td>
	  <td>&nbsp;</td>
    </tr>
  </table>
</td>
<td>&nbsp;&nbsp; </td>
<td>
  <table border="0">
    <tr>
      <td>
        <input type="checkbox" name="Monday" id="Monday" value="1">
        Monday</td>
	  <td><input type="checkbox" name="Friday" id="Friday" value="1"> Friday</td>
    </tr>
    <tr>
      <td>
        <input type="checkbox" name="Tuesday" id="Tuesday" value="1">
        Tuesday </td>
	  <td><input type="checkbox" name="Saturday" id="Saturday" value="1"> Saturday</td>
    </tr>
    <tr>
      <td>
        <input type="checkbox" name="Wednesday" id="Wednesday" value="1">
        Wednesday </td>
	  <td><input type="checkbox" name="Sunday" id="Sunday" value="1"> Sunday</td>
    </tr>
    <tr>
      <td>
        <input type="checkbox" name="Thursday" id="Thursday" value="1">
        Thursday </td>
    </tr>
 </table>
</td>
</tr>
 </table>
</div>
  <div id="Table3" style="position:absolute; left:111px; top:79px; width:410px; height:83px; z-index:2; visibility: hidden">
    <table border="0">
    <tr>
      <td><input type="radio" name="Month" id="Month" value="Number" onclick="javascript:SetT3(1);"></td>
      <td>&nbsp;&nbsp;Day</td>
      <td>
        <div align="center">
            <input type="text" name="MDays" id="MDays" size="3" maxlength="2" value="0">
        </div>
      </td>
	  <td> of every </td>
      <td>
        <div align="center">
            <input type="text" name="MApart1" id="MApart1" size="3" maxlength="2" value="0">
        </div>
      </td>
	  <td>month(s)</td>
    </tr>
  </table>
  <table border="0">
    <tr>
      <td><input type="radio" name="Month" id="Month1" value="MString" onclick="javascript:SetT3(2);"></td>
      <td>&nbsp;&nbsp;The</td>
      <td>
          <select name="MNumber" id="MNumber">
            <option></option>
            <option value="1">First</option>
			<option value="2">Second</option>
			<option value="3">Third</option>
			<option value="4">Fourth</option>
			<option value="5">Last</option>
        </select>
		</td>
		<td>
          <select name="MDay" id="MDay">
            <option></option>
			<option value="1">Sunday</option>
			<option value="2">Monday</option>
			<option value="3">Tuesday</option>
			<option value="4">Wednesday</option>
			<option value="5">Thursday</option>
			<option value="6">Friday</option>
			<option value="7">Saturday</option>
        </select>
		   <!--html_options name=MDay options=$recObj->GetWeekDaysList() selected=$recObj->GetWeekDay()-->
		</td>
        <td> of every </td>
      <td>
          <input type="text" name="MApart2" id="MApart2" size="3" maxlength="2" value="0">
      </td>
	  <td>month(s)</td>
    </tr>
  	<tr><td><input type="hidden" name="Insert" value="TRUE"></td></tr>
  </table>
</div>
</form>

</body>
</html>