<?php /* Smarty version 2.6.26, created on 2013-01-15 10:31:19
         compiled from presentation/print_questions.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'presentation/presentation_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<body bgcolor="#FFFCED" onLoad="javascript:window.print(); history.back();">

<?php unset($this->_sections['questionIdx']);
$this->_sections['questionIdx']['name'] = 'questionIdx';
$this->_sections['questionIdx']['loop'] = is_array($_loop=$this->_tpl_vars['questions']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['questionIdx']['show'] = true;
$this->_sections['questionIdx']['max'] = $this->_sections['questionIdx']['loop'];
$this->_sections['questionIdx']['step'] = 1;
$this->_sections['questionIdx']['start'] = $this->_sections['questionIdx']['step'] > 0 ? 0 : $this->_sections['questionIdx']['loop']-1;
if ($this->_sections['questionIdx']['show']) {
    $this->_sections['questionIdx']['total'] = $this->_sections['questionIdx']['loop'];
    if ($this->_sections['questionIdx']['total'] == 0)
        $this->_sections['questionIdx']['show'] = false;
} else
    $this->_sections['questionIdx']['total'] = 0;
if ($this->_sections['questionIdx']['show']):

            for ($this->_sections['questionIdx']['index'] = $this->_sections['questionIdx']['start'], $this->_sections['questionIdx']['iteration'] = 1;
                 $this->_sections['questionIdx']['iteration'] <= $this->_sections['questionIdx']['total'];
                 $this->_sections['questionIdx']['index'] += $this->_sections['questionIdx']['step'], $this->_sections['questionIdx']['iteration']++):
$this->_sections['questionIdx']['rownum'] = $this->_sections['questionIdx']['iteration'];
$this->_sections['questionIdx']['index_prev'] = $this->_sections['questionIdx']['index'] - $this->_sections['questionIdx']['step'];
$this->_sections['questionIdx']['index_next'] = $this->_sections['questionIdx']['index'] + $this->_sections['questionIdx']['step'];
$this->_sections['questionIdx']['first']      = ($this->_sections['questionIdx']['iteration'] == 1);
$this->_sections['questionIdx']['last']       = ($this->_sections['questionIdx']['iteration'] == $this->_sections['questionIdx']['total']);
?>
<div style="width:auto; overflow:visible; display:block;
     <?php if (! $this->_sections['questionIdx']['last']): ?>page-break-after:always;<?php endif; ?>">
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'admin/question_view.tpl', 'smarty_include_vars' => array('q' => $this->_tpl_vars['questions'][$this->_sections['questionIdx']['index']],'hideAction' => true,'contentOnly' => true)));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
</div>
<BR>
<?php endfor; endif; ?>

</body>
</html>