<?php /* Smarty version 2.6.26, created on 2014-08-01 10:27:15
         compiled from admin/faq_edit.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'admin/admin_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'admin/faq_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<FORM name="AdminForm" method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>
">
  <table width="680" border="0" align="top" cellpadding="2" cellspacing="3">
    <tr>
      <td class="OrgInfoText">Class ID:</td>
      <td><input class="FormDisabled" readonly type="text" name="trackId"
                 onClick="alert('This field is automatically generated and can not be changed.');"
		 value ="<?php echo $this->_tpl_vars['faq']->trackId; ?>
">
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText">FAQ ID:</td>
      <td><input class="FormDisabled" readonly type="text" name="faqId"
                 onClick="alert('This field is automatically generated and can not be changed.');"
		 value ="<?php if ($this->_tpl_vars['faq']->faqId == 0): ?>To Be Assigned<?php else: ?><?php echo $this->_tpl_vars['faq']->faqId; ?>
<?php endif; ?>">
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText" height="29">Question:</td>
      <td height="29">
        <textarea class="FormValue" rows="5" cols="75" name="question"><?php echo $this->_tpl_vars['faq']->question; ?>
</textarea>
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText">Answer:</td>
      <td>
        <textarea class="FormValue" rows="5" cols="75" name="answer"><?php echo $this->_tpl_vars['faq']->answer; ?>
</textarea>
      </td>
    </tr>
  </table>
</FORM>
</body>
</html>