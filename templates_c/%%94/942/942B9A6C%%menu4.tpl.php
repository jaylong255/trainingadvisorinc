<?php /* Smarty version 2.6.26, created on 2012-12-17 09:49:56
         compiled from presentation/menu4.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', 'presentation/menu4.tpl', 8, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'presentation/presentation_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<body class="faq">

<form name="ScorecardForm" method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>
">
<p>
<select class="FormValue" name="dateOption" onChange="javascript:this.form.submit();">
<?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['dateOptions'],'selected' => $this->_tpl_vars['dateOptionSelected']), $this);?>
</select>
</p>

<span>Completed Assignments: <?php echo $this->_tpl_vars['stats']['totalAnswered']; ?>
</span><BR>
<span>Correctly Answered: <?php echo $this->_tpl_vars['stats']['correct']; ?>
</span><BR>
<span>Incorrectly Answered: <?php echo $this->_tpl_vars['stats']['incorrect']; ?>
</span><BR>

</form>
</body>
</html>