<?php /* Smarty version 2.6.26, created on 2014-05-31 20:11:46
         compiled from admin/class_email.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'cycle', 'admin/class_email.tpl', 71, false),array('modifier', 'escape', 'admin/class_email.tpl', 73, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'admin/admin_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'admin/track_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>


<form name="AdminForm" class="orgForm" method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>
">
  <input type="hidden" name="eQueueId" id="eQueueId" value="<?php if (isset ( $this->_tpl_vars['queueEntry'] )): ?><?php echo $this->_tpl_vars['queueEntry']->emailQueueId; ?>
<?php else: ?>0<?php endif; ?>"/>
  <table width="100%" border="0">
    <?php if (isset ( $this->_tpl_vars['statusMsg'] )): ?>
    <tr>
      <td class="error" colspan="2"><?php echo $this->_tpl_vars['statusMsg']; ?>
</td>
    </tr>
    <?php endif; ?>
    <tr>
      <td class="OrgInfoText" colspan="2">You may send messages to all the participants of this class by completing the subject and body of your message below.  The email will appear to come from you.</td>
    </tr>
    <tr>
      <td class="OrgInfoText" colspan="2">NOTE: If your organization has a very large number of recipients, it will take some time for everyone to receive the message.</td>
    </tr>
    <tr>
      <td class="OrgInfoText">From:&nbsp;&nbsp;</td>
      <td>
        <input class="<?php if ($this->_tpl_vars['superUser']): ?>FormValue<?php else: ?>FormDisabled<?php endif; ?>" type="text" name="mailFrom" id="mailFrom" maxlength="80" size="110" value="<?php if (isset ( $this->_tpl_vars['queueEntry'] )): ?><?php echo $this->_tpl_vars['queueEntry']->fromAddress; ?>
<?php endif; ?>"<?php if (! $this->_tpl_vars['superUser']): ?> disabled<?php endif; ?>/>
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText">Subject:&nbsp;&nbsp;</td>
      <td>
        <input class="FormValue" type="text" name="subject" id="subject" maxlength="80" size="110" value="<?php if (isset ( $this->_tpl_vars['queueEntry'] )): ?><?php echo $this->_tpl_vars['queueEntry']->subject; ?>
<?php endif; ?>"/>
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText" valign="top">Message:&nbsp;&nbsp;</td>
      <td width="200">
        <textarea rows="10" cols="80" name="body" id="body" class="FormValue"><?php if (isset ( $this->_tpl_vars['queueEntry'] ) && $this->_tpl_vars['queueEntry']->emailQueueId > 0): ?><?php echo $this->_tpl_vars['queueEntry']->body; ?>
<?php endif; ?></textarea>
      </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td class="OrgInfoText">If you would like to schedule this message to be delivered at a later date, enter it below.<BR>
        Otherwise, leave blank and your message will be delivered as soon as possible.</td>
    <tr>
      <td class="OrgInfoText" width="90">Scheduled:</td>
      <td valign="center" width="150">
        <input class="FormValue" type="text" name="schedDate" id="schedDate" value ="<?php if (isset ( $this->_tpl_vars['queueEntry'] ) && $this->_tpl_vars['queueEntry']->emailQueueId > 0 && $this->_tpl_vars['queueEntry']->GetDatePart($this->_tpl_vars['queueEntry']->scheduledDate) != '0000-00-00'): ?><?php echo $this->_tpl_vars['queueEntry']->GetDatePart($this->_tpl_vars['queueEntry']->scheduledDate); ?>
<?php endif; ?>"
	     onchange="javascript:SetFlag();" title="Scheduled Delivery Date"/>
        <a href="javascript:doNothing()"
	   onClick="setDateField(document.getElementById('schedDate'));top.newWin=window.open('calendar.html','cal','dependent=yes,width=210,height=230,screenX=200,screenY=300,titlebar=yes')">
	  <img id="cal" src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/calendar.gif" border="0" width="16" height="16"></a>
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText">&nbsp;</td>
      <td>
        <input type="submit" name="submit" value="Submit"/>
      </td>
    </tr>
  </table>
<?php if (isset ( $this->_tpl_vars['queueIds'] ) && ! empty ( $this->_tpl_vars['queueIds'] )): ?>
<hr width="100%" align="left">
<div class="OrgInfoText">Previously sent messages:</div>
<table width="100%">
  <tr>
    <td class="table_header" width="26%"><?php echo $this->_tpl_vars['queueListLbls'][0]; ?>
</td>
    <td class="table_header" width="30%"><?php echo $this->_tpl_vars['queueListLbls'][1]; ?>
</td>
    <td class="table_header" width="7%"><?php echo $this->_tpl_vars['queueListLbls'][2]; ?>
</td>
    <td class="table_header" width="7%"><?php echo $this->_tpl_vars['queueListLbls'][3]; ?>
</td>
    <td class="table_header" width="10%" align="center">Actions</td>
  </tr>

  <?php unset($this->_sections['queueListIndex']);
$this->_sections['queueListIndex']['name'] = 'queueListIndex';
$this->_sections['queueListIndex']['loop'] = is_array($_loop=$this->_tpl_vars['queueIds']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['queueListIndex']['show'] = true;
$this->_sections['queueListIndex']['max'] = $this->_sections['queueListIndex']['loop'];
$this->_sections['queueListIndex']['step'] = 1;
$this->_sections['queueListIndex']['start'] = $this->_sections['queueListIndex']['step'] > 0 ? 0 : $this->_sections['queueListIndex']['loop']-1;
if ($this->_sections['queueListIndex']['show']) {
    $this->_sections['queueListIndex']['total'] = $this->_sections['queueListIndex']['loop'];
    if ($this->_sections['queueListIndex']['total'] == 0)
        $this->_sections['queueListIndex']['show'] = false;
} else
    $this->_sections['queueListIndex']['total'] = 0;
if ($this->_sections['queueListIndex']['show']):

            for ($this->_sections['queueListIndex']['index'] = $this->_sections['queueListIndex']['start'], $this->_sections['queueListIndex']['iteration'] = 1;
                 $this->_sections['queueListIndex']['iteration'] <= $this->_sections['queueListIndex']['total'];
                 $this->_sections['queueListIndex']['index'] += $this->_sections['queueListIndex']['step'], $this->_sections['queueListIndex']['iteration']++):
$this->_sections['queueListIndex']['rownum'] = $this->_sections['queueListIndex']['iteration'];
$this->_sections['queueListIndex']['index_prev'] = $this->_sections['queueListIndex']['index'] - $this->_sections['queueListIndex']['step'];
$this->_sections['queueListIndex']['index_next'] = $this->_sections['queueListIndex']['index'] + $this->_sections['queueListIndex']['step'];
$this->_sections['queueListIndex']['first']      = ($this->_sections['queueListIndex']['iteration'] == 1);
$this->_sections['queueListIndex']['last']       = ($this->_sections['queueListIndex']['iteration'] == $this->_sections['queueListIndex']['total']);
?>
  <tr class="<?php echo smarty_function_cycle(array('name' => 'rowItem','values' => "trDefault,trDefaultAlt"), $this);?>
">
    <?php unset($this->_sections['queueFieldIndex']);
$this->_sections['queueFieldIndex']['name'] = 'queueFieldIndex';
$this->_sections['queueFieldIndex']['loop'] = is_array($_loop=$this->_tpl_vars['queueRows'][$this->_sections['queueListIndex']['index']]) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['queueFieldIndex']['show'] = true;
$this->_sections['queueFieldIndex']['max'] = $this->_sections['queueFieldIndex']['loop'];
$this->_sections['queueFieldIndex']['step'] = 1;
$this->_sections['queueFieldIndex']['start'] = $this->_sections['queueFieldIndex']['step'] > 0 ? 0 : $this->_sections['queueFieldIndex']['loop']-1;
if ($this->_sections['queueFieldIndex']['show']) {
    $this->_sections['queueFieldIndex']['total'] = $this->_sections['queueFieldIndex']['loop'];
    if ($this->_sections['queueFieldIndex']['total'] == 0)
        $this->_sections['queueFieldIndex']['show'] = false;
} else
    $this->_sections['queueFieldIndex']['total'] = 0;
if ($this->_sections['queueFieldIndex']['show']):

            for ($this->_sections['queueFieldIndex']['index'] = $this->_sections['queueFieldIndex']['start'], $this->_sections['queueFieldIndex']['iteration'] = 1;
                 $this->_sections['queueFieldIndex']['iteration'] <= $this->_sections['queueFieldIndex']['total'];
                 $this->_sections['queueFieldIndex']['index'] += $this->_sections['queueFieldIndex']['step'], $this->_sections['queueFieldIndex']['iteration']++):
$this->_sections['queueFieldIndex']['rownum'] = $this->_sections['queueFieldIndex']['iteration'];
$this->_sections['queueFieldIndex']['index_prev'] = $this->_sections['queueFieldIndex']['index'] - $this->_sections['queueFieldIndex']['step'];
$this->_sections['queueFieldIndex']['index_next'] = $this->_sections['queueFieldIndex']['index'] + $this->_sections['queueFieldIndex']['step'];
$this->_sections['queueFieldIndex']['first']      = ($this->_sections['queueFieldIndex']['iteration'] == 1);
$this->_sections['queueFieldIndex']['last']       = ($this->_sections['queueFieldIndex']['iteration'] == $this->_sections['queueFieldIndex']['total']);
?>
    <td><?php echo ((is_array($_tmp=$this->_tpl_vars['queueRows'][$this->_sections['queueListIndex']['index']][$this->_sections['queueFieldIndex']['index']])) ? $this->_run_mod_handler('escape', true, $_tmp, 'html') : smarty_modifier_escape($_tmp, 'html')); ?>
</td>
    <?php endfor; endif; ?>
    <td>
    <?php if ($this->_tpl_vars['queueRows'][$this->_sections['queueListIndex']['index']][3] == 'Not Yet'): ?><a href="<?php echo $_SERVER['PHP_SELF']; ?>
?queueId=<?php echo $this->_tpl_vars['queueIds'][$this->_sections['queueListIndex']['index']]; ?>
">Edit</a> | <?php endif; ?>
    <a href="<?php echo $_SERVER['PHP_SELF']; ?>
?queueId=<?php echo $this->_tpl_vars['queueIds'][$this->_sections['queueListIndex']['index']]; ?>
&op=delete">Delete</a>
    </td>
  </tr>
  <?php endfor; endif; ?>
</table>
<?php endif; ?>
</form>
</body>
</html>