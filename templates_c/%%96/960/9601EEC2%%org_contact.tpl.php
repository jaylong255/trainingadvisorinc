<?php /* Smarty version 2.6.26, created on 2012-11-27 06:48:14
         compiled from admin/org_contact.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', 'admin/org_contact.tpl', 14, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'admin/admin_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'admin/org_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>


<form class="orgForm" name="OrgForm" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>
">

  <input type="hidden" name="saveMe" value="1">
  <table border="0">
    <tr>
      <td class="OrgInfoText" height="20">The Organization Contact is:&nbsp;&nbsp;</td>
      <td>
        <select class="FormValue" name="orgContactId" onChange="javascript:SetFlag();">
          <option></option>
	  <?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['orgContacts'],'selected' => $this->_tpl_vars['orgContactId']), $this);?>

        </select></td>
    </tr>
    <tr><td colspan="2">&nbsp;</td></tr>
    <tr>
      <td class="OrgInfoText" width="500"> Specify the number of questions an employee can
        leave unanswered before a delinquency report is sent to the Organization
        HR Contact. Leave the field blank if no report is desired.</td>
      <td align="left" class="OrgInfoText" valign="middle">
        <input class="FormValue" type="text" name="delinquencies" size="4" maxlength="3"
		value="<?php if ($this->_tpl_vars['delinquencies'] != 0): ?><?php echo $this->_tpl_vars['delinquencies']; ?>
<?php endif; ?>"
	        onChange="javascript:SetFlag();"
		title="Notify organization HR Contact when student has X or more questions assigned."></td>
    </tr>
  </table>
</form>

</body>
</html>