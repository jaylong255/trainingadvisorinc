<?php /* Smarty version 2.6.26, created on 2013-02-08 08:54:55
         compiled from login/perm_denied.tpl */ ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
  <TITLE>Training Advisor Portal Login</TITLE>
  <META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <LINK rel="stylesheet" href="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/desktop.css" type="text/css">
  <SCRIPT language="javascript" type="text/javascript" src="../javascript/desktop.js"></script>
</HEAD>
<BODY class="theme" onLoad="MM_preloadImages('../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/login/btn_logon_dn.gif');
      <?php if (isset ( $this->_tpl_vars['wrongTrack'] ) && $this->_tpl_vars['wrongTrack']): ?>parent.SetTab(3,0);<?php endif; ?>">
<center>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<?php if (isset ( $this->_tpl_vars['wrongTrack'] ) && $this->_tpl_vars['wrongTrack']): ?>
  <span class="WaitText"><p>You are not the supervisor for this track.  Please <a href="../admin/track_list.php" class="Links">Return to the track list</a> and select a track for which you are the supervisor.</span>
<?php else: ?>
  <span class="WaitText"><p>You have attempted to access a page that you do not have permission to access.  This access attempt has been recorded and security staff notified.</p>

<p>To continue using this application, please <a class="Links" target="_parent" href="../login/login.php">click here to log in again.</a></p><p>Thank You!</p></span>
<?php endif; ?>
</center>
</body>