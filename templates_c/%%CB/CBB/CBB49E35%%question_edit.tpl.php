<?php /* Smarty version 2.6.26, created on 2012-11-27 07:04:27
         compiled from admin/question_edit.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', 'admin/question_edit.tpl', 99, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'admin/admin_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'admin/question_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<!--div id="AreaMenu" style="position:absolute; left:28px; top:71px; z-index:30; visibility: hidden"-->

<table border="0" align="top" width="680">
  <tr>
    <td align="left" width="33%">
      <!--div id="Area" style="position:absolute; left:13px; top:52px; width:232px; height:22px; z-index:31"-->
      <a id="AreaLink" href="javascript:ShowPopupMenu('AreaMenu', 'AreaLink', true);" style="z-index:50;">
	<img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/pulldown_carrot.gif" width="9" height="8" border="0">
        <span id="AName" style="z-index:50;">Question Information</span></a>
      <!--/div-->
    <div id="AreaMenu" style="visibility:hidden; position:absolute; left:0px; top:0px; z-index:50;">
      <table class="table_popup" border="0">
        <tr><td id="Arrow1"> </td><td><a href="javascript:ChangeArea(1);">Question Information</a></td></tr>
        <tr><td id="Arrow2"> </td><td><a href="javascript:ChangeArea(2);">The Question</a></td></tr>
        <tr><td id="Arrow3"> </td><td><a href="javascript:ChangeArea(3);">Potential Answers</a></td></tr>
        <tr><td id="Arrow4"> </td><td><a href="javascript:ChangeArea(4);">The Purpose of the Question</a></td></tr>
        <tr><td id="Arrow5"> </td><td><a href="javascript:ChangeArea(5);">Possible Responses</a></td></tr>
        <tr><td id="Arrow6"> </td><td><a href="javascript:ChangeArea(6);">The Correct Response</a></td></tr>
        <tr><td id="Arrow7"> </td><td><a href="javascript:ChangeArea(7);">Supplemental Information</a></td></tr>
        <tr><td id="Arrow8"> </td><td><a href="javascript:ChangeArea(8);">Key Learning Points</a></td></tr>
        <tr><td id="Arrow9"> </td><td><a href="javascript:ChangeArea(9);">Additional Information</a></td></tr>
        <tr><td id="Arrow10"> </td><td><a href="javascript:ChangeArea(10);">Customize 'Thank You' Page</a></td></tr>
      </table>
    </div>
    </td>
    <td width="33%" align="center">
    <?php if ($this->_tpl_vars['q']->questionId): ?>
      <a href="javascript:launchPopup('Popup','../presentation/q_type.php?questionId=<?php echo $this->_tpl_vars['q']->questionId; ?>
&review=0&preview=1&domainId=<?php echo $this->_tpl_vars['q']->domainId; ?>
&languageId=<?php echo $this->_tpl_vars['q']->languageId; ?>
','no',850, 1000);">Preview</a>
    <?php else: ?>&nbsp;<?php endif; ?>
    </td>
    <td width="34%" align="right">
      <a id="SectionBack" style="visibility:hidden" href="javascript:BackSection('<?php echo $this->_tpl_vars['uiTheme']; ?>
');">
	 <img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/back_pointer.gif" width="12" height="11" border="0">
         Back</a>
      <a id="SectionNext" href="javascript:NextSection('<?php echo $this->_tpl_vars['uiTheme']; ?>
');">
	 Next <img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/pointer.gif" width="12" height="11" border="0">
      </a>
    </td>
  </tr>
</table>  





<!--div id="Back" style="position:absolute; left:12px; top:4px; width:200px; height:27px; z-index:2" class="OrgInfoText">
  <table border="0" width="100%" align="center" cellpadding="2" cellspacing="3">
    <tr>
      <td>
	<a href="javascript:CheckSaveQuestionEdit('<?php echo $this->_tpl_vars['q']->questionId; ?>
');" onMouseOut="MM_swapImgRestore()"
	   onMouseOver="MM_swapImage('Back','','../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/btn_back_down.gif',1)">
	<img name="Back" border="0" src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/btn_back_up.gif" width="23" height="21"></a>
      </td>
      <td><a class="OrgInfoText" href="javascript:CheckSaveQuestionEdit('<?php echo $this->_tpl_vars['q']->questionId; ?>
');">Back to Question List</a></td>
    </tr>
  </table>
</div>



<div id="Break" style="position:absolute; left:11px; top:29px; width:100%; height:18px; z-index:3"><hr></div-->


<form id="AdminForm" name="AdminForm" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>
">
  <div id="Section1" style="position:absolute; left:13px; top:80px; width:670px; height:286px; z-index:10; visibility: visible">
    <!--div id="Section1a" style="position:absolute; left:306px; top:16px; width:350px; height:221px; z-index:11"-->
  <table border="0">
    <tr>
      <td class="OrgInfoText">Question Number:</td>
      <td>
        <input class="FormDisabled" size="17" type="text" id="questionId" name="questionId" onchange="javascript:SetFlag();"
	       value="<?php if ($this->_tpl_vars['q']->questionId): ?><?php echo $this->_tpl_vars['q']->questionId; ?>
<?php else: ?>To Be Assigned<?php endif; ?>" disabled>
	<input type="hidden" name="questionId" id="questionId" value="<?php if ($this->_tpl_vars['q']->questionId): ?><?php echo $this->_tpl_vars['q']->questionId; ?>
<?php else: ?>To Be Assigned<?php endif; ?>">
	<input type="hidden" name="questionId" id="questionId" value="<?php if ($this->_tpl_vars['q']->questionId): ?><?php echo $this->_tpl_vars['q']->questionId; ?>
<?php else: ?>To Be Assigned<?php endif; ?>">
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText">Title:</td>
      <td>
        <!--textarea class="FormValue" rows="8" cols="28" name="title" id="title" onchange="javascript:SetFlag();"><?php echo $this->_tpl_vars['q']->title; ?>
</textarea-->
        <input class="FormValue" style="width:100%" type="text" name="title" id="title" value="<?php echo $this->_tpl_vars['q']->title; ?>
" onchange="javascript:SetFlag();">
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText">Author:</td>
      <td>
        <input class="FormValue" style="width:100%" type="text" name="author" id="author" size="30" maxlength="30" value="<?php echo $this->_tpl_vars['q']->author; ?>
"
	       onchange="javascript:SetFlag();">
      </td>
    </tr>
    <!--tr>
      <td class="OrgInfoText">Parent Question:</td>
      <td>
        <select class="FormDisabled" name="language" onchange="javascript:SetFlag();" disabled>
          <option value="0">&lt;No Parent Question&gt;</option>
          <?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['parentQuestions'],'selected' => $this->_tpl_vars['q']->parentQuestionId), $this);?>

        </select>
      </td>
    </tr-->
    <tr>
      <td class="OrgInfoText">Version:</td>
      <td>
        <input class="FormDisabled" type="text" name="versionDate" id="versionDate" maxlength="45"
	       size="30" value="<?php echo $this->_tpl_vars['q']->versionDate; ?>
" disabled>
	<input type="hidden" name="versionDate" value=<?php echo $this->_tpl_vars['q']->versionDate; ?>
>
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText"> Language: </td>
      <td>
        <select  class="FormValue" name="languageId" id="languageId" onchange="javascript:SetFlag();"
		<?php if ($this->_tpl_vars['q']->questionId > 0 && ! $this->_tpl_vars['superUser']): ?><?php endif; ?>> <!-- previously disabled but then form val does not come through -->
            <?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['q']->GetLanguages(),'selected' => $this->_tpl_vars['q']->languageId), $this);?>

        </select>
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText">Domain:&nbsp;</td>
      <td>
        <select  class="FormValue" name="domainId" onchange="javascript:SetFlag();"
		<?php if ($this->_tpl_vars['q']->questionId > 0 && ! $this->_tpl_vars['superUser']): ?> disabled<?php endif; ?>>
          <option></option>
          <?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['q']->GetDomains(),'selected' => $this->_tpl_vars['q']->domainId), $this);?>

        </select>
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText">Frame Type:</td>
      <td>
        <select  class="FormValue" name="frameTypeId" id="frameTypeId" onchange="javascript:SetFlag();">
            <?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['q']->GetFrameTypes(),'selected' => $this->_tpl_vars['q']->frameTypeId), $this);?>

        </select>
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText" height="29">Category:</td>
      <td height="29">
        <select  class="FormValue" name="categoryId" id="categoryId" onchange="javascript:SetFlag();">
          <option></option>
          <?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['q']->GetCategories(),'selected' => $this->_tpl_vars['q']->categoryId), $this);?>

        </select>
      </td>
    </tr>
    <!--div id="Section1d" style="position:absolute; left:11px; top:193px; width:235px; height:49px; z-index:14"-->
    <tr>
      <td class="OrgInfoText">Question can be used for Non-Mangagement employees:</td>
      <td>
         <input type="checkbox" name="isManagement" id="isManagement" value="1" onchange="javascript:SetFlag();"
	<?php if ($this->_tpl_vars['q']->isManagement): ?> checked<?php endif; ?>>
      </td>
    </tr>
  </table>
 </div>
 

  <!--div id="Section1b" style="position:absolute; left:3px; top:0px; width:262px; height:30px; z-index:12">
    <table border="0">
      <tr>
        <td class="OrgInfoText">Question Number:</td>
        <td>
          <input class="FormDisabled" size="17" type="text" id="questionId" name="questionId" onchange="javascript:SetFlag();"
	       value="<?php if ($this->_tpl_vars['q']->questionId): ?><?php echo $this->_tpl_vars['q']->questionId; ?>
<?php else: ?>To Be Assigned<?php endif; ?>" disabled>
        </td>
      </tr>
    </table>
  </div-->

    <!--div id="Section1c" style="position:absolute; left:3px; top:26px; width:200px; height:200px; z-index:13">
    <table border="1">
      <tr>
        <td colspan="2" class="OrgInfoText" height="17">Title:</td>
      </tr>
      <tr>
        <td>
        </td>
      </tr>
    </table>
   </div-->
  </div>
  <div id="Section2" style="position:absolute; left:13px; top:84px; width:670px; height:286px; z-index:15; visibility: hidden">
    <table border="0">
    <tr>
        <td  class="OrgInfoText" height="17">Question:</td>
    </tr>
    <tr>
       <td>
         <textarea class="FormValue" rows="10" cols="80" id="question" name="question"
		   onchange="javascript:SetFlag();"><?php echo $this->_tpl_vars['q']->question; ?>
</textarea>
       </td>
    </tr>
    <tr>
      <td class="OrgInfoText" height="17">
	<input type="checkbox" name="requireCorrectAnswer"  onchange="javascript:SetFlag();" value="1"
		<?php if ($this->_tpl_vars['q']->requireCorrectAnswer): ?> checked<?php endif; ?>>
	    Require correct answer before allowing user to complete question
      </td>
    </tr>
    <tr>
	 <td class="OrgInfoText" height="17">Media File:</td>
    </tr>
    <tr>
       <td>
         <input class="FormValue" type="text" id="media" name="media" maxlength="100" size="40" value="<?php echo $this->_tpl_vars['q']->mediaFilePath; ?>
">
       </td>
    </tr>
    <tr>
	  <td id="mediaText" class="OrgInfoText">&nbsp;</td>
    </tr>
    </table>
</div>
  <div id="Section3" style="position:absolute; left:13px; top:84px; width:742px; height:286px; z-index:16; visibility: hidden">
    <table border="0">
    <tr>
        <td class="OrgInfoText" height="17">Answer A:</td>
        <td>
          <textarea class="FormValue" rows="3" cols="75" id="answerA" name="answerA" onchange="javascript:SetFlag();"><?php echo $this->_tpl_vars['q']->multipleChoice1; ?>
</textarea>
        </td>
    </tr>
    <tr>
        <td class="OrgInfoText" height="17">Answer B:</td>
        <td>
          <textarea class="FormValue" rows="3" cols="75" id="answerB" name="answerB" onchange="javascript:SetFlag();"><?php echo $this->_tpl_vars['q']->multipleChoice2; ?>
</textarea>
        </td>
    </tr>
    <tr>
        <td class="OrgInfoText" height="17">Answer C:</td>
        <td>
          <textarea class="FormValue" rows="3" cols="75" id="answerC" name="answerC" onchange="javascript:SetFlag();"><?php echo $this->_tpl_vars['q']->multipleChoice3; ?>
</textarea>
        </td>
    </tr>
    <tr>
        <td class="OrgInfoText" height="17">Answer D:</td>
        <td>
          <textarea class="FormValue" rows="3" cols="75" id="answerD" name="answerD" onchange="javascript:SetFlag();"><?php echo $this->_tpl_vars['q']->multipleChoice4; ?>
</textarea>
        </td>
    </tr>
    <tr>
        <td class="OrgInfoText" height="17">Answer E:</td>
        <td>
          <textarea class="FormValue" rows="3" cols="75" id="answerE" name="answerE" onchange="javascript:SetFlag();"><?php echo $this->_tpl_vars['q']->multipleChoice5; ?>
</textarea>
        </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td class="OrgInfoText" height="17">
        <input type="radio" name="displayChoices" value="right"<?php if (isset ( $this->_tpl_vars['useQuestionSetting'] ) && ! $this->_tpl_vars['useQuestionSetting']): ?> disabled<?php elseif (isset ( $this->_tpl_vars['displayChoices'] ) && $this->_tpl_vars['displayChoices'] == 'right'): ?> checked<?php endif; ?>>Display these choices to the right of the question<BR>
	<input type="radio" name="displayChoices" value="bottom"<?php if (isset ( $this->_tpl_vars['useQuestionSetting'] ) && ! $this->_tpl_vars['useQuestionSetting']): ?> disabled<?php elseif (isset ( $this->_tpl_vars['displayChoices'] ) && $this->_tpl_vars['displayChoices'] == 'bottom'): ?> checked<?php endif; ?>>Display these choices below the question<BR>
	<input type="radio" name="displayChoices" value="default"<?php if (isset ( $this->_tpl_vars['useQuestionSetting'] ) && ! $this->_tpl_vars['useQuestionSetting']): ?> disabled<?php elseif (isset ( $this->_tpl_vars['displayChoices'] ) && ( $this->_tpl_vars['displayChoices'] == 'default' || $this->_tpl_vars['displayChoices'] == '' )): ?> checked<?php endif; ?>>Use the default selection from the Miscellaneious subtab of the Customize tab.<BR>
	<?php if (isset ( $this->_tpl_vars['useQuestionSetting'] ) && ! $this->_tpl_vars['useQuestionSetting']): ?><p>
	   To enable the options above on per question basis, to the customize tab and click on the Miscellaneous sub-tab.  Then select the option "Use setting from the question but if question setting is not defined, use the above selection as default option."</p><?php endif; ?>
      </td>
    </tr>
    </table>
  </div>


  <div id="Section4" style="position:absolute; left:13px; top:84px; width:670px; height:286px; z-index:17; visibility: hidden">
    <table border="0">
    <tr>
        <td  class="OrgInfoText">Purpose of the Question:</td>
    </tr>
    <tr>
        <td>
          <textarea class="FormValue" rows="10" cols="80" id="purpose" name="purpose" onchange="javascript:SetFlag();"><?php echo $this->_tpl_vars['q']->purpose; ?>
</textarea>
        </td>
    </tr>
    </table>
  </div>


  <div id="Section5" style="position:absolute; left:13px; top:84px; width:670px; height:286px; z-index:18; visibility: hidden">
  <table border="0">
    <tr>
        <td class="OrgInfoText">Response to Answer A:</td>
        <td>
          <textarea class="FormValue" rows="3" cols="65" id="responseA" name="responseA" onchange="javascript:SetFlag();"><?php echo $this->_tpl_vars['q']->feedbackChoice1; ?>
</textarea>
        </td>
    </tr>
    <tr>
        <td class="OrgInfoText">Response to Answer B:</td>
        <td>
          <textarea class="FormValue" rows="3" cols="65" id="responseB" name="responseB" onchange="javascript:SetFlag();"><?php echo $this->_tpl_vars['q']->feedbackChoice2; ?>
</textarea>
        </td>
    </tr>
    <tr>
        <td class="OrgInfoText">Response to Answer C:</td>
        <td>
          <textarea class="FormValue" rows="3" cols="65" id="responseC" name="responseC" onchange="javascript:SetFlag();"><?php echo $this->_tpl_vars['q']->feedbackChoice3; ?>
</textarea>
        </td>
    </tr>
    <tr>
        <td class="OrgInfoText">Response to Answer D:</td>
        <td>
          <textarea class="FormValue" rows="3" cols="65" id="responseD" name="responseD" onchange="javascript:SetFlag();"><?php echo $this->_tpl_vars['q']->feedbackChoice4; ?>
</textarea>
        </td>
    </tr>
    <tr>
        <td class="OrgInfoText">Response to Answer E:</td>
        <td>
          <textarea class="FormValue" rows="3" cols="65" id="responseE" name="responseE" onchange="javascript:SetFlag();"><?php echo $this->_tpl_vars['q']->feedbackChoice5; ?>
</textarea>
        </td>
    </tr>
    </table>
  </div>



  <div id="Section6" style="position:absolute; left:13px; top:84px; width:670px; height:286px; z-index:19; visibility: hidden">
    <table border="0" cellspacing="2">
    <tr>
        <td class="OrgInfoText">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td class="OrgInfoText">Correct Answer:</td>
	<td>&nbsp;&nbsp;&nbsp;&nbsp;
	  <select id="correctAnswer" name="correctAnswer" class="FormValue" onchange="javascript:SetFlag();">
	    <!--option></option-->
	    <?php if ($this->_tpl_vars['q']->correctAnswer > 0): ?>
	    <?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['correctOpts'],'selected' => $this->_tpl_vars['q']->correctAnswer), $this);?>

	    <?php else: ?>
	    <?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['correctOpts']), $this);?>

	    <?php endif; ?>
	  </select>
	</td>
    </tr>
    </table>
  </div>


  <div id="Section7" style="position:absolute; left:13px; top:84px; width:670px; height:286px; z-index:20; visibility: hidden">
    <table border="0" cellspacing="2">
    <tr>
        <td class="OrgInfoText" height="17">Supplemental Information:</td>
    </tr>
    <tr>
        <td>
          <textarea class="FormValue" rows="10" cols="80" id="supportInfo" name="supportInfo" onchange="javascript:SetFlag();"><?php echo $this->_tpl_vars['q']->supportInfo; ?>
</textarea>
        </td>
    </tr>
    </table>
  </div>


  <div id="Section8" style="position:absolute; left:13px; top:84px; width:670px; height:286px; z-index:21; visibility: hidden">
    <table border="0" cellspacing="2">
    <tr>
        <td class="OrgInfoText">Key Learning Points:</td>
    </tr>
    <tr>
        <td>
          <textarea class="FormValue" rows="10" cols="80" id="keyLearningPoints" name="keyLearningPoints"
		    onchange="javascript:SetFlag();"><?php echo $this->_tpl_vars['q']->learningPoints; ?>
</textarea>
        </td>
    </tr>
    </table>
  </div>



  <div id="Section9" style="position:absolute; left:13px; top:84px; width:670px; height:286px; z-index:22; visibility: hidden">
    <table border="0" cellspacing="2">
    <tr>
        <td class="OrgInfoText" height="17">Additional Information</td>
    </tr>
    <tr>
        <td>
          <textarea class="FormValue" rows="10" cols="80" id="notes" name="notes" onchange="javascript:SetSupport();"><?php echo $this->_tpl_vars['q']->notes; ?>
</textarea>
        </td>
    </TR>
    </TABLE>
  </DIV>



  <div id="Section10" style="position:absolute; left:13px; top:84px; width:670px; height:286px; z-index:22; visibility: hidden">
    <table border="0" cellspacing="2">
    <tr>
        <td class="OrgInfoText" height="17">Enter the 'Thank You' message that the user will see when completing the question:</td>
    </tr>
    <tr>
        <td>
          <textarea class="FormValue" rows="3" cols="80" id="capConfirmThanks" name="capConfirmThanks" onchange="javascript:SetFlag();"><?php echo $this->_tpl_vars['q']->capConfirmThanks; ?>
</textarea><BR>
	  <INPUT type="checkbox" name="CapReplicate" value="1">
		<span class="OrgInfoText">Check here to use this caption for all questions.</span>
        </td>
    </TR>
    </TABLE>
  </DIV>


</FORM>
<?php if ($this->_tpl_vars['errMsg']): ?>
  <BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR><BR>
  <SPAN class="error"><?php echo $this->_tpl_vars['errMsg']; ?>
</SPAN>
<?php endif; ?>

</BODY>
</HTML>
