<?php /* Smarty version 2.6.26, created on 2012-12-17 02:29:21
         compiled from presentation/q_type.tpl */ ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
  <META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <Title>Question</Title>
  <link rel="stylesheet" href="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/desktop.css" type="text/css">
  <link rel="stylesheet" href="../css/jt_DialogBox.css" type="text/css">
  <link rel="stylesheet" href="../css/jt_Veils.css" type="text/css">
  <SCRIPT LANGUAGE="JavaScript">
    var qType = '<?php echo $this->_tpl_vars['a']->frameTypeId; ?>
';
    <?php if (! $this->_tpl_vars['preview']): ?>
    var assignmentId = <?php echo $this->_tpl_vars['a']->assignmentId; ?>
; //$nResult;
    <?php endif; ?>
    var firstAnswer = 0; //$iFirstChoice;
    var finalAnswer = 0; //$iStudentAnswer;
    var currentPage = 0; //$iCurrentPage;
    var review = <?php echo $this->_tpl_vars['review']; ?>
;
    var preview = <?php echo $this->_tpl_vars['preview']; ?>
;
    var exitInfo = 0;
    var lastPage = 0;
    var correctAnswer = 0;
    var requireCorrectAnswer = <?php if ($this->_tpl_vars['a']->requireCorrectAnswer): ?>1<?php else: ?>0<?php endif; ?>;
    // Page numbers are:
    // 0=Question Frame 1=Why It's Important 2=Key Learning Points 3=Additional HR Comments 4=Thank You
    var strStem = "<?php echo $this->_tpl_vars['a']->question; ?>
";
    var strFoil1 = "<?php echo $this->_tpl_vars['a']->multipleChoice1; ?>
";
    var strFoil2 = "<?php echo $this->_tpl_vars['a']->multipleChoice2; ?>
";
    var strFoil3 = "<?php echo $this->_tpl_vars['a']->multipleChoice3; ?>
";
    var strFoil4 = "<?php echo $this->_tpl_vars['a']->multipleChoice4; ?>
";
    var strFoil5 = "<?php echo $this->_tpl_vars['a']->multipleChoice5; ?>
";
    var strCorrect = <?php echo $this->_tpl_vars['a']->correctAnswer; ?>
;
    var strPurpose = "<?php echo $this->_tpl_vars['a']->purpose; ?>
";
    var strFoil1_Fdbk = "<?php echo $this->_tpl_vars['a']->feedbackChoice1; ?>
";
    var strFoil2_Fdbk = "<?php echo $this->_tpl_vars['a']->feedbackChoice2; ?>
";
    var strFoil3_Fdbk = "<?php echo $this->_tpl_vars['a']->feedbackChoice3; ?>
";
    var strFoil4_Fdbk = "<?php echo $this->_tpl_vars['a']->feedbackChoice4; ?>
";
    var strFoil5_Fdbk = "<?php echo $this->_tpl_vars['a']->feedbackChoice5; ?>
";
    var strLearnPoints = "<?php echo $this->_tpl_vars['a']->learningPoints; ?>
";
    var strMedia = "<?php echo $this->_tpl_vars['a']->mediaFilePath; ?>
";
    var strSupport_Info = "<?php echo $this->_tpl_vars['a']->supportInfo; ?>
";
    var strNotes = "<?php echo $this->_tpl_vars['a']->notes; ?>
";
    var strMediaPath='../organizations/<?php echo $this->_tpl_vars['orgDirectory']; ?>
/motif/' + strMedia;
    var nextDelay = <?php echo $this->_tpl_vars['a']->mediaDurationSeconds; ?>
;
    var isAb1825 = <?php if ($this->_tpl_vars['a']->categoryId == 118): ?>1<?php else: ?>0<?php endif; ?>;
    var pageTimer;
    <?php if (! $this->_tpl_vars['preview']): ?>
    var progress = <?php echo $this->_tpl_vars['a']->progress; ?>
;
    var progStep = <?php echo $this->_tpl_vars['progStep']; ?>
;
    <?php endif; ?>
    var nextChild = <?php echo $this->_tpl_vars['a']->nextAssignmentId; ?>
;
    var startTime = <?php echo $this->_tpl_vars['startTime']; ?>
;
    var assignmentSetSize = <?php echo $this->_tpl_vars['a']->questionSetSize; ?>
.0;
    var incorrectInSet = <?php echo $this->_tpl_vars['incorrectInSet']; ?>
.0;
    var nextButtonActive = 1;
    var uiTheme = '<?php echo $this->_tpl_vars['uiTheme']; ?>
';
  </SCRIPT>
  <?php if (! $this->_tpl_vars['preview'] && ( $this->_tpl_vars['a']->progress || $this->_tpl_vars['a']->nextAssignmentId )): ?>
  <!-- jsProgressBarHandler prerequisites : prototype.js -->
  <SCRIPT TYPE="text/javascript" SRC="../javascript/prototype.js"></script>
  <!-- jsProgressBarHandler core -->
  <SCRIPT TYPE="text/javascript" SRC="../javascript/jsProgressBarHandler.js"></script>
  <?php endif; ?>
  <SCRIPT LANGUAGE="Javascript" type="text/javascript" src="../javascript/fade.js"></script>
  <SCRIPT LANGUAGE="Javascript" type="text'javascript" src="../javascript/AC_QuickTime.js"></script>
  <SCRIPT LANGUAGE="Javascript" type="text/javascript" src="../javascript/desktop.js"></script>
</HEAD>
<BODY bgcolor="#FFFFFF"
      onLoad="MM_preloadImages('../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/qbar_left_long.png', '../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/btn_FAQ_back_dim.png', '../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/btn_FAQ_back_up.png',
			       '../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/btn_continue_dim_greenbg.png', '../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/correct_check.jpg',
			       '../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/incorrect_x.jpg', '../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/contact_header.jpg', '../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/send_msg_up.jpg',
			       '../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/send_msg_down.jpg', '../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/btn_continue_down_greenbg.png',
			       '../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/btn_continue_up_greenbg.png', '../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/btn_understood_orange.png',
			       '../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/qbar_left_long_empty_with_seal.png', '../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/btn_understood_orange_over.png',
			       '../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/feedback_header.jpg', '../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/qbar_left_long_empty.png',
			       '../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/btn_continue_up_whitebg.png', '../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/btn_continue_down_whitebg.png'); GoMove(0, uiTheme);">


<?php if (! $this->_tpl_vars['preview'] && ( $this->_tpl_vars['a']->progress || $this->_tpl_vars['a']->nextAssignmentId )): ?>
<div style="position:absolute; left:150px; top:190px;">
  <span style="color:#006600;font-weight:bold;">Question Progress:</span>
  <span id="element6">[ Loading Progress Bar ]</span>
</div>
<?php endif; ?>

<div id="presentationWindow" name="presentationWindow" class="PresentationWindow">
  
  <!-- Fairly benign table at top of page displaying pretty stuff in template/header -->
  <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'common/motif.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

  <!-- Shows the question answer header with back/next images -->
  <TABLE width="95%" cellpadding="0" cellspacing="0" align="center" border="0">
    <tr>
      <td id="qbarLeft" name="qbarLeft" width="223" height="105" valign="top"
  	style="background-image:url(../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/qbar_left_long.png); background-color:#FFFFFF;">
        &nbsp;
      </td>
      <td id="qbarMid" name="qbarMid" height="105" align="center" valign="center"
          style="background-image:url(../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/qbar_mid.png); background-repeat:repeat-x; background-color:#FFFFFF;">
        <div id="textHeader" class="HeaderText" name="textHeader">The Question</div>
      </td>
      <td id="qbarBack" name="qbarBack" align="right" valign="center" width="79" height="105"
	style="background-image:url(../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/qbar_mid.png); background-repeat:repeat-x; background-color:#FFFFFF;">
	<!-- BACK BUTTON -->
	<div id="backButtonDiv" class="HideByDefault">
        <a id="btnBackLink" name="btnBackLink" href="javascript: GoMove(-1, uiTheme);" alt="Back Button"
           onmouseout="SetImage('btnBackImage', '../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/btn_FAQ_back_up.png', finalAnswer, strCorrect, requireCorrectAnswer);"
           onmouseover="SetImage('btnBackImage', '../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/btn_FAQ_back_down.png', finalAnswer, strCorrect, requireCorrectAnswer);">
        <img name="btnBackImage" id="btnBackImage" src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/btn_FAQ_back_up.png" alt="" border="0"></a>
        </div>
      </td>
      <td id="qbarNext" name="qbarNext" align="center" width="144" height="105"
          style="background-image:url(../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/qbar_mid.png); background-repeat:repeat-x; background-color:#FFFFFF;">
	<!-- NEXT BUTTON -->
	<div id="nextButtonDiv" class="HideByDefault">
        <a id="btnNextLink" name="btnNextLink" href="javascript:alert('Please wait...still loading');"  alt="Next Button"
           onmouseout="SetImage('btnNextImage', '../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/btn_continue_up_greenbg.png', finalAnswer, strCorrect, requireCorrectAnswer);"
           onmouseover="SetImage('btnNextImage', '../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/btn_continue_down_greenbg.png', finalAnswer, strCorrect, requireCorrectAnswer);">
        <img name="btnNextImage" id="btnNextImage" src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/btn_continue_dim_greenbg.png" alt="" border="0"></a>
	</div>
      </td>
      <td id="qbarRight" name="qbarRight" width="22" height="105" style="background-image:url(../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/qbar_right.png); background-color:#FFFFFF;">
        &nbsp;
      </td>
    </tr>
  </TABLE>

  <!-- QUESTION PAGES, EACH INCLUDE IS YET ANOTHER PAGE -->

  <!-- Actual question presentation -->
  <div id="page_0" name="page_0" class="presentationBodyStyle">
    <?php if (isset ( $this->_tpl_vars['contentErrorMessage'] )): ?>
	<span class="Error"><?php echo $this->_tpl_vars['contentErrorMessage']; ?>
</span>
    <?php else: ?>
        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'presentation/question.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    <?php endif; ?>
  </div>

  <!-- Why it's important -->
  <div id="page_1" name="page_1" class="presentationBodyStyle">
    <div style="width:100%; height:20px;">&nbsp;</div>
    <script language="JavaScript">
      document.writeln('<span class="Instruction">' + strPurpose + '</span>');
    </script>
  </div>

  <!-- Key Learning points -->
  <div id="page_2" name="page_2" class="presentationBodyStyle">
    <script language="JavaScript">
      document.writeln('<span class="Instruction">' + strLearnPoints + '</span>');
    </script>
  </div>

  <!-- Supplimental inforomation -->
  <div id="page_3" name="page_3" class="presentationBodyStyle">
    <script language="JavaScript">
      document.writeln('<span class="Instruction">' + strSupport_Info + '</span>');
    </script>
  </div>

  <!-- additional info -->
  <div id="page_4" name="page_4" class="presentationBodyStyle">
    <script language="JavaScript">
      document.writeln('<span class="Instruction">' + strNotes + '</span>');
    </script>
  </div>

  <!-- confirmation page -->
  <div id="page_5" name="page_5" class="presentationBodyStyle">
    <p class="Instruction" style="text-align:center;"><?php echo $this->_tpl_vars['a']->capConfirmThanks; ?>
</p>

    <div style="width:100%; height:20px;">&nbsp;</div>
    <div class="Instruction" style="font-color:#325100; text-align:center; width:auto; height:auto;">
	  NOTE: You must select one of the options below before completion of training will be recorded.</div>
    <div style="width:100%; height:20px;">&nbsp;</div>

    <p class="Instruction" style="text-align:center;">If you understand the material and have no need
        for further information, please click the button below to return to the desktop.</p>

    <p style="text-align:center;">
      <?php if (isset ( $this->_tpl_vars['genAb1825Cert'] ) && $this->_tpl_vars['genAb1825Cert']): ?>
      <a target="_blank" href="gen_ab1825_cert.php?assignmentId=<?php echo $this->_tpl_vars['a']->assignmentId; ?>
"
	 onmouseout="document.images.btnCloseImage.src='../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/btn_understood_orange.png'"
	 onmouseover="document.images.btnCloseImage.src='../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/btn_understood_orange_over.png'"
	 onclick="setTimeout('<?php if ($this->_tpl_vars['preview']): ?>window.close();<?php else: ?>exitInfo=1; SubmitAnswer();<?php endif; ?>', 5000);">
	<img name="btnCloseImage" id="btnCloseImage" src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/btn_understood_orange.png" alt="Understood" border="0">
      </a>
      <?php else: ?>
      <a href="javascript:<?php if ($this->_tpl_vars['preview']): ?>window.close();<?php else: ?>exitInfo=1; SubmitAnswer();<?php endif; ?>"
	 onmouseout="document.images.btnCloseImage.src='../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/btn_understood_orange.png'"
	 onmouseover="document.images.btnCloseImage.src='../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/btn_understood_orange_over.png'">
	<img name="btnCloseImage" id="btnCloseImage" src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/btn_understood_orange.png" alt="Understood" border="0">
      </a>
      <?php endif; ?>
    </p>

    <p class="Instruction" style="text-align:center;">If you have some questions or would like additional information, please
	 click the button below to contact someone for assistance.</p>

    <p style="text-align:center;">
      <a href="javascript:exitInfo=2; DisplayContacts();"
	 onmouseout="document.images.btnHRImage.src='../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/btn_contact.png'"
	 onmouseover="document.images.btnHRImage.src='../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/btn_contact-mo.png'">
	<img name="btnHRImage" id="btnHRImage" src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/btn_contact.png" alt="Contact" border="0">
      </a>
    </p>
  </div>

<!--/div-->


<!-- Content for feedback window displayed when a user selects an answer to a question -->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'presentation/feedback.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<!-- Content for contacts window displayed when a user selects the Contact button on the thank you page -->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'presentation/contacts.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

</BODY>
</HTML>