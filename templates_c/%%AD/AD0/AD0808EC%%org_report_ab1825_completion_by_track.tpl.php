<?php /* Smarty version 2.6.26, created on 2013-01-21 12:34:43
         compiled from admin/org_report_ab1825_completion_by_track.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'admin/admin_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'admin/org_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'admin/report_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>


<!--SCRIPT language=JavaScript src="../javascript/ajax.js" type="text/javascript"></SCRIPT-->
<?php if (! isset ( $this->_tpl_vars['reportsErrMsg'] )): ?>
<span class="OrgInfoText"><b>Report Parameters:</b></span>
<form name="AdminForm" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>
">
<table border="0">
  <tr>
    <td class="OrgInfoText" height="20" colspan="2">
      Select Classe(s):
    </td>
  </tr>
  <?php unset($this->_sections['trackIdx']);
$this->_sections['trackIdx']['name'] = 'trackIdx';
$this->_sections['trackIdx']['loop'] = is_array($_loop=$this->_tpl_vars['trackIds']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['trackIdx']['show'] = true;
$this->_sections['trackIdx']['max'] = $this->_sections['trackIdx']['loop'];
$this->_sections['trackIdx']['step'] = 1;
$this->_sections['trackIdx']['start'] = $this->_sections['trackIdx']['step'] > 0 ? 0 : $this->_sections['trackIdx']['loop']-1;
if ($this->_sections['trackIdx']['show']) {
    $this->_sections['trackIdx']['total'] = $this->_sections['trackIdx']['loop'];
    if ($this->_sections['trackIdx']['total'] == 0)
        $this->_sections['trackIdx']['show'] = false;
} else
    $this->_sections['trackIdx']['total'] = 0;
if ($this->_sections['trackIdx']['show']):

            for ($this->_sections['trackIdx']['index'] = $this->_sections['trackIdx']['start'], $this->_sections['trackIdx']['iteration'] = 1;
                 $this->_sections['trackIdx']['iteration'] <= $this->_sections['trackIdx']['total'];
                 $this->_sections['trackIdx']['index'] += $this->_sections['trackIdx']['step'], $this->_sections['trackIdx']['iteration']++):
$this->_sections['trackIdx']['rownum'] = $this->_sections['trackIdx']['iteration'];
$this->_sections['trackIdx']['index_prev'] = $this->_sections['trackIdx']['index'] - $this->_sections['trackIdx']['step'];
$this->_sections['trackIdx']['index_next'] = $this->_sections['trackIdx']['index'] + $this->_sections['trackIdx']['step'];
$this->_sections['trackIdx']['first']      = ($this->_sections['trackIdx']['iteration'] == 1);
$this->_sections['trackIdx']['last']       = ($this->_sections['trackIdx']['iteration'] == $this->_sections['trackIdx']['total']);
?>
  <?php if ($this->_sections['trackIdx']['index']%2 == 0): ?>
  <tr>
  <?php endif; ?>
    <td class="OrgInfoText" height="20"<?php if ($this->_sections['trackIdx']['index']%2 == 0 && $this->_sections['trackIdx']['last']): ?> colspan="2"<?php endif; ?>>
      <input type="checkbox" name="trackIdsSelected[]" value="<?php echo $this->_tpl_vars['trackIds'][$this->_sections['trackIdx']['index']]; ?>
" alt="<?php echo $this->_tpl_vars['trackNames'][$this->_sections['trackIdx']['index']]; ?>
"
	     id="trackCheck<?php echo $this->_sections['trackIdx']['index']; ?>
"
	     onChange="javascript:loadDistinctValues(document.AdminForm.trackIdsSelected[]);"
	     <?php if (isset ( $this->_tpl_vars['trackIdsSelected'] ) && in_array ( $this->_tpl_vars['trackIds'][$this->_sections['trackIdx']['index']] , $this->_tpl_vars['trackIdsSelected'] )): ?> checked<?php endif; ?>><?php echo $this->_tpl_vars['trackNames'][$this->_sections['trackIdx']['index']]; ?>

    </td>
  <?php if ($this->_sections['trackIdx']['index']%2 != 0): ?>
  </tr>
  <?php endif; ?>
  <?php endfor; endif; ?>
  <?php if ($this->_sections['trackIdx']['total']): ?>
  <tr>
    <td class="OrgInfoText" height="20">
      <a href="javascript:reportAccuracyToggleTracks('trackCheck', <?php echo $this->_sections['trackIdx']['total']; ?>
);"
	 alt="Select all">Toggle All Classes</a>
    </td>
  </tr>
  <?php endif; ?>
  <tr><td>&nbsp;</td></tr>
  <tr>
    <td class="OrgInfoText" height="20">
      <input type="checkbox" id="showOnlyIncomplete" name="showOnlyIncomplete"<?php if (isset ( $this->_tpl_vars['showOnlyIncomplete'] ) && $this->_tpl_vars['showOnlyIncomplete']): ?> checked<?php endif; ?>>
	Only show participats that have not yet met the AB1825 requirements
    </td>
  </tr>
  <tr>
    <td class="OrgInfoText" height="20" colspan="2"><BR>
      Select the output format for the report:<BR>
      <input type="radio" name="outputFormat" value="html"<?php if (! isset ( $this->_tpl_vars['outputFormat'] ) || $this->_tpl_vars['outputFormat'] == 'html'): ?> checked<?php endif; ?>>
      	     HTML (display as web page)<BR>
      <input type="radio" name="outputFormat" value="pdf"<?php if (isset ( $this->_tpl_vars['outputFormat'] ) && $this->_tpl_vars['outputFormat'] == 'pdf'): ?> checked<?php endif; ?>>
      	     PDF (used for hard copy, email attachments, and presentation)<BR>
      <input type="radio" name="outputFormat" value="csv"<?php if (isset ( $this->_tpl_vars['outputFormat'] ) && $this->_tpl_vars['outputFormat'] == 'csv'): ?> checked<?php endif; ?>>
      	     CSV (used for spreadsheets and charts)
    </td>
  </tr>
</table>
<?php endif; ?>
<BR>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'admin/report_footer.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
</form>
</body>
</html>