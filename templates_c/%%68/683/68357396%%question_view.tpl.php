<?php /* Smarty version 2.6.26, created on 2012-12-20 13:55:54
         compiled from admin/question_view.tpl */ ?>
<?php if (! isset ( $this->_tpl_vars['contentOnly'] ) || ! $this->_tpl_vars['contentOnly']): ?>
  <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'admin/admin_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
  <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'admin/question_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php endif; ?>


<!--div name="contentArea" id="contentArea"<?php if (isset ( $this->_tpl_vars['contentOnly'] ) && $this->_tpl_vars['contentOnly']): ?> style="width:auto; overflow:visible; display:inline; page-break-after:always;"<?php endif; ?>-->
<table border="0" cellpadding="1" cellspacing="5">
<?php if (! isset ( $this->_tpl_vars['contentOnly'] ) || ! $this->_tpl_vars['contentOnly']): ?>
  <tr>
    <td colspan="2">
      <input type="button" name="printButton" value="Print Question"
             onclick="javascript:if (window.print) window.print(); else alert('Sorry, your browser does not support this function.  Please upgrade your browser to a more recent version and try again.');">
    </td>                           
  </tr>
<?php endif; ?>
  <tr>
    <td class="OrgInfoText">Question Number:</td>
    <td class="FormValue"><?php echo $this->_tpl_vars['q']->questionId; ?>
</td>
  </tr>
  <tr>
    <td class="OrgInfoText">Parent Question Number:</td>
    <td class="FormValue"><?php if ($this->_tpl_vars['q']->parentQuestionId > 0): ?><?php echo $this->_tpl_vars['q']->parentQuestionId; ?>
<?php else: ?>No Parent<?php endif; ?></td>
  </tr>
  <tr>
    <td class="OrgInfoText">Language:</td>
    <td class="FormValue"><?php $this->assign('languages', $this->_tpl_vars['q']->GetLanguages()); ?>
			  <?php $this->assign('languageId', $this->_tpl_vars['q']->languageId); ?>
			  <?php echo $this->_tpl_vars['languages'][$this->_tpl_vars['languageId']]; ?>
</td>
  </tr>
  <tr>
    <td class="OrgInfoText">Domain:</td>
    <td class="FormValue"><?php $this->assign('domains', $this->_tpl_vars['q']->GetDomains()); ?>
			  <?php $this->assign('domainId', $this->_tpl_vars['q']->domainId); ?>
			  <?php echo $this->_tpl_vars['domains'][$this->_tpl_vars['domainId']]; ?>
</td>
  </tr>
  <tr>
    <td class="OrgInfoText">Version Date</td>
    <td class="FormValue"><?php echo $this->_tpl_vars['q']->versionDate; ?>
</td>
  </tr>
  <tr>
    <td class="OrgInfoText">Category:</td>
    <td class="FormValue"><?php $this->assign('categories', $this->_tpl_vars['q']->GetCategories()); ?>
			  <?php $this->assign('categoryId', $this->_tpl_vars['q']->categoryId); ?>
			  <?php echo $this->_tpl_vars['categories'][$this->_tpl_vars['categoryId']]; ?>
</td>
  </tr>
  <tr>
    <td class="OrgInfoText">Frametype:</td>
    <td class="FormValue"><?php $this->assign('frameTypes', $this->_tpl_vars['q']->GetFrameTypes()); ?>
			  <?php $this->assign('frameTypeId', $this->_tpl_vars['q']->frameTypeId); ?>
			  <?php echo $this->_tpl_vars['frameTypes'][$this->_tpl_vars['frameTypeId']]; ?>
</td>
  </tr>
  <tr>
    <td class="OrgInfoText">Author:</td>
    <td class="FormValue"><?php echo $this->_tpl_vars['q']->author; ?>
</td>
  </tr>
  <tr>
    <td class="OrgInfoText">Type of Question:</td>
    <td class="FormValue"><?php if ($this->_tpl_vars['q']->isManagement): ?>Management<?php else: ?>Standard<?php endif; ?></td>
  </tr>
  <tr><td class="OrgInfoText">Title:</td>
      <td class="FormValue"><?php echo $this->_tpl_vars['q']->title; ?>
</td>
  </tr>
  <?php if (! empty ( $this->_tpl_vars['q']->mediaFilePath )): ?>
  <tr><td class="OrgInfoText">Media File:</td>
      <td class="FormValue" id="media"><?php echo $this->_tpl_vars['q']->mediaFilePath; ?>
</td>
  </tr>
  <?php endif; ?>
  <tr><td class="OrgInfoText">Question:</td>
      <td class="FormValue" id="question"><?php echo $this->_tpl_vars['q']->question; ?>
</td>
  </tr>
  <tr><td class="OrgInfoText" width="80">Answer A:</td>
      <td class="FormValue" id="answerA"><?php echo $this->_tpl_vars['q']->multipleChoice1; ?>
</td>
  </tr>
  <tr><td class="OrgInfoText">Answer B:</td>
      <td class="FormValue" id="answerB"><?php echo $this->_tpl_vars['q']->multipleChoice2; ?>
</td>
  </tr>
  <tr><td class="OrgInfoText">Answer C:</td>
      <td class="FormValue" id="answerC"><?php echo $this->_tpl_vars['q']->multipleChoice3; ?>
</td>
  </tr>
  <tr><td class="OrgInfoText">Answer D:</td>
      <td class="FormValue" id="answerD"><?php echo $this->_tpl_vars['q']->multipleChoice4; ?>
</td>
  </tr>
  <tr><td class="OrgInfoText">Answer E:</td>
      <td class="FormValue" id="answerE"><?php echo $this->_tpl_vars['q']->multipleChoice5; ?>
</td>
  </tr>
  <tr>
    <td width="150" class="OrgInfoText">Correct Answer:</td>
    <td class="FormValue" id="correctAnswer"><?php echo $this->_tpl_vars['q']->correctAnswer; ?>
</td>
  </tr>
  <tr>

    <td class="OrgInfoText">Purpose:</td>
    <td class="FormValue" id="purpose"><?php echo $this->_tpl_vars['q']->purpose; ?>
</td>
  </tr>
  <tr>
    <td class="OrgInfoText" width="150">Answer A Feedback:</td>
    <td class="FormValue" id="responseA"><?php echo $this->_tpl_vars['q']->feedbackChoice1; ?>
</td>
  </tr>
  <tr>
    <td class="OrgInfoText">Answer B Feedback:</td>
    <td class="FormValue" id="responseB"><?php echo $this->_tpl_vars['q']->feedbackChoice2; ?>
</td>
  </tr>
  <tr>

    <td class="OrgInfoText">Answer C Feedback:</td>
    <td class="FormValue" id="responseC"><?php echo $this->_tpl_vars['q']->feedbackChoice3; ?>
</td>
  </tr>
  <tr>
    <td class="OrgInfoText">Answer D Feedback:</td>
    <td class="FormValue" id="responseD"><?php echo $this->_tpl_vars['q']->feedbackChoice4; ?>
</td>
  </tr>
  <tr>
    <td class="OrgInfoText">Answer E Feedback:</td>
    <td class="FormValue" id="responseE"><?php echo $this->_tpl_vars['q']->feedbackChoice5; ?>
</td>
  </tr>
  <tr>
    <td class="OrgInfoText">Key Learning Points:</td>
    <td class="FormValue" id="keyLearningPoints"><?php echo $this->_tpl_vars['q']->learningPoints; ?>
</td>
  </tr>
  <?php if (! empty ( $this->_tpl_vars['q']->supportInfo )): ?>
  <tr>
    <td class="OrgInfoText">Supplemental Information:</td>
    <td class="FormValue" id="supportInfo"><?php echo $this->_tpl_vars['q']->supportInfo; ?>
</td>
  </tr>
  <?php endif; ?>
  <?php if (! empty ( $this->_tpl_vars['q']->Notes )): ?>
  <tr>
    <td class="OrgInfoText">Additional Information:</td>
    <td class="FormValue" id="notes"><?php echo $this->_tpl_vars['q']->notes; ?>
</td>
  </tr>
  <?php endif; ?>
</table>
<!--/div-->
</body>
</html>