<?php /* Smarty version 2.6.26, created on 2012-12-17 00:55:12
         compiled from admin/user_edit.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', 'admin/user_edit.tpl', 87, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'admin/admin_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'admin/user_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<form name="AdminForm" method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>
">
  <?php if (isset ( $this->_tpl_vars['user']->userId )): ?>
  <input type="hidden" name="userId" value="<?php echo $this->_tpl_vars['user']->userId; ?>
">
  <?php endif; ?>
  <!--div id="Layer1" style="position:absolute; left:15px; top:57px; width:302px; height:31px; z-index:3"-->

<table border="0">
<tr>
<td>
  <table border="0">
    <tr>
      <td class="OrgInfoText">User ID: &nbsp;&nbsp;</td>
      <td><!--<?php echo $this->_tpl_vars['user']->userId; ?>
-->
	<input type="text" name="userId" id="userId" onClick="alert('This field is automatically generated.');"valu
	       class="FormDisabled" maxlength="45" size="30"
	       value ="<?php if ($this->_tpl_vars['user']->userId == 0): ?>To Be Assigned<?php else: ?><?php echo $this->_tpl_vars['user']->userId; ?>
<?php endif; ?>" disabled>
      </td>
    </tr>
    <tr>
        <td class="OrgInfoText" height="29">First Name: &nbsp;&nbsp;</td>
        <td height="29">
          <input class="FormValue" type="text" name="firstName" id="firstName" maxlength="45" size="30" value ="<?php echo $this->_tpl_vars['user']->firstName; ?>
"
	   onchange="javascript:SetFlag();">
        </td>
    </tr>
    <tr>
      <td class="OrgInfoText">Last Name:</td>
      <td>
          <input class="FormValue" type="text" name="lastName" id="lastName" maxlength="45" size="30" value ="<?php echo $this->_tpl_vars['user']->lastName; ?>
"
		 onchange="javascript:SetFlag();">
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText"><!--Last 6 digits<br>of SS#:-->Login:</td>
      <td>
	<!--table width="100%" cellpadding="0" cellspacing="0">
          <tr>
            <td>
              <input class="FormValue" type="text" name="ssn" maxlength="6" size="6" value ="user->ssn"
	             onchange="javascript:SetFlag();">
	    </td>
	    <td align="right" class="OrgInfoText">

            </td>
            <td align="right"-->
              <input class="FormValue" type="text" name="login" maxlength="45" size="30" value ="<?php echo $this->_tpl_vars['user']->login; ?>
"
	             onchange="javascript:SetFlag();">
	      <!--input type="text" class="FormDisabled" name="login" id="login" maxlength="8" size="8" value="<?php echo $this->_tpl_vars['user']->login; ?>
" disabled>
	    </td>
          </tr>
        </table-->
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText" height="29">Password: &nbsp;&nbsp;</td>
      <td height="29">
        <input class="FormValue" type="password" name="password" id="password" size="30" maxlength="45" value ="<?php echo $this->_tpl_vars['user']->password; ?>
"
	       onchange="javascript:SetFlag();">
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText">E-Mail:</td>
      <td>
        <input class="FormValue" type="text" name="email" maxlength="80" size="30" value ="<?php echo $this->_tpl_vars['user']->email; ?>
" onchange="javascript:SetFlag();">
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText">Phone #:</td>
      <td>
        <input class="FormValue" type="text" name="phone" size="30" maxlength="30" value ="<?php echo $this->_tpl_vars['user']->phone; ?>
" onchange="javascript:SetFlag();">
      </td>
    </tr>
  </table>
</td>


<td>
  <table border="0">
    <tr>
      <td class="OrgInfoText">Status: &nbsp;&nbsp;</td>
      <td>
        <select  class="FormValue" name="employmentStatusId" onchange="javascript:SetFlag();">
          <option></option>
	  <?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['employmentStatusValues'],'selected' => $this->_tpl_vars['user']->employmentStatusId), $this);?>

        </select>
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText">Language:</td>
      <td>
        <select class="FormValue" name="language" onchange="javascript:SetFlag();">
          <option></option>
	  <?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['languages'],'selected' => $this->_tpl_vars['user']->languageId), $this);?>

        </select>
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText">Contact:</td>
      <td>
        <select name="contactId" id="contactId" size="1" onchange="javascript:SetFlag();"
		<?php if ($this->_tpl_vars['user']->useSupervisor): ?>class="FormDisabled" disabled<?php else: ?>class="FormValue"<?php endif; ?>>
          <option value="0"></option>
	  <?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['contacts'],'selected' => $this->_tpl_vars['user']->contactId), $this);?>

        </select>
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText">State:</td>
      <td>
        <select class="FormValue" name="domainId" onchange="javascript:SetFlag();">
          <option></option>
	  <?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['domains'],'selected' => $this->_tpl_vars['user']->domainId), $this);?>

        </select>
      </td>
    </tr>
    <tr>
       <td class="OrgInfoText">Department</td>
       <td>
	 <select class="FormValue" name="departmentId" onchange="javascript:SetFlag();">
	   <option></option>
	   <?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['departments'],'selected' => $this->_tpl_vars['user']->departmentId), $this);?>

	 </select>
       </td>
     </tr>
     <tr>
       <td class="OrgInfoText">Division</td>
       <td>
	 <select class="FormValue" name="divisionId" onchange="javascript:SetFlag();">
	   <option></option>
	   <?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['divisions'],'selected' => $this->_tpl_vars['user']->divisionId), $this);?>

	 </select>
      </td>
     </tr>
    <tr>
       <td class="OrgInfoText">Role</td>
       <td>
         <?php echo smarty_function_html_options(array('name' => 'role','options' => $this->_tpl_vars['roles'],'selected' => $this->_tpl_vars['user']->role), $this);?>

       </td>
    </tr>
    <tr>
      <td colspan="2">
         <input type="checkbox" name="useSupervisor" id="useSupervisor" value="1" onchange="javascript:SetFlag();"
	    onClick="javascript:ToggleContactId('useSupervisor', 'contactId');"
            <?php if ($this->_tpl_vars['user']->useSupervisor): ?> checked<?php endif; ?>>
	    <span class="OrgInfoText">Use the Class Supervisor as the Contact</span>
      </td>
    </tr>
  </table>
</td>
</tr>


<?php if (isset ( $this->_tpl_vars['trackMemberIdList'] ) && ! empty ( $this->_tpl_vars['trackMemberIdList'] )): ?>
<tr>
  <td colspan="2" class="OrgInfoText">This employee is currently a member of the following classes:</td>
</tr>
<?php unset($this->_sections['trackMemberListIdx']);
$this->_sections['trackMemberListIdx']['name'] = 'trackMemberListIdx';
$this->_sections['trackMemberListIdx']['loop'] = is_array($_loop=$this->_tpl_vars['trackMemberIdList']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['trackMemberListIdx']['show'] = true;
$this->_sections['trackMemberListIdx']['max'] = $this->_sections['trackMemberListIdx']['loop'];
$this->_sections['trackMemberListIdx']['step'] = 1;
$this->_sections['trackMemberListIdx']['start'] = $this->_sections['trackMemberListIdx']['step'] > 0 ? 0 : $this->_sections['trackMemberListIdx']['loop']-1;
if ($this->_sections['trackMemberListIdx']['show']) {
    $this->_sections['trackMemberListIdx']['total'] = $this->_sections['trackMemberListIdx']['loop'];
    if ($this->_sections['trackMemberListIdx']['total'] == 0)
        $this->_sections['trackMemberListIdx']['show'] = false;
} else
    $this->_sections['trackMemberListIdx']['total'] = 0;
if ($this->_sections['trackMemberListIdx']['show']):

            for ($this->_sections['trackMemberListIdx']['index'] = $this->_sections['trackMemberListIdx']['start'], $this->_sections['trackMemberListIdx']['iteration'] = 1;
                 $this->_sections['trackMemberListIdx']['iteration'] <= $this->_sections['trackMemberListIdx']['total'];
                 $this->_sections['trackMemberListIdx']['index'] += $this->_sections['trackMemberListIdx']['step'], $this->_sections['trackMemberListIdx']['iteration']++):
$this->_sections['trackMemberListIdx']['rownum'] = $this->_sections['trackMemberListIdx']['iteration'];
$this->_sections['trackMemberListIdx']['index_prev'] = $this->_sections['trackMemberListIdx']['index'] - $this->_sections['trackMemberListIdx']['step'];
$this->_sections['trackMemberListIdx']['index_next'] = $this->_sections['trackMemberListIdx']['index'] + $this->_sections['trackMemberListIdx']['step'];
$this->_sections['trackMemberListIdx']['first']      = ($this->_sections['trackMemberListIdx']['iteration'] == 1);
$this->_sections['trackMemberListIdx']['last']       = ($this->_sections['trackMemberListIdx']['iteration'] == $this->_sections['trackMemberListIdx']['total']);
?>
<?php if ($this->_sections['trackMemberListIdx']['index']%2 == 0): ?>
<tr>
<?php endif; ?>
  <td class="OrgInfoText">
    <input type="checkbox" name="keepTrackIds[]" value="<?php echo $this->_tpl_vars['trackMemberIdList'][$this->_sections['trackMemberListIdx']['index']]; ?>
" checked>
        <?php echo $this->_tpl_vars['trackMemberNameList'][$this->_sections['trackMemberListIdx']['index']]; ?>

  </td>
  <?php if ($this->_sections['trackMemberListIdx']['last'] && $this->_sections['trackMemberListIdx']['index']%2 == 0): ?>
  <td>&nbsp;</td>
  <?php endif; ?>
<?php if ($this->_sections['trackMemberListIdx']['index']%2 != 0): ?>
</tr>
<?php endif; ?>
<?php endfor; endif; ?>
<tr>
  <td colspan="2" class="OrgInfoText" width="600">
  To remove a user from any of the classes above, uncheck the boxes next to each class and then click on the 'Update' button at the upper right.  To add a user to a class click on the Classes tab, click on the Edit link of the class to which you want to add this user, then click on the Participants subtab.  Click on the Show all users link and then browse to or search for the user(s) you would like to add.  Then click the Add to Class link in the last column of the page.
  </td>
</tr>
<?php endif; ?>
</table>

</form>

</body>
</html>