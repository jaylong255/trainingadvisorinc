<?php /* Smarty version 2.6.26, created on 2012-12-17 01:01:38
         compiled from presentation/logout.tpl */ ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
  <META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <Title>WTP Desktop Logout</Title>
  <link rel='stylesheet' href='../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/desktop.css' type='text/css'>
  <script language='javascript' type='text/javascript' src='../javascript/desktop.js'></script>
  <script language='javascript'>
    var imgBack = new Image();
    imgBack.src = "../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/goback_up.gif";
    var imgBackb = new Image();
    imgBackb.src = "../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/goback_down.gif";
    var imgExit = new Image();
    imgExit.src = "../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/exit_up.gif";
    var imgExitb = new Image();
    imgExitb.src = "../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/exit_down.gif";
  </script>
</HEAD>
<BODY bgcolor="#FFFFFF">

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'common/motif.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<BR>
<table border='0' cellpadding='0' cellspacing='0' width='100%' height='100%'>
  <tr>
    <td align='center' valign='top'>
      <table border='0' cellpadding='0' cellspacing='0' width='100%'>
	<tr>
	  <td align='center' valign='top'>
	    <table border='0' cellpadding='10' cellspacing='0' width='474' height='57' background='../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/bg_logout.gif' style='background-repeat:no-repeat;'>
	      <tr>
		<td valign='top'>
		  <span class="OrgInfoText"><?php echo $this->_tpl_vars['statusMsg']; ?>
</span>
		</td>
	      </tr>
	      <tr>
		<td align='center' valign='top'><br>
		  <p class='BigBlack'>Click <b>Exit</b> to leave the Training site.</p>
		  <p><a href='../login/<?php echo $this->_tpl_vars['loginUrl']; ?>
' onMouseOver='swap(exit,imgExitb);' onMouseOut='swap(exit,imgExit);'>
		  <img name='exit' src='../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/exit_up.gif' border='0'></a></p>
		</td>
	      </tr>
	      <tr>
		<td align='center' valign='top'><br>
		  <p class='BigBlack'>Click <b>Go Back</b> to return to the Training site.</p>
		  <p><a href='javascript:history.go(-1);' onMouseOver='swap(back,imgBackb);' onMouseOut='swap(back,imgBack);'>
		  <img name='back' src='../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/presentation/goback_up.gif' border='0'></a></p>
		</td>
	      </tr>
	    </table>
	  </td>
	</tr>
      </table>
    </td>
  </tr>
</TABLE>

</BODY>
</HTML>