<?php /* Smarty version 2.6.26, created on 2014-03-03 06:56:33
         compiled from admin/track_edit.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', 'admin/track_edit.tpl', 67, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'admin/admin_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'admin/track_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>


<form name="AdminForm" class="orgForm" method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>
">
  <!--div id="Layer1" style="position:absolute; left:15px; top:55px; width:363px; height:31px; z-index:5"-->
  <!-- TODO test to see if this hidden field is even necessary.  Now that the track is stored in seession I do not think so -->
  <!--input type="hidden" name="trackId" value="<?php echo $this->_tpl_vars['track']->trackId; ?>
"-->
  <table width="100%" border="0">
  <tr>
    <td class="OrgInfoText" width="90">Class ID:</td>
    <td width="150">
      <input class="FormDisabled" readonly type="text" name="trackId"
	     onClick="alert('This field is automatically generated and can not be changed.');"
	     value ="<?php if (! $this->_tpl_vars['track']->trackId): ?>To Be Assigned<?php else: ?><?php echo $this->_tpl_vars['track']->trackId; ?>
<?php endif; ?>">
    </td>
    <td width="20">&nbsp;</td>
    <td valign="top" rowspan="6">
      <p><input class="FormValue" type="checkbox" name="isManagement" id="isManagement"
	    value="1" onchange="javascript:SetFlag();"<?php if ($this->_tpl_vars['track']->isManagement): ?> checked<?php endif; ?>>
	    Management Class - (Make all questions available to class.)</p>
      </p>
      <?php if (! $this->_tpl_vars['track']->trackId): ?>
      <p>
        <input class="FormValue" type="checkbox" name="groupAssignments" id="groupAssignments" value="1"
	    <?php if ($this->_tpl_vars['licenseAb1825'] && ! $this->_tpl_vars['track']->trackId): ?> checked disabled<?php endif; ?>>
	    Group Assignments - (Automatically add all assignments.)
      </p>
      <?php endif; ?>
      <p>
	<input class="FormValue" type="checkbox" name="isBinding" id="isBinding" value="1"
	    onchange="javascript:SetFlag();"
	    <?php if ($this->_tpl_vars['track']->isBinding || ! $this->_tpl_vars['track']->trackId): ?> checked<?php endif; ?>
	    <?php if ($this->_tpl_vars['track']->trackId || ( $this->_tpl_vars['licenseAb1825'] && ! $this->_tpl_vars['track']->trackId )): ?> disabled<?php endif; ?>>
            Bind Class - (Synchronize questions for all participants.)
      </p>
      <p>
	<input class="FormValue" type="checkbox" name="isLooping" id="isLooping"
	    value="1" onchange="javascript:SetFlag();" <?php if ($this->_tpl_vars['track']->isLooping): ?> checked<?php endif; ?>
	    <?php if ($this->_tpl_vars['licenseAb1825'] && ! $this->_tpl_vars['track']->trackId): ?> disabled<?php endif; ?>>
	    Loop Class - (Keep repeating questions in this class.)
      </p>
      <p>
      <?php if ($this->_tpl_vars['licenseAb1825']): ?>
	<input class="FormValue" type="checkbox" name="isAb1825" id="isAb1825" value="1"
	    onchange="javascript:SetFlag(); ToggleAb1825Track();"
	    <?php if ($this->_tpl_vars['track']->isAb1825 || ! $this->_tpl_vars['track']->trackId): ?> checked<?php endif; ?>
	    <?php if (is_numeric ( $this->_tpl_vars['track']->trackId ) && $this->_tpl_vars['track']->trackId > 0): ?> disabled<?php endif; ?>>
	    AB1825 Class - (Meets California AB1825 training requirements.)
      <?php endif; ?>
      </p>
    </td>
  </tr>
  <tr>
    <td class="OrgInfoText" width="90"> Name: &nbsp;&nbsp;</td>
    <td width="150">
      <input class="FormValue" type="text" name="trackName" id="trackName" maxlength="45"
	     size="30" value ="<?php echo $this->_tpl_vars['track']->trackName; ?>
" onchange="javascript:SetFlag();">
    </td>
    <td width="20">&nbsp;</td>
  </tr>
  <tr>
    <td class="OrgInfoText" valign="middle" width="90">Supervisor: &nbsp;&nbsp;</td>
    <td width="150">
      <select name="supervisorId" class="FormValue" onchange="javascript:SetFlag();">
	<option></option>
        <?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['track']->supervisors,'selected' => $this->_tpl_vars['track']->supervisorId), $this);?>

      </select>
    </td>
    <td width="20">&nbsp;</td>
  </tr>
  <tr>
    <td class="OrgInfoText" width="90">E-Mail Reply:</td>
    <td width="150">
      <input class="FormValue" type="text" name="emailReplyTo" maxlength="50" size="30" value ="<?php echo $this->_tpl_vars['track']->emailReplyTo; ?>
"
	     onchange="javascript:SetFlag();">
    </td>
    <td width="20">&nbsp;</td>
  </tr>
  <tr>
    <td class="OrgInfoText" width="90">Start Date:</td>
    <td valign="center" width="150">
      <input class="FormValue" type="text" name="startDate" id="startDate" value ="<?php echo $this->_tpl_vars['track']->startDate; ?>
"
	     onchange="javascript:SetFlag();" title="Schedule when questions will be delivered">
        <a href="javascript:doNothing()"
	   onClick="setDateField(document.getElementById('startDate'));top.newWin=window.open('calendar.html','cal','dependent=yes,width=210,height=230,screenX=200,screenY=300,titlebar=yes')">
	  <img id="cal" src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/calendar.gif" border="0" width="16" height="16"></a>
    </td>
    <td width="20">&nbsp;</td>
  </tr>
  <tr>
    <td class="OrgInfoText" width="90">Recurrence:</td>
    <td width="150">
      <textarea class="FormValue" rows="2" cols="25" name="strRecurrence" id="strRecurrence"
		onchange="javascript:SetFlag();" readonly><?php echo $this->_tpl_vars['track']->strRecurrence; ?>
</textarea>
      <input class="FormValue" type="hidden" name="recurrence" id="recurrence" value ="<?php echo $this->_tpl_vars['track']->recurrence; ?>
"
	     onchange="javascript:SetFlag();">
    </td>
    <td valign="center" align="left" width="20" style="width:20px;">
      <a href="javascript:doNothing();"
	 onClick="window.open('recurrence.php?recurrence='+document.getElementById('recurrence').value,'recurrence','dependent=yes,width=515,height=200,screenX=200,screenY=300,titlebar=yes')" align="left">
      <img src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/calendar.gif" border="0" width="17" height="16" align="left"></a>
    </td>
  </tr>
  <!--tr>
    <td class="OrgInfoText">Class News URL:</td>
    <td colspan="4">
      <input class="FormValue" type="text" name="trackNewsUrl" id="trackNewsUrl"
	     maxlength="150" size="85" value ="<?php echo $this->_tpl_vars['track']->trackNewsUrl; ?>
" onchange="javascript:SetFlag();">
    </td>
  </tr-->
  <tr>
    <td class="OrgInfoText" width="90">Delinquency:</td>
    <td colspan="3" valign="center">
      <div style="float:left; vertical-align:middle"><input class="FormValue" style="vertical-align:middle;" type="text" name="delinquencyNotification" id="delinquencyNotification" size="4"
	     maxlength="3" value ="<?php if ($this->_tpl_vars['track']->delinquencyNotification > 0): ?><?php echo $this->_tpl_vars['track']->delinquencyNotification; ?>
<?php endif; ?>"
	     onchange="javascript:SetFlag();"
	     title="Notify class organizer when student has X or more questions assigned." align="left"></div>
	The number of questions a participant can fall behind before a delinquency report is sent by email. Leave blank if no report is desired.
    </td>
  </tr>
</table>
</form>
</body>
</html>