<?php /* Smarty version 2.6.26, created on 2012-12-31 10:25:44
         compiled from admin/org_report_accuracy_by_track_results.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'math', 'admin/org_report_accuracy_by_track_results.tpl', 60, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'admin/admin_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'admin/org_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'admin/report_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<!--SCRIPT language=JavaScript src="../javascript/ajax.js" type="text/javascript"></SCRIPT-->

<?php if (! isset ( $this->_tpl_vars['reportsErrMsg'] )): ?>
<table border="0" width="680" align="top" cellpadding="2" cellspacing="3">
  <tr>
    <td width="23">
      <a href="org_reports.php" onMouseOut="MM_swapImgRestore()"
	 onMouseOver="MM_swapImage('Back','','../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/btn_back_down.gif',1)">
        <img name="Back" border="0" src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/btn_back_up.gif" width="23" height="21" align="center"></a>
    </td>
    <td align="left">
      <a class="OrgInfoText"
	href="org_report_accuracy_by_track.php?trackIdsSelected[]=<?php echo $this->_tpl_vars['trackIdsSelected']; ?>
<?php if ($this->_tpl_vars['dateFrom']): ?>&dateFrom=<?php echo $this->_tpl_vars['dateFrom']; ?>
<?php endif; ?><?php if ($this->_tpl_vars['dateTo']): ?>&dateTo=<?php echo $this->_tpl_vars['dateTo']; ?>
<?php endif; ?><?php if ($this->_tpl_vars['questionId']): ?>&questionId=<?php echo $this->_tpl_vars['questionId']; ?>
<?php endif; ?><?php if ($this->_tpl_vars['outputFormat']): ?>&outputFormat=<?php echo $this->_tpl_vars['outputFormat']; ?>
<?php endif; ?><?php if ($this->_tpl_vars['resultFormatNumbers']): ?>&resultFormatNumbers=1<?php endif; ?><?php if ($this->_tpl_vars['resultFormatPercentages']): ?>&resultFormatPercentages=1<?php endif; ?>"
	alt="Return to Report Parameters">Return to Report Parameters</a>
    </td>
    <td align="right">
	&nbsp;
    </td>
  </tr>
  <tr>
    <td colspan="3"><hr width="100%"></td>
  </tr>
  <?php if (empty ( $this->_tpl_vars['trackList'] )): ?>
  <tr>
    <td colspan="2">
      There is not data in this report.  Please return the report parameters and ensure that the parameters you have selected should actually contain data.
    </td>
  </tr>
  <?php endif; ?>
</table>



<?php unset($this->_sections['trackListIdx']);
$this->_sections['trackListIdx']['name'] = 'trackListIdx';
$this->_sections['trackListIdx']['loop'] = is_array($_loop=$this->_tpl_vars['trackList']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['trackListIdx']['show'] = true;
$this->_sections['trackListIdx']['max'] = $this->_sections['trackListIdx']['loop'];
$this->_sections['trackListIdx']['step'] = 1;
$this->_sections['trackListIdx']['start'] = $this->_sections['trackListIdx']['step'] > 0 ? 0 : $this->_sections['trackListIdx']['loop']-1;
if ($this->_sections['trackListIdx']['show']) {
    $this->_sections['trackListIdx']['total'] = $this->_sections['trackListIdx']['loop'];
    if ($this->_sections['trackListIdx']['total'] == 0)
        $this->_sections['trackListIdx']['show'] = false;
} else
    $this->_sections['trackListIdx']['total'] = 0;
if ($this->_sections['trackListIdx']['show']):

            for ($this->_sections['trackListIdx']['index'] = $this->_sections['trackListIdx']['start'], $this->_sections['trackListIdx']['iteration'] = 1;
                 $this->_sections['trackListIdx']['iteration'] <= $this->_sections['trackListIdx']['total'];
                 $this->_sections['trackListIdx']['index'] += $this->_sections['trackListIdx']['step'], $this->_sections['trackListIdx']['iteration']++):
$this->_sections['trackListIdx']['rownum'] = $this->_sections['trackListIdx']['iteration'];
$this->_sections['trackListIdx']['index_prev'] = $this->_sections['trackListIdx']['index'] - $this->_sections['trackListIdx']['step'];
$this->_sections['trackListIdx']['index_next'] = $this->_sections['trackListIdx']['index'] + $this->_sections['trackListIdx']['step'];
$this->_sections['trackListIdx']['first']      = ($this->_sections['trackListIdx']['iteration'] == 1);
$this->_sections['trackListIdx']['last']       = ($this->_sections['trackListIdx']['iteration'] == $this->_sections['trackListIdx']['total']);
?>
<div class="Stacking">
  <span class="OrgInfoText">Report for Track: <?php echo $this->_tpl_vars['trackList'][$this->_sections['trackListIdx']['index']][0][1]; ?>
</span>
</div>

<div class="Stacking">
<table border="0" width="100%" align="center" bgcolor="#FFFFFF">
  <tr bgcolor="#DDDDDD">
    <th class="OrgInfoText">Question #</th>
    <th class="OrgInfoText">Topic</th>
    <th class="OrgInfoText">Delivery Date</th>
    <th class="OrgInfoText">Assigned</th>
    <th class="OrgInfoText">Completed</th>
    <th class="OrgInfoText">Understood</th>
    <th class="OrgInfoText">Correct</th>
  </tr>
  <?php unset($this->_sections['trackListDataIdx']);
$this->_sections['trackListDataIdx']['name'] = 'trackListDataIdx';
$this->_sections['trackListDataIdx']['loop'] = is_array($_loop=$this->_tpl_vars['trackList'][$this->_sections['trackListIdx']['index']]) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['trackListDataIdx']['start'] = (int)1;
$this->_sections['trackListDataIdx']['show'] = true;
$this->_sections['trackListDataIdx']['max'] = $this->_sections['trackListDataIdx']['loop'];
$this->_sections['trackListDataIdx']['step'] = 1;
if ($this->_sections['trackListDataIdx']['start'] < 0)
    $this->_sections['trackListDataIdx']['start'] = max($this->_sections['trackListDataIdx']['step'] > 0 ? 0 : -1, $this->_sections['trackListDataIdx']['loop'] + $this->_sections['trackListDataIdx']['start']);
else
    $this->_sections['trackListDataIdx']['start'] = min($this->_sections['trackListDataIdx']['start'], $this->_sections['trackListDataIdx']['step'] > 0 ? $this->_sections['trackListDataIdx']['loop'] : $this->_sections['trackListDataIdx']['loop']-1);
if ($this->_sections['trackListDataIdx']['show']) {
    $this->_sections['trackListDataIdx']['total'] = min(ceil(($this->_sections['trackListDataIdx']['step'] > 0 ? $this->_sections['trackListDataIdx']['loop'] - $this->_sections['trackListDataIdx']['start'] : $this->_sections['trackListDataIdx']['start']+1)/abs($this->_sections['trackListDataIdx']['step'])), $this->_sections['trackListDataIdx']['max']);
    if ($this->_sections['trackListDataIdx']['total'] == 0)
        $this->_sections['trackListDataIdx']['show'] = false;
} else
    $this->_sections['trackListDataIdx']['total'] = 0;
if ($this->_sections['trackListDataIdx']['show']):

            for ($this->_sections['trackListDataIdx']['index'] = $this->_sections['trackListDataIdx']['start'], $this->_sections['trackListDataIdx']['iteration'] = 1;
                 $this->_sections['trackListDataIdx']['iteration'] <= $this->_sections['trackListDataIdx']['total'];
                 $this->_sections['trackListDataIdx']['index'] += $this->_sections['trackListDataIdx']['step'], $this->_sections['trackListDataIdx']['iteration']++):
$this->_sections['trackListDataIdx']['rownum'] = $this->_sections['trackListDataIdx']['iteration'];
$this->_sections['trackListDataIdx']['index_prev'] = $this->_sections['trackListDataIdx']['index'] - $this->_sections['trackListDataIdx']['step'];
$this->_sections['trackListDataIdx']['index_next'] = $this->_sections['trackListDataIdx']['index'] + $this->_sections['trackListDataIdx']['step'];
$this->_sections['trackListDataIdx']['first']      = ($this->_sections['trackListDataIdx']['iteration'] == 1);
$this->_sections['trackListDataIdx']['last']       = ($this->_sections['trackListDataIdx']['iteration'] == $this->_sections['trackListDataIdx']['total']);
?>
  <tr bgcolor="#FFFFFF">
    <td><?php echo $this->_tpl_vars['trackList'][$this->_sections['trackListIdx']['index']][$this->_sections['trackListDataIdx']['index']][0]; ?>
</td>
    <td><?php echo $this->_tpl_vars['trackList'][$this->_sections['trackListIdx']['index']][$this->_sections['trackListDataIdx']['index']][1]; ?>
</td>
    <td><?php echo $this->_tpl_vars['trackList'][$this->_sections['trackListIdx']['index']][$this->_sections['trackListDataIdx']['index']][2]; ?>
</td>
    <td align="center"><?php echo $this->_tpl_vars['trackList'][$this->_sections['trackListIdx']['index']][$this->_sections['trackListDataIdx']['index']][3]; ?>
</td>
    <td align="center"><?php if ($this->_tpl_vars['resultFormatNumbers']): ?><?php echo $this->_tpl_vars['trackList'][$this->_sections['trackListIdx']['index']][$this->_sections['trackListDataIdx']['index']][4]; ?>
<?php endif; ?>
	<?php if ($this->_tpl_vars['resultFormatPercentages']): ?><?php if ($this->_tpl_vars['resultFormatNumbers']): ?> (<?php endif; ?><?php echo smarty_function_math(array('equation' => ($this->_tpl_vars['trackList'][$this->_sections['trackListIdx']['index']][$this->_sections['trackListDataIdx']['index']][4])."/".($this->_tpl_vars['trackList'][$this->_sections['trackListIdx']['index']][$this->_sections['trackListDataIdx']['index']][3])."*100.0",'format' => "%.1f"), $this);?>
%<?php if ($this->_tpl_vars['resultFormatNumbers']): ?>)<?php endif; ?><?php endif; ?></td>
    <td align="center"><?php if ($this->_tpl_vars['resultFormatNumbers']): ?><?php echo $this->_tpl_vars['trackList'][$this->_sections['trackListIdx']['index']][$this->_sections['trackListDataIdx']['index']][5]; ?>
<?php endif; ?>
	<?php if ($this->_tpl_vars['resultFormatPercentages']): ?><?php if ($this->_tpl_vars['resultFormatNumbers']): ?> (<?php endif; ?><?php echo smarty_function_math(array('equation' => ($this->_tpl_vars['trackList'][$this->_sections['trackListIdx']['index']][$this->_sections['trackListDataIdx']['index']][5])."/".($this->_tpl_vars['trackList'][$this->_sections['trackListIdx']['index']][$this->_sections['trackListDataIdx']['index']][4])."*100.0",'format' => "%.1f"), $this);?>
%<?php if ($this->_tpl_vars['resultFormatNumbers']): ?>)<?php endif; ?><?php endif; ?></td>
    <td align="center"><?php if ($this->_tpl_vars['resultFormatNumbers']): ?><?php echo $this->_tpl_vars['trackList'][$this->_sections['trackListIdx']['index']][$this->_sections['trackListDataIdx']['index']][6]; ?>
<?php endif; ?>
	<?php if ($this->_tpl_vars['resultFormatPercentages']): ?><?php if ($this->_tpl_vars['resultFormatNumbers']): ?> (<?php endif; ?><?php echo smarty_function_math(array('equation' => ($this->_tpl_vars['trackList'][$this->_sections['trackListIdx']['index']][$this->_sections['trackListDataIdx']['index']][6])."/".($this->_tpl_vars['trackList'][$this->_sections['trackListIdx']['index']][$this->_sections['trackListDataIdx']['index']][4])."*100.0",'format' => "%.1f"), $this);?>
%<?php if ($this->_tpl_vars['resultFormatNumbers']): ?>)<?php endif; ?><?php endif; ?></td>
  </tr>
  <?php endfor; endif; ?>
  <tr bgcolor="#DDDDDD">
    <td colspan="3"><b>Summary Totals:</b></td>
    <td align="center"><b><?php echo $this->_tpl_vars['trackList'][$this->_sections['trackListIdx']['index']][0][2]; ?>
</b></td>
    <td align="center"><b><?php if ($this->_tpl_vars['resultFormatNumbers']): ?><?php echo $this->_tpl_vars['trackList'][$this->_sections['trackListIdx']['index']][0][3]; ?>
<?php endif; ?>
    	<?php if ($this->_tpl_vars['resultFormatPercentages']): ?><?php if ($this->_tpl_vars['resultFormatNumbers']): ?> (<?php endif; ?><?php echo smarty_function_math(array('equation' => ($this->_tpl_vars['trackList'][$this->_sections['trackListIdx']['index']][0][3])."/".($this->_tpl_vars['trackList'][$this->_sections['trackListIdx']['index']][0][2])."*100.0",'format' => "%.1f"), $this);?>
%<?php if ($this->_tpl_vars['resultFormatNumbers']): ?>)<?php endif; ?><?php endif; ?></b></td>
    <td align="center"><b><?php if ($this->_tpl_vars['resultFormatNumbers']): ?><?php echo $this->_tpl_vars['trackList'][$this->_sections['trackListIdx']['index']][0][4]; ?>
<?php endif; ?>
    	<?php if ($this->_tpl_vars['resultFormatPercentages']): ?><?php if ($this->_tpl_vars['resultFormatNumbers']): ?> (<?php endif; ?><?php echo smarty_function_math(array('equation' => ($this->_tpl_vars['trackList'][$this->_sections['trackListIdx']['index']][0][4])."/".($this->_tpl_vars['trackList'][$this->_sections['trackListIdx']['index']][0][3])."*100.0",'format' => "%.1f"), $this);?>
%<?php if ($this->_tpl_vars['resultFormatNumbers']): ?>)<?php endif; ?><?php endif; ?></b></td>
    <td align="center"><b><?php if ($this->_tpl_vars['resultFormatNumbers']): ?><?php echo $this->_tpl_vars['trackList'][$this->_sections['trackListIdx']['index']][0][5]; ?>
<?php endif; ?>
    	<?php if ($this->_tpl_vars['resultFormatPercentages']): ?><?php if ($this->_tpl_vars['resultFormatNumbers']): ?> (<?php endif; ?><?php echo smarty_function_math(array('equation' => ($this->_tpl_vars['trackList'][$this->_sections['trackListIdx']['index']][0][5])."/".($this->_tpl_vars['trackList'][$this->_sections['trackListIdx']['index']][0][3])."*100.0",'format' => "%.1f"), $this);?>
%<?php if ($this->_tpl_vars['resultFormatNumbers']): ?>)<?php endif; ?><?php endif; ?></b></td>
  </tr>
</table>
</div>
<div class="Stacking">
  <hr width="100%">
</div>
<?php endfor; endif; ?>
<?php endif; ?>
<!--BR>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'admin/report_footer.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?> -->

</body>
</html>