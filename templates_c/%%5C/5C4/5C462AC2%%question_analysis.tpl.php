<?php /* Smarty version 2.6.26, created on 2012-12-17 01:01:26
         compiled from admin/question_analysis.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_options', 'admin/question_analysis.tpl', 65, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'admin/admin_header.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<BODY class="ContentFrameBody">


<?php if (isset ( $this->_tpl_vars['searchResults'] )): ?>

<!--div id="Title" style="position:absolute; left:11px; top:11px; width:100%; height:18px; z-index:2"-->
<p><b>Analysis Results:</b></p>
<!--/div-->

<p>
<!--div class="OrgInfoText" id="Results" style="position:absolute; left:60px; top:37px; width:548px; height:102px; z-index:6"-->
 <table border="0" cellpadding="0" cellspacing="0" width="700" class="table_list">
 <tr>
 <td class="table_header" width="20">&nbsp;</td>
 <td class="table_header" >&nbsp;&nbsp;Total</td>
 <td class="table_header" >&nbsp;&nbsp;Correct</td>
 <td class="table_header" >&nbsp;&nbsp;Incorrect</td>
 <td class="table_header" >&nbsp;&nbsp;Incomplete</td>
 <td class="table_header" >&nbsp;&nbsp;Understood</td>
 <td class="table_header" >&nbsp;&nbsp;Contact</td>
 <td class="table_header" >&nbsp;&nbsp;A</td>
 <td class="table_header" >&nbsp;&nbsp;B</td>
 <td class="table_header" >&nbsp;&nbsp;C</td>
 <td class="table_header" >&nbsp;&nbsp;D</td>
 <td class="table_header" >&nbsp;&nbsp;E</td>
 </tr>
 <tr>
 <td width="20">&nbsp;</td>
 <td class='sm_font'>&nbsp;&nbsp;<?php echo $this->_tpl_vars['searchResults']['numResults']; ?>
</td>
 <td class='sm_font'>&nbsp;&nbsp;<?php echo $this->_tpl_vars['searchResults']['numCorrect']; ?>
</td>
 <td class='sm_font'>&nbsp;&nbsp;<?php echo $this->_tpl_vars['searchResults']['numIncorrect']; ?>
</td>
 <td class='sm_font'>&nbsp;&nbsp;<?php echo $this->_tpl_vars['searchResults']['numIncomplete']; ?>
</td>
 <td class='sm_font'>&nbsp;&nbsp;<?php echo $this->_tpl_vars['searchResults']['numUnderstood']; ?>
</td>
 <td class='sm_font'>&nbsp;&nbsp;<?php echo $this->_tpl_vars['searchResults']['numContact']; ?>
</td>
 <td class='sm_font'>&nbsp;&nbsp;<?php echo $this->_tpl_vars['searchResults']['numAnswerA']; ?>
</td>
 <td class='sm_font'>&nbsp;&nbsp;<?php echo $this->_tpl_vars['searchResults']['numAnswerB']; ?>
</td>
 <td class='sm_font'>&nbsp;&nbsp;<?php echo $this->_tpl_vars['searchResults']['numAnswerC']; ?>
</td>
 <td class='sm_font'>&nbsp;&nbsp;<?php echo $this->_tpl_vars['searchResults']['numAnswerD']; ?>
</td>
 <td class='sm_font'>&nbsp;&nbsp;<?php echo $this->_tpl_vars['searchResults']['numAnswerE']; ?>
</td>
 </tr>
 <tr>
   <td colspan="11" align="center">'X' indicates that the column is part of the search criteria.<br>
   '--' indicates that the column is excluded from the search.</td>
 </tr>
 </table>
</p>
<!--/DIV-->
<?php endif; ?>




<p><b>Analysis Criteria:</b></p>

<p>
<form name="AdminForm" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>
">
    <table border="0" width="100%">
      <tr>
        <td class="OrgInfoText" width="21%">Question Number:</td>
        <td width="79%">
          <select class="FormValue" name="questionNumber" id="questionNumber">
          <option></option>
          <?php echo smarty_function_html_options(array('values' => $this->_tpl_vars['questionNumbers'],'output' => $this->_tpl_vars['questionNumbers'],'selected' => $this->_tpl_vars['selectedQuestionNumber']), $this);?>

          </select>
      </td>
	</tr>
	<tr>
        <td class="OrgInfoText" width="21%">Version Date:</td>
        <td width="79%"><span class="OrgInfoText">from</span>
          <select class="FormValue" name="versionDateFrom" id="versionDateFrom">
            <option></option>
            <?php echo smarty_function_html_options(array('values' => $this->_tpl_vars['versionDates'],'output' => $this->_tpl_vars['versionDates'],'selected' => $this->_tpl_vars['selectedVersionDateFrom']), $this);?>

          </select>
          <span class="OrgInfoText">to</span>
          <select class="FormValue" name="versionDateTo" id="versionDateTo">
            <option></option>
            <?php echo smarty_function_html_options(array('values' => $this->_tpl_vars['versionDates'],'output' => $this->_tpl_vars['versionDates'],'selected' => $this->_tpl_vars['selectedVersionDateTo']), $this);?>

	  </select>
        </td>
    </tr>
	<tr>
        <td class="OrgInfoText" width="21%">Completion Date:</td>
        <td width="79%"><span class="OrgInfoText">from</span>
          <select class="FormValue" name="completionDateFrom" id="completionDateFrom">
            <option></option>
            <?php echo smarty_function_html_options(array('values' => $this->_tpl_vars['completionDates'],'output' => $this->_tpl_vars['completionDates'],'selected' => $this->_tpl_vars['selectedCompletionDateFrom']), $this);?>

          </select>
          <span class="OrgInfoText">to</span>
          <select class="FormValue" name="completionDateTo" id="completionDateTo">
            <option></option>
            <?php echo smarty_function_html_options(array('values' => $this->_tpl_vars['completionDates'],'output' => $this->_tpl_vars['completionDates'],'selected' => $this->_tpl_vars['selectedCompletionDateTo']), $this);?>

          </select>
        </td>
    </tr>
    <tr>
      <td class="OrgInfoText" height="20" width="21%">Domain: &nbsp;&nbsp;</td>
      <td height="20" width="79%">
        <select class="FormValue" name="domainId" id="domainId">
          <option></option>
          <?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['domainIds'],'selected' => $this->_tpl_vars['selectedDomainId']), $this);?>

        </select>
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText" width="21%">Class:</td>
      <td width="79%">
        <select class="FormValue" name="trackId" id="trackId">
          <option></option>
          <?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['trackIds'],'selected' => $this->_tpl_vars['selectedTrackId']), $this);?>

        </select>
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText" width="21%">Department:</td>
      <td width="79%">
        <select class="FormValue" name="departmentId" id="departmentId">
          <option></option>
          <?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['departmentIds'],'selected' => $this->_tpl_vars['selectedDepartmentId']), $this);?>

        </select>
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText" width="21%">Result:</td>

      <td width="79%">
        <select class="FormValue" name="resultId" id="resultId">
	  <option></option>
	  <?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['resultIds'],'selected' => $this->_tpl_vars['selectedResultId']), $this);?>

	</select>
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText" width="21%">Answer Selected:</td>

      <td width="79%">
        <select class="FormValue" name="multipleChoiceAnswerSelected" id="multipleChoiceAnswerSelected">
	  <option></option>
	  <?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['multipleChoiceAnswerSelected'],'selected' => $this->_tpl_vars['selectedMultipleChoiceAnswerSelected']), $this);?>

	</select>
      </td>
    <tr>
      <td class="OrgInfoText" width="21%">Exit Information:</td>
      <td width="79%">
        <select class="FormValue" name="exitInfo" id="exitInfo">
          <option></option>
          <?php echo smarty_function_html_options(array('options' => $this->_tpl_vars['exitInfos'],'selected' => $this->_tpl_vars['selectedExitInfo']), $this);?>

        </select>
      </td>
    </tr>
    <tr>
      <td class="OrgInfoText" width="21%">&nbsp;</td>
      <td width="79%">
        <input type="image" name="execute" id="execute" value="1" src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/btn_search_up.gif"  onMouseOut="MM_swapImgRestore()"
               onMouseOver="MM_swapImage('Update','','../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/btn_search_down.gif',1)">
      </td>
    </tr>
    <!--tr><td><input type="hidden" name="Insert" value="TRUE"></td></tr>
    <tr><td><input type="hidden" name="get_return_name" value=""></td></tr>
    <tr><td><input type="hidden" name="get_return_URL" value=""></td></tr-->
  </table>

<!--div id="UpdateDiv" style="position:absolute; left:500px; top:160px; width:67px; height:25px; z-index:10"-->
    <!--img name="Update" border="0" src="../themes/<?php echo $this->_tpl_vars['uiTheme']; ?>
/admin/btn_search_up.gif" width="60" height="22"></a>
</div-->

</form>
</p>

</BODY>
</HTML>