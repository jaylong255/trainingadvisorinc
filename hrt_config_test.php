<?php

// Author:      Vector <vector@brokaw.net>
// Date:        23 May, 2004
// File:        hrt_config_test.php
// Description: This is the master configuration file for the HR Tools Desktop Application


define ('APPLICATION_ROOT', '/var/www/c13690/test.trainingadvisorinc.com/htdocs/desktop');
define ('URL_ROOT', '/desktop');
define ('FQ_URL_ROOT', 'http://test.trainingadvisorinc.com/desktop');
define ('THEME_ROOT', '/var/www/c13690/test.trainingadvisorinc.com/htdocs/desktop/themes');

require_once(APPLICATION_ROOT.'/includes/constants.php');

// This is the envelope sender address that will be used when the agent sends email
define('AGENT_ENVELOPE_SENDER_ADDRESS', 'agent@trainingadvisorinc.com');

// This should always be set to DEBUG_NONE in production environments,
// only set to one of the available values at the top of the constants.php
// file in dev environments.
define ('DEBUG', DEBUG_SQL);
define ('ENV', 'TEST');

// Smarty configuration for this application
define ('SMARTY_ROOT', '/var/www/c13690/test.trainingadvisorinc.com');
define ('SMARTY_TEMPLATE_DIR', SMARTY_ROOT.'/templates');
define ('SMARTY_COMPILE_DIR', SMARTY_ROOT.'/templates_c');
define ('SMARTY_CONFIG_DIR', SMARTY_ROOT.'/configs');
define ('SMARTY_CACHE_DIR', SMARTY_ROOT.'/cache');
define ('SMARTY_USE_SUBDIRS', TRUE);
define ('SMARTY_DEBUGGING', TRUE);

define ('DEFAULT_ORG_EMAIL_DIR', 'ORG2');

define ('SA_DB_USER', 'HR_Tool_sa');
define ('SA_DB_PASS', 'Adm1N1stR8');

define ('AUTH_DB_NAME', 'HR_Tools_Authentication');
define ('AUTH_DB_USER', 'HR_Login');
define ('AUTH_DB_PASS', 'HR_Tools');

define ('ORG_ADMIN_USER', 'HR_Org_Admin');
define ('ORG_ADMIN_PASS', 'orG8Admin');

define ('DB_HOST', ':/var/lib/mysql-test/mysql.sock');
define ('DB_NAME', 'ORG2');
define ('DB_USER', 'HR_Auth_User');
define ('DB_PASS', 'Author1zed4USe');

// If your mysql client binaries are not in the path, then
// set this to the location of your mysql binary cli command
define ('MYSQL_CLIENT_PATH', '');

?>
